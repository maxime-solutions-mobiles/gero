# Step 1: build Angular app
FROM node:8 as build

RUN mkdir /app
WORKDIR /app

# Install dependencies before copying source files to improve build reuse
COPY package*.json ./
COPY .npmrc ./
RUN npm ci

# Copy app files
COPY . .

# Build Angular app
RUN npm run build

# Step 2: package static files in Nginx
FROM nginx:alpine

# Copy built Angular assets into server container
COPY --from=build /app/dist /app

# Add Nginx configuration
COPY nginx/*.conf /etc/nginx/conf.d/

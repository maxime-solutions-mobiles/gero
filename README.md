GERO 3
======

Frontend Angular de Gero 3.

Technos
-------

  * Angular 7 (https://angular.io/)
  * Matérial angular (https://material.angular.io)
  * Font awesome (http://fontawesome.io/)
  * Normalize (http://necolas.github.io/normalize.css/)

Installation
------------

  * `npm install`
  * `ng update`

Comandes
--------

  * Dev:  
    `npm start`
  * Build with Docker:  
    `docker build -t gero/front-v3 .`
  * Run production build:  
    `docker run -p 8080:80 -d gero/front-v3`

# Configuration dynamic forms #

##keys obligatoires (tous types de champs) :##
* type (button,input,switch,select,separator)
* name (nom du champ = nom dans la bdd, n'est pas affiché, doit être unique)

##Types de champ ##

###button###

###separator###
* label : texte affiché sur le séparateur*

###input###
* inputType : type d'input (text,number....)
* placeholder : label et placeholder du champ
* validation : Array de validateurs angular sous la forme : 
[Validators.required,...]
(liste : https://angular.io/api/forms/Validators)
* feedBacks : feedback personnalisé en fonction du validateur sous la forme : 
{ required: 'Ce champ est très important, merci de le remplir',... }
En cas d'abscence le feedback par defaut est utilisé.

###select###
* placeholder, validation, feedBacks : identique input
* options : Liste des options sous la forme : [option1, option2 ...]

###switch###
* placeholder, options :  identique select

####Classe####
input, select et switch disposent d'une option class qui permet de personnaliser son apparence.
classes communes disponibles (définissent la largeur du champ) :

* grid-full
* grid-tier
* grid-2tier
* grid-quart
* grid-3quart

Par défaut les champs prennent 50% de la largeur du formulaire

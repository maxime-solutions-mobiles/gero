export const environment = {
  production: false,
  portalUrl: 'https://v3.gero.fr',
  authServiceUrl: 'https://v3.gero.fr/api/auth',
  databaseUrl: 'https://v3.gero.fr/api/db',
  docServiceUrl: 'https://v3.gero.fr/api/documents',
  notificationServiceUrl: 'https://v3.gero.fr/api/notifications',
};

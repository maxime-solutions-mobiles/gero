export const environment = {
  production: false,
  portalUrl: window.location.origin,
  authServiceUrl: `${window.location.origin}/api/auth`,
  databaseUrl: `${window.location.origin}/api/db`,
  docServiceUrl: `${window.location.origin}/api/documents`,
  notificationServiceUrl: `${window.location.origin}/api/notifications`,
};

import { Component } from '@angular/core';
import { AuthService } from './auth/auth.service';

import '../sass/main.scss';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  constructor(private authService: AuthService) {}

  public isAuthorized() {
    return this.authService.isAuthorized();
  }
}

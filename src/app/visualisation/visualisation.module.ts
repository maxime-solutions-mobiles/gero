import { InterventionService } from './../services/intervention.service';
import { PassageService } from './../services/passage.service';
import { CasService } from './../services/cas.service';
import { VoletModule } from './volet/volet.module';
import { MatButtonModule, MatTabsModule, MatButtonToggleModule, MatSlideToggleModule, MatInputModule } from '@angular/material';
import { DynamicFormModule } from './../forms/dynamic-form.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Visualisation } from './visualisation.page';
import { VisualistationService } from './visualisation.service';
import { ListeModule } from './liste/liste.module';
import { McnoModule } from './../mcno/mcno.module';
import { MoyenService } from '../services/moyen.service';
import { DBService } from '../services/db.service';
import { FileService } from '../services/file.service';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DynamicFormModule,
        MatButtonModule,
        MatTabsModule,
        MatSlideToggleModule,
        MatButtonToggleModule,
        MatInputModule,
        VoletModule,
        ListeModule,
        OrderModule,
        McnoModule
    ],
    declarations: [
        Visualisation
    ],
    exports: [
        Visualisation
    ],
    providers: [
        DBService,
        VisualistationService,
        CasService,
        InterventionService,
        PassageService,
        MoyenService,
        FileService,
    ]
})
export class VisualisationModule { }
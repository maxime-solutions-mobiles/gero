
import { ListService } from './../../services/list.service';
import { InterventionModel } from './../models/intervention.model';
import { CasModel } from './../models/cas.model';
import { VisualistationService } from '../visualisation.service';
import { Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MoyenModel } from '../models/moyen.model';
import { Observable } from 'rxjs';
import { InterventionService } from '../../services/intervention.service';



@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'intervention',
    template: `
    <header (click)="select()">
        <div class="numero">
            <span>{{intervention.numero}}</span>
        </div>
        <div class="inter-details">
            <div>
                <span class="inter-heure">{{intervention.timeCode | date:'HH:mm'}}<span *ngIf="intervention.clos"> - {{intervention.clos.timeCode | date:'HH:mm'}}</span></span>
            </div>
            <div>
                <span class="inter-motif">{{intervention.motif | slice:0:15}}</span> <span class="inter-lieu">{{intervention.lieu | slice:0:15}}</span>
            </div>
            <div>
                <span *ngIf="intervention.observation" class="inter-observation">{{intervention.observation | slice:0:15}}</span>
                <span *ngIf="intervention.position" class="inter-observation">{{intervention.position | slice:0:15}}</span>
                <span *ngIf="intervention.urgence" class="inter-observation">{{intervention.urgence | slice:0:15}}</span>
            </div>
        </div>
    </header>
    <div (click)="select()">
        <section *ngIf="(INTER.currentInterChange | async) == intervention && (INTER.currentInterChange | async)?.updating == 'cas';else casList">
            <i class="fas fa-spinner-third fa-spin"></i>
        </section>
        <ng-template #casList>
            <section>
                <cas *ngFor="let cas of filteredCasList" [cas]="cas" class="short" (click)="selectCas($event,cas._id)"></cas>
            </section>
        </ng-template>

        <section *ngIf="(INTER.currentInterChange | async) == intervention && (INTER.currentInterChange | async)?.updating == 'moyen';else moyenList">
            <i class="fas fa-spinner-third fa-spin"></i>
        </section>
        <ng-template #moyenList>
            <aside class="moyens">
                    <liste-intervention-moyen *ngFor="let moyen of filteredMoyenList" [moyen]="moyen" [updating]="moyen.updating" [interID]="intervention._id" [interStarted] = "intervention.started" (click)="selectMoyen($event,moyen._id)"></liste-intervention-moyen>
            </aside>
        </ng-template>
        <div class="loadScreen" [ngClass]="{'show':selected &&(INTER.currentUpdate | async)}">
            <div><i class="fas fa-spinner-third fa-spin"></i></div>
        </div>
    </div>
    `
})

export class InterventionComponent implements OnInit {
    @Input() intervention: InterventionModel;
    @Input() selected: boolean;

    public filteredMoyenList: MoyenModel[];
    public filteredCasList: CasModel[];

    constructor(
        public VISU: VisualistationService,
        public INTER: InterventionService,
        private LIST: ListService
    ) { }

    ngOnInit() {
        this.filteredCasList = this.LIST.casListById(this.intervention.cas);
        this.filteredMoyenList = this.LIST.moyensListById(this.intervention.moyen);
    }

    public select() {
        this.VISU.navTo({ intervention: this.intervention._id });
    }

    public selectCas(event, casID) {
        event.stopPropagation();
        this.VISU.voletShow({ cas: casID, rubrique: 'info', sousRubrique: 'pec' });
    }

    public selectMoyen(event, moyenID) {
        event.stopPropagation();
        this.VISU.voletShow({ moyen: moyenID });
    }

}

import { ParametreService } from './../../services/parametre.service';
import { ListService } from './../../services/list.service';

import { Component, OnInit, OnDestroy } from '@angular/core';


@Component({
    selector: 'filtre-moyen',
    template: `
    <div class="ligne1">
        <div class="filtre_type_list">
            <span *ngFor="let type of typeList" (click)="filtreByType(type)" [ngClass]="{'active':currentFilters.type == type || currentFilters.type == null}">{{type}}</span>
        </div>
        <div class="filtre_reset"> <i (click)="resetFilters()" class="fal fa-sync closeBtn"></i></div> 
        
    </div>
    <div class="ligne3">
            <div class="filtre_etat_list">
            <span *ngFor="let etat of etatList" (click)="filtreByEtat(etat)" [ngClass]="{'active':currentFilters.etat == etat || currentFilters.etat == null}">{{etat}}</span>
        </div>        
    </div>
    `
})


export class FiltreMoyenComponent implements OnInit, OnDestroy {
    settingFilters: boolean;
    public user: any;
    public typeList: string[];
    public etatList: string[];
    private currentFilters: any = {};
    private subscription: any;
    constructor(
        private LIST: ListService,
        private PARAM: ParametreService
    ) { }
    ngOnInit() {
        this.typeList = this.PARAM.moyenType;
        this.etatList = this.PARAM.moyenEtat;
        this.subscription = this.LIST.currentFiltersObservable.subscribe(filters => this.setFilters(filters.moyen));

        if (this.LIST.currentFilters.moyen) this.setFilters(this.LIST.currentFilters.moyen);
        else {
            this.setFilters()
            this.goFilter();
        }

    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    private setFilters(newFilters: any = {}) {
        if (this.currentFilters == newFilters) return;
        this.settingFilters = true;
        let filtres = { ...newFilters };
        //// here comes trouble
        this.currentFilters = filtres;
        this.settingFilters = false;
    }

    private goFilter() {
        if (this.settingFilters) return;
        this.LIST.filtreList('moyen', this.currentFilters);
    }

    public resetFilters() {
        this.LIST.resetList('moyen');
    }
    public filtreByType(type) {
        if (this.currentFilters.type == type)
            delete this.currentFilters.type;
        else this.currentFilters.type = type
        this.goFilter();
    }
    public filtreByEtat(etat) {
        if (this.currentFilters.etat == etat)
            delete this.currentFilters.etat;
        else this.currentFilters.etat = etat
        this.goFilter();
    }
}

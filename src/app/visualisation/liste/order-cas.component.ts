import { VisualistationService } from './../visualisation.service';
import { ListService } from '../../services/list.service';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';


@Component({
    selector: 'order-cas',
    host: { '[class]': 'classNames' },
    template: `
        <div class="urgence">
            <span (click)="orderBy('urgence')" [class]="arrow.urgence">
            <i class="fal"></i> 
            Ux
            </span>
        </div>
        <div *ngIf="medium"  class="nom">
            <span (click)="orderBy('nom')" [class]="arrow.nom">
            <i class="fal"></i> 
            Nom
            </span>
        </div>
        <div  class="niv">
            <span (click)="orderBy('niv')"  [class]="arrow.niv">
            <i class="fal"></i>
            Niv
            </span>
        </div>
        <div class="age">
            <span (click)="orderBy('age')" [class]="arrow.age">
            <i class="fal"></i>
            Age
            </span>
        </div>
        <div class="date">
            <span (click)="orderBy('timeCode')" [class]="arrow.timeCode">
            <i class="fal"></i>
            Date
            </span>
        </div>
        <div *ngIf="!medium" ></div>
        <div *ngIf="!medium" ></div>
        <div></div>
            `
})


export class OrderCasComponent implements OnInit, OnDestroy {
    @Input() nivMode: any;
    medium: boolean;
    private subscription: any;
    classNames: string;
    public arrow: any = {
        nom: null,
        niv: null,
        age: null,
        urgence: null,
        timeCode: null

    };
    constructor(private LIST: ListService, private visu: VisualistationService) {}
    ngOnInit(): void {


        this.medium = this.visu.currentDisplay.casMode == 'medium' || !this.visu.currentDisplay.casMode;
        this.classNames = this.visu.currentDisplay.casMode || 'medium';

        this.subscription =   this.visu.display.subscribe(display => {
            console.log('jts cas passage');
            this.medium = display.casMode == 'medium'
            this.classNames = display.casMode || 'medium';
        });


    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public orderBy(field) {
        Object.keys(this.arrow).forEach(key => {
            if (key != field) this.arrow[key] = null;
            else if (!this.arrow[key] || this.arrow[key] == 'desc') this.arrow[key] = 'asc';
            else this.arrow[key] = 'desc';

        });


        // this.LIST.orderList('cas', [{ [field]: this.arrow[field] }])

    }

}

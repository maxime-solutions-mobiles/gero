import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import * as moment from 'moment';

import { CasService } from 'src/app/services/cas.service';
import { ParametreService } from 'src/app/services/parametre.service';
import { PassageService } from 'src/app/services/passage.service';
import { UserService } from 'src/app/services/user.service';
import { VisualistationService } from '../visualisation.service';
import { PassageModel } from '../models/passage.model';
import { CasModel } from './../models/cas.model';
import { ListService } from 'src/app/services/list.service';


@Component({
    selector: 'passage',
    templateUrl: './passage.component.html',
})
export class PassageComponent implements OnInit, OnChanges, OnDestroy {

    @HostBinding('class') classNames = '';

    @Input() passage: PassageModel;
    @Input() class: string;

    @Output() selectPassage: EventEmitter<any> = new EventEmitter<any>();
    @Output() selectPassageMulti: EventEmitter<any> = new EventEmitter<any>();

    public mode = {
        autocomplete: false,
        large: false,
        medium: false,
        short: false
    };

    private subscription: any;
    public position: any;
    public conclusion: any;
    public cas: CasModel;

    constructor(
        private LIST: ListService,
        private visu: VisualistationService,
        private PASSAGE: PassageService,
        private CAS: CasService,
        private USER: UserService,
        private PARAM: ParametreService,
    ) { }

    ngOnInit() {
        this.setClass(this.class);
        this.defineMode(this.visu.currentDisplay.casMode);
        this.subscription = this.visu.display.subscribe(display => this.defineMode(display.casMode));
        this.initCas();
        this.initPosition();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.class) {
            this.setClass(changes.class.currentValue);
        }
        if (changes.cas) {
            this.initCas();
        }
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public select(rubrique?: string, sousRubrique?: string, edit?: boolean) {
        const path: any = { cas: this.cas._id, passage: this.passage._id };
        if (rubrique) {
            path.rubrique = rubrique;
        }
        if (sousRubrique) {
            path.sousRubrique = sousRubrique;
        }
        path.action = edit ? 'edit' : 'show';
        this.selectPassage.emit(path);
    }

    public selectMulti() {
        this.selectPassageMulti.emit(this.cas._id);
    }

    public getPatient() {
      this.visu.selectPatient = this.cas.patient;
    }

    public async entree() {
        if (this.cas.position && this.USER.INFO.poste) {
            await this.CAS.closeCasPassage(this.cas);
        }

        // bugfix: check for unlinked open passages
        const existingPassageId = await this.PASSAGE.bugfixFindPassageAndCloseExtraneous(this.cas._id, this.LIST.listsRef.passage);
        if (existingPassageId) {
            await this.PASSAGE.forceClosePassage(existingPassageId, 'Close passage before creating a new one');
        }

        if (this.USER.INFO.poste) {
            await this.CAS.openCasPassage(this.cas, this.USER.INFO.poste)
        }

        this.LIST.resetList('passage');
        this.LIST.resetList('cas');

        this.visu.navTo({
            passage: this.cas._id,
            rubrique: 'info',
            sousRubrique: 'pec',
        });
    }

    public delay() {
        if (!this.cas || !this.cas.positionCreated) {
            return null;
        }
        return moment().diff(this.cas.positionCreated, 'minutes');
    }

    public delayExpired() {
        const delay = this.delay();
        return !delay || (delay > 30);
    }

    private defineMode(mode) {
        if (this.classNames.includes('short')) {
            mode = 'short';
        }
        if (this.classNames.includes('medium')) {
            mode = 'medium';
        }
        if (this.classNames.includes('large')) {
            mode = 'large';
        }
        Object.keys(this.mode).forEach(k => this.mode[k] = (k === mode));
    }

    private async initCas() {
        this.cas = await this.PASSAGE.getCas(this.passage.cas);
        this.setClass(this.class);
    }

    private async initPosition() {
        this.position = await this.PASSAGE.getPosition(this.passage);
    }

    private setClass(pclass: string) {
        this.classNames = pclass; // attribut class du custom element
        if (this.cas) {
            if (this.cas.clos) {
              this.classNames += ' clos';
            }
            this.classNames += ` ${this.cas.urgence}`;
        }

        Object.keys(this.mode)
            .filter((k) => this.classNames.includes(k))
            .forEach((k) => this.defineMode(k));
    }

}

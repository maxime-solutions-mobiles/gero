import { Component, Input, SimpleChanges, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { MoyenService } from 'src/app/services/moyen.service';
import { MoyenModel } from '../models/moyen.model';
import { VisualistationService } from '../visualisation.service';

@Component({
  selector: 'moyen',
  template: `
    <article
      class="{{moyen?.etat}} {{statesRef[moyen.etat]?.color}}"
      [ngClass]="{ indisponible: !moyen.disponible }">

      <header (click)="select()">
        <div>
          <span (click)="toggleDispo()">
            <i class="fa fa-pause" *ngIf="moyen.disponible"></i>
            <i class="fa fa-play" *ngIf="!moyen.disponible"></i>
          </span>
          <h3>{{moyen.nom || '???'}}</h3>
        </div>
        <div [ngSwitch]="moyen.type" class="picto">
          <span class="{{currentState?.color}}">
            <i class="fal fa-ambulance"     *ngSwitchCase="'Ambulance'"></i>
            <i class="fal fa-car"           *ngSwitchCase="'VL'"></i>
            <i class="fal fa-walking"       *ngSwitchCase="'Pédestre'"></i>
            <i class="fal fa-helicopter"    *ngSwitchCase="'Hélico'"></i>
            <i class="fal fa-truck-monster" *ngSwitchCase="'Quad'"></i>
            <i class="fal fa-bicycle"       *ngSwitchCase="'Vélo'"></i>
            <i class="fal fa-motorcycle"    *ngSwitchCase="'Moto'"></i>
            <i class="fal fa-horse"         *ngSwitchCase="'Equestre'"></i>
            <i class="fal fa-car"           *ngSwitchCase="'Golfette'"></i>
            <i class="fal fa-user-md"       *ngSwitchCase="'personnel'"></i>
          </span>
          <span *ngIf="moyen.currentIntervention">{{ getNextState(moyen.etat)?.label }}</span>
        </div>
      </header>

      <div>
        <div *ngIf="!moyen.disponible">
          <span class="indisponible">Indisponible</span>
        </div>
        <div class="bready" *ngIf="moyen.currentStates && moyen.disponible">
          <div class="{{currentState?.color}}">
            <span>
              <span>{{currentState?.label}}</span>
              <span>{{currentState?.timeCode | date:'HH:mm'}}</span>
            </span>
          </div>
          <div *ngIf="moyen.currentIntervention && !moyen.standBy" class="btn_section">
            <span>
              <span class="btn {{getNextState(moyen.etat)?.color}}" (click)="nextState($event)">
                {{getNextState(moyen.etat)?.label}}
              </span>
            </span>
          </div>
        </div>
      </div>
    </article>
    `
})
export class MoyenComponent implements OnInit, OnChanges {

  @Input() moyen: MoyenModel;
  @Output() selectMoyen: EventEmitter<any> = new EventEmitter<any>();

  public currentState: any;
  public statesRef = this.MOYEN.states;

  constructor(public visu: VisualistationService, private MOYEN: MoyenService) { }

  ngOnInit() {
    this.init();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.init();
  }

  private init() {
    if (this.moyen.currentStates.length > 0) {
      const currentState = this.moyen.currentStates[this.moyen.currentStates.length - 1];
      this.currentState = { ...this.statesRef[currentState.etat], ...currentState };
    } else {
      this.currentState = { ...this.statesRef.disponible };
    }
  }

  public getNextState(curState): any {
    return this.MOYEN.getNextState(curState);
  }

  public select(rubrique: string = null, sousRubrique: string = null) {
    const path: any = { moyen: this.moyen._id };
    if (rubrique) {
      path.rubrique = rubrique;
    }
    if (sousRubrique) {
      path.sousRubrique = sousRubrique;
    }
    this.selectMoyen.emit(path);
  }

  public toggleDispo() {
    this.MOYEN.toggleDispo(this.moyen);
  }

  public nextState(event: Event) {
    event.stopPropagation();
    this.MOYEN.nextState(this.moyen);
  }

}

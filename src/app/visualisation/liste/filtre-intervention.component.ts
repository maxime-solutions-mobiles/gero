import { Component, OnInit, OnDestroy } from '@angular/core';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { Subscription } from 'rxjs';
import { ListService } from './../../services/list.service';
import { UserService } from '../../services/user.service';
import { DBService } from '../../services/db.service';

import * as moment from 'moment';
import 'moment/locale/fr';

@Component({
    selector: 'filtre-intervention',
    template: `
    <div class="ligne1">
        <div  class="filtre_state">
            <span [ngClass]="{'active':stateSwitch == 'en_cours'}" (click)="changeState('en_cours')">En&#8239;cours</span>
            <span [ngClass]="{'active':stateSwitch == 'clos'}" (click)="changeState('clos')">Clos</span>
        </div>
        <div  class="filtre_search">
            <mat-select
            placeholder="{{selectedField}}" (selectionChange)="changeField($event)">
            <mat-option *ngFor="let option of fields" [value]="option">
                {{ option }}
            </mat-option>
            </mat-select>
            <input matInput  type="text" placeholder={{selectedField}}  (keyup)="search($event)" #input [value]="searchBarValue">
        </div>
        
        <div class="filtre_csv" *ngIf="user?.fonction =='administrateur'">
            <button mat-button (click)="generateCSV()">CSV</button>
        </div>
        <div class="filtre_reset"> <i (click)="resetFilters()" class="fal fa-sync closeBtn"></i></div>
        <div class="wait" *ngIf="loading"><i class="fas fa-spinner-third fa-spin"></i></div>        
    </div>
    `
})
export class FiltreInterventionComponent implements OnInit, OnDestroy {

    public settingFilters: boolean;
    public fields = [
        'numero',
        'motif',
        'lieu',
        'requerant',
        'adresse',
        'auteur',

    ];
    public searchBarValue: string = null;
    private currentFilters: any = {};
    public stateSwitch: any = null;
    public selectedField = 'nom';
    public user: any;
    public loading: boolean;
    private subscription: Subscription[] = [];

    constructor(
        private LIST: ListService,
        private USER: UserService,
        private DB: DBService
    ) { }

    ngOnInit() {
        this.subscription.push(this.USER.profile.subscribe((profile) => {
            this.user = profile;
        }));

        this.subscription.push(this.LIST.currentFiltersObservable.subscribe((filters) => {
            this.setFilters(filters.intervention);
        }));

        if (this.LIST.currentFilters.intervention) {
            this.setFilters(this.LIST.currentFilters.intervention);
        } else {
            this.setFilters();
            this.goFilter();
        }
    }

    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    private setFilters(newFilters: any = {}) {
        if (this.currentFilters === newFilters) {
            return;
        }
        this.settingFilters = true;
        const filtres = { ...newFilters };

        // define en cours/clos
        if (filtres.clos) {
            this.stateSwitch = filtres.clos.$exists ? 'clos' : 'en_cours';
        } else {
            this.stateSwitch = null;
        }

        // define search bar
        this.fields.forEach(field => {
            if (filtres[field]) {
                this.selectedField = field;
                if (filtres[field].$regex) {
                    this.searchBarValue = filtres[field].$regex.toString().split('/')[1];
                } else {
                    this.searchBarValue = '';
                }
            }
        });
        if (!filtres[this.selectedField]) {
            this.searchBarValue = '';
        }
        this.currentFilters = filtres;
        this.settingFilters = false;
    }

    private goFilter() {
        if (this.settingFilters) {
            return;
        }
        this.LIST.filtreList('intervention', this.currentFilters);
    }

    public search(evt) {
        if (evt.target.value) {
            const r = new RegExp(evt.target.value, 'gi');
            this.currentFilters[this.selectedField] = { $regex: r };
        } else {
            delete this.currentFilters[this.selectedField];
        }
        this.goFilter();
    }

    public changeField(evt: any) {
        this.fields.forEach(field => {
            delete this.currentFilters[field];
        });
        this.selectedField = evt.value;
        this.searchBarValue = '';
        this.goFilter();
    }

    public changeState(toogleState: string) {
        if (this.stateSwitch === toogleState) {
            this.stateSwitch = null;
            delete this.currentFilters.clos;
        } else {
            this.stateSwitch = toogleState;
            this.currentFilters.clos = (this.stateSwitch === 'clos') ? { $exists: true } : { $exists: false };
        }
        this.goFilter();
    }

    public resetFilters() {
        this.LIST.resetList('intervention');
    }

    async getInterventionList() {
        const interventions = await this.DB.getAll('intervention');

        for (const i of interventions) {
            if (i.cas !== undefined) {
                const cases = [];
                for (const id of i.cas) {
                    const cas = await this.DB.getById('cas', id);
                    if (cas) {
                        cases.push(cas);
                    }
                }
                i.cas = cases;
            }

            if (i.moyen !== undefined) {
                const moyens = [];
                for (const id of i.moyen) {
                    const moyen = await this.DB.getById('moyen', id);
                    if (moyen) {
                        moyens.push(moyen);
                    }
                }
                i.moyen = moyens;
            }
        }

        return interventions;
    }

    generateCSV() {
        this.loading = true;

        this.getInterventionList().then(list => {
            this.loading = false;

            const rows = [];

            for (const i of list) {

                const row: any = {
                    numero: i.numero ? i.numero : '',
                    timeCode: i.timeCode ? moment(i.timeCode).format('DD/MM/YYYY HH:mm') : '',
                    lieu: i.lieu ? i.lieu : '',
                    motif: i.motif ? i.motif : '',
                    observation: i.observation ? i.observation : '',
                    origine: i.origine ? i.origine : '',
                    requerant: i.requerant ? i.requerant : '',
                    adresse: i.adresse ? i.adresse : '',
                    city: i.city ? i.city : '',
                    postcode: i.postcode ? i.postcode : '',
                    lat: i.lat ? i.lat : '',
                    lng: i.lng ? i.lng : '',
                    telephone: i.telephone ? i.telephone : ''
                };

                let discussion = '';
                let cas = '';
                let moyen = '';

                if (i.discussion) {
                    discussion = i.discussion.reduce((result, d) => {
                        if (d) {
                            result.push(['message: ' + d.message, 'auteur:' + d.auteur].join(', '));
                        }
                        return result;
                    }, []).join('|');
                }

                if (i.cas) {
                    cas = i.cas.reduce((result, c) => {
                        if (c) {
                            result.push(['nom: ' + c.nom, 'prenom: ' + c.prenom].join(', '));
                        }
                        return result;
                    }, []).join('|');
                }

                if (i.moyen) {
                    moyen = i.moyen.reduce((result, m) => {
                        if (m) {
                            result.push([
                                'balise: ' + m.balise,
                                'capacite: ' + m.capacite,
                                'chef: ' + m.chef,
                                'disponible: ' + m.disponible,
                                'draggable: ' + m.draggable,
                                'etat: ' + m.etat,
                                'famille: ' + m.famille,
                                'geoloc: ' + m.geoloc,
                                'lat: ' + m.lat,
                                'lng: ' + m.lng,
                                'nom: ' + m.nom,
                                'poste: ' + m.poste,
                                'standBy: ' + m.standBy,
                                'telephone: ' + m.telephone,
                                'type: ' + m.type,
                            ].join(', '));
                        }
                        return result;
                    }, []).join('|');
                }

                row.discussion = discussion;

                row.cas = cas;

                row.moyen = moyen;

                rows.push(row);
            }

            const csvReport = new Angular5Csv(rows, 'Interventions', {
                fieldSeparator: ';',
                quoteStrings: '"',
                showLabels: true,
                headers: [
                    'numero',
                    'timeCode',
                    'lieu',
                    'motif',
                    'observation',
                    'origine',
                    'requerant',
                    'adresse',
                    'city',
                    'postcode',
                    'lat',
                    'lng',
                    'telephone',
                    'discussion',
                    'cas',
                    'moyen'
                ]
            });
        });
    }

}

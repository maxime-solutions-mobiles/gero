import { Component, Input, SimpleChanges, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { CasService } from './../../services/cas.service';
import { VisualistationService } from '../visualisation.service';
import { CasModel } from './../models/cas.model';
import { UserService } from '../../services/user.service';
import { ParametreService } from '../../services/parametre.service';
import { PassageService } from 'src/app/services/passage.service';
import { ListService } from 'src/app/services/list.service';

@Component({
    selector: 'cas',
    host: { '[class]': 'classNames' },
    template: `

    <div class="urgence {{cas?.urgence}}" (click)="select()">
        <p>{{cas?.urgence}}</p>
    </div>

    <div class="nom" (click)="select('info','patient')" *ngIf="(!mode.large &&  cas?.patient) && (!mode.short && cas?.patient)  ; else noName">
        <p >{{cas?.nom}}</p>
        <p >{{cas?.prenom}}</p>
    </div>
    <ng-template #noName>
        <div class="nom"  (click)="select('info','patient',true)" *ngIf="!cas?.patient">
            <span class="link success" *ngIf="mode.autocomplete"><i class="fal fa-user-plus"></i> Patient</span>
            <button mat-raised-button *ngIf="!mode.autocomplete"  color="success" >
                <i class="fal fa-user-plus"></i> Patient
            </button>
        </div>
    </ng-template>




    <div class="niv" (click)="select('info','pec')" >
        <strong class="niv {{cas?.urgence}}">{{cas?.niv}}</strong>
        <em *ngIf="cas?.note">{{cas?.note}}</em>
    </div>


    <div *ngIf="getAge(cas?.age) < 18" class="alert" (click)="select('info','pec')">
        <p class="{{cas?.sexe}}">
        <span *ngIf="cas?.sexe"><i class="fal fa-mars"></i><i class="fal fa-venus"></i></span>
        <span *ngIf="cas?.age">{{getAge(cas?.age)}} ans</span>
        </p>
        <p>{{cas?.pathologie}}</p>
    </div>
    <div *ngIf="getAge(cas?.age) >= 18" class="info" (click)="select('info','pec')">
        <p class="{{cas?.sexe}}">
        <span *ngIf="cas?.sexe"><i class="fal fa-mars"></i><i class="fal fa-venus"></i></span>
        <span *ngIf="cas?.age">{{getAge(cas?.age)}} ans</span>
        </p>
        <p>{{cas?.pathologie}}</p>
    </div>


    <div class="duree" *ngIf="!mode.short && !mode.autocomplete" [ngClass]="{'danger' : outOfDelay && !cas?.clos,'dark':cas?.clos}" (click)="select('info','pec')">
        <!--<i *ngIf="!mode.large && currentPosition" class="fal fa-clock"></i> -->
        <span *ngIf="currentPosition">{{currentPosition}}</span>
        <span *ngIf="!currentPosition">?</span>
        <p *ngIf="mainTC && (outOfDelay || cas?.clos)">{{mainTC | date:'HH:mm'}}</p>
        <p *ngIf="delay && !outOfDelay && !cas?.clos">{{delay}}'</p>
    </div>



    <div  *ngIf="mode.large" class="cas-toolbar-large">
        <div class="observation" [ngClass]="{'success' : lastObservation}"  (click)="select('observation')" >
            <i class="fal fa-stethoscope"></i>
            <p *ngIf="lastObservation">{{lastObservation | date:'HH:mm'}}</p>
        </div>

        <div class="action" [ngClass]="{'success' : lastAction}"  (click)="select('action')" >
            <i class="fal fa-medkit"></i>
            <p *ngIf="lastAction">{{lastAction | date:'HH:mm'}}</p>
        </div>


        <div  class="close" *ngIf="(!notif && !cas?.currentIntervention) || cas?.clos"  [ngClass]="{'success' : cas?.clos}"  (click)="select('conclusion')" >
            <i class="fal fa-check"></i>
            <p *ngIf="cas?.clos">{{cas?.clos.timeCode | date:'HH:mm'}}</p>
        </div>
        <div class="notif" *ngIf="notif && !cas?.clos && !cas?.currentIntervention" [ngSwitch]="notif?.type" >
            <i class="fal fa-bell danger jingle-bell "  *ngSwitchCase="'intervention'"></i>
            <i class="fal fa-exchange warning"  *ngSwitchCase="'regulation'"   (click)="select('conclusion')" ></i>
            <i class="fal fa-comment success"  *ngSwitchCase="'discussion'"   (click)="select('conclusion','discussion')" ></i>
            <p class="{{notif?.type}}" *ngIf="mode.large">{{notif?.tc | date:'HH:mm'}}</p>
        </div>
        <div class="inter" *ngIf="cas?.currentIntervention">
            <i class="fal fa-shipping-fast warning" ></i>
        </div>

    </div>

    <div *ngIf="mode.medium"  class="cas-toolbar-medium">
        <div class="observation" *ngIf="!lastAction && !notif && !cas?.clos && !cas?.currentIntervention" [ngClass]="{'success' : lastObservation}"  (click)="select('observation')" >
            <i class="fal fa-stethoscope"></i>
            <p *ngIf="lastObservation">{{lastObservation | date:'HH:mm'}}</p>
        </div>
        <div class="action" *ngIf="lastAction && !notif && !cas?.clos && !cas?.currentIntervention"  [ngClass]="{'success' : lastAction}"  (click)="select('action')" >
            <i class="fal fa-medkit"></i>
            <p *ngIf="lastAction">{{lastAction | date:'HH:mm'}}</p>
        </div>
        <div  class="close" *ngIf="(!notif && !cas?.currentIntervention) && cas?.clos"  [ngClass]="{'success' : cas?.clos}"  (click)="select('conclusion')" >
        <i class="fal fa-check"></i>
        <p *ngIf="cas?.clos">{{cas?.clos.timeCode | date:'HH:mm'}}</p>
        </div>
        <div class="notif" *ngIf="notif && !cas?.clos && !cas?.currentIntervention" [ngSwitch]="notif?.type" >
            <i class="fal fa-bell danger jingle-bell "  *ngSwitchCase="'intervention'"></i>
            <i class="fal fa-exchange warning"  *ngSwitchCase="'regulation'" (click)="select('conclusion')" ></i>
            <i class="fal fa-comment success"  *ngSwitchCase="'discussion'" (click)="select('conclusion','discussion')"></i>
            <p class="{{notif?.type}}" *ngIf="mode.large">{{notif?.tc | date:'HH:mm'}}</p>
        </div>
        <div class="inter" *ngIf="cas?.currentIntervention">
            <i class="fal fa-shipping-fast warning" ></i>
        </div>
    </div>


    <div *ngIf="mode.autocomplete"  class="cas-toolbar-patient">
        <button mat-raised-button  color="primary"  (click)="entree()" >
            <i class="fal fa-file-medical-alt"></i> NIV
        </button>
        <button mat-raised-button  *ngIf="cas?.patient"  color="accent" (click)="getPatient()" >
            <i class="fal fa-copy"></i> patient
        </button>

    </div>







   <!-- <span class="check" [ngClass]="{'active':ppp}" (click)="selectMulti()"></span>-->

    `

})

export class CasComponent implements OnInit, OnDestroy {
    public mainTC: Date;
    public delay: number;
    public outOfDelay: boolean;
    private subscription: any;
    @Input() cas: CasModel;
    @Input() class: string;
    public mode = {
        autocomplete: false,
        large: false,
        medium: false,
        short: false
    };
    @Output() selectCas: EventEmitter<any> = new EventEmitter<any>();
    @Output() selectCasMulti: EventEmitter<any> = new EventEmitter<any>();
    private classNames: string = '';
    public duree: any;

    public currentPosition: string;
    public lastAction: Date;
    public lastObservation: Date;
    public notif: { tc: Date, type: string };

    constructor(
        private visu: VisualistationService,
        private CAS: CasService,
        private USER: UserService,
        private PARAM: ParametreService,
        private PASSAGE: PassageService,
        private LIST: ListService,
    ) { }

    private defineMode(mode) {
        if (this.classNames.includes('short')) mode = 'short';
        if (this.classNames.includes('medium')) mode = 'medium';
        if (this.classNames.includes('large')) mode = 'large';
        Object.keys(this.mode).forEach(k => this.mode[k] = k == mode ? true : false);
    }
    ngOnChanges(changes: SimpleChanges) {

        if (changes.class) this.setClass(changes.class.currentValue);
        if (changes.cas) this.initCas();
    }
    ngOnInit() {
        this.setClass(this.class);
        this.defineMode(this.visu.currentDisplay.casMode);
        this.subscription = this.visu.display.subscribe(display => this.defineMode(display.casMode));

        this.initCas();

    }
    ngOnDestroy() {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
    }

    private initCas() {
        if (!this.cas) return;
        if (this.cas.action)
            this.lastAction = new Date(parseInt(this.cas.action.split(':')[1]));


        if (this.cas.observation)
            this.lastObservation = new Date(parseInt(this.cas.observation.split(':')[1]));


        if (this.cas.notification && this.cas.notification.length > 0) {
            this.notif = { tc: null, type: null };
            // definition du type à afficher (hyerarchie)
            let types = this.cas.notification.map(id => id.split(':')[1]);
            if (types.includes('intervention')) this.notif.type = 'intervention';
            else if (types.includes('regulation')) this.notif.type = 'regulation';
            else if (types.includes('discussion')) this.notif.type = 'discussion';
            // definition du tc
            let id = this.cas.notification.find(id => id.split(':')[1] == this.notif.type);
            this.notif.tc = new Date(parseInt(id.split(':')[2]));
        }

        if (this.cas.position) {

            let postionArr = this.cas.position.split(':');

            this.mainTC = new Date(parseInt(postionArr[postionArr.length - 1]));
            this.delay = this.getDiffInMin(this.mainTC);
            this.outOfDelay = this.delay > 30;

            if (postionArr.length < 3) return this.currentPosition = null;

            let poste = postionArr[1];
            let detailPoste: any;
            if (this.PARAM.currentParametre.poste)
                detailPoste = this.PARAM.currentParametre.poste.find(p => p.nom == poste);

            let nick: string;
            if (detailPoste && detailPoste.nick) nick = detailPoste.nick;
            else nick = poste.substring(0, 2);

            this.currentPosition = nick;
        }

    }

    private setClass(pclass: string) {
        this.classNames = '';
        this.classNames += pclass;//attribut class du custom element
        if (this.cas) {
            if (this.cas.clos) this.classNames += ' clos';
            this.classNames += ' ' + this.cas.urgence;

        }

        Object.keys(this.mode).forEach(k => { if (this.classNames.includes(k)) this.defineMode(k) });
    }

    getDiff(tc: Date) {
        if (!tc) return '...';
        let diff = new Date(new Date().getTime() - new Date(tc).getTime())
        return diff;
    }
    getDiffInMin(tc: Date) {
        if (!tc) return 0;

        let diff: number = new Date().getTime() - new Date(tc).getTime()
        return Math.floor((diff) / (1000 * 60));
    }

    getAge(tc: Date) {
        if (!tc) return 0;
        var formattedAge = moment().diff(moment(tc), "years");

        return formattedAge;
    }

    public select(rubrique?: string, sousRubrique?: string, edit?: boolean) {
        let path: any = { cas: this.cas._id };
        if (rubrique) path.rubrique = rubrique;
        if (sousRubrique) path.sousRubrique = sousRubrique;
        path.action = edit ? 'edit' : 'show';
        this.selectCas.emit(path);
    }

    public selectMulti() {
        this.selectCasMulti.emit(this.cas._id);

    }

    public getPatient() {
        this.visu.selectPatient = this.cas.patient;
        console.log(this.visu.selectPatient);
      }

    public async entree() {
        if (this.cas.position && this.USER.INFO.poste) {
            await this.CAS.closeCasPassage(this.cas);
        }

        // bugfix: check for unlinked open passages
        const existingPassageId = await this.PASSAGE.bugfixFindPassageAndCloseExtraneous(this.cas._id, this.LIST.listsRef.passage);
        if (existingPassageId) {
            await this.PASSAGE.forceClosePassage(existingPassageId, 'Close passage before creating a new one');
        }

        await this.CAS.openCasPassage(this.cas);
        this.visu.navTo({ cas: this.cas._id, rubrique: 'info', sousRubrique: 'pec' });
    }

}

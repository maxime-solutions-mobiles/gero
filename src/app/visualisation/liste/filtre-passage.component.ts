import { Component, OnInit, OnDestroy } from '@angular/core';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { Subscription } from 'rxjs';
import { VisualistationService, CasMode } from '../visualisation.service';
import { ParametreService } from '../../services/parametre.service';
import { ListService } from '../../services/list.service';
import { UserService } from '../../services/user.service';
import { DBService } from '../../services/db.service';

import * as moment from 'moment';
import 'moment/locale/fr';

@Component({
    selector: 'filtre-passage',
    template: `
    <div class="ligne1">
        <div class="filtre_place_switch">
            <span *ngIf="user?.isOmnipotent" [ngClass]="{'active':placeSwitch == 'poste'}" (click)="changePlace('poste', $event)">poste</span>
            <span *ngIf="!user?.isOmnipotent" (click)="toggleCasFlotant()"  [ngClass]="{'active':posteOnly}">poste</span>
        </div>
        <div class="filtre_place_select">
            <div *ngIf="user?.isOmnipotent">
                <mat-select *ngIf="placeSwitch"
                [placeholder]="placeValue" (selectionChange)="selectPlace($event)">
                <mat-option
                *ngFor="let option of placesDisplay"
                [value]="option"
                >
                {{ option }}
                </mat-option>
                </mat-select>
                <p *ngIf="!placeSwitch">Partout</p>
            </div>
            <div *ngIf="!user?.isOmnipotent"  (click)="toggleCasFlotant()">
                <p *ngIf="user?.poste" [ngClass]="{'active':posteOnly}">{{user?.poste}}</p>
            </div>
        </div>

        <div class="filtre_state">
            <span [ngClass]="{'active':stateSwitch == 'present'}" (click)="changeState('present')">PRESENT</span>
            <span [ngClass]="{'active':stateSwitch == 'tous'}" (click)="changeState('tous')">SORTIS</span>
        </div>
        <div  class="filtre_search">
            <mat-select
            placeholder="{{selectedField}}" (selectionChange)="changeField($event)">
            <mat-option *ngFor="let option of fields" [value]="option">
              {{ option }}
            </mat-option>
            </mat-select>

            <input matInput  type="text" placeholder={{selectedField}}  (keyup)="search($event)" #input [value]="searchBarValue">
        </div>
        <div class="filtre_toggle">
            <i class="fal " [ngClass]="{'fa-angle-up':showLigne2,'fa-angle-down':!showLigne2}" (click)="toggleLigne2()"></i>
        </div>
        <div class="filtre_reset"> <i (click)="resetFilters()" class="fal fa-sync closeBtn"></i></div>
        <div class="filtre_csv" *ngIf="user?.fonction =='administrateur'">
            <button mat-button (click)="generateCSV()">CSV</button>
        </div>
        <div class="wait" *ngIf="loading"><i class="fas fa-spinner-third fa-spin"></i></div>
    </div>
    <div class="ligne2" [ngClass]="{'show':showLigne2}">
        <div class="filtre_pictos">
            <i class="fal fa-exchange" [ngClass]="{'active':etatDisplay.regulation}" (click)="changePicto('regulation')"></i>
            <i class="fal fa-comment"  [ngClass]="{'active':etatDisplay.discussion}" (click)="changePicto('discussion')"></i>
            <i class="fal fa-bell"  [ngClass]="{'active':etatDisplay.intervention}" (click)="changePicto('intervention')"></i>
            <i class="fal fa-hospital-symbol"  [ngClass]="{'active':etatDisplay.conc_regulation_type}" (click)="changePicto('conc_regulation_type')"></i>
            <i class="fal fa-question"  [ngClass]="{'active':etatDisplay.notif_regulation_type}" (click)="changePicto('notif_regulation_type')"></i>
            <!--<i class="fal fa-ambulance"  [ngClass]="{'active':etatDisplay.transport}" (click)="changePicto('')"></i> -->
        </div>
        <div class="filtre_casmode">
            <i class="fal" [ngClass]="{'fa-eye-slash':casMode == 'large','fa-eye':casMode == 'medium'}" (click)="toggleCasMode()"></i>
        </div>
    </div>
    `
})
export class FiltrePassageComponent implements OnInit, OnDestroy {

    public settingFilters: boolean;
    public user: any;
    public posteOnly: boolean;
    public showLigne2 = false;

    public casMode: CasMode;
    public fields = [
        'nom',
        'prenom',
        'niv',
        'pathologie',
        'auteur',
        'urgence',
        'sexe',
        'age',
        'sinus'
    ];
    public searchBarValue: string = null;

    private currentFilters: any = {};

    private places: any = {
        zone: [],
        poste: []
    };
    public placesDisplay: string[] = [];
    public placeValue: string;
    public etatDisplay: any = {
        regulation: false,
        discussion: false,
        transport: false,
        intervention: false,
        conc_regulation_type:false,
        notif_regulation_type: false
    };
    public placeSwitch: any = null;
    public stateSwitch: any = null;
    public selectedField = 'nom';
    public loading: boolean;
    private subscription: Subscription[] = [];

    constructor(
        private LIST: ListService,
        private USER: UserService,
        private PARAM: ParametreService,
        private VISU: VisualistationService,
        public  DB: DBService,
    ) { }

    ngOnInit() {
        let sub: Subscription;
        sub = this.USER.profile.subscribe((profile) => {
            this.user = profile;
            if (this.user.isOmnipotent) {
                this.showLigne2 = true;
            }
            this.setFilters(this.LIST.currentFilters.passage);
        });
        this.subscription.push(sub);

        sub = this.PARAM.current.subscribe(r => {
            if (r.zone) {
                this.places.zone = r.zone.map(l => l.nom);
            }
            if (r.poste) {
                const postes = r.poste.map(l => l.nom);
                postes.push('TOUS');
                this.places.poste = postes;
            }
        });
        this.subscription.push(sub);

        sub = this.VISU.display.subscribe((display) => {
            console.log('jts filter passage');
            this.casMode = display.casMode;
        });
        this.subscription.push(sub);

        sub = this.LIST.currentFiltersObservable.subscribe((filters) => {
            this.setFilters(filters.passage);
        });
        this.subscription.push(sub);

        if (this.PARAM.currentParametre.zone) {
            this.places.zone = this.PARAM.currentParametre.zone.map(l => l.nom);
        }

        if (this.PARAM.currentParametre.poste) {
            this.places.poste = this.PARAM.currentParametre.poste.map(l => l.nom);
        }

        this.casMode = this.VISU.currentDisplay.casMode;

        if (this.LIST.currentFilters.passage) {
            this.setFilters(this.LIST.currentFilters.passage);
        } else {
            this.setFilters();
            this.goFilter();
        }
    }

    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    private setFilters(newFilters: any = {}) {
        if (this.currentFilters === newFilters) {
            return;
        }
        this.settingFilters = true;
        const filtres = { ...newFilters };

        // define pictos
        Object.keys(this.etatDisplay).forEach(etat => {
            this.etatDisplay[etat] = false;
        });
        if (filtres.notification) {
            const refs = filtres.notification.$in;
            if (refs && Array.isArray(refs) && refs.length > 0) {
                refs.forEach(r => this.etatDisplay[r.split(':')[1]] = true);
                // reset???
            }
        }

        // define poste switch & filter
        if (this.user && this.user.poste && this.user.poste !== 'pc') {
            if (filtres.poste) {
                this.posteOnly = true;
            }
            this.placeSwitch = 'poste';
            this.placesDisplay = this.places.poste;
            this.placeValue = this.user.poste;
        }

        if (this.user && this.user.isOmnipotent && filtres.poste) {
            this.placeSwitch = 'poste';
            this.placesDisplay = this.places.poste;
            this.placeValue = 'TOUS';
        }

        // define en cours/clos
        if (filtres.lieu) {
            this.stateSwitch = filtres.lieu.$exists ? 'PRESENT' : 'TOUS';
        } else {
            this.stateSwitch = 'PRESENT';
        }

        // define search bar

        this.fields.forEach(field => {
            if (filtres[field]) {
                this.selectedField = field;
                if (filtres[field].$regex) {
                    this.searchBarValue = filtres[field].$regex.toString().split('/')[1];
                } else {
                    this.searchBarValue = '';
                }
            }
        });

        if (!filtres[this.selectedField]) {
            this.searchBarValue = '';
        }

        this.currentFilters = filtres;
        this.settingFilters = false;
    }

    private goFilter() {
        this.LIST.filtreList('passage', this.currentFilters);
    }

    public search(event) {
        const query = event.target.value;
        if (!query) {
            delete this.currentFilters.cas;
            this.goFilter();
            return;
        }

        const casIds = this.LIST.listsRef.cas
          .filter((cas) => {
            const prop = cas[this.selectedField];
            const regex = new RegExp(query, 'gi');
            return prop && regex.test(prop);
          })
          .map((cas) => cas._id);
        this.currentFilters.cas = casIds;

        this.goFilter();
    }

    public changeField(evt: any) {
        this.fields.forEach(field => {
            delete this.currentFilters[field];
        });
        this.selectedField = evt.value;
        this.searchBarValue = '';
        this.goFilter();
    }

    public changePlace(tooglePlace: string, evt: any) {
        this.placeSwitch = (this.placeSwitch === tooglePlace) ? null : tooglePlace;
        this.placesDisplay = this.places[tooglePlace];

        if (this.placeSwitch == null) {
            if (this.currentFilters.lieu) {
                delete this.currentFilters.lieu;
            }
        } else if (this.user.isOmnipotent) {
            if (this.user.poste) {
                this.currentFilters.lieu = this.user.poste;
            } else if (evt.value) {
                this.currentFilters.lieu = evt.value;
            }
        }
        this.goFilter();
    }

    public selectPlace(evt: any) {
        if (evt.value === 'TOUS') {
            if (this.currentFilters.sortie) {
                delete this.currentFilters.sortie;
            }
        } else {
            this.currentFilters.lieu = evt.value;
        }
        this.goFilter();
    }

    public toggleCasFlotant() {
        // affichage/toggle cas du poste / cas du poste + flottant
        this.posteOnly = !this.posteOnly;
        this.currentFilters.poste = this.posteOnly ? this.user.poste : null;
        this.goFilter();
    }

    public changeState(toogleState: string) {
        if (this.stateSwitch === toogleState) {
            this.stateSwitch = null;
            delete this.currentFilters.sortie;
        } else {
            this.stateSwitch = toogleState;
            this.currentFilters.sortie = this.stateSwitch === 'present' ? { $exists: false } : { $exists: true };
        }

        this.goFilter();
    }

    public changePicto(pictoName: string) {
        this.etatDisplay[pictoName] = !this.etatDisplay[pictoName];

        const finalList: string[] = [];
        const list = this.LIST.lists.notification;
        let activePicto = 0;

        Object.keys(this.etatDisplay).forEach(etat => {
            if (this.etatDisplay[etat]) {
                activePicto++;
                finalList.push(...list[etat]);
            }
        });

        if (activePicto > 0) {
            this.currentFilters = { cas: finalList };
        } else {
            delete this.currentFilters.cas;
        }

        this.goFilter();
    }

    public resetFilters() {
        this.LIST.resetList('passage');
    }

    public toggleLigne2() {
        this.showLigne2 = !this.showLigne2;
    }

    public toggleCasMode() {
        const mode: CasMode = (this.casMode === 'medium') ? 'large' : 'medium';
        this.VISU.changeCasMode(mode);
    }

    async getPassageList() {
        const passages = await this.DB.getAll('passage');

        for (const passage of passages) {
            if (passage.conclusion !== undefined) {
                const conclusion = await this.DB.getById('conclusion', passage.conclusion);
                if (conclusion.regulation) {
                    passage.conclusion = conclusion.regulation[conclusion.regulation.length - 1];
                }
            }

            if (passage.patient !== undefined) {
                const patient = await this.DB.getById('patient', passage.patient);
                passage.patient = patient;
            }

            if (passage.position !== undefined) {
                const position = await this.DB.getById('passage', passage.position);
                passage.position = position;
            }

            if (passage.observation !== undefined) {
                const observation = await this.DB.getById('observation', passage.observation);
                passage.observation = observation;
            }

        }

        return passages;
    }

    generateCSV() {
        this.loading = true;

        this.getPassageList().then(list => {
            this.loading = false;

            const rows = [];

            for (const cas of list) {
                const row = {
                    timecode: cas.timeCode ?  moment(cas.timeCode).format('DD/MM/YYYY HH:mm') : '',
                    auteur: cas.auteur ? cas.auteur : '',
                    niv: cas.niv ? cas.niv : '',
                    poste: cas.poste ? cas.poste : '',
                    pathologie: cas.pathologie ? cas.pathologie : '',
                    nom: cas.nom ? cas.nom : '',
                    prenom: cas.prenom ? cas.prenom : '',
                    age: cas.age ? moment(cas.age).format('DD/MM/YYYY') : '',
                    sexe: cas.seze ? cas.seze : '',
                    urgence: cas.urgence ? cas.urgence : ''
                };

                row['patient nom'] = cas.patient && cas.patient.nom ? cas.patient.nom : '';
                row['patient prenom'] = cas.patient && cas.patient.prenom ? cas.patient.prenom : '';
                row['patient age'] = cas.patient && cas.patient.age ? moment(cas.patient.age).format('DD/MM/YYYY') : '';
                row['patient sexe'] = cas.patient && cas.patient.sexe ? cas.patient.sexe : '';
                row['patient types'] = cas.patient && cas.patient.types ? cas.patient.types  : '';
                row['patient nationalite'] = cas.patient && cas.patient.nationalite ? cas.patient.nationalite : '';
                row['patient adresse'] = cas.patient && cas.patient.adresse ? cas.patient.adresse : '';
                row['patient postcode'] = cas.patient && cas.patient.postcode ? cas.patient.postcode : '';
                row['patient ville'] = cas.patient && cas.patient.city ? cas.patient.city : '';
                row['patient telephone'] = cas.patient && cas.patient.telephone ? cas.patient.telephone : '';
                row['patient poids'] = cas.patient && cas.patient.poids ? cas.patient.poids : '';
                row['patient taille'] = cas.patient && cas.patient.taille ? cas.patient.taille : '';

                row['conclusion type'] = cas.conclusion && cas.conclusion.type ? cas.conclusion.type : '';
                row['conclusion commentaires'] = cas.conclusion && cas.conclusion.commentaires ? cas.conclusion.commentaires : '';
                row['conclusion hopital'] = cas.conclusion && cas.conclusion.hopital ? cas.conclusion.hopital : '';
                row['conclusion moyen'] = cas.conclusion && cas.conclusion.moyen ? cas.conclusion.moyen : '';
                row['conclusion position'] = cas.conclusion && cas.conclusion.position ? cas.conclusion.position : '';
                row['conclusion service'] = cas.conclusion && cas.conclusion.service ? cas.conclusion.service : '';
                row['conclusion urgence'] = cas.conclusion && cas.conclusion.urgence ? cas.conclusion.urgence : '';

                row['passage lieu'] = cas.position && cas.position.lieu ? cas.position.lieu : '';
                row['passage pathologie'] = cas.position && cas.position.pathologie ? cas.position.pathologie : '';

                const circonstances = [];
                const signe = [];
                const plainte = [];
                const cinetique = [];

                let exposure = '';
                let soap = '';
                let surveillance = '';
                let vitales = '';

                if (cas.observation) {

                    if (cas.observation.circonstancielle) {
                        for (const circonstancielle of cas.observation.circonstancielle) {
                            if (circonstancielle) {
                                switch (circonstancielle.type) {
                                    case 'circonstances':
                                        if (circonstancielle.circonstances && circonstancielle.circonstances !== '') {
                                            circonstances.push(circonstancielle.circonstances);
                                        }
                                        break;
                                    case 'signe':
                                        if (circonstancielle.signe && circonstancielle.signe !== '') {
                                            signe.push(circonstancielle.signe);
                                        }
                                        break;
                                    case 'plainte':
                                        if (circonstancielle.plainte && circonstancielle.plainte !== '') {
                                            plainte.push(circonstancielle.plainte);
                                        }
                                        break;
                                    case 'cinetique':
                                        cinetique.push([
                                            'avp: ' + circonstancielle.avp, 'chute: ' + circonstancielle.chute,
                                            'cinetique: ' + circonstancielle.cinetique,
                                            'vitesse: ' + circonstancielle.vitesse
                                        ].join(', '));
                                        break;
                                }
                            }
                        }
                    }

                    if (cas.observation.exposure) {
                        exposure = cas.observation.exposure.reduce((result, e) => {
                            if (e) {
                                result.push([
                                    'edescription: ' + e.edescription,
                                    'elateralisation: ' + e.elateralisation,
                                    'elocalisation: ' + e.elocalisation,
                                    'etype: ' + e.etype
                                ].join(', '));
                            }
                            return result;
                        }, []).join('|');
                    }

                    if (cas.observation.soap) {
                        soap = cas.observation.soap.reduce((result, s) => {
                            if (s) {
                                result.push(['examen: ' + s.examen, 'hdm: ' + s.hdm, 'type: ' + s.type].join(', '));
                            }
                            return result;
                        }, []).join('|');
                    }

                    if (cas.observation.surveillance) {
                        surveillance = cas.observation.surveillance.reduce((result, s) => {
                            if (s) {
                                result.push([
                                    'controler: ' + s.controler,
                                    'dsgm: ' + s.dsgm,
                                    'dsgv: ' + s.dsgv,
                                    'dsgy: ' + s.dsgy,
                                    'etco2: ' + s.etco2,
                                    'evs: ' + s.evs,
                                    'fc: ' + s.fc,
                                    'fr: ' + s.fr,
                                    'glasgow: ' + s.glasgow,
                                    'glycemie: ' + s.glycemie,
                                    'hemocue: ' + s.hemocue,
                                    'heure: ' + s.heure,
                                    'oxygene: ' + s.oxygene,
                                    'padd: ' + s.padd,
                                    'padg: ' + s.padg,
                                    'pamd: ' + s.pamd,
                                    'pamg: ' + s.pamg,
                                    'pasd: ' + s.pasd,
                                    'pasg: ' + s.pasg,
                                    'spo2: ' + s.spo2,
                                    'temperature: ' + s.temperature,
                                    'trc: ' + s.trc,
                                    'type:' + s.type,
                                    'uniteglycemie: ' + s.uniteglycemie
                                ].join(', '));
                            }
                            return result;
                        }, []).join('|');
                    }

                    if (cas.observation.vitale) {
                        vitales = cas.observation.vitale.map(vitale => {

                            let v = [];

                            if (vitale.airways) {
                                v.push('aliberte: ' + vitale.airways.aliberte);
                                v.push('arachis: ' + vitale.airways.arachis);
                                v.push('arespiration: ' + vitale.airways.arespiration);
                                v.push('avomissement: ' + vitale.airways.avomissement);
                            }

                            if (vitale.breathing) {
                                v.push('bbruits: ' + vitale.breathing.bbruits);
                                v.push('bbruitscote: ' + vitale.breathing.bbruitscote);
                                v.push('bdetresse: ' + vitale.breathing.bdetresse);
                                v.push('befficace: ' + vitale.breathing.befficace);
                                v.push('bparler: ' + vitale.breathing.bparler);
                                v.push('bqualification: ' + vitale.breathing.bqualification);
                            }

                            if (vitale.circulation) {
                                v.push('cdetresse: ' + vitale.circulation.cdetresse);
                                v.push('chemorragie: ' + vitale.circulation.chemorragie);
                                v.push('cpouls: ' + vitale.circulation.cpouls);
                                v.push('cqualification: ' + vitale.circulation.cqualification);
                                v.push('ctrc: ' + vitale.circulation.ctrc);
                            }

                            if (vitale.disabilities) {
                                v.push('controler: ' + vitale.disabilities.controler);
                                v.push('dconscience: ' + vitale.disabilities.dconscience);
                                v.push('dconvulsion: ' + vitale.disabilities.dconvulsion);
                                v.push('dconvulsionsduree: ' + vitale.disabilities.dconvulsionsduree);
                                v.push('dmotricite: ' + vitale.disabilities.dmotricite);
                                v.push('dorientation: ' + vitale.disabilities.dorientation);
                                v.push('dpci: ' + vitale.disabilities.dpci);
                                v.push('dpciduree: ' + vitale.disabilities.dpciduree);
                                v.push('dpupilledroite: ' + vitale.disabilities.dpupilledroite);
                                v.push('dpupillegauche: ' + vitale.disabilities.dpupillegauche);
                                v.push('dsensibilite: ' + vitale.disabilities.dsensibilite);
                                v.push('dsgm: ' + vitale.disabilities.dsgm);
                                v.push('dsgv: ' + vitale.disabilities.dsgv);
                                v.push('dsgy: ' + vitale.disabilities.dsgy);
                                v.push('glasgow: ' + vitale.disabilities.glasgow);
                            }

                            return v.join(', ');
                        }).join('|');
                    }
                }

                row['circonstancielle circonstances'] = circonstances.join('|');
                row['circonstancielle signe'] = signe.join('|');
                row['circonstancielle plainte'] = plainte.join('|');
                row['circonstancielle cinetique'] = cinetique.join('|');

                row['observation exposure'] = exposure;
                row['observation soap'] = soap;
                row['observation surveillance'] = surveillance;
                row['observation vitale'] = vitales;

                rows.push(row);
            }

            const csvReport = new Angular5Csv(rows, 'CAS', {
                fieldSeparator: ';',
                quoteStrings: '"',
                showLabels: true,
                headers: [
                    'timeCode',
                    'auteur',
                    'niv',
                    'poste',
                    'pathologie',
                    'nom',
                    'prenom',
                    'age',
                    'sexe',
                    'urgence',
                    'patient nom',
                    'patient prenom',
                    'patient age',
                    'patient sexe',
                    'patient types',
                    'patient nationalite',
                    'patient adresse',
                    'patient postcode',
                    'patient ville',
                    'patient telephone',
                    'patient poids',
                    'patient taille',
                    'conclusion type',
                    'conclusion commentaires',
                    'conclusion hopital',
                    'conclusion moyen',
                    'conclusion position',
                    'conclusion service',
                    'conclusion urgence',
                    'passage lieu',
                    'passage pathologie',
                    'observation circonstancielle circonstances',
                    'observation circonstancielle signe',
                    'observation circonstancielle plainte',
                    'observation circonstancielle cinetique',
                    'observation exposure',
                    'observation soap',
                    'observation surveillance',
                    'observation vitale'
                ]
            });
        });
    }

}

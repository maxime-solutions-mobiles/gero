import {
  OnInit,
  OnChanges,
  Component,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  HostBinding,
} from '@angular/core';

import { ListService } from 'src/app/services/list.service';
import { MoyenService } from 'src/app/services/moyen.service';
import { MoyenModel } from '../models/moyen.model';

@Component({
  selector: 'liste-intervention-moyen',
  template: `
    <span class="name {{className}}" [ngClass]="{'current':!currentState}"><i class="fal fa-ambulance"></i> {{moyen.nom}}</span>

    <span *ngFor="let state of states"
      [ngClass]="{ archive: state.timeCode, next: state === nextState, current: state === currentState }"
      class="{{className}}">
      <ng-container *ngIf="state.timeCode">
        <span>{{state.label}}</span>
        <span *ngIf="state.timeCode !== '-'; else notDate">{{state.timeCode | date:'HH:mm'}}</span>
        <ng-template #notDate><span>{{state.timeCode}}</span></ng-template>
      </ng-container>
      <ng-container *ngIf="!state.timeCode && state.ref !== updating" >
        <span class="btn {{state?.color}}"
          [ngClass]="{'disabled': !interStarted }"
          (click)="changeStateTo($event,state.ref)">
          {{state.label}}
        </span>
      </ng-container>
      <ng-container *ngIf="!state.timeCode && state.ref === updating" >
        <span class="btn {{state?.color}}"><i class="fas fa-spinner-third fa-spin"></i></span>
      </ng-container>
    </span>
    <span *ngIf="className !== 'success' && className !== 'archive'" class="dispo">
        <span class="btn success"
          [ngClass]="{ disabled: !interStarted }"
          (click)="changeStateTo($event, 'disponible')">
          Disponible
        </span>
    </span>
  `
})
export class ListeInterventionMoyen implements OnInit, OnChanges {

  @HostBinding('class') className: string = null;

  @Input() moyen: MoyenModel;
  @Input() interID: string;
  @Input() interStarted: boolean;
  @Input() updating: any;

  public states: any[] = [];
  public nextState: string;
  public currentState: string;

  private stateRef: string[];
  private stateDefinition;

  constructor(private MOYEN: MoyenService, private LIST: ListService) {
    this.stateDefinition = this.MOYEN.states;
    this.stateRef = this.MOYEN.stateRef;
    this.stateRef.splice(-1, 1);
  }

  ngOnInit() {
    this.states = this.formatStates(this.moyen);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.states = this.formatStates(this.moyen);
  }

  private formatStates(moyen: MoyenModel) {
    if (!moyen) {
      return;
    }
    let states = [];

    // choix des states à afficher (current ou archive)
    const historique = moyen.historique.find(s => s.intervention === this.interID);

    if (moyen.currentIntervention === this.interID) {
      states = moyen.currentStates;
      this.className = this.stateDefinition[this.moyen.etat]
        ? this.stateDefinition[this.moyen.etat].color
        : 'success';
    } else if (historique) {
      states = historique.etats;
      this.className = 'archive';
    }

    // definitions des etats à afficher
    const etats = this.stateRef.map((k, i) => ({
      ...this.stateDefinition[k],
      ...states.find(s => s.etat === k),
    }));

    this.nextState = etats.find(e => !e.timeCode);
    let nextStateIndex = etats.findIndex(e => !e.timeCode);

    nextStateIndex = nextStateIndex !== -1 ? nextStateIndex : etats.length;
    this.currentState = etats[nextStateIndex - 1];
    return etats;
  }

  public changeStateTo(event, stateRef) {
    if (!this.interStarted) {
      return;
    }
    event.stopPropagation();

    this.moyen.updating = stateRef;
    // bugfix: ensure intervention is set
    if (!this.moyen.currentIntervention) {
      this.moyen.currentIntervention = this.interID;
    }
    this.LIST.updatingListItem('moyen', this.moyen);
    this.MOYEN.goToState(this.moyen, stateRef);
  }

}

import { CommonModule } from '@angular/common';
import { InterventionComponent } from './intervention.component';
import { CasComponent } from './cas.component';
import { PassageComponent } from './passage.component';
import { NgModule } from '@angular/core';


import { MoyenComponent } from './moyen.component';
import { FiltreCasComponent } from './filtre-cas.component';
import { FiltrePassageComponent } from './filtre-passage.component';
import { MatSelectModule, MatButtonModule } from '@angular/material';
import { OrderCasComponent } from './order-cas.component';
import { FiltreInterventionComponent } from './filtre-intervention.component';
import { FiltreMoyenComponent } from './filtre-moyen';
import { ListeInterventionMoyen } from './liste-intervention-moyen.component';

@NgModule({
    imports: [
        CommonModule,
        MatSelectModule,
        MatButtonModule
    ],
    exports: [
        MoyenComponent,
        CasComponent,
        PassageComponent,
        InterventionComponent,
        FiltreCasComponent,
        FiltreInterventionComponent,
        FiltreMoyenComponent,
        OrderCasComponent,
        ListeInterventionMoyen,
        FiltrePassageComponent,
    ],
    declarations: [
        MoyenComponent,
        CasComponent,
        PassageComponent,
        InterventionComponent,
        FiltreCasComponent,
        FiltreInterventionComponent,
        FiltreMoyenComponent,
        OrderCasComponent,
        ListeInterventionMoyen,
        FiltrePassageComponent,
    ],
    providers: [],
})
export class ListeModule { }

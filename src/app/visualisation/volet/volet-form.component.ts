import { CasModel } from '../models/cas.model';
import { DBService } from '../../services/db.service';
import { VisualistationService } from '../visualisation.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ListService } from '../../services/list.service';
import { CasService } from '../../services/cas.service';




@Component({
    selector: 'volet-form',
    templateUrl: './volet-form.template.html',
    host: { '[class]': 'classe' }
})
export class VoletForm implements OnInit, OnDestroy {
    public action: any;
    public patient: any;
    public curObjects: any;
    public sousRubrique: string;
    private subscription: any[] = [];
    public cas: CasModel;
    public classe: string = 'close';
    constructor(
        public DB: DBService,
        public VISU: VisualistationService,
        public LIST: ListService,
        public CAS: CasService
    ) { }

    ngOnInit() {
        this.sousRubrique = this.VISU.currentURL.sousRubrique;
        this.subscription[0] = this.VISU.URL.subscribe(url => {
            this.sousRubrique = url.sousRubrique;
            this.classe = url.action == "edit" || url.action == "add" ? 'open' : 'close';
            this.action = url.action;
        });

        this.subscription[1] = this.CAS.casObservable.subscribe(cas => this.cas = cas);
        this.subscription[2] = this.CAS.patientObservable.subscribe(patient => this.patient = patient);
    }

    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    public submit(categorie: string[], nouvelItem: any, close: boolean = true) {
        // TODO : test if cas or else
        this.CAS.addNewItem(categorie, nouvelItem)//.then(r => this.LIST.editListItem('cas', this.CAS.currentObjects.cas));
        if (close) this.close();
    }

    public addPatient(nouveauPatient) {
        this.CAS.addPatient(nouveauPatient);
        this.LIST.refreshList('cas');
        this.LIST.refreshList('passages');
        this.close();
    }

    public close() {
        this.VISU.navTo({ action: 'show' });
        this.classe = 'close';
    }
    public searchDB(evt, el) {
        if (this.VISU.currentURL.action != "edit") return;
        this.VISU.autoCompleteSearch(evt)
    }
}


import { VisualistationService } from '../visualisation.service';
import { Component, OnInit, Input, OnDestroy } from "@angular/core";

@Component({
    selector: 'volet',
    host: { '[class]': 'classNames' },
    template: `
    <volet-tablette-header [display]="showVolet" (close)="close()"></volet-tablette-header>
    <section [ngSwitch]="showVolet">

        <header [ngClass]="{'hide':showVolet !='nouveauCas'&& showVolet !='nouvelleIntervention'}">
                    <div id="switchNewForm">
            <ul>
                <li [ngClass]="{'active':showVolet =='nouveauCas'}" (click)="selectForm('cas')">
                    <i class="fal fa-procedures"></i>
                    <span>Cas</span>
                </li>
                <li [ngClass]="{'active':showVolet =='nouvelleIntervention'}" (click)="selectForm('intervention')">
                    <i class="fal fa-bell"></i>
                    <span>Interventions</span>
                </li>
            </ul>
        </div>
        </header>

        <detail-nouveau-cas *ngSwitchCase="'nouveauCas'" (searchDB)="searchDB($event)"></detail-nouveau-cas>
        <app-detail-intervention-form *ngSwitchCase="'nouvelleIntervention'"></app-detail-intervention-form>

        <detail-moyen *ngSwitchCase="'moyen'"></detail-moyen>
        <app-detail-moyen-form *ngSwitchCase="'editMoyen'"></app-detail-moyen-form>

        <detail-intervention *ngSwitchCase="'intervention'"></detail-intervention>

        <detail-cas *ngSwitchCase="'cas'"></detail-cas>
        <detail-footer *ngSwitchCase="'cas'"></detail-footer>

        <volet-form></volet-form>
    </section>


    `
})
export class VoletComponent implements OnInit, OnDestroy {
    public classNames: string = "";
    public showVolet: string;
    private subscription: any;
    @Input() class: string;
    constructor(public VISU: VisualistationService) { }
    ngOnInit() {
        this.classNames = this.class;
        this.defineVoletContent(this.VISU.currentURL);
        this.subscription = this.VISU.URL.subscribe(url => this.defineVoletContent(url));
        if (this.showVolet && !this.classNames.includes('show') && !this.classNames.includes('hide')) this.classNames += ' show';
    }

    ngOnDestroy() {
      if (this.subscription) {
          this.subscription.unsubscribe();
      }
    }

    public selectForm(formName) {
        this.showVolet = formName == 'cas' ? 'nouveauCas' : 'nouvelleIntervention';
    }

    private defineVoletContent(url) {
        this.classNames = this.classNames.replace('hide', '');

        let show: string;

        if (url.moyen && typeof url.moyen === 'string') {
          show = 'moyen';
          if (url.rubrique === 'edit') {
            show = 'editMoyen';
          }
        }

        if (url.cas && typeof url.cas === 'string' && url.cas != 'new') show = 'cas';

        if (url.cas && typeof url.cas === 'string' && url.cas == 'new') show = 'nouveauCas';

        if (url.passage && typeof url.passage === 'string' && url.passage != 'new') show = 'cas';

        if (url.passage && typeof url.passage === 'string' && url.passage == 'new') show = 'nouveauCas';

        if (url.intervention && typeof url.intervention === 'string' && url.intervention != 'new') show = 'intervention';

        if (url.intervention && typeof url.intervention === 'string' && url.intervention == 'new') show = 'nouvelleIntervention';

        if (show && this.showVolet != show) this.showVolet = show;

        this.showVolet = show;
    }

    public searchDB(evt) {
        if (this.showVolet != "nouveauCas") return;
        this.VISU.autoCompleteSearch(evt);
    }
    public close() {
        this.classNames = this.classNames.replace('show', 'hide');
    }

}

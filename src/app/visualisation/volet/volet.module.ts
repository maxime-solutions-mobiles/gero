import { NgModule } from '@angular/core';

import { VoletForm } from './volet-form.component';
import { VoletComponent } from './volet.component';
import { DetailCasModule } from '../detail-cas/detail-cas.module';
import { DetailMoyenModule } from '../detail-moyen/detail-moyen.module';
import { DetailInterventionModule } from '../detail-intervention/detail-intervention.module';
import { CommonModule } from '@angular/common';
import { voletTabletteHeaderComponent } from './volet-tablette-header';
import { ListeModule } from '../liste/liste.module';

@NgModule({
    imports: [
        CommonModule,
        DetailCasModule,
        DetailMoyenModule,
        DetailInterventionModule,
        ListeModule

    ],
    exports: [
        VoletForm,
        VoletComponent,
        voletTabletteHeaderComponent
    ],
    declarations: [
        VoletForm,
        VoletComponent,
        voletTabletteHeaderComponent
    ],
    providers: [],
})
export class VoletModule { }

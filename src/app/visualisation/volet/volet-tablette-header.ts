import { CasModel } from './../models/cas.model';
import { VisualistationService } from './../visualisation.service';
import { Component, OnInit, Input, OnDestroy, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { CasService } from '../../services/cas.service';

@Component({
    selector: 'volet-tablette-header',
    host: { '[class]': 'classNames' },
    template: ` 
    
    
    
    <div [ngSwitch]='display' >
        <cas *ngSwitchCase="'cas'" [cas]="cas" class="medium noToolBar"></cas>
    </div>
    <div (click)="closeVolet()"> <i class="fal fa-times"></i></div>
    
    `
})

export class voletTabletteHeaderComponent implements OnInit, OnDestroy, OnChanges {
    @Input() display: string;
    @Output() close: EventEmitter<any> = new EventEmitter<any>();

    private subscription: any;
    public cas: CasModel;
    public classNames: string = "";

    constructor(private VISU: VisualistationService, private CAS: CasService) { }
    ngOnChanges(): void { this.defineClass() }

    ngOnInit() {
        this.cas = this.CAS.currentObjects.cas;
        this.defineClass();
        this.subscription = this.CAS.casObservable.subscribe(cas => {
            this.cas = cas;
            this.defineClass();
        })
    }
    private defineClass() {
        if (this.display == 'cas' && this.cas) this.classNames = this.cas.urgence;
        this.classNames += ' ' + this.display;

    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public closeVolet() { this.close.emit(true) }

}

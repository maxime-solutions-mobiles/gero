import { MoyenService } from './../services/moyen.service';
import { ListService } from './../services/list.service';
import { UserService } from 'src/app/services/user.service';
import { InterventionModel } from './models/intervention.model';
import { CasModel } from './models/cas.model';
import { PassageModel } from './models/passage.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { VisualistationService } from './visualisation.service';
import { MoyenModel } from './models/moyen.model';
import { CasService } from '../services/cas.service';
import { PassageService } from '../services/passage.service';
import { InterventionService } from '../services/intervention.service';


@Component({
    selector: 'visualisation',
    host: { '[class]': 'classNames' },
    templateUrl: './visualisation.page.html'
})
export class Visualisation implements OnInit, OnDestroy {
    pagination: { cas: any, passage:any, moyen: any, intervention: any } =
        {
            cas: { next: false, last: false },
            passage: { next: false, last: false },
            moyen: { next: false, last: false },
            intervention: { next: false, last: false }
        };

    public casMode: string;
    public currentPage: number = 0;
    public multiSelectedCas: string[] = [];
    public multiSelectedPassage: string[] = [];
    public mode: string = 'display';
    public nivMode: boolean = false;
    public currentCas: string = null;
    public currentPassage: string = null;
    public currentMoyen: string = null;
    public currentIntervention: string = null;
    public casList: CasModel[] = [];
    public passageList: PassageModel[] = [];
    public moyensList: MoyenModel[] = [];
    public interventionsList: InterventionModel[] = [];
    public classNames: string = null;
    public list: { cas: CasModel[], passage: PassageModel[], moyen: MoyenModel[], intervention: InterventionModel[] } = {
        cas: [],
        passage: [],
        moyen: [],
        intervention: []
    };
    public order = 'nom';
    public showNewCas: boolean;
    private subscription: any = {};
    public showList: string;
    public moyensListStatic = [
        {
            nom: 'SMUR1',
            type: 'ambulance',
            personnel: [],
            cas: [],
            disponible: true
        },
        {
            nom: 'SMUR3',
            type: 'ambulance',
            personnel: [],
            cas: [],
            interventions: [],
            disponible: true
        }
        ,
        {
            nom: 'Corso',
            type: 'poste',
            personnel: [],
            cas: [],
            interventions: [],
            disponible: true
        }
    ];
    public loadingList: boolean;
    private observableListsList = ['cas', 'moyen', 'passage', 'intervention']

    constructor(
        public VISU: VisualistationService,
        private LIST: ListService,
        public CAS: CasService,
        public INTER: InterventionService,
        public MOYEN: MoyenService,
        public PASSAGE: PassageService,
        private USER: UserService
    ) { }

    ngOnInit() {
        if (this.VISU.currentURL.intervention) this.currentIntervention = this.VISU.currentURL.intervention;
        if (this.VISU.currentURL.passage) this.currentPassage = this.VISU.currentURL.passage;
        if (this.VISU.currentURL.cas) this.currentCas = this.VISU.currentURL.cas;
        if (this.VISU.currentURL.moyen) this.currentMoyen = this.VISU.currentURL.moyen;

        this.subscription['URL'] = this.VISU.URL.subscribe(url => {
            if (url.cas && url.cas != this.currentCas) {
                this.currentCas = url.cas;
                this.VISU.voletShow({ cas: url.cas });
                if (this.CAS.currentObjects.cas && url.cas != this.CAS.currentObjects.cas._id) this.CAS.currentCas = url.cas;
            }

            if (url.passage) {
                this.currentPassage = url.passage;
                this.VISU.voletShow({ passage: url.passage });
            }
            if (url.intervention && url.intervention != this.currentIntervention) {
                this.currentIntervention = url.intervention;
                this.VISU.voletShow({ intervention: url.intervention });
            }
            if (url.moyen && url.moyen != this.currentMoyen) {
                this.currentMoyen = url.moyen;
                this.VISU.voletShow({ moyen: url.moyen });
            }

        });

        this.showList = this.VISU.currentDisplay.list;
        if (!this.showList) this.showList = 'cas';
        this.setList(this.showList);

        this.subscription['display'] = this.VISU.display.subscribe(display => {
            if (this.showList != display.list) {
                this.observableListsList
                    .filter(s => s != display.list && this.subscription[s])
                    .forEach(o => this.subscription[o] && this.subscription[o].unsubscribe());

                this.setList(display.list);
            }

            this.showList = display.list;
            this.mode = display.listMod;
            this.nivMode = display.nivMode;
            this.casMode = display.casMode;
        })



        // pagination
        if (this.LIST.currentPagination) this.pagination = this.LIST.currentPagination;
        this.subscription['pagination'] = this.LIST.paginationObservable.subscribe(pagination => this.pagination = pagination);

        // filtering effect
        this.subscription['filtering'] = this.LIST.filtering.subscribe(filtering => this.loadingList = filtering);
    }

    private setList(listName) {
        const assignList = (list) => {
            if (listName === 'cas') {
                this.casList = list;
            }
            if (listName === 'passage') {
                this.passageList = list;
            }
            if (listName === 'moyen') {
                this.moyensList = list;
            }
            if (listName === 'intervention') {
                this.interventionsList = list;
            }
        };

        assignList(this.LIST.pageList[listName]);
        this.classNames = listName;
        if (this.subscription.list) {
            this.subscription.list.unsubscribe();
        }
        this.subscription.list = this.LIST.pageListObservables[listName]
            .subscribe((list) => assignList(list));
    }

    ngOnDestroy() {
        Object.keys(this.subscription).forEach(sub => this.subscription[sub] && this.subscription[sub].unsubscribe())
    }

    public toggleNivMode(evt) {
        this.VISU.changeCasMode(evt.checked ? 'large' : 'medium')
    }

    public selectList(listName: 'cas'| 'passage' | 'intervention' | 'moyen') {
        this.VISU.currentDisplay = { list: listName, listMod: 'display' };
    }

    public newItem() {
        let type = this.showList || 'cas';
        if (this.showList === 'moyen') {
            type = 'intervention';
        }
        if (this.showList === 'passage' && !this.USER.poste) {
            type = 'cas';
        }
        this.VISU.navTo({ [type]: 'new' });
    }

    private selectCas(path: any) {
        if (this.mode == 'select') {
            this.INTER.affect('cas', path.cas);
            let cas = this.casList.find(c => c._id == path.cas)
            this.CAS.affectToInter(cas, this.INTER.currentInter);
            this.VISU.changeListMode();
            this.VISU.currentDisplay = { list: 'intervention', listMod: 'display' };
        } else {
            this.VISU.navTo(path);
        }
    }
    private selectCasMulti(id: any) {
        if (this.multiSelectedCas.includes(id)) this.multiSelectedCas = this.multiSelectedCas.filter(l => l != id);
        else this.multiSelectedCas.push(id);
    }
    private IsSelectCasMulti(id: any) {
        if (this.multiSelectedCas && this.multiSelectedCas.length > 0 && this.multiSelectedCas.includes(id)) return true;
        else return false;
    }

    private selectPassage(path: any) {
        this.selectCas(path);
    }

    private selectPassageMulti(id: any) {
        if (this.multiSelectedPassage.includes(id)) this.multiSelectedPassage = this.multiSelectedPassage.filter(l => l != id);
        else this.multiSelectedPassage.push(id);
    }

    private IsSelectPassageMulti(id: any) {
      if (!this.multiSelectedPassage) {
        return false;
      }
      if (!this.multiSelectedPassage.length) {
        return false;
      }
      return this.multiSelectedPassage.includes(id);
    }

    private isPassageActive(passage) {
      if (this.IsSelectPassageMulti(passage._id)) {
        return true;
      }
      if (this.currentCas === passage.cas) {
        return true;
      }
      return (this.currentPassage === passage._id);
    }

    public async selectMoyen(path: any) {
        if (this.mode !== 'select') {
          this.VISU.navTo(path);
          return;
        }
        this.INTER.affect('moyen', path.moyen);
        this.VISU.changeListMode();
        this.VISU.currentDisplay = { list: 'intervention', listMod: 'display' };
        const moyen = this.moyensList.find(m => m._id === path.moyen);
        await this.MOYEN.affectToInter(moyen, this.INTER.currentInter);
    }

    public goNextPage() {
        if (!this.pagination[this.showList].next) return;
        this.LIST.goPage(this.showList, this.pagination[this.showList].page + 1)
    }
    public goLastPage() {
        if (!this.pagination[this.showList].last || this.pagination[this.showList].page == 0) return;
        this.LIST.goPage(this.showList, this.pagination[this.showList].page - 1)
    }

    public async getCasFromPassage(passage: PassageModel) {
        return await this.PASSAGE.getCas(passage.cas);
    }

}

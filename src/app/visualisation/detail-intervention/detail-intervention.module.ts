import { NgModule } from '@angular/core';

import { DetailInterventionComponent } from './detail-intervention.component';
import { CommonModule } from '@angular/common';
import { DetailInterventionFormComponent } from './detail-intervention-form.component';
import { DynamicFormModule } from '../../forms/dynamic-form.module';
import { MatButtonModule, MatButtonToggleModule, MatTabsModule, MatCheckboxModule } from '@angular/material';
import { ListeModule } from '../liste/liste.module';
import { DetailMoyenModule } from '../detail-moyen/detail-moyen.module';

@NgModule({
    imports: [
        CommonModule,
        DynamicFormModule,
        MatButtonModule,
        MatButtonToggleModule,
        ListeModule,
        DetailMoyenModule,
        MatTabsModule,
        MatCheckboxModule
    ],

    exports: [
        DetailInterventionComponent,
        DetailInterventionFormComponent
    ],
    declarations: [
        DetailInterventionComponent,
        DetailInterventionFormComponent
    ],
    providers: [],
})
export class DetailInterventionModule { }

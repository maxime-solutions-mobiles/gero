import { Component, ViewChild, OnInit } from '@angular/core';

import { DBService } from 'src/app/services/db.service';
import { ParametreService } from 'src/app/services/parametre.service';
import { ListService } from 'src/app/services/list.service';
import { CasService } from 'src/app/services/cas.service';
import { MoyenService } from 'src/app/services/moyen.service';
import { FieldConfig } from 'src/app/forms/field-config.interface';
import { DynamicFormComponent } from 'src/app/forms/dynamic-form.component';

import { VisualistationService } from '../visualisation.service';
import { InterventionModel } from '../models/intervention.model';
import { CasModel } from '../models/cas.model';
import { MoyenModel } from '../models/moyen.model';

@Component({
  selector: 'app-detail-intervention-form',
  template: `
    <div>
      <div class='nouveau-cartouche-option' [ngClass]="{ active: hasAssociate }" *ngIf="moyen || cas">
        Associer à la position de {{ moyenName || niv }}
        <span><mat-checkbox (change)="toggleAssociate()"></mat-checkbox></span>
      </div>
      <dynamic-form
        [config]="config"
        #form="dynamicForm"
        (submit)="submit($event)">
      </dynamic-form>
    </div>
  `
})
export class DetailInterventionFormComponent implements OnInit {

  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;

  public moyen: MoyenModel;
  public showBox: boolean;
  public poste: string;
  public adresse: string;
  public cas: CasModel;
  public niv: string;
  public moyenName: string;
  public hasAssociate: boolean;
  public position: { lng: number, lat: number };

  public config: FieldConfig[] = [
    {
      type: 'select',
      name: 'origine',
      options: ['PC', 'SAMU', 'SDIS', 'POLICE', 'Autre'],
      placeholder: 'Origine',
      class: 'grid-demi'
    },
    {
      type: 'select',
      name: 'motif',
      options: [
        'Inconscient',
        'Blessé',
        'Malade',
        'ACR',
        'Autre',
        'Medical',
        'Accident',
        'Information',
      ],
      placeholder: 'Motif',
      class: 'grid-demi'
    },
    {
      type: 'input',
      name: 'requerant',
      placeholder: 'Requérant',
      class: 'grid-tier'
    },
    {
      type: 'phone',
      name: 'telephone',
      placeholder: 'Téléphone',
      class: 'grid-2tier'
    },

    {
      type: 'address',
      name: 'adresse',
      placeholder: 'Adresse',
      class: 'grid-full'
    },
    {
      type: 'textArea',
      name: 'observation',
      placeholder: 'Observation',
      class: 'grid-full'
    },
    {
      type: 'switch',
      name: 'lieu',
      class: 'grid-full',
      options: this.PARAM.currentParametre && this.PARAM.currentParametre.zone
        ? this.PARAM.currentParametre.zone.map(l => l.nom) : [],
      placeholder: 'Zone'

    }, {
      type: 'input',
      name: 'lat',
      placeholder: 'Latitude',
      class: 'grid-tier'
    }, {
      type: 'input',
      name: 'lng',
      placeholder: 'Longitude',
      class: 'grid-tier'
    },
    {
      label: 'Valider',
      name: 'submit',
      type: 'button'
    }
  ];

  constructor(
    private DB: DBService,
    private VISU: VisualistationService,
    private LIST: ListService,
    private PARAM: ParametreService,
    private CAS: CasService,
    private MOYEN: MoyenService
  ) { }

  ngOnInit() {
    if (this.VISU.currentURL.intervention && this.VISU.currentURL.intervention !== 'new') {
      const interId  = this.VISU.currentURL.intervention;
      this.DB.getById('intervention', interId).then((existing) => {
        this.form.setValues(existing);
      });
      return;
    }

    const intervention = new InterventionModel();
    intervention.origine = 'PC';

    this.DB.getTableLength('intervention').then(lnt => {
      intervention.numero = lnt + 1;
      this.form.setValues(intervention);
    });

    const current = this.VISU.currentURL.current;
    if (!current) {
      return;
    }

    const table = current.split(':')[0];
    if (table === 'intervention') {
      return;
    }

    this.DB.getById(table, current).then(item => {
      if (item.poste) {
        this.poste = item.poste;
      }
      if (item.lat && item.lng) {
        this.position = { lat: item.lat, lng: item.lng };
      }
      if (table === 'moyen') {
        this.moyen = item;
        this.moyenName = item.nom;
      }
      if (table === 'cas') {
        this.niv = item.niv;
        this.cas = item;
      }
    });
  }

  public submit(value: InterventionModel) {
    if (value._id) {
      this.update(value);
    } else {
      this.create(value);
    }
  }

  private async create(inter: InterventionModel) {
    if (this.cas && this.hasAssociate) {
      inter.cas = [this.cas._id];
    }
    if (this.moyen && this.hasAssociate) {
      inter.moyen = [this.moyen._id];
    }

    const lastId = await this.DB.getTableLength('intervention');
    inter.numero = lastId + 1;
    inter._id = await this.DB.put('intervention', inter);
    this.LIST.addItemToList('intervention', inter);

    if (this.cas && this.hasAssociate) {
      await this.CAS.affectToInter(this.cas, inter);
    }
    if (this.moyen && this.hasAssociate) {
      await this.MOYEN.affectToInter(this.moyen, inter);
    }

    this.VISU.navTo({ intervention: inter._id });
  }

  private update(value: InterventionModel) {
    this.DB.update('intervention', value).then(() => {
      this.LIST.editListItem('intervention', value);
      this.VISU.navTo({ intervention: value._id, action: 'show' });
    });
  }

  private toggleAssociate() {
    this.hasAssociate = !this.hasAssociate;
    if (!this.moyen) {
      return;
    }
    if (this.hasAssociate) {
      this.form.setValue('moyen', [this.moyen._id]);
      this.form.setValue('lat', this.moyen.lat);
      this.form.setValue('lng', this.moyen.lng);
    } else {
      this.form.setValue('moyen', null);
      this.form.setValue('lat', null);
      this.form.setValue('lng', null);
    }
  }

}

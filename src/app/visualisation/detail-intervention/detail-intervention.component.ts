import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Validators } from '@angular/forms';

import { ListService } from './../../services/list.service';
import { InterventionService } from './../../services/intervention.service';
import { MoyenModel } from './../models/moyen.model';
import { InterventionModel } from './../models/intervention.model';
import { VisualistationService } from '../visualisation.service';
import { CasModel } from '../models/cas.model';
import { CasService } from '../../services/cas.service';
import { MoyenService } from '../../services/moyen.service';
import { FieldConfig } from '../../forms/field-config.interface';
import { DocumentService } from './../../services/document.service';

@Component({
    selector: 'detail-intervention',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `

    <div [ngClass]="{'started':intervention?.started && !intervention?.clos,'closed':intervention?.clos}">
        <header>
            <div class="numero">#{{intervention?.numero}}</div>
            <div>
                <h1>
                <span>{{intervention?.motif}}</span><span>{{intervention?.lieu}}</span>
                </h1>
                <p>
                <span class="inter-date">{{intervention?.timeCode | date:'dd/MM/yy'}}</span> <span class="inter-heure">{{intervention?.timeCode | date:'HH:mm'}}</span> créée par : <span class="inter-heure">{{intervention?.auteur}}</span>
                </p>
            </div>
        </header>

            <section class="detail-inter-info">
                <div>
                    <span *ngIf="intervention?.requerant"><i class="fal fa-user-md"></i>{{intervention?.requerant}}</span>
                    <span *ngIf="intervention?.telephone"><i class="fal fa-phone"></i>{{intervention?.telephone}}</span>
                    <span *ngIf="intervention?.adresse"><i class="fal fa-map-marker-alt"></i>{{intervention?.adresse}}</span>
                    <span *ngIf="intervention?.lieu"><i class="fal fa-map-marker-alt"></i>{{intervention?.lieu}}</span>

                </div>
                <div *ngIf="intervention?.observation">{{intervention?.observation}}</div>

            </section>

            <mat-tab-group #tabGroup>
            <mat-tab label="detail">

            <section class="detail-inter-moyen-list"  *ngIf="moyenList?.length">
                <div *ngFor="let moyen of moyenList">
                    <liste-intervention-moyen
                      [moyen]="moyen"
                      [updating]="moyen.updating"
                      [interID]="intervention?._id"
                      [interStarted] = "intervention.started">
                    </liste-intervention-moyen>
                    <p class="cancel-bar"><span class="cancel" (click)="cancelMoyen(moyen)" *ngIf="!closed">Annuler moyen</span></p>
                </div>
            </section>

            <section class="detail-inter-cas-list" *ngIf="casList?.length">
                <div  *ngFor="let cas of casList">
                    <cas [cas]="cas" ></cas>
                    <p class="cancel-bar"><span class="cancel" (click)="cancelCas(cas)" *ngIf="!closed">Annuler cas</span></p>
                </div>
            </section>

            <div class="action" >
                <button mat-raised-button  *ngIf="displayMoyenBtn == 'select' && !closed" color="success" (click)="selectMoyen()"><i class="fas fa-plus"></i> moyen</button>
                <button mat-raised-button *ngIf="displayCasBtn== 'select' && !closed"   color="success"  (click)="selectCas()"><i class="fas fa-plus"></i> cas</button>
                <button mat-raised-button [disabled]="moyenList?.length === 0" color="primary" (click)="startInter()" *ngIf="!started && !closed "><i class="fas fa-check"></i> Acquitter</button>
                <button mat-raised-button color="accent" (click)="closeInter()"  *ngIf="started && !closed"><i class="fas fa-check"></i> Clore</button>
            </div>

            <p class="cancel-bar"><span class="cancel" (click)="cancelInter()"  *ngIf="!closed">Annuler l'intervention</span></p>

            <p *ngIf="intervention?.clos">Intervention terminée ({{intervention?.clos.auteur}} - {{intervention?.clos.timeCode | date:'dd/MM/yy - HH:mm'}})</p>

              <button mat-raised-button color="primary" (click)="edit()">
                Modifier
              </button>

            </mat-tab>

            <mat-tab label="Discussion">
            <div id="displayDiscussion">

            <dynamic-form
             [config]="config"
             #form="dynamicForm"
             (submit)="submitComment($event)">
             </dynamic-form>

             <article  *ngFor="let comment of discussion">
             <p> {{comment.message}}</p>
                 <footer>{{comment.auteur}}<span>- {{comment.timeCode | date:'dd/MM/yy'}} {{comment.timeCode | date:'hh:mm'}}</span></footer>
             </article>
         </div>
            </mat-tab>
            <mat-tab label="Ordre de départ">
                <div>
                    <button class="mat-raised-button" color="success" (click)="printInformations(intervention, formatedMoyenList)">
                    IMPRIMER L'ORDRE DE DÉPART
                    </button>
                </div>
                <br>
                <div>
                    <button
                        (click)="notifyMoyens()"
                        [disabled]="!allowSendingNotifs()"
                        color="success"
                        mat-raised-button>
                        <i class="fas fa-bell"></i>
                        Envoyer une notification
                    </button>
                    <div *ngIf="intervention?.notificationsSentAt" style="margin: 10px 0;">
                        <i class="fa fa-info-circle"></i>
                        Notification envoyée le {{ intervention.notificationsSentAt | date : "dd/MM/yy" }}
                        à {{ intervention.notificationsSentAt | date : "HH'h'mm" }}.
                    </div>
                    <div *ngFor="let moyen of moyenList">
                        <div *ngIf="!moyen.telephone" style="color: #a00; margin: 10px 0;">
                            <i class="fa fa-exclamation-triangle"></i>
                            Aucun numéro de téléphone défini pour «&nbsp;{{moyen.nom}}&nbsp;».
                        </div>
                    </div>
                </div>
            </mat-tab>
            </mat-tab-group>

            <div class="loadScreen" [ngClass]="{'show':(INTER.currentUpdate | async)}">
                <div>
                    <i class="fas fa-spinner-third fa-spin"></i>
                </div>
            </div>
        </div>


    `
})
export class DetailInterventionComponent implements OnInit, OnDestroy {
    private subscription: any[] = [];
    public started: boolean;
    public closed: boolean;
    public intervention: InterventionModel;
    public casList: CasModel[];
    public moyenList: MoyenModel[];
    public displayMoyenBtn: any = 'select';
    public displayCasBtn: any = 'select';
    public discussion: any[];
    public sendingNotifs = false;

    public config: FieldConfig[] = [
        {
            type: 'textArea',
            name: 'message',
            placeholder: 'Votre message...',
            validation: [Validators.required],
            class: 'grid-full',
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button',
        }
    ];

    public formatedMoyenList: any[];// TODO !!!!

    printInformations(intervention, formatedMoyenList) {
        this.docs.pdfIntervention(intervention, formatedMoyenList);

    }

    constructor(
        private VISU: VisualistationService,
        public CAS: CasService,
        public INTER: InterventionService,
        public MOYEN: MoyenService,
        private docs: DocumentService,
        public LIST: ListService
    ) { }

    ngOnInit() {
        this.subscription[0] = this.INTER.current.subscribe(inter => {
            this.INTER.currentInter.updating = false;
            this.stopUpdate();
            this.intervention = inter;

            this.moyenList = this.LIST.moyensListById(this.intervention.moyen);
            this.casList = this.LIST.casListById(this.intervention.cas);

            this.started = this.intervention.started ? true : false;
            this.closed = this.intervention.clos ? true : false;
            this.discussion = this.intervention.discussion;
        });

        this.subscription[1] = this.VISU.display.subscribe(display => {
            if (display.list === 'moyen' && display.listMod === 'select') {
                this.displayMoyenBtn = 'valid';
            }
            if (display.list === 'cas' && display.listMod === 'select') {
                this.displayCasBtn = 'valid';
            }

            if (display.listMod === 'display') {
                this.displayMoyenBtn = 'select';
                this.displayCasBtn = 'select';
            }
        });

    }

    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    public selectMoyen() {
        this.INTER.currentInter.updating = 'moyen';
        this.startUpdate('moyen');
        this.VISU.currentDisplay = { list: 'moyen', listMod: 'select' };
        this.VISU.selectListMode('moyen', { disponible: { $exists: true }, currentIntervention: { $exists: false } });
    }

    public selectCas() {
        this.INTER.currentInter.updating = 'cas';
        this.startUpdate('cas');
        this.VISU.currentDisplay = { list: 'cas', listMod: 'select' };
        this.VISU.selectListMode('cas', { clos: { $exists: false }, currentIntervention: { $exists: false } });
    }

    public allowSendingNotifs() {
      if (this.sendingNotifs || !this.moyenList) {
        return false;
      }
      const moyensWithPhones = this.moyenList.filter((moyen) => moyen.telephone);
      return this.moyenList && moyensWithPhones.length;
    }

    public notifyMoyens() {
      if (!this.allowSendingNotifs()) {
        return;
      }
      this.sendingNotifs = true;
      const moyens = this.moyenList.filter((moyen) => moyen.telephone);
      this.INTER.notifiyMoyens(this.intervention, moyens, this.casList).then(() => {
        this.sendingNotifs = false;
      });
    }

    public edit() {
      this.VISU.navTo({
        intervention: this.intervention._id,
        sousRubrique: 'intervention',
        action: 'edit',
      });
    }

    public cancelMoyen(moyen: MoyenModel) {
        this.INTER.cancel('moyen', moyen._id);
        this.MOYEN.resetStates(moyen);
    }
    public cancelCas(cas: CasModel) {
        this.INTER.cancel('cas', cas._id);
        this.CAS.cancelIntervention(cas);
    }

    private startUpdate(param?: string) {
        this.INTER.setCurrentUpdate(param || true);
    }

    private stopUpdate() {
        this.INTER.setCurrentUpdate(false);
    }

    public closeInter() {
        this.startUpdate('disponible');
        let plist = [];
        this.LIST
            .moyensListById(this.intervention.moyen)
            .forEach((m: MoyenModel) => plist.push(this.MOYEN.closeInter(m)))

        this.LIST
            .casListById(this.intervention.cas)
            .forEach((c: CasModel) => plist.push(this.CAS.closeInter(c, this.intervention._id)));

        plist.push(this.INTER.close());

        Promise.all(plist).then(v => this.stopUpdate());
    }

    public startInter() {
        this.startUpdate('alerte');
        this.INTER.currentInter.updating = 'alerte';
        let plist = [];
        this.LIST
            .moyensListById(this.intervention.moyen)
            .forEach((m: MoyenModel) => plist.push(this.MOYEN.startInter(m)));

        this.LIST
            .casListById(this.intervention.cas)
            .forEach((c: CasModel) => plist.push(this.CAS.startInter(c)))

        plist.push(this.INTER.start());

        Promise.all(plist).then(v => this.stopUpdate());

    }

    public cancelInter() {
        let plist = [];
        this.LIST
            .moyensListById(this.intervention.moyen)
            .forEach((m: MoyenModel) => plist.push(this.MOYEN.resetStates(m)))

        this.LIST
            .casListById(this.intervention.cas)
            .forEach((c: CasModel) => plist.push(this.CAS.cancelIntervention(c)))

        plist.push(this.INTER.cancelInter())
        Promise.all(plist).then(v => this.stopUpdate());
    }

    submitComment(value: any) {
        this.INTER.addComment(value);
    }
}

import { ListService } from './../../services/list.service';



import { Injectable, OnInit } from '@angular/core';

import { MoyenService } from './../../services/moyen.service';
import { CasService } from './../../services/cas.service';
import { InterventionService } from './../../services/intervention.service';

import { InterventionModel } from '../models/intervention.model';
import { CasModel } from '../models/cas.model';
import { MoyenModel } from './../models/moyen.model';
import { DBService } from '../../services/db.service';
import { Subject } from 'rxjs';

@Injectable()
export class DetailInterventionService implements OnInit {

    private logStyle = "color: #16a085;";
    private _interList: InterventionListUnitModel[];
    public interListObservables = {
        cas: new Subject<CasModel[]>(),
        intervention: new Subject<InterventionModel[]>(),
        moyen: new Subject<MoyenModel[]>()
    }
    constructor(
        private CAS: CasService,
        public INTER: InterventionService,
        public MOYEN: MoyenService,
        private DB: DBService,
        private LIST: ListService
    ) {



    }
    ngOnInit() {



    }
    public init() {
        console.log('%c----------INTER LIST DETAILS', this.logStyle);
        // set inter list
        this._interList = this.LIST.interventionList.map((inter) => {
            let moyenDetailList = this.getMoyenDetailList(inter);
            let casDetailList = this.getCasDetailList(inter);
            return {
                interventionDetail: inter,
                moyenDetailList: moyenDetailList,
                casDetailList: casDetailList
            }


        })
        console.log('this._interList ===> ', this._interList);
        // maintain inter list

        this.LIST.casListObservable.subscribe(list => { console.log('casListObservable => ', list) });
        this.LIST.moyenListObservable.subscribe(list => { console.log('moyenListObservable => ', list) });
        this.LIST.interventionListObservable.subscribe(list => { console.log('interventionListObservable => ', list) });


    }
    ngOnDestroy() {

        console.log('%c---------DESTROY', this.logStyle);
    }
    private async getMoyenDetailList(inter) {
        if (inter.moyen.length > 0) this.DB.getByIdArray('moyen', inter.moyen).then((list: MoyenModel[]) => { return list })
        else return [];
    }
    private async getCasDetailList(inter) {
        if (inter.cas.length > 0) this.DB.getByIdArray('cas', inter.cas).then((list: CasModel[]) => { return list })
        else return [];
    }
    public get interList() {
        return this._interList;
    }

};


export class InterventionListUnitModel {
    interventionDetail: InterventionModel;
    moyenDetailList: Promise<MoyenModel[]>;

    casDetailList: Promise<CasModel[]>;
}
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MoyenService } from 'src/app/services/moyen.service';
import { DBService } from 'src/app/services/db.service';
import { ListService } from 'src/app/services/list.service';
import { InterventionModel } from '../models/intervention.model';
import { MoyenModel } from '../models/moyen.model';
import { VisualistationService } from '../visualisation.service';

@Component({
  selector: 'detail-moyen',
  templateUrl: './detail-moyen.component.html',
})
export class DetailMoyenComponent implements OnInit, OnDestroy {

  public moyen: MoyenModel;
  public states = this.MOYEN.states;
  public currentState: any;
  public loading = false;
  private subscription: any;

  constructor(
    private VISU: VisualistationService,
    private MOYEN: MoyenService,
    private DB: DBService,
    private LIST: ListService,
  ) { }

  ngOnInit() {
    this.moyen = this.MOYEN.currentMoyen;
    this.subscription = this.MOYEN.current.subscribe(moyen => this.moyen = moyen);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public getNextState(state) {
    return this.MOYEN.getNextState(state);
  }

  public nextState() {
    this.MOYEN.nextState(this.moyen);
  }

  public selectInter() {
    this.VISU.navTo({ intervention: this.moyen.currentIntervention });
  }

  public async createInter() {
    this.loading = true;

    const inter = new InterventionModel();
    const lastId = await this.DB.getTableLength('intervention');

    inter.numero = lastId + 1;
    inter.motif = 'Autodéclenchement';
    inter.requerant = 'PC';
    inter.observation = 'Suite à demande autodéclenchement';
    inter.moyen = [this.moyen._id];
    inter.lng   = [this.moyen.lat];
    inter.lat   = [this.moyen.lng];
    inter.timeCode = new Date();
    inter.started = true;

    inter._id = await this.DB.put('intervention', inter);
    this.LIST.addItemToList('intervention', inter);

    this.moyen.currentIntervention = inter._id;
    await this.DB.update('moyen', this.moyen);
    this.LIST.editListItem('moyen', this.moyen);

    this.VISU.navTo({
      intervention: inter._id,
      rubrique: 'info'
    });
  }

  public reset() {
    this.MOYEN.resetStates(this.moyen);
  }

  public edit() {
    this.VISU.navTo({ moyen: this.moyen._id, rubrique: 'edit' });
  }

}

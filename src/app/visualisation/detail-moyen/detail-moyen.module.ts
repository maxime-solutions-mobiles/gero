import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule, MatButtonModule } from '@angular/material';

import { MoyenService } from 'src/app/services/moyen.service';
import { DynamicFormModule } from 'src/app/forms/dynamic-form.module';
import { ListeModule } from '../liste/liste.module';
import { DetailMoyenComponent } from './detail-moyen.component';
import { DetailMoyenFormComponent } from './detail-moyen-form.component';
import { FriseMoyenComponent } from './frise-moyen.component';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    MatButtonModule,
    ListeModule,
    DynamicFormModule,
  ],
  exports: [
    DetailMoyenComponent,
    DetailMoyenFormComponent,
    FriseMoyenComponent,
  ],
  declarations: [
    DetailMoyenComponent,
    DetailMoyenFormComponent,
    FriseMoyenComponent,
  ],
  providers: [MoyenService],
})
export class DetailMoyenModule { }

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { DBService } from 'src/app/services/db.service';
import { MoyenService } from 'src/app/services/moyen.service';
import { ListService } from 'src/app/services/list.service';
import { FieldConfig } from 'src/app/forms/field-config.interface';
import { DynamicFormComponent } from 'src/app/forms/dynamic-form.component';
import { VisualistationService } from '../visualisation.service';
import { MoyenModel } from '../models/moyen.model';

@Component({
  selector: 'app-detail-moyen-form',
  template: `
    <article>
      <dynamic-form
        #form
        (submit)="submit($event)"
        [config]="formConfig">
      </dynamic-form>
    </article>
  `,
})
export class DetailMoyenFormComponent implements OnInit, OnDestroy {

  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;

  public formConfig: FieldConfig[] = [
    {
      type: 'input',
      name: 'nom',
      placeholder: 'Nom',
      class: 'grid-full',
      validation: [Validators.required]
    },
    {
      type: 'input',
      name: 'lat',
      placeholder: 'Latitude'
    },
    {
      type: 'input',
      name: 'lng',
      placeholder: 'Longitude'
    },
    {
      type: 'input',
      name: 'capacite',
      placeholder: 'Capacité'
    },
    {
      type: 'select',
      name: 'type',
      placeholder: 'Type',
      options: [
        'VL',
        'Ambulance',
        'Golfette',
        'Hélico',
        'Vélo',
        'Moto',
        'Quad',
        'Pédestre',
        'Equestre',
      ],
    },
    {
      type: 'select',
      name: 'famille',
      placeholder: 'Famille',
      options: [
        'CRF',
        'FNPC',
        'OM',
        'FFSS',
        'ANPS',
        'CIR',
        'MS2C',
        'MIP',
        'Médical',
        'SDIS',
        'SAMU',
        'Police',
        'Gendarmerie',
      ],
    },
    {
      type: 'switch',
      name: 'draggable',
      placeholder: 'Bougeable avec la souris',
      options: ['true', 'false']
    },
    {
      type: 'switch',
      name: 'geoloc',
      placeholder: 'Geolocalisation',
      options: ['true', 'false']
    },
    {
      type: 'switch',
      name: 'visible',
      placeholder: 'Visible sur la carte',
      options: ['true', 'false']
    },
    {
      type: 'input',
      name: 'balise',
      placeholder: 'Balise'
    },
    {
      type: 'input',
      name: 'chef',
      placeholder: 'Chef de bord'
    },
    {
      type: 'input',
      name: 'telephone',
      placeholder: 'Téléphone du chef de bord'
    },
    {
      label: 'Valider',
      name: 'submit',
      type: 'button'
    },
  ];

  private subscription: any;

  constructor(
    private DB: DBService,
    private MOYEN: MoyenService,
    private VISU: VisualistationService,
    private LIST: ListService,
  ) { }

  ngOnInit() {
    this.form.setValues(this.MOYEN.currentMoyen);
    this.subscription = this.MOYEN.current.subscribe((moyen) => {
      this.form.setValues(moyen);
    });
  }

  ngOnDestroy() {
      if (this.subscription) {
          this.subscription.unsubscribe();
      }
  }

  submit(moyen) {
    this.DB.put('moyen', moyen).then((id) => {
      moyen._id = id;
      this.LIST.editListItem('moyen', moyen);
      this.VISU.navTo({ moyen: id });
    });
  }

}

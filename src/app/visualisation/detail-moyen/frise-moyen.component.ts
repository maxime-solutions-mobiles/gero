import { ListService } from './../../services/list.service';
import { MoyenService } from './../../services/moyen.service';
import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { MoyenModel } from '../models/moyen.model';

@Component({
    selector: 'frise-moyen',
    template: `
    
        <div *ngFor="let state of statesDetail; let i = index" class="{{archiveMode?'archive':state?.color}}" [ngClass]="{'hide':state.etat == 'disponible'}">
            <span >
                <span>{{state.label}}</span>
                <span *ngIf="state.timeCode != '-'; else notDate">{{state.timeCode | date:'d/MM - HH:mm'}}</span>
                <ng-template #notDate><span>{{state.timeCode}}</span></ng-template>
            </span>
            
        </div>
        <div  *ngFor="let state of nextStates">
                <span class="btn {{state?.color}}"  (click)="goToState(state)">{{state?.label}}</span>
        </div>
        <!--  <div *ngIf="showBtn && !archiveMode &&  moyen.currentIntervention && !moyen.standBy" class="btn_section">
         <span>
           <span class="btn {{nextState?.color}}"  (click)="goNextState()">
                {{nextState?.label}}
            </span> 
             </span> 
        </div>-->
            
        
        
       
    `
})

export class FriseMoyenComponent implements OnInit, OnChanges {
    public archiveMode: boolean;
    public showBtn: boolean;
    public showDispo: boolean;
    @Input() states: any[] = [];
    @Input() moyen: MoyenModel;
    public currentState: any;
    public nextState: any;
    public nextStates: any[] = [];
    public stateRef: any;
    public statesDetail: any[] = [];
    constructor(private MOYEN: MoyenService, private LIST: ListService) {



    }
    ngOnChanges(changes: SimpleChanges) { if (this.states && this.states.length > 0) this.init() }
    ngOnInit() { if (this.states && this.states.length > 0) this.init(); }
    private init() {
        this.stateRef = this.MOYEN.states;
        let statesKeys = this.MOYEN.stateRef;

        this.statesDetail = this.states.map(state => { return { ...state, ...this.stateRef[state.etat] } });


        let lnt = this.states.length;
        this.showDispo = lnt == 1;
        this.showBtn = lnt <= statesKeys.length;

        let currentState = this.states[lnt - 1];
        this.currentState = { ...this.stateRef[currentState.etat], ...currentState }
        let nextState = statesKeys[lnt] || statesKeys[0];
        this.nextState = this.stateRef[nextState];

        if (this.states != this.moyen.currentStates) this.archiveMode = true;

        if (this.archiveMode) return this.nextStates = [];

        let currentStateIndex = statesKeys.findIndex(s => s == currentState.etat);
        let stateNames = statesKeys.filter((l, i) => i > currentStateIndex)
        if (currentStateIndex == statesKeys.length - 1) stateNames.push(statesKeys[0]);
        this.nextStates = stateNames.map(s => { return { etat: s, ...this.stateRef[s] } })

    }
    private getNextState(state) { return this.MOYEN.getNextState(state) }

    private goNextState() { this.MOYEN.nextState(this.moyen); }
    private goToState(state) {
        this.MOYEN.goToState(this.moyen, state.etat)
    }
}
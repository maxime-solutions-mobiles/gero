import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CasService } from '../../../services/cas.service';
import { VisualistationService } from '../../visualisation.service';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'detail-body',
    template: `
    `
})
export class DetailBodyComponent implements OnInit, OnDestroy {

    
    constructor(
        public VISU: VisualistationService,
        public CAS: CasService,
        private USER: UserService
    ) { }

    ngOnInit() {
    
    }

    ngOnDestroy() {
    }

}

import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Validators } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material/tabs/typings';
import { Subscription, of, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ParametreService } from './../../../services/parametre.service';
import { DBService } from './../../../services/db.service';
import { UserService } from './../../../services/user.service';
import { DocumentService } from './../../../services/document.service';
import { FieldConfig } from '../../../forms/field-config.interface';
import { VisualistationService } from './../../visualisation.service';
import { CasModel } from './../../models/cas.model';
import { CasService, Cloture } from '../../../services/cas.service';
import { InterventionModel } from './../../models/intervention.model';
import { ListService } from './../../../services/list.service';
import { FileService } from './../../../services/file.service';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { PassageService } from 'src/app/services/passage.service';

@Component({
    selector: 'detail-conclusion',
    templateUrl: './detail-conclusion.component.html',
})
export class DetailConclusionComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(DynamicFormComponent) transfertForm: DynamicFormComponent;

    public user: any;
    public showDeces: boolean;
    public showDecharge: boolean;
    public showConfie: boolean;
    public showAutre: boolean;
    public showTransfert: boolean;
    public showReevaluation: boolean;
    public show = {
        deces: false,
        reevaluation: false,
        decharge: false,
        confie: false,
        autre: false,
        transfert: false,
        regulation: false,
        buttons: true,
        forms: false
    };

    public courierList: any;
    public convocList: any;
    public ordoList: any;
    public notification: any;
    public regulation: any;
    public addReg: boolean;
    public creatingInter: boolean;
    public conclusion: any;
    public cas: CasModel;
    public discussion: any[];
    public diagnostic: any[];
    public document: any[] = [];
    public autre: any;
    public labels = ['regulation', 'discussion', 'document', 'diagnostic'];
    public _sousRubrique: any;
    public activeIndex;
    public showFileForm: boolean;
    private medecinList = [];
    private interneList = [];
    private infirmierList = [];
    public config: FieldConfig[] = [
        {
            type: 'textArea',
            name: 'message',
            placeholder: 'Votre message...',
            validation: [Validators.required],
            class: 'grid-full',
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button',
        },
    ];

    public configDiag: FieldConfig[] = [
        {
            type: 'input',
            name: 'diagnostic',
            placeholder: 'Diagnostic Libre',
            class: 'grid-full',
        },
        {
            type: 'select',
            name: 'categorie',
            placeholder: 'Catégorie médicale',
            options: [
                'Traumatologie',
                'Toxiques',
                'Cardio',
                'Respi',
                'Digestif',
                'dermato - Allergo',
                'Neurologie',
                'Rhumato',
                'ORL',
                'Gynéco Urinaire',
                'Autre',
            ],
            class: 'grid-full',
            validation: [Validators.required],
        },
        {
            type: 'select',
            name: 'ccmu',
            placeholder: 'CCMU',
            options: ['CCMU-P', 'CCMU-1', 'CCMU-2', 'CCMU-3', 'CCMU-4', 'CCMU-5', 'CCMU-D'],
            class: 'grid-full'
        },
        {
            type: 'select',
            name: 'medecin',
            placeholder: 'Médecin',
            options: this.PARAM.currentParametre.medecin ? this.PARAM.currentParametre.medecin.map(l => l.nom) : [],
            class: 'grid-full'
        },
        {
            type: 'select',
            name: 'interne',
            placeholder: 'Interne',
            options: this.PARAM.currentParametre.interne ? this.PARAM.currentParametre.interne.map(l => l.nom) : [],
            class: 'grid-full'
        },
        {
            type: 'select',
            name: 'infirmier',
            placeholder: 'Infirmier',
            options: this.PARAM.currentParametre.infirmier ? this.PARAM.currentParametre.infirmier.map(l => l.nom) : [],
            class: 'grid-full'
        },
        {
            type: 'textArea',
            name: 'commentaires',
            placeholder: 'Commentaires',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public configFile: FieldConfig[] = [
        {
            type: 'input',
            name: 'titre',
            placeholder: 'Titre du document',
            class: 'grid-full',
        },
        {
            type: 'select',
            name: 'type',
            placeholder: 'Type de document',
            options: ['Photo', 'ECG', 'Bilan biologique', 'Ordonnance', 'Compte-rendu', 'Décharge', 'Autre'],
            class: 'grid-demi',
        },
        {
            type: 'file',
            name: 'doc',
            placeholder: 'Selectionnez le document',
            class: 'grid-demi',
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button',
        },
    ];

    public configDeces: FieldConfig[] = [
        {
            type: 'time',
            name: 'heure',
            placeholder: 'Heure du décès',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }
    ];

    public configReevaluation: FieldConfig[] = [
        {
            type: 'select',
            name: 'delais',
            placeholder: 'A réévaluer dans',
            options: ['15 minutes', '30 minutes'],
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }
    ];

    public configDecharge: FieldConfig[] = [
        {
            type: 'switch',
            name: 'decharge',
            placeholder: 'Document signé',
            options: ['OUI', 'NON'],
            class: 'grid-full'
        },
        {
            type: 'input',
            name: 'referent',
            placeholder: 'Responsable des secours'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }
    ];

    public configConfieForm: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom'
        },
        {
            type: 'input',
            name: 'prenom',
            placeholder: 'Prenom'
        },
        {
            type: 'input',
            name: 'lien',
            placeholder: 'Lien'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }
    ];

    public configAutreForm: FieldConfig[] = [
        {
            type: 'textArea',
            name: 'message',
            placeholder: 'Votre message...',
            validation: [Validators.required],
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }
    ];

    public configTransfertForm: FieldConfig[] = [
        {
            type: 'select',
            name: 'destination',
            placeholder: 'Destination'
        },
        {
            type: 'switch',
            name: 'urgence',
            options: ['faible', 'moyenne', 'élevée'],
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'position',
            options: ['allongé', 'assis', 'debout'],
            class: 'grid-full'
        },
        {
            type: 'textArea',
            name: 'commentaire',
            placeholder: 'Commentaire du régulateur',
            class: "grid-full"
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }
    ];

    public activeNotification = { regulation: false, intervention: false, discussion: false };

    private subscription: Subscription[] = [];

    constructor(
        private VISU: VisualistationService,
        private docs: DocumentService,
        private sanitizer: DomSanitizer,
        private USER: UserService,
        public  CAS: CasService,
        private DB: DBService,
        private LIST: ListService,
        private PARAM: ParametreService,
        private fileService: FileService,
        private PASSAGE: PassageService,
    ) { }

    ngOnInit() {
        this.subscription.push(this.USER.profile.subscribe((profile) => this.user = profile));
        this.subscription.push(this.CAS.casObservable.subscribe((cas) => this.cas = cas));

        this.subscription.push(this.CAS.conclusionObservable.subscribe((conclusion) => {
            this.init(conclusion);
            this.defineActiveNotifs(this.cas.notification);
        }));

        this.init(this.CAS.currentObjects.conclusion);
        this.cas = this.CAS.currentObjects.cas;
        this.sousRubrique = this.VISU.currentURL.sousRubrique;
        this.autre = this.CAS.currentObjects.autre;
        this.getDocs(this.CAS.currentObjects.document);

        // this.closeNotification();

        this.initMedecinList(this.PARAM.currentParametre);
        this.initInterneList(this.PARAM.currentParametre);
        this.initInfirmierList(this.PARAM.currentParametre);
        this.subscription.push(this.PARAM.current.subscribe(r => { if (r.medecin) this.initMedecinList(r) }));
        this.subscription.push(this.PARAM.current.subscribe(r => { if (r.interne) this.initInterneList(r) }));
        this.subscription.push(this.PARAM.current.subscribe(r => { if (r.infirmier) this.initInfirmierList(r) }));
    }

    ngAfterViewChecked() {
            if (this.medecinList != this.PARAM.currentParametre.medecin) this.initMedecinList(this.PARAM.currentParametre);
            if (this.interneList != this.PARAM.currentParametre.interne) this.initMedecinList(this.PARAM.currentParametre);
            if (this.infirmierList != this.PARAM.currentParametre.infirmier) this.initMedecinList(this.PARAM.currentParametre);
    }
    private initMedecinList(r) {
        if (!r || !r.medecin) return;
        //if (!r || !r.medecin || !this.configDiag.configLine('medecin')) return;
        this.medecinList = r.medecin;
        //this.configDiag.configLine('medecin').options = r.medecin.map(l => l.nom);
    }
    private initInterneList(r) {
        if (!r || !r.interne) return;
        this.interneList = r.interne;
        //this.configDiag.configLine('interne').options = r.interne.map(l => l.nom);
    }
    private initInfirmierList(r) {
        if (!r || !r.infirmier) return;
        this.infirmierList = r.infirmier;
        //this.configDiag.configLine('infirmier').options = r.infirmier.map(l => l.nom);
    }

    private configLine(name) {
        let index = this.config.findIndex(l => l.name == name);
        return this.config[index]
    }



    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    ngAfterViewInit() {
        this.initTransfertPosteList();
    }

    private init(conclusion) {
        this.conclusion  = conclusion;
        this.discussion  = conclusion.discussion || [];
        this.regulation  = conclusion.regulation || [];
        this.notification  = conclusion.notification || [];
        this.diagnostic  = conclusion.diagnostic || [];
        this.ordoList    = conclusion.ordonnance;
        this.courierList = conclusion.courier;
        this.convocList  = conclusion.convoc;

        const documentID = conclusion.document || [];

        if (documentID.length > 0) {
            this.showFileForm = !(documentID.length > 0);
            this.getDocs(documentID);
        }
    }

    private initTransfertPosteList() {
        this.configTransfertForm = [
            {
                type: 'select',
                name: 'destination',
                options: this.PARAM.currentParametre.poste.map(l => l.nom),
                placeholder: 'Destination'
            },
            {
                type: 'switch',
                name: 'urgence',
                options: ['faible', 'moyenne', 'élevée'],
                class: 'grid-full'
            },
            {
                type: 'switch',
                name: 'position',
                options: ['allongé', 'assis', 'debout'],
                class: 'grid-full'
            },
            {
                type: 'textArea',
                name: 'commentaire',
                placeholder: 'Commentaire du régulateur',
                class: "grid-full"
            },
            {
                label: 'Valider',
                name: 'submit',
                type: 'button'
            }
        ];
    }

    private getDocs(documentIds: string[]) {
        if (!documentIds || documentIds.length < 1) {
            return;
        }

        const sub = of(...documentIds)
            .pipe(mergeMap((docId) => from(this.DB.getById('document', docId))))
            .subscribe((doc) => {
                if (!this.document.find((d) => d._id === doc._id)) {
                    this.document.push(doc);
                }
            });
        this.subscription.push(sub);
    }

    private defineActiveNotifs(notifs: string[]) {
        if (!notifs || !Array.isArray(notifs) || notifs.length < 1) {
            return;
        }
        const activeNotifNames = notifs.map(m => m.split(':')[1]);
        Object.keys(this.activeNotification).forEach(k => this.activeNotification[k] = activeNotifNames.includes(k));
    }

    public getOrdoName(prescription) {
        return prescription.map(p => p.medicament).join(' / ');
    }

    public set sousRubrique(val) {
        this._sousRubrique = val;
        this.activeIndex = this.labels.indexOf(val);
    }

    public selectSousRubrique(event: MatTabChangeEvent) {
        let sousRubrique = event.tab.textLabel.toLowerCase();
        this.VISU.voletShow({ sousRubrique: sousRubrique });
    }
    submitComment(value: any) {
        this.CAS.addNewItem(['conclusion', 'discussion'], value);
        this.CAS.addNotif('discussion');
    }
    submitDiag(value: any) {
        this.CAS.addNewItem(['conclusion', 'diagnostic'], value);
    }
    submitRegulation(value) {
        this.CAS.addNewItem(['conclusion', 'regulation'], value);
        this.CAS.clearNotif(['regulation']);
        this.showCl(['buttons']);
    }

    downloadDetailPdf() {
        this.docs.pdfDetail(this.CAS.currentObjects);
    }

    downloadAnonymousPdf() {
        this.docs.pdfAnonymes();
    }

    downloadConseilTCPdf() {
        this.docs.pdfConseilTC(this.CAS.currentObjects);
    }

    downloadConseilTCENPdf() {
        this.docs.pdfConseilTCEN(this.CAS.currentObjects);
    }

    downloadConseilPlaiesPdf() {
        this.docs.pdfConseilPlaies(this.CAS.currentObjects);
    }

    downloadFicheMedicalePdf() {
        this.docs.pdfFicheMed(this.CAS.currentObjects);
    }

    downloadOrdonnance(ordo) {
        this.docs.pdfOrdonnance(this.CAS.currentObjects, ordo);
    }

    downloadCourier(courier) {
        this.docs.pdfCourier(this.CAS.currentObjects, courier);
    }

    downloadConvoc(convoc) {
        this.docs.pdfConvoc(this.CAS.currentObjects, convoc);
    }

    downloadDecharge() {
        this.docs.pdfDecharge(this.CAS.currentObjects);
    }

    downloadDoc(doc) {
        this.fileService.download(doc.fileId, doc.key, doc.fileName);
    }

    submitFileForm(file) {
        this.showFileForm = false;
        this.subscription.push(this.CAS.addFile(file).subscribe((docId) => {
            this.getDocs([docId]);
        }));
    }

    public async createInter(item, motif) {
        this.creatingInter = true;
        const inter = new InterventionModel();
        const lastId = await this.DB.getTableLength('intervention');

        inter.numero = lastId + 1;
        inter.motif = motif;
        inter.lieu = this.cas.poste;
        inter.requerant = 'PC';
        inter.cas = [this.cas._id];
        inter.urgence = item.urgence;
        inter.position = item.position;

        this.DB.put('intervention', inter).then(id => {
            inter._id = id;
            this.LIST.addItemToList('intervention', inter);
            this.VISU.navTo({ intervention: id });
            this.creatingInter = true;
        });
    }

    public imgUrl(file) {
        if (!file) {
            return;
        }
        const binaryData = [];
        binaryData.push(file.data);
        const url = window.URL.createObjectURL(binaryData);
        const imgUrl = this.sanitizer.bypassSecurityTrustUrl(url);
        return imgUrl;
    }

    public newOrdo() {
        this.VISU.navTo({ sousRubrique: 'ordonnance', action: 'edit' });
    }

    public newCourier() {
        this.VISU.navTo({ sousRubrique: 'courier', action: 'edit' });
    }

    public newConvoc() {
        this.VISU.navTo({ sousRubrique: 'convoc', action: 'edit' });
    }

    public cloreCas(type: Cloture = null, info: any = null) {
        this.CAS.cloreCurrentCas(type, info);
        this.showCl(['buttons']);
    }

    public async conclusionCas(type: Cloture = null, info: any = null) {
        const merged = { type, ...info };
        this.CAS.addNewItem2(['conclusion', 'regulation'], merged);
        this.showCl(['buttons']);
        if (!this.cas.position) {
          this.cas.position = await this.PASSAGE.bugfixFindPassageAndCloseExtraneous(this.cas._id, this.LIST.listsRef.passage);
        }
        await this.CAS.closePassage(this.cas.position, info);
        this.LIST.refreshList('cas');
    }

    public reevalCas(type: Cloture = null, info: any = null) {
        const merged = { type, ...info };
        this.CAS.addNewItem2(['conclusion', 'regulation'], merged);
        this.showCl(['buttons']);
    }

    public transfert(info) {
        console.log('conclusionCas transfert : ', info);
        if (this.cas.position) {
            const merged = { type: 'transfert', ...info };
            this.CAS.addNewItem2(['conclusion', 'regulation'], merged);
            this.showCl(['buttons']);
            //this.CAS.closePassage(this.cas.position, info).then(r => this.LIST.refreshList('cas'));
        } else {
            console.error('POSITION ' + this.cas.position);
        }
    }

    public showCl(name: string[]) {
        Object.keys(this.show).forEach(k => this.show[k] = name.includes(k) ? true : false);
        if (this.show.transfert === true) {
            this.initTransfertPosteList();
        }

        // checking notification
        this.closeNotification();
    }

    public askReg() {
        this.CAS.addNotif('regulation');
        const askreg = { type: 'demande' };
        this.CAS.addNewItem2(['conclusion', 'regulation'], askreg);
    }

    private closeNotification() {
        if (this.cas) {
            this.DB.filter('notification', {
                cas: this.cas._id
            }).then((notifs)=>{
                if(notifs && notifs.list) {
                    console.log('jts update list === ')
                    console.log(notifs.list);
                    for (let notif of notifs.list) {
                        if(notif.demande == "regulation" && !notif.close) {
                            notif.close = this.USER.CONTEXT;
                            this.DB.update('notification', notif);
                        }
                    }
                }
            })
        }

    }
}

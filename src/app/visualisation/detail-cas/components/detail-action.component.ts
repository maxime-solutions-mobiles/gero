import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CasService } from './../../../services/cas.service';
import { VisualistationService } from '../../visualisation.service';
import { UserService } from '../../../services/user.service';
import { DocumentService } from './../../../services/document.service';


@Component({
    selector: 'detail-action',
    template: `
    <div>
        <h2 (click)="add('geste')">Gestes<i class="fal fa-plus"></i></h2>

        <article *ngFor="let geste of geste" [ngSwitch]="geste.typeGeste">

            <p *ngSwitchCase="'attelle'">
                Pose d'une attelle <span class="primary">{{geste.type}}</span>
                sur <span class="primary">{{geste.position}} {{geste.lateralite}}</span>.
            </p>
            <p *ngSwitchCase="'atelle'">
                Pose d'une attelle <span class="primary">{{geste.type}}</span>
                sur <span class="primary">{{geste.position}} {{geste.lateralite}}</span>.
            </p>
            <p *ngSwitchCase="'iot'">
                    Intubation avec un tube de <strong>{{geste.tube}}</strong>, <br>
                    Cormack : {{geste.cormack}}, Intubation difficile :  <span class="primary">{{geste.difficile}}</span> <br>
                    Intubation au moyen de <span class="primary">{{geste.technique}}</span>. <br>
                    EtCO2 à l'intubation : {{geste.etco2}}</p>
            <p *ngSwitchCase="'position'">
                    Mise en position <strong>{{geste.position}} </strong></p>
            <p *ngSwitchCase="'oxygene'">
                    Oxygénothérapie par <strong>{{geste.mode}} </strong>, débit : <strong>{{geste.debit}} </strong> l/m</p>
            <p *ngSwitchCase="'rcp'">
                    Réanimation cardio respiratoire :
                        <li>Ventilation <strong>{{geste.va}} </strong>
                        <li> Compression :  <strong>{{geste.compression}} </strong>
                        <li> usage d'un défibrilatteur : <strong>{{geste.defibrillation}} </strong><br>
                         Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'aspiration'">
                    Aspiration
                        <li> faite  <strong>{{geste.aspiration}} </strong>
                        <li> sécrétion abondantes :  <span class="primary">{{geste.abondant}} </span>
                        <li> aspect des sécrétions <strong>{{geste.aspect}}</strong> </p>
            <p *ngSwitchCase="'vvp'">
                    Pose de VVP <strong>{{geste.catheter}} G</strong>, à :
                    <span class="primary">{{geste.position}} à {{geste.lateralite}}</span><br>
                    Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'vvc'">
                    Pose de VVC : <li>  {{geste.voies}} voies <li>position : <strong>{{geste.position}} </strong>
                        <li> pose écho guidée :  <span class="primary">{{geste.echo}}</span> <br>
                    Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'suture'">
                    Suture de <strong>{{geste.site}} </strong> <br>
                    fil :  <span class="primary">{{geste.fil}} de type {{geste.typefil}}</span>
                    <span class="retour"> Remarques : {{geste.remarques}}</span></p>
                    <br>
                    <button *ngSwitchCase="'suture'" mat-raised-button color="success" (click)="createOrdofils(geste)">
                        <span>Ordonnance de retrait de fils</span>
                    </button>
            <p *ngSwitchCase="'sng'">
                    Pose de SNG <strong>{{geste.taille}} </strong>, <br>
                    position :  <span class="primary">{{geste.position}} en {{geste.usage}}</span> <br>
                    Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'dio'">
                    Pose de dispositif Intra Osseux <strong>{{geste.position}} </strong>,*
                    coté : <strong>{{geste.lateralite}}</strong> <br>
                    Type {{geste.type}} et taille : {{geste.taille}}<br>
                    Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'conditionnement'">
                    Couverture <strong>{{geste.couverture}} </strong> <br>
                    Immobilisation {{geste.immobilisation}} <br>
                    Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'bu'">
                    Bandelette Urinaire
                        <li> leucocytes : {{geste.leucocytes}} </li>
                        <li> nitrites : {{geste.nitrites}} </li>
                        <li> urobilinogene : {{geste.urobilinogene}} </li>
                        <li> protéines : {{geste.proteines}} </li>
                        <li> pH : {{geste.ph}} </li>
                        <li> sang : {{geste.sang}} </li>
                        <li> Densité : {{geste.sg}} </li>
                        <li> cetones : {{geste.cetones}} </li>
                        <li> bilirubine : {{geste.bilirubine}} </li>
                        <li> glucose : {{geste.glucose}} </li>
                    Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'su'">
                    Sondage vésical <strong>{{geste.type}} </strong>, <br>
                    Sonde : <strong>{{geste.sonde}}</strong> de taille  {{geste.taille}}<br>
                    Remarques : {{geste.remarques}}</p>
            <p *ngSwitchCase="'pansement'">
                    Pansement : <strong>{{geste.localisation}} </strong> <br>
                    Latéralite : <strong>{{geste.lateralite}} </strong><br>
                    Type : <strong>{{geste.type}} </strong><br>
                    Remarques : {{geste.commentaire}}</p>


            <p *ngSwitchCase="'dc'">
                    Compression direct <strong>{{geste.compressionmoyen}} </strong>, <br>
                    Site : <strong>{{geste.compressionsite}}</strong> <br>
                    Pose de garrot : {{geste.garrotsite}}, heure de pose : {{geste.garrotheure}} <br>
                    Airways : {{geste.airways}} <br>
                    Breathing : {{geste.breathing}} <br>
                    Choc : {{geste.choc}} <br>
                    Head / Hypothermia : {{geste.head}} <br>
                    Traitements : {{geste.traitements}} <br>
                    Remarques : {{geste.commentaire}}</p>

           <p>
               <span>{{geste.auteur}}</span> le <span >{{geste.timeCode | date:'dd/MM/yy'}}</span>
               à <span >{{geste.timeCode | date:'HH:mm'}}</span>
            </p>
        </article>

        <div *ngIf="geste?.length == 0" class="emptyAtions">
            <p>Aucun geste effectué.</p>
            <button  mat-raised-button color="success" (click)="add('geste')"><i class="fa fa-plus"></i>Ajouter un geste</button>
        </div>

        <h2 (click)="add('traitement')" *ngIf="showMedic">traitements<i class="fal fa-plus"></i></h2>

        <article *ngFor="let traitement of traitement" [ngSwitch]="traitement.typeTraitement">

         <p *ngSwitchCase="'Medicaments'">
             Prise de <strong>{{traitement.medicament}}</strong>
             <span class="primary">{{traitement.posologie}} {{traitement.unite}}</span>
             par voie <span class="primary">{{traitement.voie}}</span>.
         </p>
         <p *ngSwitchCase="'Solutes'">
             Prise de <strong>{{traitement.solute}}</strong>
             <span class="primary">{{traitement.quantite}}</span> par voie
             <span class="primary">{{traitement.vitesse}}</span>.
         </p>
        <p>
            <span>{{traitement.auteur}}</span> le <span >{{traitement.timeCode | date:'dd/MM/yy'}}</span>
            à <span >{{traitement.timeCode | date:'HH:mm'}}</span></p>
     </article>

        <div *ngIf="(!traitement||  traitement?.length == 0) && showMedic" class="emptyAtions">
        <p>Aucun traitement prescrit.</p>
        <button  mat-raised-button color="success" (click)="add('traitement')"><i class="fa fa-plus"></i>Ajouter un traitement</button>
    </div>
    `
})
export class DetailactionComponent implements OnInit, OnDestroy {

    public traitement: any[];
    public geste: any[];
    public showMedic: boolean;
    public currentUser: any;
    private subscription: Subscription;

    constructor(
        public VISU: VisualistationService,
        public CAS: CasService,
        private USER: UserService,
        private docs: DocumentService
   ) { }

    ngOnInit() {

        this.subscription = this.USER.profile.subscribe((profile) => {
            this.showMedic = profile.isMedic;
            this.currentUser = profile;
        });


        const subscription = this.CAS.actionObservable.subscribe(action => {

            this.geste = action.geste || [];
            this.traitement = action.traitement || [];
        });
        this.subscription.add(subscription);
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    createOrdofils(geste) {
        this.docs.pdfOrdofil(this.CAS.currentObjects, geste);
    }

    public add(type) {
        this.VISU.navTo({ sousRubrique: type, action: 'edit' });
    }

}

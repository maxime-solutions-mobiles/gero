import { PassageModel } from './../../models/passage.model';
import { UserService } from './../../../services/user.service';
import { PatientModel } from './../../models/patient.model';
import { FieldConfig } from '../../../forms/field-config.interface';
import { DBService } from '../../../services/db.service';
import { MatTabChangeEvent } from '@angular/material/tabs/typings';
import { CasModel } from './../../models/cas.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { VisualistationService } from '../../visualisation.service';
import { DocumentService } from './../../../services/document.service';
import * as moment from 'moment';
import { CasService } from '../../../services/cas.service';


@Component({
    selector: 'detail-info',
    template: `
    <mat-tab-group  (selectedTabChange)="selectSousRubrique($event)" [selectedIndex]="activeIndex" #tabGroup>

    <mat-tab label="PEC">
    <div class="detail-cas">
   
    <div class="wait" *ngIf="!cas"> <i class="fas fa-spinner-third fa-spin"></i><p>{{feedBack}}</p></div>
    
    <div *ngIf="cas">
        <div>
        <p *ngIf="cas.niv">         <span>NIV</span>:          <span>{{cas.niv}}</span></p>
        <p *ngIf="cas.sinus">       <span>SINUS</span>:        <span>{{cas.sinus}}</span></p>
        <p *ngIf="cas.pathologie">  <span>Motif</span>:        <span>{{cas.pathologie}}</span></p>
        <p *ngIf="cas.note">        <span>Précision du motif de prise en charge</span>: <span>{{cas?.note}}</span></p>
        <p ><span></span></p>
        <p *ngIf="cas.position">     <span>Lieu</span>:<span>{{casLieu || 'Pris en charge'}} depuis {{casDate | date:'HH:mm'}}</span></p>
        <p *ngIf="cas.modes">        <span>Mode d'admission  </span>:        <span>{{cas?.modes}}</span></p>
        <p *ngIf="cas.types">        <span>Type de patient   </span>:        <span>{{cas?.types}}</span></p>
        <p *ngIf="cas.typeprecision"><span>Précision type    </span>:        <span>{{cas?.typeprecision}}</span></p>
        </div>
    </div>

    <div class="action" >
        <button  mat-raised-button color="primary"  (click)="edit('cas')">Editer</button>
        
    </div>
    <div class="action" >
        <button  mat-raised-button color="primary"  (click)="downloadFicheMedicalePdf()">Impression fiche de consultation</button>
    </div>

    <div *ngIf="passages?.length > 0">
        <h2>passages</h2>

        <p *ngFor="let passage of passages">
        <span>{{passage?.poste || passage?.lieu}}</span> : <span>du {{passage?.entree?.timeCode| date:'d/M à HH:mm'}}</span> au <span>{{passage?.sortie?.timeCode| date:'d/M à HH:mm'}}</span>
        
        
        </p>
    
    </div>

    <div *ngIf="otherPatientCas.length>0">
        <h2>Autres PEC</h2>
        <div *ngFor="let cass of otherPatientCas">
            <p>Le {{cass?.timeCode | date:'d/M/yy'}} à {{cass?.timeCode | date:'HH:mm'}} -  {{cass?.niv}} </p>
            <cas [cas]="cass" class="short"></cas>
        </div>
    </div>


    </div>




     
     
    </mat-tab>

    <mat-tab label="Patient">
        <div id="displayAdmin">
        
        <div *ngIf="cas?.patient " class="fiche">
            <h2>Etat civil</h2>
            <div class="wait" *ngIf="!patient"> <i class="fas fa-spinner-third fa-spin"></i><p>{{feedBack}}</p></div>
            <div *ngIf="patient">
                <p>{{patient?.prenom}} <span class="maj">{{patient?.nom}}</span></p>
                <p>{{patient?.sexe}} </p>
                <p>{{patient?.nationalite}} </p>
                
                <p *ngIf="patient?.age">{{getAge(patient?.age)}} ans</p>
                <div>
                <h2 *ngIf="patient?.adresse || patient?.telephone">Coordonnées</h2>
                <p>{{patient?.adresse}}</p>
                <p>{{patient?.cp}} <span class="maj">{{patient?.ville}}</span></p>
                <p>{{patient?.telephone}}</p>
                </div>
            </div>
        </div>

        <div class="action"  *ngIf="cas?.patient">
        <button  mat-raised-button color="primary" (click)="edit('patient')">Editer</button>   
        </div>

        <div *ngIf="!cas?.patient" class="emptyAtions">
            <p>Aucun patient lié à ce cas.</p>
            <button mat-raised-button color="success" (click)="edit('patient')">
                <i class="fa fa-plus"></i> Ajouter un patient
            </button>
           
        </div>




    <div  class="fiche">
    
        <h2>Personnes à prévenir
            
            <i  (click)="toggleEditprevenir()" [ngClass]="{toClose : showEditPrevenir}" class="fal fa-plus"></i>
        </h2>
        <p *ngFor="let prevenir of prevenirs">
        <b>{{prevenir.lien}} </b><br>
         {{prevenir.nom}}, {{prevenir.prenom}} <br>
         <span *ngIf="prevenir.tel">{{prevenir.tel}}</span>  <br>
         <i> {{prevenir.commentaire}}</i>
         <i class="fal fa-trash" (click)="deletePrevenir(prevenir)"></i></p>
        
            <dynamic-form *ngIf="showEditPrevenir"
            [config]="prevenirConfig"
            #prevenirForm="dynamicForm"
            (submit)="submitPrevenir($event)">
            </dynamic-form>
    </div>
   
    

    
    </div>
   
    </mat-tab>

    <mat-tab label="Atcd">

    <div id="displayAtcd">
    <div  class="formRepeat">

        <h2 (click)="togglePassif('antecedent')">Maladies et Hospitalisations 
        <i  [ngClass]="{toClose : showEditPassif.antecedent}" class="fal fa-plus"></i>
           
        </h2>
        <p  *ngFor="let antecedent of antecedent">{{antecedent.antecedent}} <i class="fal fa-trash" (click)="deletePassif('antecedent',antecedent)"></i></p>
        
            <dynamic-form *ngIf="showEditPassif.antecedent"
            [config]="antecedentConfig"
            #antecedentForm="dynamicForm"
            (submit)="submitPassif('antecedent',$event)">
            </dynamic-form>
    </div>
    
    <div  class="formRepeat">
        <h2  (click)="togglePassif('traitement')">Traitements

        <i  [ngClass]="{toClose : showEditPassif.traitement}" class="fal fa-plus"></i>
          
        </h2>

        <p  *ngFor="let traitement of traitement">{{traitement.traitement}} <i class="fal fa-trash" (click)="deletePassif('traitement',traitement)"></i></p>
       
        
            <dynamic-form *ngIf="showEditPassif.traitement"
            [config]="traitementConfig"
            #traitementForm="dynamicForm"
            (submit)="submitPassif('traitement',$event)">
            </dynamic-form>
       
    </div>
    
    
    <div  class="formRepeat">
        <h2 (click)="togglePassif('allergie')">allergies

        <i  [ngClass]="{toClose : showEditPassif.allergie}" class="fal fa-plus"></i>
          
            
        </h2>

        <p  *ngFor="let allergie of allergie">{{allergie.allergie}} <i class="fal fa-trash" (click)="deletePassif('allergie',allergie)"></i></p>
       
       
            <dynamic-form *ngIf="showEditPassif.allergie"
            [config]="allergieConfig"
            #allergieForm="dynamicForm"
            (submit)="submitPassif('allergie',$event)">
            </dynamic-form>
       
    </div>
    </div>
    </mat-tab>


    </mat-tab-group>
    `
})

export class DetailInfoComponent implements OnInit, OnDestroy {
    public feedBack: string = '';
    public passages: any[] = [];
    public casDate: Date;
    public otherPatientCas: CasModel[] = [];
    public casLieu: any;
    public prevenirs: any[];
    public options: string[] = ['u1', 'u2', 'u3'];
    public cas: CasModel;
    public _sousRubrique: any;
    public showEdit: boolean;
    public patient: PatientModel;
    public labels = ['cas', 'patient', 'atcd'];
    public antecedent: any = [];
    public traitement: any = [];
    public allergie: any = [];
    public showEditPassif: any = {
        antecedent: false,
        allergie: false,
        traitement: false
    };
    private subscription: any[] = [];

    public showEditPrevenir: boolean = false;
    public activeIndex;
    public set sousRubrique(val) {
        this._sousRubrique = val
        this.activeIndex = this.labels.indexOf(val);
    };
    public get sousRubrique() { return this._sousRubrique }

    public prevenirConfig: FieldConfig[] = [

        {
            type: 'input',
            name: 'nom',
            inputType: 'text',
            placeholder: "Nom"
        },
        {
            type: 'input',
            name: 'prenom',
            inputType: 'text',
            placeholder: "Prénom"
        },
        {
            type: 'select',
            name: 'lien',
            options: ['père', 'mère', 'tuteur légal', 'représentant légal', 'accompagnateur', 'ami', 'autre'],
            placeholder: "Lien avec le patient"
        },
        {
            type: 'input',
            name: 'tel',
            inputType: 'text',
            placeholder: "Téléphone"
        },
        {
            type: 'textArea',
            name: 'commentaire',
            inputType: 'text',
            placeholder: "Commentaire",
            class: 'grid-full'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];
    public antecedentConfig: FieldConfig[] = [

        {
            type: 'input',
            name: 'antecedent',
            inputType: 'text',
            placeholder: "Antécédent",
            class: 'grid-full'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];
    public traitementConfig: FieldConfig[] = [

        {
            type: 'input',
            name: 'traitement',
            inputType: 'text',
            placeholder: "Traitement",
            class: 'grid-full'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];
    public allergieConfig: FieldConfig[] = [

        {
            type: 'input',
            name: 'allergie',
            inputType: 'text',
            placeholder: "Allergie",
            class: 'grid-full'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];
    constructor(
        private DB: DBService,
        public VISU: VisualistationService,
        public USER: UserService,
        private docs: DocumentService,
        public CAS: CasService
    ) { }

    ngOnInit() {



        this.subscription[0] = this.CAS.casObservable.subscribe(cas => {
            this.cas = cas;
            this.getPostition();
        });

        this.subscription[1] = this.CAS.patientObservable.subscribe(patient => this.init(patient));
        this.subscription[2] = this.VISU.voletDisplay.subscribe(show => this.sousRubrique = show.sousRubrique);

    }

    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    private init(patient) {
        if (!patient) return;
        this.patient = patient;
        this.prevenirs = patient.prevenir || [];
        if (patient.passif) {
            this.antecedent = patient.passif.antecedent || [];
            this.traitement = patient.passif.traitement || [];
            this.allergie = patient.passif.allergie || [];
        }

        if (!patient._id && (!this.sousRubrique || this.sousRubrique == 'patient')) {
            this.sousRubrique = 'patient';
            this.showEdit = true;
        } else this.showEdit = false;

        if (!patient._id && (!this.sousRubrique || this.sousRubrique == 'atcd')) {
            this.sousRubrique = 'atcd';
            this.showEdit = true;
        } else this.showEdit = false;

        if (patient.cas && patient.cas.length > 0) {
            this.DB.getByIdArray('cas', patient.cas).then(casList => this.otherPatientCas = casList);
        }

        if (this.cas.historique && this.cas.historique.length > 0) {
            let passageIDList = this.cas.historique.filter(l => typeof l == "string" && l.includes('passage')).reverse();
            this.DB.getByIdArray('passage', passageIDList).then(passageList => this.passages = passageList);

        }


    }
    public getPostition() {

        if (!this.cas || !this.cas.position) return;

        this.DB.getById('passage', this.cas.position).then((p: PassageModel) => {
            if (!p) return;
            if (p.lieu) this.casLieu = p.lieu;
            if (p.entree && p.entree.timeCode) this.casDate = p.entree.timeCode;

        });

    }
    public getPatientCas(refs = []) {
        // TO DO : use filters
        return [this.cas];
    }
    setOption(value: any) {
        this.cas.urgence = value;
        this.DB.update('cas', this.cas);
    }
    public submitPrevenir(prevenir) {
        if (!this.patient.prevenir) this.patient.prevenir = [];
        this.patient.prevenir.push(prevenir);
        this.CAS.addPatient(this.patient);
        this.showEditPrevenir = false;
    }
    public deletePrevenir(item) {

        this.patient.prevenir = this.patient.prevenir.filter(l => l != item);
        this.CAS.addPatient(this.patient);

    }
    public toggleEditprevenir() {
        this.showEditPrevenir = !this.showEditPrevenir;

    }
    public submitPassif(cat, value) {


        if (!this.patient.passif) this.patient.passif = { antecedent: [], traitement: [], allergie: [] };

        this.patient.passif[cat].push(value);
        this.CAS.addPatient(this.patient);
        this.showEditPassif[cat] = false;
    }

    public togglePassif(cat) { this.showEditPassif[cat] = !this.showEditPassif[cat] }
    public deletePassif(cat, item) {

        this.patient.passif[cat] = this.patient.passif[cat].filter(l => l != item);
        this.CAS.addPatient(this.patient);

    }
    public edit(type) {
        this.VISU.navTo({ sousRubrique: type, action: 'edit' });
    }


    public selectSousRubrique(event: MatTabChangeEvent) {
        let sousRubrique = event.tab.textLabel.toLowerCase();
        this.VISU.voletShow({ sousRubrique: sousRubrique });
    }


    public add(type) {
        this.showEdit = true;
        let sousR = type.toLowerCase();
        this.sousRubrique = sousR;
        this.VISU.navTo({ sousRubrique: sousR, action: 'edit' });
    }

    getAge(tc: Date) {
        if (!tc) return;
        var formattedAge = moment().diff(moment(tc), "years");
        return formattedAge;
    }



    downloadFicheMedicalePdf() {
        this.docs.pdfFicheMed(this.CAS.currentObjects);

    }

}

import { CasModel } from './../../models/cas.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { VisualistationService } from '../../visualisation.service';
import { CasService } from '../../../services/cas.service';



@Component({
    selector: 'detail-menu',
    template: `
    <ul>
        <li (click)="select('info')"  
        [ngClass]="{'active' : notifs.info, 'selected':rubrique=='info'}">
            <i class="fa fa-info"></i>
        </li>

        <li (click)="select('observation')" 
        [ngClass]="{'success' : cas && cas.observation, 'selected':rubrique=='observation'}">
            <i class="fal fa-stethoscope"></i>
        </li>

        <li (click)="select('action')" 
        [ngClass]="{'success' : cas && cas.action, 'selected':rubrique=='action'}">
            <i class="fal fa-medkit"></i>
        </li>
        
        <li (click)="select('conclusion')"
        [ngClass]="{'success' : cas && cas.conclusion, 'selected':rubrique=='conclusion'}" 
        class="rotate"> 
            <!-- <span>{{notifs.regulation}}</span>-->
            <i class="fal fa-exchange"></i>
        </li>
    </ul>
    `

})

export class DetailMenuComponent implements OnInit, OnDestroy {
    public notifs: any = { regulation: 1 };
    public cas: CasModel;
    public rubrique: any = 'info';
    private subscription: any[] = [];
    constructor(
        public VISU: VisualistationService,
        public CAS: CasService) { }

    ngOnInit() {
        this.subscription[0] = this.VISU.voletDisplay.subscribe(show => { if (show.rubrique) this.rubrique = show.rubrique });
        this.subscription[1] = this.CAS.casObservable.subscribe(cas => this.cas = cas);
    }

    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    select(rubrique: string) { this.VISU.voletShow({ template: 'cas', rubrique: rubrique }) }



}

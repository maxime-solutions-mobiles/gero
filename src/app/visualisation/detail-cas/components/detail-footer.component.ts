import { Component, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ListService } from './../../../services/list.service';
import { VisualistationService } from '../../visualisation.service';
import { CasModel } from './../../models/cas.model';
import { DBService } from '../../../services/db.service';
import { UserService } from '../../../services/user.service';
import { CasService } from '../../../services/cas.service';
import { PassageService } from 'src/app/services/passage.service';

@Component({
    selector: 'detail-footer',
    template: `
    <div *ngIf="!cas?.clos">

        <div *ngIf="cas?.notification?.length < 1" >
            <button mat-raised-button
               color="primary"
               [disabled]="loading"
               (click)="sortie()">
                <i class="fal fa-sign-out"></i> Sortie
            </button>

            <button  mat-raised-button color="primary"
                [disabled]="loading || (casPoste && !poste)"
                (click)="entree()">
                <i class="fal fa-sign-in"></i> Entrée
            </button>

            <button  mat-raised-button color="warning"
                [disabled]="loading || !cas?.observation && cas?.notif_regulation"
                (click)="askReg()">
                <i class="fal fa-exchange"></i> Régulation
            </button>

            <!--
            <button  mat-raised-button color="danger" (click)="askInter()" *ngIf="!cas?.currentIntervention">
                <i class="fas fa-bell"></i> Demande d'intervention
            </button>
            -->

            <button mat-raised-button
                color="accent"
                (click)="selectInter()"
                *ngIf="cas?.currentIntervention && currentUser?.fonction!='user'">
                <i class="fas fa-ambulance"></i> Intervention en cours
            </button>
        </div>

        <div *ngIf="cas?.notification?.length >0">
            <button mat-raised-button color="success"
                *ngIf="currentUser?.fonction!='user'"
                (click)="clearNotif(['discussion','regulation','intervention'])">
                <i class="fas fa-check"></i> Supprimer toutes les notifications
            </button>
        </div>

    </div>
    <div *ngIf="cas?.clos">
        <p>Cas clos le {{cas?.clos.timeCode | date:'dd/MM/yy'}} à {{cas?.clos.timeCode | date:'HH:mm'}} par {{cas?.clos.auteur}}.</p>
    </div>
    `
})
export class DetailFooter implements OnInit, OnDestroy {

    public loading = true;
    public cas: CasModel;
    public poste: string;
    public casPoste: string;
    public options: string[] = ['u1', 'u2', 'u3'];
    public currentUser: any;
    private subscription: Subscription;

    constructor(
        private DB: DBService,
        private VISU: VisualistationService,
        public USER: UserService,
        public CAS: CasService,
        private LIST: ListService,
        private PASSAGE: PassageService,
    ) { }

    ngOnInit() {
        this.subscription = this.USER.profile.subscribe((profile) => {
            this.poste = profile.poste;
            this.currentUser = profile;
        });
        const sub = this.CAS.casObservable.subscribe(cas => {
            this.cas = cas;
            this.casPoste = cas.poste;
            this.loading = false;
        });
        this.subscription.add(sub);
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public setOption(value: any) {
        this.cas.urgence = value;
        this.DB.update('cas', this.cas);
    }

    public resetInter() {
        this.CAS.cancelIntervention(this.cas)
    }

    public test(t) { return t.split(':')[1] }
    public cloreCas() { this.CAS.cloreCurrentCas('lsp'); }
    public clearNotif(type) { this.CAS.clearNotif(type) }
    public askReg() { this.CAS.addNotif('regulation'); }
    public askInter() { this.CAS.addNotif('intervention'); }

    public async sortie() {
        this.loading = true;
        if (this.CAS.currentObjects.conclusion && this.CAS.currentObjects.conclusion.regulation.length > 0) {
            // check for bug: position is missing from cas
            if (!this.cas.position) {
                this.cas.position = await this.PASSAGE.bugfixFindPassageAndCloseExtraneous(this.cas._id, this.LIST.listsRef.passage);
            }
            await this.CAS.closePassage(this.cas.position);
            this.LIST.refreshList('cas');
            this.LIST.resetList('passage');
        } else {
            this.VISU.navTo({ rubrique: 'conclusion', sousRubrique: 'conclusion' });
        }
        this.loading = false;
    }

    public async entree() {
        this.loading = true;
        if (this.cas.position && this.USER.INFO.poste) {
            await this.CAS.closeCasPassage(this.cas)
        }

        // bugfix: check for unlinked open passages
        const existingPassageId = await this.PASSAGE.bugfixFindPassageAndCloseExtraneous(this.cas._id, this.LIST.listsRef.passage);
        if (existingPassageId) {
            await this.PASSAGE.forceClosePassage(existingPassageId, 'Close passage before creating a new one');
        }

        await this.CAS.openCasPassage(this.cas, this.poste);
        this.LIST.resetList('passage');
        this.LIST.resetList('cas');
        this.loading = false;
      }

    public selectInter() {
        this.VISU.navTo({ intervention: this.cas.currentIntervention });
    }

}

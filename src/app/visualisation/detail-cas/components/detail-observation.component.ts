
import { VisualistationService } from '../../visualisation.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { MatTabChangeEvent } from '@angular/material/tabs/typings';
import { Subscription } from 'rxjs';
import { CasService } from '../../../services/cas.service';
import { UserService } from '../../../services/user.service';

        
@Component({
    selector: 'detail-observation',
    templateUrl: './detail-observation.component.html'
})
export class DetailObservationComponent implements OnInit, OnDestroy {

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    public ABCD: any[];
    public circonstancielle: any[];
    public vitale: any[];
    public clinique: any[];
    public surveillance: any[];
    public soap: any[];
    public exposure: any[];
    public dataSource = new MatTableDataSource(this.surveillance);
    public displayedColumns = [
        'heure',
        'pad',
        'pag',
        'fr',
        'fc',
        'trc',
        'spo2',
        'glycemie',
        'eva',
        'temperature',
        'hemocue',
        'etco2',
        'glasgow',
    ];
    public showMedic: boolean;
    public listRef = ['circonstancielle', 'vitale', 'surveillance', 'soap', 'exposure'];
    private subscription: Subscription;

    constructor(
        public VISU: VisualistationService,
        public CAS: CasService,
        private USER: UserService,
    ) { }

    ngOnInit() {
        this.subscription = this.USER.profile.subscribe((profile) => this.showMedic = profile.isMedic);

        const sub = this.CAS.observationObservable.subscribe(observation => {
            if (observation) {
                this.listRef.forEach(l => this[l] = this.CAS.currentObjects.observation[l] || [])
                if (this.surveillance.length > 0) {
                    this.dataSource = new MatTableDataSource(this.surveillance);
                    this.dataSource.sort = this.sort;
                    this.dataSource.paginator = this.paginator;
                }

                if (this.showMedic) {
                    this.clinique = observation.clinique || [];
                }
                this.ABCD = this.vitale ? this.vitale.filter(l => l.type === 'ABCD') : [];
            } else {
                this.listRef.forEach(l => this[l] = [])
                if (this.surveillance.length > 0) {
                    this.dataSource = new MatTableDataSource(this.surveillance);
                    this.dataSource.sort = this.sort;
                    this.dataSource.paginator = this.paginator;
                }
                this.clinique = [];
                this.ABCD = [];
            }
        });
        this.subscription.add(sub);
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public selectSousRubrique(event: MatTabChangeEvent) {
        let sousRubrique = event.tab.textLabel.toLowerCase();
        this.VISU.navTo({ sousRubrique: sousRubrique });
    }

    public add(type) {
        this.VISU.navTo({ sousRubrique: type, action: 'add' });

    }

}

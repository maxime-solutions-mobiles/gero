
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { UserService } from './../../../services/user.service';





@Component({
    selector: 'conclusion-courier-form',
    template:
        `
    <dynamic-form 
    [config]="config"
    #courierForm="dynamicForm"
    (submit)="submit($event)">
    </dynamic-form>
    `
})

export class ConclusionCourierForm {

    @Output() submitCourier: EventEmitter<CourierModel> = new EventEmitter<CourierModel>();

    @ViewChild(DynamicFormComponent) courierForm: DynamicFormComponent;

    constructor  (
        private USER: UserService)
    {
    }


    public config: FieldConfig[] = [
        {
            type: 'input',
            name: 'titre',
            placeholder: 'Titre',
            class: "grid-full"
        },
        {
            type: 'textArea',
            name: 'texte',
            placeholder: 'Texte',
            class: "grid-full"
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];


    public submit(val) {
        let courier: CourierModel = new CourierModel();
        let user = this.USER.INFO;
        courier.titre = val.titre;
        courier.texte = val.texte;
        courier.rpps  = user.rpps;
        this.submitCourier.emit(courier);

    }


}

export class CourierModel {
    timeCode?: Date;
    auteur?: string;
    rpps?: string;
    metier?: string;
    texte: string;
    titre: string;
    constructor() { }
};

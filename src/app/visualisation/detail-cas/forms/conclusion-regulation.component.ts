import { ParametreService } from './../../../services/parametre.service';

import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, ViewChild, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';


@Component({
    selector: 'conclusion-regulation-form',
    template: `

    <dynamic-form
  [initialValues]="initialValue"
  [config]="config"
  #regulationform="dynamicForm"
  (submit)="submit($event)">
  </dynamic-form>
    `
})

export class ConclusionRegulationForm implements OnInit, OnDestroy {
    @Output() submitRegulation: EventEmitter<any[]> = new EventEmitter<any[]>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild(DynamicFormComponent) regulationform: DynamicFormComponent;
    public initialValue: Regulation = new Regulation('', '', '', '', '', '');
    private hopitalList = [];
    private subscription: any;
    public config: FieldConfig[] = [
        {
            type: 'select',
            name: 'hopital',
            options: this.PARAM.currentParametre.hopital ? this.PARAM.currentParametre.hopital.map(l => l.nom) : [],

            /**valueChanges: (val) => {
                let hopital = this.hopitalList.find(l => l.nom == val);
                if (hopital && hopital.services) this.regulationform.configLine('service').options = hopital.services.split(',');
            },**/
            class: 'grid-demi',
            placeholder: "Destination",
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'service',
            class: 'grid-demi',
            placeholder: "Service"

        },
        {
            type: 'switch',
            name: 'moyen',
            options: this.PARAM.currentParametre.moyenevac ? this.PARAM.currentParametre.moyenevac.map(l => l.nom) : [],
            //options: ['ASSU', 'SMUR', 'HELLISMUR', 'VPSP', 'VL', 'pédestre', 'quad'],
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'urgence',
            options: ['faible', 'moyenne', 'élevée'],
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'position',
            options: ['allongé', 'assis', 'debout'],
            class: 'grid-full'
        },
        {
            type: 'textArea',
            name: 'commentaires',
            placeholder: 'Commentaire du régulateur',
            class: "grid-full"
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];

    constructor(private PARAM: ParametreService) { }
    ngOnInit() {
        this.initHopitalList(this.PARAM.currentParametre);
        this.subscription = this.PARAM.current.subscribe(r => { if (r.hopital) this.initHopitalList(r) });
    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    ngAfterViewChecked() { if (this.hopitalList != this.PARAM.currentParametre.hopital) this.initHopitalList(this.PARAM.currentParametre); }
    private initHopitalList(r) {
        if (!r || !r.hopital || !this.regulationform.configLine('hopital')) return;
        this.hopitalList = r.hopital;
        this.regulationform.configLine('hopital').options = r.hopital.map(l => l.nom);
    }


    public submit(value: any) {
        var concl = [];
        concl["type"] = 'regulation';
        let merged = { ...concl, ...value };
        this.submitRegulation.emit(merged);
    }

    private close() { this.closeForm.emit(true); }
}

export class Regulation {
    timeCode?: Date
    auteur?: string
    type?: string
    constructor(
        public hopital: string,
        public service: string,
        public moyen: string,
        public urgence: string,
        public position: string,
        public commentaires: string
    ) { }
};

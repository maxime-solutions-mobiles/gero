
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { UserService } from './../../../services/user.service';



@Component({
    selector: 'conclusion-ordonnance-form',
    template:
        `
    <p *ngFor="let prescription of prescriptionList">
         <strong>{{prescription.medicament}}</strong>  posologie <span class="primary">{{prescription.posologie}}  QSP :{{prescription.qsp}}</span>.
    </p>
    <dynamic-form 
    [initialValues]="initialValue"
    [config]="config"
    #prescriptionForm="dynamicForm"
    (submit)="submitPrescription($event)">
    </dynamic-form>
    
     <button  mat-raised-button color="success" (click)="submitOrdo()"><i class="fa fa-plus"></i>Valider l'ordonnance</button>
    `
})

export class ConclusionOrdonnanceForm {

    @Output() submitOrdonnance: EventEmitter<OrdonnanceModel> = new EventEmitter<OrdonnanceModel>();
    public initialValue: Prescription = { medicament: null, qsp: null, posologie: null };
    @ViewChild(DynamicFormComponent) prescriptionForm: DynamicFormComponent;

    constructor  (
        private USER: UserService)
    {
    }

    public prescriptionList: Prescription[] = [];
    public config: FieldConfig[] = [
        {
            type: 'input',
            name: 'medicament',
            inputType: 'text',
            placeholder: "Médicament",
            class: 'grid-tier'
        }, 
        {
            type: 'input',
            name: 'posologie',
            inputType: 'text',
            placeholder: "Posologie",
            class: 'grid-tier'
        },
        {
            type: 'input',
            name: 'qsp',
            inputType: 'text',
            placeholder: "QSP",
            class: 'grid-tier'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];

    public submitPrescription(prescription: Prescription) {
        this.prescriptionList.push(prescription);
        this.prescriptionForm.setValues(this.initialValue);
    }
    public submitOrdo() {
        let ordonnance: OrdonnanceModel = new OrdonnanceModel();
        let user = this.USER.INFO;
        ordonnance.prescripteur  = user.nom +' ' +user.prenom;
        ordonnance.rpps  = user.rpps; 
        ordonnance.prescription = this.prescriptionList;
        this.submitOrdonnance.emit(ordonnance);
        this.prescriptionList = [];
    }


}

export class OrdonnanceModel {
    timeCode?: Date;
    auteur?: string;
    prescripteur?:string;
    rpps?: string;
    metier?: string;
    prescription: Prescription[];
    constructor() { }
};
export interface Prescription {
    medicament: string;
    posologie: string;
    qsp: string;
}
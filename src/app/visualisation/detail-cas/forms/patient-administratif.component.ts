
import { VisualistationService } from './../../visualisation.service';
import { ParametreService } from './../../../services/parametre.service';
import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, ViewChild, Output, EventEmitter, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';


@Component({
    selector: 'patient-administratif-form',
    template: `
     <dynamic-form 
     [initialValues]="patient"
     [config]="config"
     #patientAdminform="dynamicForm"
     (submit)="submit($event)">
     </dynamic-form>   
    `
})

export class PatientAdministratifForm implements OnInit, OnDestroy {
    private subscription: any[] = [];
    @Input() patient: PatientModel;
    @Output() submitPatientAdministratif: EventEmitter<PatientModel> = new EventEmitter<PatientModel>();
    @ViewChild(DynamicFormComponent) patientAdminform: DynamicFormComponent;

    ngOnChanges(changes: SimpleChanges) {
        if (changes.patient) this.setVal(changes.patient.currentValue)
    }
    private setVal(val) {

        if (!this.patientAdminform) return;
        if (!val) return this.patientAdminform.reset();
        this.patientAdminform.setValues(val).then(r => this.initCurval(val));

    }

    private curVals: PatientModel = new PatientModel();
    @Output() searchDB: EventEmitter<any> = new EventEmitter<any>();
    private initCurval(p) {
        this.curVals = new PatientModel();
        this.curVals = { ...this.curVals, ...p };

    }
    ngAfterViewInit() {
        if (this.patient) this.setVal(this.patient)
        this.subscription[0] = this.VISU.selectPatient.subscribe(patient => this.submit(patient));
    }
    private typesList = [];
    public config: FieldConfig[] = [

        {
            type: 'separator',
            name: 'nom',
            inputType: 'text',
            placeholder: 'Etat civil '
        },
        {
            type: 'input',
            name: 'nom',
            inputType: 'text',
            placeholder: 'Nom du patient',
            class: 'grid-demi',
            valueChanges: (nom) => {
                if (nom != this.curVals.nom && nom != null) {
                    let query = { $regex: new RegExp(nom, "gi") };
                    this.searchDB.emit({ nom: query });
                    this.curVals.nom = nom;
                }
            }
        },
        {
            type: 'input',
            name: 'prenom',
            inputType: 'text',
            placeholder: 'Prénom du patient',
            class: 'grid-demi',
            /*valueChanges: (prenom) => {
                if (prenom != this.curVals.prenom && prenom != null) {
                    let query = { $regex: new RegExp(prenom, "gi") };
                    this.searchDB.emit({ prenom: query });
                    this.curVals.prenom = prenom;
                }
            }*/
        },
        {
            type: 'date',
            name: 'age',
            inputType: 'number',
            placeholder: 'DDN',
            class: 'grid-demi',
            validation: [Validators.required]
        },
        {
            type: 'select',
            name: 'nationalite',
            options: this.PARAM.currentParametre.nationalite ? this.PARAM.currentParametre.nationalite.map(l => l.nom) : [],
            //options: ['Français', 'anglais', 'Belge', 'Allemand', 'Américain', 'Italien'],
            placeholder: 'Nationalité',
            class: 'grid-demi'
        },
        {
            type: 'switch',
            name: 'sexe',
            options: ['homme', 'femme'],
            placeholder: 'Sexe',
            class: 'm-f grid-tier'
        },
        {
            type: 'input',
            name: 'taille',
            inputType: 'number',
            placeholder: 'taille (en cm)',
            class: 'grid-tier'
        },
        {
            type: 'input',
            name: 'poids',
            inputType: 'number',
            placeholder: 'poids (en kg)',
            class: 'grid-tier'
        },
        {
            type: 'switch',
            name: 'types',
            class: 'grid-full',
            placeholder: 'Type'
        },
        {
            type: 'input',
            name: 'typeprecision',
            class: 'grid-full',
            placeholder: 'Précision du type de patient'
        },
        {
            type: 'separator',
            name: 'nom',
            inputType: 'text',
            placeholder: 'Coordonnées'
        },
        {
            type: 'address',
            name: 'adresse',
            placeholder: 'Adresse',
            class: 'grid-full',
        },
        {
            type: 'phone',
            name: 'telephone',
            inputType: 'number',
            placeholder: 'Téléphone',
            class: 'grid-full',
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];
    constructor(
        private VISU: VisualistationService,
        private PARAM: ParametreService) {
        this.initCurval(this.patient);
    }
    ngOnInit() {
        this.initTypesList(this.PARAM.currentParametre);
        this.subscription[1] = this.PARAM.current.subscribe(r => { if (r.medicament) this.initTypesList(r) });
    }
    ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    private initTypesList(r) {
        if (!r || !r.types) return;
        this.typesList = r.types;
        this.configLine('types').options = r.types.map(l => l.nom);
    }
    private configLine(name) {
        let index = this.config.findIndex(l => l.name == name);
        return this.config[index]
    }

    submit(value: any) {
        this.submitPatientAdministratif.emit(value);

    }
}

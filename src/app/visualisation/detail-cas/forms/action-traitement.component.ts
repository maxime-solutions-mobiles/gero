import { ParametreService } from './../../../services/parametre.service';

import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';

import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';


@Component({
    selector: 'action-traitement-forms',
    template: `

    <mat-accordion>


    <!--reproduire/adapter ce code (debut)-->
    <!--remplacer 0 par 1,2...-->
    <mat-expansion-panel [expanded]="step == 0" (opened)="setStep(0)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
    
      Médicaments
    </mat-panel-title>
    <mat-panel-description>
     administration de médicaments...
    </mat-panel-description>
  </mat-expansion-panel-header>
  <!--remplacer #,initialValue et config-->
  <dynamic-form
  [initialValues]="MedicamentInitialValue"
  [config]="medicamentConfig"
  #medicamentform="dynamicForm"
  (submit)="submittraitement($event)">
  </dynamic-form>   

  </mat-expansion-panel>

  <!--reproduire/adapter ce code (fin)-->

     <mat-expansion-panel [expanded]="step == 1" (closed)="setStep(1)" hideToggle="false">
    <mat-expansion-panel-header>
    <mat-panel-title>
    
      Solutes
    </mat-panel-title>
    <mat-panel-description>
     Solutés et remplissages
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="SolutesInitialValue"
  [config]="solutesConfig"
  #solutesform="dynamicForm"
  (submit)="submittraitement($event)">
  </dynamic-form>   

  </mat-expansion-panel>


     </mat-accordion>
    `
})

export class ActiontraitementForm {


    @Output() submitactiontraitement: EventEmitter<any> = new EventEmitter<any>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();

    //déclarer un dynamic form par formulaire (correspond avec # dans le template)
    @ViewChild(DynamicFormComponent) medicamentForm: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) solutesform: DynamicFormComponent;

    // Definir les valeurs par defaut
    public MedicamentInitialValue: Medicaments = new Medicaments('', '', '', '', '');
    public SolutesInitialValue: Solutes = new Solutes('', '', '');
    private traitement: any[] = []

    public step = 0;

    private medicamentList = [];

    public setStep(index: number) {
        this.step = index;
    }

    // definir le formulaire lui même
    // definir le formulaire lui même
    public medicamentConfig: FieldConfig[] = [
        {
            type: 'time',
            name: 'heure',
            placeholder: "heure",
            class: 'grid-tier'
        },
        {
            type: 'autocomplete',
            name: 'medicament',
            valueChanges: (val) => {
                let medicament = this.medicamentList.find(l => l.nom == val);
            },
            placeholder: "Produit",
            class: 'grid-2tier'
        },

        {
            type: 'input',
            name: 'posologie',
            inputType: 'text',
            placeholder: "Posologie",
            class: 'grid-tier'
        },
        {
            type: 'select',
            name: 'unite',
            options: ['mg', 'gr', 'ml', 'UI', 'mg/h', 'gr/h', 'gamma', 'gamma / h', 'UI', 'autre'],
            placeholder: "Unité",
            class: 'grid-tier'
        },
        {
            type: 'select',
            name: 'voie',
            options: ['IVSE', 'IVL', 'IVD', 'IM', 'per os', 'inhalation', 'sous cutanée', 'intra rectal', 'intra osseuse', 'intra trachéal', 'vaginal', 'intranasal', 'occulaire', 'intra vésical', 'intra auriculaire', 'trans cutanée', 'autre'],
            placeholder: "Voie d'administration",
            class: 'grid-tier'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];

    // definir le formulaire lui même
    public solutesConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'solute',
            options: ['NaCl 0,9%', 'G5%', 'G10%', 'Ringer Lactate', 'SSH 7,5%', 'HEA 6%', 'PLYO', 'Mannitol 20%', 'HCO3- 4,2%', 'HCO3- 8,4%'],
            placeholder: "Solute",
            class: "grid-full"
        },

        {
            type: 'switch',
            name: 'quantite',
            options: ['50 ml', '100 ml', '250 ml', '500 ml', '1000 ml'],
            placeholder: "Quantité",
            class: "grid-full"
        },
        {
            type: 'switch',
            name: 'vitesse',
            options: ['débit libre', '10 minutes', '20 minutes', '1 heure', '6 heures', 'Garde veine'],
            placeholder: "Vitesse",
            class: "grid-full"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];

    constructor(private PARAM: ParametreService) { }
    ngOnInit() {
        this.initMedicamentList(this.PARAM.currentParametre);
        this.PARAM.current.subscribe(r => { if (r.medicament) this.initMedicamentList(r) });
    }

    private initMedicamentList(r) {
        if (!r || !r.medicament) return;
        this.medicamentList = r.medicament;
        this.configLine('medicament').options = r.medicament.map(l => l.nom);
    }

    private configLine(name) {
        let index = this.medicamentConfig.findIndex(l => l.name == name);
        return this.medicamentConfig[index]
    }


    submittraitement(value: any) {

        this.traitement.push(value);
        this.submitactiontraitement.emit(value);

    }
    close() {
        this.closeForm.emit(true);
        //this.solutesform.reset();
    }
}

export interface Traitement {
    typeTraitement: string,
    timeCode?: Date,
    auteur?: string

};


// Creer un model de ce type pour chaque type de geste
export class Medicaments implements Traitement {
    typeTraitement = 'Medicaments';
    constructor(public heure: string, public produit: string, public posologie: string, public unite: string, public voie: string) { }

}
export class Solutes implements Traitement {
    typeTraitement = 'Solutes';
    constructor(public solute: string, public quantite: string, public vitesse: string) { }

}
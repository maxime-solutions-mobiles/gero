
import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import { Exposure } from './observation.vitale.component';


@Component({
    selector: 'observation-exposure-form',
    template: `


    <mat-accordion>

    <mat-expansion-panel [expanded]="step === 0" (opened)="setStep(0)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Exposure : Lésionel
    </mat-panel-title>
    <mat-panel-description>
   
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
    <dynamic-form
    [initialValues]="exposureInitialValue"
    [config]="exposureConfig"
    #exposureform="dynamicForm"
    (submit)="submit($event)">
    </dynamic-form>   
 

    </mat-expansion-panel>



    </mat-accordion>


   `
})

export class ObservationExposureForm {


    @Output() submitObservationExposure: EventEmitter<any> = new EventEmitter<any>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(DynamicFormComponent) exposureform: DynamicFormComponent;
    public exposureInitialValue: Exposure = new Exposure('', '', '', '');




    public step = 0;

    public setStep(index: number) {
        this.step = index;
    }


    public exposureConfig: FieldConfig[] = [


        {
            type: 'switch',
            name: 'etype',
            options: ['plaie', 'brulure', 'dermabrasion', 'perte substance'],
            class: "grid-full"

        },
        {
            type: 'select',
            name: 'elocalisation',
            options: ['main', 'bras', 'face', 'tête', 'cou', 'thorax', 'abdomen', 'cuisse', 'jambe', 'pied', 'dos', 'pelvis'],
            placeholder: "Localisation",
            class: "grid-demi"
        },
        {
            type: 'switch',
            name: 'elateralisation',
            options: ['gauche', 'droite'],
            class: "grid-demi"
        },
        {
            type: 'input',
            name: 'edescription',
            placeholder: "Description",
            class: "grid-full"

        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }
    ];




    constructor() { }


    submit(value: any) {


        this.submitObservationExposure.emit(value);
    }
    close() {
        this.closeForm.emit(true);
    }
}

export interface Soap {
    type: string,
    timeCode?: Date,
    auteur?: string

};


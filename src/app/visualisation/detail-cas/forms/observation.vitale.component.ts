

import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, ViewChild, Output, EventEmitter, ElementRef, QueryList, ViewChildren, Type } from '@angular/core';
import { Validators } from '@angular/forms';


@Component({
    selector: 'observation-vitale-form',
    template: `

    <mat-vertical-stepper linear="true">
    <mat-step [stepControl]="airwaysform">
        <ng-template matStepLabel>A : Airways, liberté des VAS et rachis cervical</ng-template>

        <dynamic-form
        [initialValues]="airwaysInitialValue"
        [config]="airwaysConfig"
        #airwaysform="dynamicForm">
        </dynamic-form>   
        <div>
        <button mat-button matStepperNext>Suivant</button>
      </div>
    </mat-step>

    <mat-step [stepControl]="respirationform">
        <ng-template matStepLabel>B : Breathing, Ventilation et hématose</ng-template>

        <dynamic-form
        [initialValues]="respirationInitialValue"
        [config]="respirationConfig"
        #respirationform="dynamicForm">
        </dynamic-form>   
        <div>
        <button mat-button matStepperNext>Suivant</button>
      </div>

    </mat-step>

    <mat-step [stepControl]="circulationform">
        <ng-template matStepLabel>C : Circulation, hémodynamique et hémorragie</ng-template>

        <dynamic-form
        [initialValues]="circulationInitialValue"
        [config]="circulationConfig"
        #circulationform="dynamicForm">
        </dynamic-form>   
        <div>
        <button mat-button matStepperNext>Suivant</button>
      </div>

    </mat-step>

    <mat-step [stepControl]="disabilitiesform">
        <ng-template matStepLabel>D : Dysabilities, etat neurologique et douleur</ng-template>

        <dynamic-form
        [initialValues]="disabilitiesInitialValue"
        [config]="disabilitiesConfig"
        #disabilitiesform="dynamicForm">
        </dynamic-form>   

        <div>
        <button mat-button matStepperNext>Suivant</button>
      </div>
    </mat-step>
    </mat-vertical-stepper>



<div class="action">
<button  mat-raised-button color="success" (click)="submitABCD()">Valider</button>
</div>

    





    `
})

export class ObservationVitaleForm {


    @Output() submitObservationVitale: EventEmitter<any> = new EventEmitter<any>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();

    @ViewChildren(DynamicFormComponent) dynamicForms: QueryList<DynamicFormComponent>;

    close() {
        this.closeForm.emit(true);
    }


    private airwaysform: DynamicFormComponent;
    private respirationform: DynamicFormComponent;
    private circulationform: DynamicFormComponent;
    private disabilitiesform: DynamicFormComponent;




    public airwaysInitialValue: Airways = new Airways('', '', '', '');
    public respirationInitialValue: Respiration = new Respiration('', '', '', '', '', '');
    public circulationInitialValue: Circulation = new Circulation('', '', '', '', '');
    public disabilitiesInitialValue: Disabilities = new Disabilities();
    public exposureInitialValue: Exposure = new Exposure('', '', '', '');



    public airwaysConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'aliberte',
            options: ['oui', 'non'],
            placeholder: "Liberté des VAS",
            validation: [Validators.required]
        },
        {
            type: 'switch',
            name: 'avomissement',
            options: ['oui', 'non'],
            placeholder: "Vomissements"
        },
        {
            type: 'switch',
            name: 'arachis',
            options: ['oui', 'non'],
            placeholder: "Notion trauma cranien ou rachis",
            validation: [Validators.required]
        },
        {
            type: 'switch',
            name: 'arespiration',
            options: ['oui', 'non'],
            placeholder: "Respiration présente"
        }
    ];



    public respirationConfig: FieldConfig[] = [

        {
            type: 'switch',
            name: 'befficace',
            options: ['oui', 'non'],
            class: 'grid-full',
            placeholder: "Ventilation efficace"
            //prévoir comment en sélectionner plusieurs ???...
        },
        {
            type: 'switch_multi_select',
            name: 'bqualification',
            options: ['normale', 'superficielle', 'irreguliere', 'balancement', 'polypnéique', 'bradypnéique'],
            class: 'grid-full',
            placeholder: "Qualité de la respiration"
            //prévoir comment en sélectionner plusieurs ???...
        },
        {
            type: 'switch_multi_select',
            name: 'bdetresse',
            options: ['cyanose', 'sueurs', 'assymétrie thoracique', 'tirage', 'bruit', 'aucune'],
            class: 'grid-full',
            placeholder: "Signe de détresse"
        },
        {
            type: 'switch',
            name: 'bparler',
            options: ['normal', 'essouflé', 'impossible'],
            class: 'grid-full',
            placeholder: "Capacitè à parler"
        },
        {
            type: 'switch_multi_select',
            name: 'bbruits',
            options: ['murmure vésiculaire', 'Crépitants', 'Ronchis', 'Ronflements', 'Squawk', 'Stridor', 'Sibilants'],
            class: 'grid-full',
            placeholder: "Bruits auscultatoires"
        },
        {
            type: 'switch',
            name: 'bbruitscote',
            options: ['gauche', 'droit', 'bilatéral'],
            class: 'grid-full',
            placeholder: "Coté"
        }

    ];


    public circulationConfig: FieldConfig[] = [


        {
            type: 'switch',
            name: 'chemorragie',
            options: ['oui', 'non'],
            placeholder: "Hémorragie",
            class: "grid-full"
        },
        {
            type: 'switch',
            name: 'cpouls',
            options: ['oui', 'non'],
            placeholder: "Pouls radial présent"
        },

        {
            type: 'switch',
            name: 'ctrc',
            options: ['oui', 'non'],
            placeholder: "TRC < 3 secondes"
        },
        {
            type: 'switch_multi_select',
            name: 'cqualification',
            options: ['régulier', 'irrégulier', 'bien frappé', 'filant'],
            placeholder: "Qualification du pouls",
            class: "grid-full"
            //prévoir comment en sélectionner plusieurs ???...

        },
        {
            type: 'switch_multi_select',
            name: 'cdetresse',
            options: ['marbrures', 'paleurs', 'signes allergique', 'sensation froid', 'sensation de soif', 'aucune'],
            class: 'grid-full',
            placeholder: "Signe de détresse"
            //prévoir comment en sélectionner plusieurs ???...
        },
    ];
    private glasgowElements = {
        dsgy: 0,
        dsgv: 0,
        dsgm: 0

    };
    private glasgow: number = 0;
    public disabilitiesConfig: FieldConfig[] = [




        {
            type: 'switch',
            name: 'dconscience',
            options: ['oui', 'non'],
            placeholder: "Conscient",
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'dpci',
            options: ['oui', 'non'],
            placeholder: "PCI",
        },
        {
            type: 'input',
            name: 'dpciduree',
            placeholder: "Durée de PCI (minutes)"
            //prévoir comment le décomposer et calcul auto en 3 champs select...
        },
        {
            type: 'switch',
            name: 'dconvulsion',
            options: ['oui', 'non'],
            placeholder: "Convulsions",
        },
        {
            type: 'input',
            name: 'dconvulsionsduree',
            placeholder: "Durée de convulsions (minutes)"
        },
        {
            type: 'separator',
            name: 'controler',
            placeholder: "Glasgow",
            class: "grid-full"
        },
        {
            type: 'select',
            inputType: 'number',
            name: 'dsgy',
            options: ['1', '2', '3', '4'],
            placeholder: "Yeux",
            class: "grid-quart",
            valueChanges: (val) => {
                this.glasgowElements.dsgy = parseInt(val);
                this.defineGlasgow();
            }
        },
        {
            type: 'select',
            inputType: 'number',
            name: 'dsgv',
            options: ['1', '2', '3', '4', '5'],
            placeholder: "Verbal",
            class: "grid-quart",
            valueChanges: (val) => {
                this.glasgowElements.dsgv = parseInt(val);
                this.defineGlasgow();
            }
        },
        {
            type: 'select',
            inputType: 'number',
            name: 'dsgm',
            options: ['1', '2', '3', '4', '5', '6'],
            placeholder: "Moteur",
            class: "grid-quart",
            valueChanges: (val) => {
                this.glasgowElements.dsgm = parseInt(val);
                this.defineGlasgow();
            }
        },
        {
            type: 'input',
            name: 'glasgow',
            inputType: 'number',
            value: this.glasgow,
            validation: [Validators.min(3), Validators.max(15)],
            placeholder: "Glasgow",
            class: "grid-quart"
        },

        {
            type: 'switch',
            name: 'dorientation',
            options: ['oui', 'non'],
            placeholder: "Orienté dans espace et temps",
            class: "grid-full"
        },
        {
            type: 'switch_multi_select',
            name: 'dsensibilite',
            options: ['membre supérieur gauche', 'membre supérieur droit', 'membre inférieur gauche', 'membre inférieur droit', 'visage', 'tronc'],
            placeholder: "Trouble de la sensibilité"
        },
        {
            type: 'switch_multi_select',
            name: 'dmotricite',
            options: ['membre supérieur gauche', 'membre supérieur droit', 'membre inférieur gauche', 'membre inférieur droit', 'visage', 'tronc'],
            placeholder: "Trouble de la motricité"
        },
        {
            type: 'switch_multi_select',
            name: 'dpupilledroite',
            options: ['réactive', 'aréactive', 'normale', 'mydriase', 'myosis'],
            placeholder: "Pupille droite",
            class: "grid-full"
            //prévoir comment en sélectionner plusieurs ???...
        },
        {
            type: 'switch_multi_select',
            name: 'dpupillegauche',
            options: ['réactive', 'aréactive', 'normale', 'mydriase', 'myosis'],
            placeholder: "Pupille gauche",
            class: "grid-full"
            //prévoir comment en sélectionner plusieurs ???...
        }
    ];


    constructor() { }
    private defineGlasgow() {
        let glasgow = 0;
        Object.keys(this.glasgowElements).forEach(k => glasgow += parseInt(this.glasgowElements[k]))
        this.disabilitiesform.setValue('glasgow', glasgow);
        this.glasgow = glasgow;
    }
    ngAfterViewInit() {

        this.airwaysform = this.dynamicForms.find(l => l.initialValues.type == "airways");
        this.respirationform = this.dynamicForms.find(l => l.initialValues.type == "respiration");
        this.circulationform = this.dynamicForms.find(l => l.initialValues.type == "circulation");
        this.disabilitiesform = this.dynamicForms.find(l => l.initialValues.type == "disabilities");

    }


    submitABCD() {

        let ABCD = {
            airways: this.airwaysform.value,
            breathing: this.respirationform.value,
            circulation: this.circulationform.value,
            disabilities: this.disabilitiesform.value
        };
        this.submitObservationVitale.emit(ABCD);

        this.close();

    }

}

export interface Vital {
    type: string,
    timeCode?: Date,
    auteur?: string

};


export class Airways implements Vital {
    type = 'airways';
    constructor(public aliberte: string, public avomissement: string, public arachis: string, public arespiration: string) { }

}
export class Respiration implements Vital {
    type = 'respiration';
    constructor(
        public befficace: string,
        public bdetresse: string,
        public bqualification: string,
        public bparler: string,
        public bbruit: string,
        public bbruitscote: string
    ) { }

}
export class Circulation implements Vital {
    type = 'circulation';
    constructor(
        public chemorragie: string,
        public cpouls: string,
        public cdetresse: string,
        public ctrc: string,
        public cqualification: string
    ) { }

}
export class Disabilities implements Vital {
    type = 'disabilities';
    public dconscience: string;
    public dpci: string;
    public dpciduree: string;
    public dconvulsions: string;
    public dconvulsionsduree: string;
    public dsgy: number;
    public dsgv: number;
    public dsgm: number;
    public glasgow: number;
    public dorientation: string;
    public dsensibilite: string;
    public dmotricite: string;
    public dpupilledroite: string;
    public dpupillegauche: string;
    constructor(

    ) { }

}
export class Exposure implements Vital {
    type = 'exposure';
    constructor(
        public etype: string,
        public elocalisation: string,
        public elateralisation: string,
        public edescription: string
    ) { }

}
import { Component, Input, ViewChild, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from './../../../services/user.service';
import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';

@Component({
    selector: 'observation-circonstancielle-form',
    template: `
    <mat-accordion>

    <mat-expansion-panel [expanded]="step === 0" (opened)="setStep(0)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>Circonstances</mat-panel-title>
        <mat-panel-description>
             Description formulaire Circonstances
        </mat-panel-description>
    </mat-expansion-panel-header>

    <dynamic-form
        [initialValues]="circonstancesInitialValue"
        [config]="circonstancesConfig"
        #circonstancesform="dynamicForm"
        (submit)="submitCirconstancielle($event)">
    </dynamic-form>

    </mat-expansion-panel>

    <mat-expansion-panel [expanded]="step === 1" (opened)="setStep(1)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>Signes</mat-panel-title>
        <mat-panel-description>
             Description formulaire signes
        </mat-panel-description>
    </mat-expansion-panel-header>

    <dynamic-form
        [initialValues]="signeInitialValue"
        [config]="signeConfig"
        #signeform="dynamicForm"
        (submit)="submitCirconstancielle($event)">
    </dynamic-form>

    </mat-expansion-panel>

    <mat-expansion-panel [expanded]="step === 2" (opened)="setStep(2)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>Plaintes</mat-panel-title>
        <mat-panel-description>
             Les plaintes du patient
        </mat-panel-description>
    </mat-expansion-panel-header>

    <dynamic-form
        [initialValues]="plainteInitialValue"
        [config]="plainteConfig"
        #plainteform="dynamicForm"
        (submit)="submitCirconstancielle($event)">
    </dynamic-form>

    </mat-expansion-panel>

    <mat-expansion-panel [expanded]="step === 3" (opened)="setStep(3)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>Cinétique</mat-panel-title>
        <mat-panel-description>
        </mat-panel-description>
    </mat-expansion-panel-header>

    <dynamic-form
        [initialValues]="cinetiqueInitialValue"
        [config]="cinetiqueConfig"
        #cinetiqueform="dynamicForm"
        (submit)="submitCirconstancielle($event)">
    </dynamic-form>

    </mat-expansion-panel>

     <mat-expansion-panel
         [expanded]="step === 4"
         (opened)="setStep(4)"
         hideToggle="true"
         *ngIf="isMedic"> <!-- masque si user ismedic (cf user service)-->
    <mat-expansion-panel-header>
    <mat-panel-title>Intoxication</mat-panel-title>
        <mat-panel-description>

        </mat-panel-description>
    </mat-expansion-panel-header>

    <dynamic-form
        [initialValues]="intoxInitialValue"
        [config]="intoxConfig"
        #cinetiqueform="dynamicForm"
        (submit)="submitCirconstancielle($event)">
    </dynamic-form>

    </mat-expansion-panel>

     </mat-accordion>
    `
})
export class ObservationCirconstancielleForm implements OnInit, OnDestroy {

    @Output() submitObservationCirconstancielle: EventEmitter<any> = new EventEmitter<any>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(DynamicFormComponent) circonstancesform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) signeform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) plainteform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) cinetiqueform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) intoxform: DynamicFormComponent;

    public circonstancesInitialValue: Circonstances = new Circonstances('');
    public signeInitialValue: Signe = new Signe('');
    public plainteInitialValue: Plainte = new Plainte('');
    public cinetiqueInitialValue: Cinetique = new Cinetique('', '', '', '');
    public intoxInitialValue: Intox = new Intox('', '');

    public step = 0;

    public circonstancesConfig: FieldConfig[] = [
        {
            type: 'textArea',
            name: 'circonstances',
            placeholder: 'Circonstances'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public signeConfig: FieldConfig[] = [
        {
            type: 'textArea',
            name: 'signe',
            placeholder: 'Signes'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public plainteConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'plainte',
            placeholder: 'Plainte'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public cinetiqueConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'chute',
            placeholder: 'Chute de grande hauteur',
            options: ['oui', 'non']
        },
        {
            type: 'switch',
            name: 'vitesse',
            placeholder: 'Grande vitesse',
            options: ['oui', 'non']
        },
        {
            type: 'switch',
            name: 'avp',
            placeholder: 'Victime d\'accident',
            options: ['incarcéré', 'éjecté', '2 roues non casqué', 'passager non ceinturé'],
            class: 'grid-full'
        },
        {
            type: 'textArea',
            name: 'cinetique',
            placeholder: 'Observation',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public intoxConfig: FieldConfig[] = [
        {
            type: 'switch_multi_select',
            name: 'produit',
            placeholder: 'Intoxication',
            options: ['CO', 'fumées', 'médicaments', 'stupéfiants', 'alcool'],
            class: 'grid-full'

        },
        {
            type: 'textArea',
            name: 'intoxdetails',
            placeholder: 'détails',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public isMedic: boolean;

    private subscription: Subscription;

    constructor(private USER: UserService) { }

    ngOnInit() {
        this.subscription = this.USER.profile.subscribe((profile) => {
            this.isMedic = profile.isMedic;
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public setStep(index: number) {
        this.step = index;
    }

    public submitCirconstancielle(value: any) {
        this.submitObservationCirconstancielle.emit(value);
    }

    public close() {
        this.closeForm.emit(true);
    }
}

export interface Circonstanciel {
    type: string;
    timeCode?: Date;
    auteur?: string;
};

// Creer un model de ce type pour chaque type de geste
export class Circonstances implements Circonstanciel {
    type = 'circonstances';
    constructor(public circonstances: string) { }
}

export class Signe implements Circonstanciel {
    type = 'signe';
    constructor(public signe: string) { }
}

export class Plainte implements Circonstanciel {
    type = 'plainte';
    constructor(public plainte: string) { }
}

export class Cinetique implements Circonstanciel {
    type = 'cinetique';
    constructor(public cinetique: string, public chute: string, public vitesse: string, public avp: string) { }
}

export class Intox implements Circonstanciel {
    type = 'intox';
    constructor(public produit: string, public intoxdetails: string) { }
}

import { Component, Input, ViewChild, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'action-geste-forms',
    template: `
    <mat-accordion>

    <mat-expansion-panel [expanded]="step == 0" (opened)="setStep(0)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
      Attelle
    </mat-panel-title>
    <mat-panel-description>Pose d'attelle
    </mat-panel-description>
  </mat-expansion-panel-header>

  <dynamic-form
  [initialValues]="attelleInitialValue"
  [config]="attelleConfig"
  #attelleform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>
    <mat-expansion-panel
      [expanded]="step == 1"
      (opened)="setStep(1)"
      hideToggle="true"
      *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>
      Intubation
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="iotInitialValue"
  [config]="iotConfig"
  #iotform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>
     <mat-expansion-panel [expanded]="step == 2" (opened)="setStep(2)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
      RCP
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="rcpInitialValue"
  [config]="rcpConfig"
  #rcpform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel [expanded]="step == 3" (opened)="setStep(3)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
      Aspiration
    </mat-panel-title>
    <mat-panel-description>Patient intubé ou non
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="aspirationInitialValue"
  [config]="aspirationConfig"
  #aspirationform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel [expanded]="step == 4" (opened)="setStep(4)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
      Position
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="positionInitialValue"
  [config]="positionConfig"
  #positionform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel [expanded]="step == 5" (opened)="setStep(5)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
      Pansement
    </mat-panel-title>
    <mat-panel-description>Pansement
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="pansementInitialValue"
  [config]="pansementConfig"
  #pansementform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

    <mat-expansion-panel
      [expanded]="step == 6"
      (opened)="setStep(6)"
      hideToggle="true"
      *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>
      VVP
    </mat-panel-title>
    <mat-panel-description>

    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="vvpInitialValue"
  [config]="vvpConfig"
  #vvpform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel
    [expanded]="step == 7"
    (opened)="setStep(7)"
    hideToggle="true"
    *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>
      VVC
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="vvcInitialValue"
  [config]="vvcConfig"
  #vvcform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel
    [expanded]="step == 8"
    (opened)="setStep(8)"
    hideToggle="true"
    *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>Suture
    </mat-panel-title>
    <mat-panel-description>
     Suture
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="sutureInitialValue"
  [config]="sutureConfig"
  #sutureform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel
    [expanded]="step == 9"
    (opened)="setStep(9)"
    hideToggle="true"
    *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>Intra Osseuse
    </mat-panel-title>
    <mat-panel-description></mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="dioInitialValue"
  [config]="dioConfig"
  #dioform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel
    [expanded]="step == 10"
    (opened)="setStep(10)"
    hideToggle="true"
    *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>SNG
    </mat-panel-title>
    <mat-panel-description>
     Pose de SNG
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="sngInitialValue"
  [config]="sngConfig"
  #sngform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

 <mat-expansion-panel [expanded]="step == 11" (opened)="setStep(11)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>Conditionnement
    </mat-panel-title>
    <mat-panel-description>
     Conditionnement du patient
    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="conditionnementInitialValue"
  [config]="conditionnementConfig"
  #conditionnementform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel
    [expanded]="step == 12"
    (opened)="setStep(12)"
    hideToggle="true"
    *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>Bandelette Urinaire
    </mat-panel-title>
    <mat-panel-description>

    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="buInitialValue"
  [config]="buConfig"
  #buform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel
    [expanded]="step == 13"
    (opened)="setStep(13)"
    hideToggle="true"
    *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>Damage Control
    </mat-panel-title>
    <mat-panel-description>

    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="dcInitialValue"
  [config]="dcConfig"
  #dcform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>

  <mat-expansion-panel
    [expanded]="step == 14"
    (opened)="setStep(14)"
    hideToggle="true"
     *ngIf="currentUser?.metier =='doctor' || currentUser?.metier =='nurse'" >
    <mat-expansion-panel-header>
    <mat-panel-title>Sondage vésical
    </mat-panel-title>
    <mat-panel-description>

    </mat-panel-description>
  </mat-expansion-panel-header>
  <dynamic-form
  [initialValues]="suInitialValue"
  [config]="suConfig"
  #suform="dynamicForm"
  (submit)="submitGeste($event)">
  </dynamic-form>

  </mat-expansion-panel>
     </mat-accordion>
    `
})
export class ActionGesteForm implements OnInit, OnDestroy {

    @Output() submitactionGeste: EventEmitter<any[]> = new EventEmitter<any[]>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();

    // déclarer un dynamic form par formulaire (correspond avec # dans le template)
    @ViewChild(DynamicFormComponent) attelleform: DynamicFormComponent;

    // Definir les valeurs par defaut
    public attelleInitialValue: Attelle = new Attelle('', '', '');
    public iotInitialValue: Iot = new Iot('', '', '', '', '', '');
    public positionInitialValue: Position = new Position('');
    public pansementInitialValue: Pansement = new Pansement('', '', '', '');
    public rcpInitialValue: Rcp = new Rcp('', '', '', '');
    public aspirationInitialValue: Aspiration = new Aspiration('', '', '');
    public vvpInitialValue: Vvp = new Vvp('', '', '', '');
    public vvcInitialValue: Vvc = new Vvc('', '', '', '');
    public sutureInitialValue: Suture = new Suture('', '', '', '', '');
    public sngInitialValue: Sng = new Sng('', '', '', '', '');
    public dioInitialValue: Dio = new Dio('', '', '', '', '');
    public conditionnementInitialValue: Conditionnement = new Conditionnement('', '', '');
    public buInitialValue: Bu = new Bu('', '', '', '', '', '', '', '', '', '', '');
    public dcInitialValue: Dc = new Dc('', '', '', '', '', '', '', '', '', '');
    public suInitialValue: Su = new Su('', '', '', '');

    private geste: any[] = []

    public step: number;

    public currentUser: any;

    // definir le formulaire lui même
    public attelleConfig: FieldConfig[] = [
        {
            type: 'select',
            name: 'type',
            options: ['attelle', 'collier cervical', 'écharpe', 'plan dur', 'MID', 'attelle en traction', 'Poche de froid'],
            class: 'grid-tier',
            placeholder: 'Type d\'attelle',
            validation: [Validators.required]
        },
        {
            type: 'select',
            name: 'position',
            options: ['main', 'bras', 'avant bras', 'coude', 'épaule', 'jambe', 'genou', 'cuisse', 'pied', 'cheville', 'corps entier'],
            class: 'grid-tier',
            placeholder: 'Position'
        },
        {
            type: 'switch',
            name: 'lateralite',
            options: ['gauche', 'droit'],
            class: 'grid-full',
            placeholder: 'Latéralité'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire IOT
    public iotConfig: FieldConfig[] = [
        {
            type: 'select',
            name: 'tube',
            options: ['5.0', '5.5', '6.0', '6.5', '7', '7.5', '8.0', '8.5'],
            placeholder: 'Sonde',
            validation: [Validators.required]
        },

        {
            type: 'switch',
            name: 'cormack',
            options: ['1', '2', '3', '4'],
            placeholder: 'cormack'
        },
        {
            type: 'select',
            name: 'technique',
            options: ['laryngo', 'fastrack', 'Eschmann', 'Vidéo'],
            placeholder: 'Technique',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'etco2',
            placeholder: 'EtCO2 mm Hg'
        },
        {
            type: 'input',
            name: 'repere',
            placeholder: 'repère'
        },
        {
            type: 'input',
            name: 'pression',
            placeholder: 'Pression ballonnet en cm d eau'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire Position
    public positionConfig: FieldConfig[] = [
        {
            type: 'select',
            name: 'position',
            options: ['Décubitus complet', 'Demi Assis', 'Décubitus latéral droit', 'Décubitus Latéral Gauche', 'PLS', 'Décubitus Ventral'],
            placeholder: 'Position',
            validation: [Validators.required]
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire Oxygene
    public pansementConfig: FieldConfig[] = [
        {
            type: 'switch_multi_select',
            name: 'localisation',
            options: ['tete', 'bras', 'main', 'coude', 'abdomen', 'thorax', 'dos', 'fesses', 'périné', 'cuisse', 'jambe', 'genou', 'pied'],
            placeholder: 'Localisation',
            // validation: [Validators.required] // TODO fix switch_multi_select & require
        },
        {
            type: 'switch',
            name: 'lateralite',
            options: ['gauche', 'droit'],
            class: 'grid-full',
            placeholder: 'Latéralité'
        },
        {
            type: 'switch',
            name: 'type',
            options: ['sec', 'alcoolisé', 'gras', 'médicamenteux (préciser)'],
            class: 'grid-full',
            placeholder: 'Type'
        },
        {
            type: 'textArea',
            name: 'commentaire',
            placeholder: 'Commentaire'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire RCP
    public rcpConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'va',
            options: ['oui', 'non'],
            placeholder: 'BAVU'
        },
        {
            type: 'switch',
            name: 'compression',
            options: ['oui', 'non'],
            placeholder: 'Compressions Thoraciques'
        },
        {
            type: 'select',
            name: 'defibrillateur',
            options: ['DSA', 'DAE', 'Défibrillateur manuel'],
            placeholder: 'Défibrillation'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            validation: [Validators.required]
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire Aspiration
    public aspirationConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'aspiration',
            options: ['oui', 'non'],
            placeholder: 'Aspiration'
        },
        {
            type: 'switch',
            name: 'abondant',
            options: ['oui', 'non'],
            placeholder: 'Abondant'
        },
        {
            type: 'input',
            name: 'aspect',
            placeholder: 'Aspect',
            validation: [Validators.required]
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire VVP
    public vvpConfig: FieldConfig[] = [
        {
            type: 'select',
            name: 'catheter',
            options: ['14G', '16G', '18G', '20G', '22G'],
            placeholder: 'Catheter (en Gauge)',
            class: 'grid-tier'
        },
        {
            type: 'select',
            name: 'position',
            options: ['main', 'avant-bras', 'Bras', 'jambe', 'jugulaire externe', 'épicranien', 'pied', 'autre'],
            placeholder: 'Position',
            class: 'grid-tier',
        },
        {
            type: 'switch',
            name: 'lateralite',
            options: ['gauche', 'droite'],
            placeholder: 'Lateralite',
            class: 'grid-tier'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire VVC
    public vvcConfig: FieldConfig[] = [
        {
            type: 'select',
            name: 'position',
            options: ['JID', 'JIG', 'Fem Droit', 'Fem Gauch', 'Sous clav Droite', 'Sous Clav Gauche'],
            placeholder: 'Position'
        },
        {
            type: 'switch',
            name: 'echo',
            options: ['oui', 'non'],
            placeholder: 'pose echo-guidée'
        },
        {
            type: 'select',
            name: 'voies',
            options: ['1', '2', '3', '4', '5'],
            placeholder: 'Nombre de voies'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            validation: [Validators.required]
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire Suture
    public sutureConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'site',
            placeholder: 'Site suture'
        },
        {
            type: 'switch',
            name: 'fil',
            options: ['0', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0'],
            placeholder: 'Taille de fil',
            class: 'grid-full'
        },
        {
            type: 'select',
            name: 'typefil',
            options: ['vycril', 'résorbable', 'monocryl'],
            placeholder: 'Type de fil',
            class: 'grid-full'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            validation: [Validators.required]
        },
        {
            type: 'switch',
            name: 'ablation',
            options: ['3', '4', '5', '6', '7', '8', '9'],
            placeholder: 'Nombre de jours pour ablation des fils',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire SNG
    public sngConfig: FieldConfig[] = [
        {
            type: 'select',
            name: 'taille',
            options: ['14', '16', '18'],
            placeholder: 'Taille'
        },
        {
            type: 'switch',
            name: 'position',
            options: ['nasale', 'orale'],
            placeholder: 'Position'
        },
        {
            type: 'select',
            name: 'usage',
            options: ['Aspiration douce', 'Siphonage'],
            placeholder: 'usage'
        },
        {
            type: 'select',
            name: 'verification',
            options: ['oui', 'non'],
            placeholder: 'Vérfication de la position'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            validation: [Validators.required]
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    // formulaire DIO
    public dioConfig: FieldConfig[] = [
        {
            type: 'select',
            name: 'position',
            options: ['Plateau tibial', 'huméral', 'fémoral'],
            placeholder: 'Position'
        },
        {
            type: 'switch',
            name: 'lateralite',
            options: ['gauche', 'droit'],
            placeholder: 'Latéralité'
        },
        {
            type: 'select',
            name: 'type',
            options: ['BIG', 'EZIO', 'manuel'],
            placeholder: 'type'
        },
        {
            type: 'select',
            name: 'taille',
            options: ['Rose 15 mm', 'Bleu 25 mm', 'Jaune 45 mm'],
            placeholder: 'taille'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            validation: [Validators.required]
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public conditionnementConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'couverture',
            options: ['Couverture de survie', 'Couverture bactériostatique', 'autre'],
            placeholder: 'Couverture',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'immobilisation',
            options: ['MID', 'plan dur', 'ACT', 'ceinture pelvienne'],
            placeholder: 'Immobilisation',
            class: 'grid-full'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public buConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'leucocytes',
            options: ['negatif', 'trace', '+ faible', '++ modéré', '+++ forte'],
            placeholder: 'Leucocytes',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'nitrites',
            options: ['negatif', 'positif'],
            placeholder: 'Nitrites',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'urobilinogene',
            options: ['normal < 1', ' 2', '3', '4'],
            placeholder: 'Urobilinogène',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'proteines',
            options: ['négatif', 'trace', '+ 20', '++ 100', '+++ 200', '++++ >2000'],
            placeholder: 'Protéines',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'ph',
            options: ['5.0', '6.0', '6.5', '7.0', '7.5', '8.0', '8.5'],
            placeholder: 'pH',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'sang',
            options: ['négatif', 'trace', '+ faible', '++ modéré', '+++ forte'],
            placeholder: 'Sang',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'sg',
            options: ['1.000', '1.005', '1.010', '1.015', '1.020', '1.025', '1.030'],
            placeholder: 'Gravité spécifique',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'cetones',
            options: ['négatif', 'trace', 'faible', 'modéré', 'fort'],
            placeholder: 'Corps cétoniques',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'bilirubine',
            options: ['négatif', '+ faible', '++ modéré', '+++ fort'],
            placeholder: 'Bilirubine',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'glucose',
            options: ['négatif', '100 mg/ml', '250 mg/ml', '500 mg/ml', '1000 mg/ml', '> 2000 mg/ml'],
            placeholder: 'Glucose',
            class: 'grid-full'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public dcConfig: FieldConfig[] = [
        {
            type: 'separator',
            name: 'controler',
            placeholder: 'Massive Bleeding Control : Contrôler les Hémmoragies',
            class: 'grid-full'
        },
        {
            type: 'separator',
            name: 'compression',
            placeholder: 'Compression direct',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'compressionmoyen',
            options: ['Pansement compressif', 'Pansement Hémostastique', 'Ceinture Pelvienne'],
            placeholder: 'type',
            class: 'grid-full'
        },
        {
            type: 'input',
            name: 'compressionsite',
            placeholder: 'Site',
            class: 'grid-full'
        },
        {
            type: 'separator',
            name: 'garrot',
            placeholder: 'Garrot de membre ',
            class: 'grid-full'
        },
        {
            type: 'input',
            name: 'garrotsite',
            placeholder: 'Site'
        },
        {
            type: 'time',
            name: 'garrotheure',
            placeholder: 'Heure de pose du garrot'
        },
        {
            type: 'separator',
            name: 'A',
            placeholder: 'Airways',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'airways',
            options: ['Retrait CE', 'LVA', 'Coniotomie'],
            placeholder: '',
            class: 'grid-full'
        },
        {
            type: 'separator',
            name: 'B',
            placeholder: 'Breathing',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'breathing',
            options: ['Oxygénation', 'Exsufflation', 'Pansement trois coté', 'valve d asherman'],
            placeholder: '',
            class: 'grid-full'
        },
        {
            type: 'separator',
            name: 'C',
            placeholder: 'Choc',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'choc',
            options: ['VVP', 'SSH 250 ml', 'Adrénaline'],
            placeholder: '',
            class: 'grid-full'
        },
        {
            type: 'separator',
            name: 'D',
            placeholder: 'Head / Hypothermia',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'head',
            options: ['Asymétrie pupilles', 'Couverture de survie'],
            placeholder: '',
            class: 'grid-full'
        },

        {
            type: 'separator',
            name: 'T',
            placeholder: 'Traitements',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'traitements',
            options: ['Exacyl 1 gr', 'Augmentin 2 gr', 'Morphine titration'],
            placeholder: '',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    public suConfig: FieldConfig[] = [
        {
            type: 'switch',
            name: 'type',
            options: ['Sondage à demeure', 'Sondage évacuateur', 'autre'],
            placeholder: 'type',
            class: 'grid-full'
        },
        {
            type: 'switch_multi_select',
            name: 'sonde',
            options: ['foley', 'rush '],
            placeholder: 'type de sonde',
            class: 'grid-full'
        },
        {
            type: 'select',
            name: 'taille',
            options: ['14', '16 ', '18', '20', '22'],
            placeholder: 'Taille de la sonde',
            class: 'grid-full'
        },
        {
            type: 'input',
            name: 'remarques',
            placeholder: 'Remarques',
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        },
    ];

    private subscription: Subscription;

    constructor(private USER: UserService) { }

    ngOnInit() {
      this.subscription = this.USER.profile.subscribe((profile) => {
        this.currentUser = profile;
      });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public submitGeste(value: any) {
        this.geste.push(value);
        this.submitactionGeste.emit(value);
    }

    public close() {
        this.closeForm.emit(true);
    }

    public setStep(index: number) {
        this.step = index;
    }

}

export interface Geste {
    typeGeste: string;
    timeCode?: Date;
    auteur?: string;
};

// Creer un model de ce type pour chaque type de geste

export class Attelle implements Geste {
    typeGeste = 'attelle';
    constructor(public type: string, public position: string, public lateralite: string) { }
}

export class Iot implements Geste {
    typeGeste = 'iot';
    constructor(public tube: string,  public technique: string, public etco2: string, public cormack: string, public pression: string, public repere: string) { }
}

export class Position implements Geste {
    typeGeste = 'position';
    constructor(public position: string) { }
}

export class Pansement implements Geste {
    typeGeste = 'pansement';
    constructor(public localisation: string, public commentaire: string, public lateralite: string, public type: string) { }
}
export class Rcp implements Geste {
    typeGeste = 'rcp';
    constructor(public va: string, public compression: string, public defibrillateur: string, public remarques: string) { }
}

export class Aspiration implements Geste {
    typeGeste = 'aspiration';
    constructor(public aspiration: string, public abondant: string, public aspect: string) { }
}

export class Vvp implements Geste {
    typeGeste = 'vvp';
    constructor(public catheter: string, public lateralite: string, public position: string, public remarques: string) { }
}

export class Vvc implements Geste {
    typeGeste = 'vvc';
    constructor(public position: string, public echo: string, public voies: string, public remarques: string) { }
}

export class Suture implements Geste {
    typeGeste = 'suture';
    constructor(public site: string, public fil: string, public typefil: string, public ablation: string, public remarques: string) { }
}

export class Sng implements Geste {
    typeGeste = 'sng';
    constructor(public taille: string, public position: string, public verification: string, public usage: string, public remarques: string) { }
}

export class Dio implements Geste {
    typeGeste = 'dio';
    constructor(public position: string, public lateralite: string, public type: string, public taille: string, public remarques: string) { }
}

export class Conditionnement implements Geste {
    typeGeste = 'conditionnement';
    constructor(public couverture: string, public immobilisation: string, public remarques: string) { }
}

export class Bu implements Geste {
    typeGeste = 'bu';
    constructor(public leucocytes: string, public nitrites: string, public urobilinogene: string,  public proteines: string, public ph: string, public sang: string, public sg: string, public cetones: string,  public bilirubine: string, public glucose: string, public remarques: string) { }
}

export class Dc implements Geste {
    typeGeste = 'dc';
    constructor(public compressionmoyen: string, public compressionsite: string, public garrot: string, public garrotsite: string, public garrotheure: string, public airways: string, public breathing: string, public choc: string, public head: string, public traitements: string) { }
}

export class Su implements Geste {
    typeGeste = 'su';
    constructor(public type: string, public sonde: string, public taille: string, public remarques: string) { }
}

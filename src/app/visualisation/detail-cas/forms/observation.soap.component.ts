
import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';


@Component({
    selector: 'observation-soap-form',
    template: `

    <mat-accordion>

    <mat-expansion-panel [expanded]="step === 0" (opened)="setStep(0)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Histoire de la maladie et examens clinique
    </mat-panel-title>
    <mat-panel-description>
   
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
              <dynamic-form
              [initialValues]="DARInitialValue"
              [config]="DARConfig"
              #ACRform="dynamicForm"
              (submit)="submit($event)">
              </dynamic-form>   
    </mat-expansion-panel>



     </mat-accordion>

   `
})

export class ObservationoapForm {


    @Output() submitObservationSoap: EventEmitter<any> = new EventEmitter<any>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(DynamicFormComponent) DARform: DynamicFormComponent;



    public DARInitialValue: DAR = new DAR('', '');


    public step = 0;

    public setStep(index: number) {
        this.step = index;
    }


    public DARConfig: FieldConfig[] = [

        {
            type: 'textArea',
            name: 'hdm',
            placeholder: "Histoire de la maladie",
            class: 'grid-full'

        },
        {
            type: 'textArea',
            name: 'examen',
            placeholder: "Examen clinique",
            class: 'grid-full'
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];




    constructor() { }


    submit(value: any) {


        this.submitObservationSoap.emit(value);
    }
    close() {
        this.closeForm.emit(true);
    }
}

export interface Soap {
    type: string,
    timeCode?: Date,
    auteur?: string

};


// Creer un model de ce type pour chaque type de geste
export class DAR implements Soap {
    type = 'DAR';
    constructor(public hdm: string, public examen: string) { }

}


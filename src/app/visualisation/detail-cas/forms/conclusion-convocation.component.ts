
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { UserService } from './../../../services/user.service';





@Component({
    selector: 'conclusion-convoc-form',
    template:
        `
    <dynamic-form 
    [config]="config"
    #convocForm="dynamicForm"
    (submit)="submit($event)">
    </dynamic-form>
    `
})

export class ConclusionConvocForm {

    @Output() submitConvoc: EventEmitter<ConvocModel> = new EventEmitter<ConvocModel>();

    @ViewChild(DynamicFormComponent) convocForm: DynamicFormComponent;

    constructor  (
        private USER: UserService)
    {
    }


    public config: FieldConfig[] = [
        {
            type: 'input',
            name: 'titre',
            placeholder: 'Titre',
            class: "grid-full"
        },
        {
            type: 'textArea',
            name: 'texte',
            placeholder: 'Texte',
            class: "grid-full"
        },
        {
            type: 'input',
            name: 'input1',
            placeholder: 'Texte',
            class: "grid-full"
        },
        {
            type: 'input',
            name: 'input2',
            placeholder: 'Texte',
            class: "grid-full"
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        }];


    public submit(val) {
        let convoc: ConvocModel = new ConvocModel();
        //let user      = this.USER.INFO;
        convoc.titre  = val.titre;
        convoc.input1 = val.input1;
        convoc.input2 = val.input2;
        convoc.texte  = val.texte;
        //convoc.rpps   = user.rpps;
        this.submitConvoc.emit(convoc);

    }


}

export class ConvocModel {
    timeCode?: Date;
    auteur?: string;
    rpps?: string;
    metier?: string;
    texte: string;
    titre: string;
    input1: string;
    input2: string;
    constructor() { }
};


import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';


@Component({
    selector: 'observation-surveillance-form',
    template: `

    <mat-accordion>

    <mat-expansion-panel [expanded]="step === 0" (opened)="setStep(0)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Paramètres Vitaux
    </mat-panel-title>
    <mat-panel-description>
   
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
              <dynamic-form
              [initialValues]="ParamInitialValue"
              [config]="ParamConfig"
              #Paramform="dynamicForm"
              (submit)="submit($event)">
              </dynamic-form>   
    </mat-expansion-panel>

     </mat-accordion>

   `
})

export class ObservationSurveillanceForm {


    @Output() submitObservationSurveillance: EventEmitter<any> = new EventEmitter<any>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(DynamicFormComponent) Paramform: DynamicFormComponent;



    public ParamInitialValue: Param = new Param('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');


    public step = 0;

    public setStep(index: number) {
        this.step = index;
    }

    private glasgowElements = {
        dsgy: 4,
        dsgv: 6,
        dsgm: 5

    };
    private glasgow: number = 15;
    public ParamConfig: FieldConfig[] = [

        {
            type: 'time',
            name: 'heure',
            inputType: 'time',
            placeholder: "Heure de recueil de données",
            class: 'grid-full'

        },
        {
            type: 'input',
            name: 'pasd',
            validation: [Validators.min(30), Validators.max(300)],
            feedBacks: [{ min: 'ce champs doit être entre 30 et 300' }, { max: 'ce champs doit être entre 30 et 300' }],
            inputType: 'number',
            placeholder: "PA systolique droite"

        },
        {
            type: 'input',
            name: 'padd',
            validation: [Validators.min(30), Validators.max(300)],
            feedBacks: [{ min: 'ce champs doit être entre 30 et 300' }, { max: 'ce champs doit être entre 30 et 300' }],
            inputType: 'number',
            placeholder: "PA diastolique droite"

        },
        
        {
            type: 'input',
            name: 'pasg',
            validation: [Validators.min(30), Validators.max(300)],
            feedBacks: [{ min: 'ce champs doit être entre 30 et 300' }, { max: 'ce champs doit être entre 30 et 300' }],
            inputType: 'number',
            placeholder: "PA systolique gauche"


        },
        {
            type: 'input',
            name: 'padg',
            validation: [Validators.min(30), Validators.max(300)],
            feedBacks: [{ min: 'ce champs doit être entre 30 et 300' }, { max: 'ce champs doit être entre 30 et 300' }],
            inputType: 'number',
            placeholder: "PA diastolique gauche"


        },
        {
            type: 'input',
            name: 'fc',
            validation: [Validators.min(30), Validators.max(300)],
            feedBacks: [{ min: 'ce champs doit être entre 30 et 300' }, { max: 'ce champs doit être entre 30 et 300' }],
            inputType: 'number',
            placeholder: "FC"

        },
        {
            type: 'select',
            name: 'trc',
            options: ['< 1"', '< 3"', '> 3"'],
            placeholder: "TRC"

        },
        {
            type: 'input',
            name: 'fr',
            inputType: 'number',
            placeholder: "FR",
            class: 'grid-full',
            validation: [Validators.min(6), Validators.max(50)]
        },
        {
            type: 'input',
            name: 'spo2',
            validation: [Validators.max(100)],
            feedBacks: [{ max: 'ce champs doit être inférieur ou égal à 100' }],
            placeholder: "SpO2"

        },
        {
            type: 'select',
            name: 'oxygene',
            options: ['Air Ambiant', '1 à 4 l/m lunettes', '> 4l /m masque', '> 5l/m masque', '> 6l/m MHC', 'intubé'],
            placeholder: "oxygène"
        },
        {
            type: 'input',
            name: 'glycemie',
            inputType: 'number',
            placeholder: "Glycémie"
        },
        {
            type: 'switch',
            name: 'uniteglycemie',
            options: ['mmol.l-1', 'g.dl']
        },
        {
            type: 'input',
            name: 'evs',
            validation: [Validators.min(0), Validators.max(10)],
            feedBacks: [{ min: 'ce champs doit être entre 0 et 10' }, { max: 'ce champs doit être entre 0 et 10' }],
            inputType: 'number',
            placeholder: "EVS /10"
        },
        {
            type: 'input',
            name: 'temperature',
            placeholder: "Température C° "
        },
        {
            type: 'input',
            name: 'hemocue',
            inputType: 'number',
            placeholder: "Hémocue g/dl"
        },
        {
            type: 'input',
            name: 'etco2',
            inputType: 'number',
            placeholder: "EtCO2 mm Hg",
            validation: [Validators.min(0), Validators.max(50)]
        }, {
            type: 'separator',
            name: 'controler',
            placeholder: "Glasgow",
            class: "grid-full"
        },
        {
            type: 'select',
            name: 'dsgy',
            options: ['1', '2', '3', '4'],
            placeholder: "Yeux",
            class: "grid-quart",
            valueChanges: (val) => {
                this.glasgowElements.dsgy = parseInt(val);
                this.defineGlasgow();
            }
        },
        {
            type: 'select',
            name: 'dsgv',
            options: ['1', '2', '3', '4', '5'],
            placeholder: "Verbal",
            class: "grid-quart",
            valueChanges: (val) => {
                this.glasgowElements.dsgv = parseInt(val);
                this.defineGlasgow();
            }
        },
        {
            type: 'select',
            name: 'dsgm',
            options: ['1', '2', '3', '4', '5', '6'],
            placeholder: "Moteur",
            class: "grid-quart",
            valueChanges: (val) => {
                this.glasgowElements.dsgm = parseInt(val);
                this.defineGlasgow();
            }
        },
        {
            type: 'input',
            name: 'glasgow',
            value: this.glasgow,
            validation: [Validators.min(3), Validators.max(15)],
            placeholder: "Glasgow",
            class: "grid-quart"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];




    constructor() { }
    private defineGlasgow() {
        let glasgow = 0;
        Object.keys(this.glasgowElements).forEach(k => glasgow += parseInt(this.glasgowElements[k]))
        this.Paramform.setValue('glasgow', glasgow);
        this.glasgow = glasgow;

    }

    public submit(value: any) {

        this.submitObservationSurveillance.emit(value);
    }
    close() {
        this.closeForm.emit(true);
    }
}

export interface Surveillance {
    type: string,
    timeCode?: Date,
    auteur?: string

};


// Creer un model de ce type pour chaque type de geste
export class Param implements Surveillance {
    type = 'Param';
    constructor(
        public pasd: string,
        public padd: string,
        public pasg: string,
        public padg: string,
        public fc: string,
        public trc: string,
        public fr: string,
        public spo2: string,
        public oxygene: string,
        public glycemie: string,
        public uniteglycemie: string,
        public evs: string,
        public temperature: string,
        public hemocue: string,
        public etco2: string,
        public glasgow: string
    ) { }

}


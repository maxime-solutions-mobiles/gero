
import { PatientModel } from './../../models/patient.model';
import { DynamicFormComponent } from '../../../forms/dynamic-form.component';
import { FieldConfig } from '../../../forms/field-config.interface';
import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';


@Component({
    selector: 'observation-clinique-form',
    template: `

    <mat-accordion>

    <mat-expansion-panel [expanded]="step === 0" (opened)="setStep(0)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Prise en charge ACR
    </mat-panel-title>
    <mat-panel-description>
   
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
              <dynamic-form
              [initialValues]="ACRInitialValue"
              [config]="ACRConfig"
              #ACRform="dynamicForm"
              (submit)="submit($event)">
              </dynamic-form>   
    </mat-expansion-panel>

    <mat-expansion-panel [expanded]="step === 1" (opened)="setStep(1)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Douleur Thoracique
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
             <dynamic-form
              [initialValues]="DTInitialValue"
              [config]="DTConfig"
              #DTform="dynamicForm"
              (submit)="submit($event)">
             </dynamic-form>   
    </mat-expansion-panel> 


    <mat-expansion-panel [expanded]="step === 2" (opened)="setStep(2)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Détresse respiratoire
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
             <dynamic-form
              [initialValues]="DRInitialValue"
              [config]="DRConfig"
              #DRform="dynamicForm"
              (submit)="submit($event)">
             </dynamic-form>   
    </mat-expansion-panel> 

    <mat-expansion-panel [expanded]="step === 3" (opened)="setStep(3)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Troubles Neurologiques
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
             <dynamic-form
              [initialValues]="TNInitialValue"
              [config]="TNConfig"
              #TNform="dynamicForm"
              (submit)="submit($event)">
             </dynamic-form>   
    </mat-expansion-panel> 


    <mat-expansion-panel [expanded]="step === 4" (opened)="setStep(4)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            ECG
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
             <dynamic-form
              [initialValues]="ECGInitialValue"
              [config]="ECGConfig"
              #ECGform="dynamicForm"
              (submit)="submit($event)">
             </dynamic-form>   
    </mat-expansion-panel> 



    <mat-expansion-panel [expanded]="step === 5" (opened)="setStep(5)" hideToggle="true">
    <mat-expansion-panel-header>
    <mat-panel-title>
            Accouchement
    </mat-panel-title>
    <mat-panel-description>
    </mat-panel-description>
    
    </mat-expansion-panel-header>  
             <dynamic-form
              [initialValues]="AccouchementInitialValue"
              [config]="AccouchementConfig"
              #Accouchementform="dynamicForm"
              (submit)="submit($event)">
             </dynamic-form>   
    </mat-expansion-panel> 


     </mat-accordion>


    `
})

export class ObservationcliniqueForm {

    @Output() submitObservationClinique: EventEmitter<any> = new EventEmitter<any>();
    @Output() closeForm: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(DynamicFormComponent) ACRform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) DTform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) Accouchementform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) DRform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) TNform: DynamicFormComponent;
    @ViewChild(DynamicFormComponent) ECGform: DynamicFormComponent;
    


    public ACRInitialValue: ACR = new ACR('', '', '', '', '', '', '', '');
    public DTInitialValue: DT = new DT('', '', '', '', '', '', '');
    public AccouchementInitialValue: Accouchement = new Accouchement('', '', '', '', '', '', '', '', '', '', '', '', '');
    public DRInitialValue: DR = new DR('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    public TNInitialValue: TN = new TN('', '', '', '');
    public ECGInitialValue: ECG = new ECG('', '', '', '', '', '', '','');

    public step = 0;

    public setStep(index: number) {
        this.step = index;
    }


    public ACRConfig: FieldConfig[] = [

        {
            type: 'input',
            name: 'heureacr',
            placeholder: "Heure supposée de l'ACR"
        },
        {
            type: 'input',
            name: 'heurepec',
            placeholder: "Heure prise en charge"
        },
        {
            type: 'input',
            name: 'noflow',
            placeholder: "durée de No flow"
        },
        {
            type: 'input',
            name: 'lowflow',
            placeholder: "durée de Low flow"
        },
        {
            type: 'switch',
            name: 'ecg',
            options: ['Asystolie', 'FV', 'DEM', 'TV'],
            placeholder: "ECG à prise en charge"
        },
        {
            type: 'switch',
            name: 'dsa',
            options: ['oui', 'non'],
            placeholder: "Utilisation DSA / DM"
        },
        {
            type: 'select',
            name: 'chocs',
            options: ['1', '2', '3', '4', '5', '6', '7'],
            placeholder: "Nombre de chocs"
        },
        {
            type: 'input',
            name: 'heureracs',
            placeholder: "Heure de RACS"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];


    public DTConfig: FieldConfig[] = [

        {
            type: 'input',
            name: 'heuredebut',
            placeholder: "Heure  de début de la douleur"
        },
        {
            type: 'input',
            name: 'duree',
            placeholder: "Durée de la douleur"
        },

        {
            type: 'switch',
            name: 'tnt',
            options: ['oui', 'non', 'inconnu'],
            placeholder: "Sensible à la TNT"
        },
        {
            type: 'switch',
            name: 'palpation',
            options: ['oui', 'non', 'inconnu'],
            placeholder: "Reproductible à la palpation"
        },
        {
            type: 'switch',
            name: 'inspiration',
            options: ['oui', 'non', 'inconnu'],
            placeholder: "Majoration à inspiration"
        },
        {
            type: 'switch',
            name: 'description',
            options: ['irradiation', 'serrement', 'écrasement', 'barre', 'constrictive'],
            placeholder: "Description de la douleur"
        },
        {
            type: 'textArea',
            name: 'commentaire',
            placeholder: "Commentaire"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];

    public AccouchementConfig: FieldConfig[] = [

        {
            type: 'select',
            name: 'gestite',
            options: ['0', '1', '2', '3', '4', '5', '6', '7'],
            placeholder: "Gestité"
        },
        {
            type: 'select',
            name: 'parite',
            options: ['0', '1', '2', '3', '4', '5', '6', '7'],
            placeholder: "Parité"
        },
        {
            type: 'input',
            name: 'terme',
            placeholder: "Terme (en SA)"
        },
        {
            type: 'input',
            name: 'dateterme',
            placeholder: "Date du Terme"
        },
        {
            type: 'switch',
            name: 'contractionduree',
            options: ['< 1min', '>1min', 'continue'],
            placeholder: 'Durée des Contraction'
        },
        {
            type: 'switch',
            name: 'contractioninterv',
            options: ['< 1min', '>1min', '> 5 min'],
            placeholder: "Intervalles des contraction"
        },
        {
            type: 'textArea',
            name: 'examen',
            placeholder: "Examen de la patiente"
        },
        {
            type: 'textArea',
            name: 'probleme',
            placeholder: "Problèmes connus de la grossesse"
        },
        {
            type: 'textArea',
            name: 'commentaire',
            placeholder: "Commentaire"
        },
        {
            type: 'input',
            name: 'heurenaissance',
            placeholder: "Heure Naissance"
        },
        {
            type: 'input',
            name: 'apgar1',
            class: 'grid-tier',
            placeholder: "Apgar à 1 minute"
        },
        {
            type: 'input',
            name: 'apgar3',
            class: 'grid-tier',
            placeholder: "Apgar à 3 minute"
        },
        {
            type: 'input',
            name: 'apgar5',
            class: 'grid-tier',
            placeholder: "Apgar à 5 minute"
        },
        {
            type: 'input',
            name: 'heuredelivrance',
            placeholder: "Heure de la délivrance"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];


    public DRConfig: FieldConfig[] = [

        {
            type: 'select',
            name: 'Auscultation',
            options: ['Sibilant', 'Crépitant', 'Silence'],
            placeholder: "auscultation"
        },


        {
            type: 'separator',
            name: 'lutte',
            inputType: 'text',
            placeholder: 'Signes de lutte '
        },
        {
            type: 'switch',
            name: 'balancement',
            options: ['oui', 'non'],
            placeholder: "Balancement thoraco abdo"
        },
        {
            type: 'switch',
            name: 'tirage',
            options: ['oui', 'non'],
            placeholder: "Tirage"
        },
        {
            type: 'switch',
            name: 'turgecence',
            options: ['oui', 'non'],
            placeholder: "Turgecence des jugulaires"
        },
        {
            type: 'switch',
            name: 'difficulteaparler',
            options: ['oui', 'non'],
            placeholder: "Difficulté à parler"
        },
        {
            type: 'separator',
            name: 'fatigue',
            inputType: 'text',
            placeholder: 'Signes de fatigue respiratoire '
        },
        {
            type: 'switch',
            name: 'troubleconscience',
            options: ['oui', 'non'],
            placeholder: "Trouble de la conscience"
        },
        {
            type: 'switch',
            name: 'coma',
            options: ['oui', 'non'],
            placeholder: "Coma"
        },
        {
            type: 'switch',
            name: 'cyanose',
            options: ['oui', 'non'],
            placeholder: "Cyanose"
        },
        {
            type: 'separator',
            name: 'hypercapnie',
            inputType: 'text',
            placeholder: 'Signes d hypercapnie'
        },
        {
            type: 'switch',
            name: 'sueurs',
            options: ['oui', 'non'],
            placeholder: "Sueurs"
        },
        {
            type: 'switch',
            name: 'hta',
            options: ['oui', 'non'],
            placeholder: "Hypertension artérielle"
        },
        {
            type: 'switch',
            name: 'agitation',
            options: ['oui', 'non'],
            placeholder: "Agitation"
        },
        {
            type: 'separator',
            name: 'cardiovasculaires',
            inputType: 'text',
            placeholder: 'Signes cardio-vasculaires, hémodynamiques et cardiaques droits'
        },
        {
            type: 'switch',
            name: 'tachycardie',
            options: ['oui', 'non'],
            placeholder: "Tachycardie"
        },
        {
            type: 'switch',
            name: 'marbrures',
            options: ['oui', 'non'],
            placeholder: "Marbrures"
        },
        {
            type: 'switch',
            name: 'signechoc',
            options: ['oui', 'non'],
            placeholder: "Signes de choc"
        },
        {
            type: 'switch',
            name: 'signedroit',
            options: ['oui', 'non'],
            placeholder: "Signe de coeur droit"
        },
        {
            type: 'separator',
            name: 'commentaire',
            inputType: 'text',
            placeholder: 'Observation libres'
        },
        {
            type: 'textArea',
            name: 'commentaires',
            placeholder: "Commentaire"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];


    public TNConfig: FieldConfig[] = [

        {
            type: 'separator',
            name: 'neurologiques',
            inputType: 'text',
            placeholder: 'Signes Neurologiques'
        },
        {
            type: 'switch',
            name: 'pci',
            options: ['oui', 'non'],
            placeholder: "PCI"
        },
        {
            type: 'switch',
            name: 'agitation',
            options: ['oui', 'non'],
            placeholder: "Agitation"
        },
        {
            type: 'switch',
            name: 'obnubilation',
            options: ['oui', 'non'],
            placeholder: "Obnubilation"
        },
        {
            type: 'switch',
            name: 'pupillessymetriques',
            options: ['oui', 'non'],
            placeholder: "Pupilles symétriques"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];

    public ECGConfig: FieldConfig[] = [

        {
            type: 'separator',
            name: 'ecg',
            inputType: 'text',
            placeholder: 'Lecture ECG'
        },
        {
            type: 'switch',
            name: 'regulier',
            options: ['oui', 'non'],
            placeholder: "Régulier"
        },
        {
            type: 'switch',
            name: 'sinusal',
            options: ['oui', 'non'],
            placeholder: "sinusal"
        },
        {
            type: 'switch',
            name: 'frequence',
            options: ['Normale', 'Tachycarde', 'Bradycarde'],
            placeholder: "Fréquence cardiaque"
        },
        {
            type: 'switch',
            name: 'axe',
            options: ['Normal', 'Droit', 'Gauche'],
            placeholder: "Axe"
        },
        {
            type: 'switch',
            name: 'susdecalagest',
            options: ['oui', 'non'],
            placeholder: "présence sus décalage ST"
        },
        {
            type: 'switch',
            name: 'bav',
            options: ['oui', 'non'],
            placeholder: "BAV"
        },
        {
            type: 'switch',
            name: 'fa',
            options: ['oui', 'non'],
            placeholder: "FA"
        },
        {
            type: 'textArea',
            name: 'commentaires',
            placeholder: "Commentaire"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }];


    constructor() { }


    submit(value: any) {


        this.submitObservationClinique.emit(value);
    }
    close() {
        this.closeForm.emit(true);
    }
}

export interface Clinique {
    type: string,
    timeCode?: Date,
    auteur?: string

};


// Creer un model de ce type pour chaque type de geste
export class ACR implements Clinique {
    type = 'ACR';
    constructor(public heureacr: string, public heurepec: string, public noflow: string, public lowflow: string, public ecg: string, public dsa: string, public chocs: string, public heureracs: string) { }

}

export class DT implements Clinique {
    type = 'DT';
    constructor(public heuredebut: string, public duree: string, public tnt: string, public palpation: string, public inspiration: string, public popo: string, public commentaire: string) { }

}
export class Accouchement implements Clinique {
    type = 'Accouchement';
    constructor(public gestite: string, public parite: string, public terme: string, public dateterme: string, public contractionduree: string, public contractioninterv: string, public examen: string, public problemes: string, public heurenaissance: string, public apgar1: string, public apgar3: string, public apgar5: string, public heuredelivrance: string) { }

}

export class DR implements Clinique {
    type = 'DR';
    constructor(public lutte: string, public balancement: string, public tirage: string, public turgecence: string, public difficulteaparler: string, public fatigue: string, public troubleconscience: string, public coma: string, public cyanose: string, public hypercapnie: string, public sueurs: string, public hta: string, public agitation: string, public tachycardie: string, public marbrures: string, public signechoc: string, public signedroit: string, public commentaires: string) { }

}

export class TN implements Clinique {
    type = 'TN';
    constructor(public pci: string, public agitation: string, public obnubilation: string, public pupillessymetriques: string) { }

}

export class ECG implements Clinique {
    type = 'ECG';
    constructor(public regulier: string, public sinusal: string, public frequence: string, public axe: string, public susdecalagest: string, public bav: string, public fa: string, public commentaire: string) { }

}
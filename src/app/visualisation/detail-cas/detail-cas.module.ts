
import { MatStepperModule } from '@angular/material/stepper';
import { MatExpansionModule } from '@angular/material/expansion';


import { DynamicFormModule } from '../../forms/dynamic-form.module';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatTabsModule, MatButtonToggleModule, MatTableModule, MatSortModule, MatPaginatorModule, MatCheckboxModule } from '@angular/material';

import { CommonModule } from '@angular/common';

import { DetailCasComponent } from './detail-cas.component';
import { DetailMenuComponent } from './components/detail-menu.component';
import { DetailFooter } from './components/detail-footer.component';

import { DetailObservationComponent } from './components/detail-observation.component';
import { DetailactionComponent } from './components/detail-action.component';
import { DetailBodyComponent } from './components/detail-body.component';

import { DetailInfoComponent } from './components/detail-info.component';
import { DetailConclusionComponent } from './components/detail-conclusion.component';
import { DetailNouveauCasComponent } from './detail-nouveau-cas.component';
import { PatientAdministratifForm } from './forms/patient-administratif.component';
import { ActionGesteForm } from './forms/action-geste.component';
import { ActiontraitementForm } from './forms/action-traitement.component';
import { ObservationCirconstancielleForm } from './forms/observation.circonstancielle.component';
import { ObservationVitaleForm } from './forms/observation.vitale.component';
import { ObservationSurveillanceForm } from './forms/observation.surveillance.component';
import { ObservationcliniqueForm } from './forms/observation.clinique.component';
import { ObservationoapForm } from './forms/observation.soap.component';


import { ConclusionRegulationForm } from './forms/conclusion-regulation.component';
import { ObservationExposureForm } from './forms/observation-exposure.component';
import { ListeModule } from '../liste/liste.module';
import { ConclusionOrdonnanceForm } from './forms/conclusion-ordonnance.component';
import { ConclusionCourierForm } from './forms/conclusion-courier.component';
import { ConclusionConvocForm } from './forms/conclusion-convocation.component';



@NgModule({
    imports: [
        CommonModule,
        DynamicFormModule,
        MatButtonModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatExpansionModule,
        MatStepperModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        ListeModule,
        MatCheckboxModule

    ],
    declarations: [
        DetailMenuComponent,
        DetailInfoComponent,
        DetailactionComponent,
        DetailObservationComponent,
        DetailConclusionComponent,
        DetailCasComponent,
        DetailNouveauCasComponent,
        DetailFooter,
        PatientAdministratifForm,
        ActionGesteForm,
        ActiontraitementForm,
        ObservationCirconstancielleForm,
        ObservationVitaleForm,
        ObservationSurveillanceForm,
        ObservationcliniqueForm,
        ObservationoapForm,
        ObservationExposureForm,
        ConclusionRegulationForm,
        ConclusionOrdonnanceForm,
        ConclusionCourierForm,
        ConclusionConvocForm,
        DetailBodyComponent,
    ],
    exports: [
        DetailCasComponent,
        DetailNouveauCasComponent,
        DetailFooter,
        PatientAdministratifForm,
        ActionGesteForm,
        ActiontraitementForm,
        ObservationCirconstancielleForm,
        ObservationVitaleForm,
        ObservationSurveillanceForm,
        ObservationcliniqueForm,
        ObservationoapForm,
        ObservationExposureForm,
        ConclusionRegulationForm,
        ConclusionOrdonnanceForm,
        ConclusionCourierForm,
        ConclusionConvocForm
    ]
})
export class DetailCasModule { }

import { VisualistationService } from './../visualisation.service';
import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';



@Component({
    selector: 'detail-cas',
    template: `
    <detail-menu></detail-menu>

    <div id="detail-content" [ngSwitch]="(this.VISU.voletDisplay | async)?.rubrique || 'info'">
        <detail-info *ngSwitchCase="'info'"></detail-info>
        <detail-observation  *ngSwitchCase="'observation'"></detail-observation>
        <detail-action  *ngSwitchCase="'action'" ></detail-action>
        <detail-conclusion   *ngSwitchCase="'conclusion'"></detail-conclusion>
    </div>
    `

})

export class DetailCasComponent {
    constructor(public VISU: VisualistationService) { }


}

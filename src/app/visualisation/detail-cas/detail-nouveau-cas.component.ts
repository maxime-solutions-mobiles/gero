import { PassageModel } from './../models/passage.model';
import { ParametreService } from './../../services/parametre.service';
import { ListService } from './../../services/list.service';
import { CustomValidatorService } from './../../forms/custom-validator.service';
import { PatientModel } from './../models/patient.model';
import { DynamicFormComponent } from '../../forms/dynamic-form.component';
import { FieldConfig } from '../../forms/field-config.interface';
import { CasModel } from './../models/cas.model';
import { Component, Input, Output, EventEmitter, ViewChild, SimpleChanges, OnDestroy, OnInit } from '@angular/core';
import { DBService } from '../../services/db.service';
import { Validators } from '@angular/forms';
import { VisualistationService } from '../visualisation.service';
import { UserService } from '../../services/user.service';
import { CasService } from '../../services/cas.service';
import { InterventionModel } from '../models/intervention.model';
import { InterventionService } from '../../services/intervention.service';
import { PassageService } from 'src/app/services/passage.service';


@Component({
    selector: 'detail-nouveau-cas',
    template: `
        <div *ngIf="config">
        <div class="nouveau-cartouche-option" [ngClass]="{'active':associate}" *ngIf="this.cas && !this.cas._id && numero">
        Associer à l'intervention #{{numero}}
        <!--<span [ngClass]="{'active':associate}" (click)="associate = !associate"><i class="fal fa-check"></i></span>-->
        <span><mat-checkbox class="example-margin" (change)="associate = !associate"></mat-checkbox></span>

        </div>
            <dynamic-form
                    valid
                    [config]="config"
                    #form="dynamicForm"
                    (submit)="submit($event)">
            </dynamic-form>
        </div>
        <div *ngIf="!config"> <i class="fas fa-spinner-third fa-spin waitPicto"></i></div>
    `

})

export class DetailNouveauCasComponent implements OnInit, OnDestroy {
    private intervention: InterventionModel;
    private subscription: any;
    @Input() cas: CasModel;
    public numero: any;
    public associate: boolean;
    ngOnChanges(changes: SimpleChanges) {
        if (!this.form) return;
        this.settingVals = true;
        this.cas = changes.cas.currentValue;

        if (this.cas && this.cas._id) {
            this.curVals = this.cas;
            this.initForm(this.cas).then(r => { this.form.setDisabled('niv', true); this.settingVals = false });
        } else this.initData()
    }
    public NIV: any;
    private settingVals: boolean = false;
    @ViewChild(DynamicFormComponent) form: DynamicFormComponent;
    public config: FieldConfig[];
    private patient: PatientModel;
    private curVals: CasModel;
    // private dynamicForm

    @Output() searchDB: EventEmitter<any> = new EventEmitter<any>();

    ngAfterViewInit() {
        this.subscription = this.VISU.selectPatient.subscribe(newPatient => {
            this.patient = newPatient;
            this.form.setValue('nom', newPatient.nom);
            this.form.setValue('niv', newPatient.niv);
            this.form.setValue('prenom', newPatient.prenom);
            this.form.setValue('age', newPatient.age);
            this.form.setValue('sexe', newPatient.sexe);
            this.form.setValue('patient', newPatient._id);
            this.form.setValue('types', newPatient.types);
            this.form.setValue('typeprecision', newPatient.typeprecision);
        })
    }

    constructor(
        private DB: DBService,
        private VISU: VisualistationService,
        private USER: UserService,
        private customValidator: CustomValidatorService,
        private CAS: CasService,
        private INTER: InterventionService,
        private PARAM: ParametreService,
        private LIST: ListService,
        private PASSAGE: PassageService,

    ) { }

    ngOnInit() {
        if (!this.cas) this.initData().then(vals => this.initForm(vals));
        else {
            this.curVals = this.cas;
            this.initForm(this.cas).then(r => { this.form.setDisabled('niv', true); });
        }
        let current = this.VISU.currentURL.current
        if (!current) return;
        let table = current.split(':')[0];
        if (table != 'intervention') return;
        this.DB.getById(table, current).then(item => {
            if (item.numero) this.numero = item.numero;
            if (item) this.intervention = item;

        })
    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    private initForm(valsToSet) {
        return new Promise((resolve) => {
            let vals: any;
            if (this.cas && this.cas._id) vals = { ...valsToSet, ...this.cas };
            else vals = { ...valsToSet };
            if (this.PARAM.currentParametre.niv && this.PARAM.currentParametre.niv[0] && this.PARAM.currentParametre.niv[0]._id) {
                this.USER.INFO.niv = this.PARAM.currentParametre.niv[0].value;
            }
            let nivDisable: boolean = (this.cas && this.cas.niv || !this.USER.INFO.niv) ? true: false;

            this.config = [
                {
                    type: 'input',
                    name: 'niv',
                    inputType: 'text',
                    placeholder: 'NIV',
                    //value: vals.niv,
                    value: nivDisable? vals.niv: '',
                    //value: nivDisable? '': vals.niv,
                    validation: this.USER.INFO.niv?[Validators.required, Validators.minLength(4), Validators.maxLength(8)] : [Validators.required],
                    disabled: nivDisable,
                    // readonly: nivDisable,
                    // TODO : NIV EXIST REMOVED FOR OXFAM, how deal with that problem fast...
                    //asyncValidator: [this.customValidator.nivExist(this.DB)],
                    valueChanges: (niv) => {
                        if (niv && niv != this.curVals.niv) {
                            let query = { $regex: new RegExp(niv, "gi") };
                            this.searchDB.emit({ niv: query });
                            this.curVals.niv = niv;
                        }
                    },
                    focused: true
                },
                {
                    type: 'input',
                    name: 'sinus',
                    inputType: 'text',
                    value: vals.sinus,
                    placeholder: 'SINUS'
                },
                {
                    type: 'input',
                    name: 'nom',
                    inputType: 'text',
                    placeholder: 'Nom',
                    disabled: vals.nom,
                    value: vals.nom,
                    valueChanges: (nom) => {
                        if (nom && nom != this.curVals.nom) {
                            let query = { $regex: new RegExp(nom, "gi") };
                            this.searchDB.emit({ nom: query });
                            this.curVals.nom = nom;
                        }
                    },
                    validation: [Validators.required],
                    focused: nivDisable
                },
                {
                    type: 'input',
                    name: 'prenom',
                    inputType: 'text',
                    placeholder: 'Prenom',
                    disabled: vals.prenom && vals.prenom.trim(),
                    value: vals.prenom,
                    valueChanges: (prenom) => {
                        if (prenom != this.curVals.prenom && prenom != null) {
                            let query = { $regex: new RegExp(prenom, "gi") };
                            this.searchDB.emit({ prenom: query });
                            this.curVals.prenom = prenom;
                        }
                    }
                },
                {
                    type: 'date',
                    name: 'age',
                    value: vals.age,
                    disabled: vals.age,
                    inputType: 'number',
                    placeholder: 'DDN'
                },
                {
                    type: 'switch',
                    name: 'sexe',
                    value: vals.sexe,
                    disabled: vals.sexe,
                    options: ['homme', 'femme'],
                    placeholder: 'Sexe',
                    class: 'm-f grid-tier'
                },
                {
                    type: 'select',
                    name: 'types',
                    value: vals.types,
                    options: this.PARAM.currentParametre.types ? this.PARAM.currentParametre.types.map(l => l.nom) : [],
                    placeholder: 'Type'
                },
                {
                    type: 'select',
                    name: 'modes',
                    value: vals.modes,
                    options: this.PARAM.currentParametre.modes ? this.PARAM.currentParametre.modes.map(l => l.nom) : [],
                    placeholder: "Mode d'admission"
                },
                {
                    type: 'input',
                    name: 'typeprecision',
                    placeholder: "précision du type de patient",
                    class: "grid-full",
                    value: vals.typeprecision,
                },
                {
                    type: 'select',
                    name: 'pathologie',
                    value: vals.pathologie,
                    validation: [Validators.required],
                    //options: this.PARAM.currentParametre.motif ? this.PARAM.currentParametre.motif.map(l => l.nom) : [],
                    options:["Malaise", "Traumatologie",  "suivi", "Allergie","Cardio-vasculaire","Dermatologie","Digestif","Douleur dentaire","Endocrinologie","Gynécologie","Infectieux / Parasite","Intoxication Ethylique","Neurologie ", "Ophtalmologie","Pathologie musculosqueletique", "Détresse respiratoire",  "Urologie - néphrologie", "Trouble de l'humeur", "consultation médicale initiale", "Autre"],
                    placeholder: "motif"
                },
                {
                    type: 'select',
                    name: 'poste',
                    options: this.PARAM.currentParametre.poste ? this.PARAM.currentParametre.poste.map(l => l.nom) : [],
                    placeholder: this.USER.INFO.poste || 'poste',
                    value: vals.poste || this.USER.INFO.poste,
                    validation: [Validators.required]
                },
                {
                    type: 'textArea',
                    name: 'note',
                    placeholder: "précision du motif de prise en charge",
                    class: "grid-full",
                    value: vals.note,
                },
                {
                    type: 'switch',
                    name: 'urgence',
                    options: ['u1', 'u2', 'u3'],
                    class: 'urgence',
                    value: vals.urgence || 'u3',

                },
                {
                    label: 'Valider',
                    name: 'submit',
                    type: 'button',
                    validation: [Validators.required]
                }
            ];

            resolve();
        });
    }
    private initData() {
        this.cas = new CasModel();
        this.curVals = new CasModel();
        let prefix = 'NIV-';


        let poste = 'PC-';
        let detailPoste: any;
        if (this.USER.INFO.poste)
            detailPoste = this.USER.INFO.poste;

        const nick = this.USER.INFO.id.slice(-4).toUpperCase() + '-';
        // if (detailPoste) nick = detailPoste.substring(4, 8) + '-';
        // else nick = poste;




        return this.DB.getNivList().then(nivList => {

            let autoGenUsedNumbers = nivList.filter(niv => niv.includes(nick))
                .map(niv => parseInt(niv.replace(nick, '')))
                .sort((a, b) => a - b);

            if (autoGenUsedNumbers.length == 0) {
                this.curVals.niv = nick + 1;
                return this.curVals;
            }

            let lastIndex = autoGenUsedNumbers.findIndex((e, i, a) => e > a[i - 1] + 1) - 1;
            if (lastIndex == -2) {
                this.curVals.niv = nick + (autoGenUsedNumbers.length + 1);
                return this.curVals;
            }

            let nextNumber = autoGenUsedNumbers[lastIndex] + 1;
            this.curVals.niv = nick + nextNumber;
            return this.curVals;
        })





    }
    public newInter() {
        this.VISU.navTo({ intervention: 'new' });
    }

    @Output() nouveauCas: EventEmitter<any> = new EventEmitter<any>();

    public async submit(value: any) {



        // let poste = value.poste;

        // define cas
        let cas: CasModel = { ...this.cas, ...value };

        cas.timeCode = new Date();
        // define patient datas
        let patientData: PatientModel = new PatientModel();
        if (cas.nom) patientData.nom = cas.nom;
        if (cas.niv) patientData.niv = cas.niv;
        if (cas.prenom) patientData.prenom = cas.prenom;
        if (cas.age) patientData.age = cas.age;
        if (cas.sexe) patientData.sexe = cas.sexe;
        if (cas.types) patientData.types = cas.types;
        if (cas.typeprecision) patientData.typeprecision = cas.typeprecision;

        if (this.patient && this.patient._id) cas.patient = this.patient._id;



        if (this.cas && this.cas._id) {
            if (!cas.niv) cas.niv = this.curVals.niv;
            await this.editCas(cas, patientData);
        } else {
            if (!cas.niv) cas.niv = this.curVals.niv;
            await this.addCas(cas, patientData);
        }

        if (this.associate) {
            await this.CAS.affectToInter(cas, this.intervention);
            await this.INTER.affect('cas', cas._id);
            this.VISU.navTo({
                intervention: this.intervention._id
            });
            return;
        }

        if (this.VISU.currentDisplay.list == 'passage') {
            this.VISU.navTo({
                passage: cas.position,
                cas: cas._id,
                rubrique: 'info',
                sousRubrique: 'patient'
            });
        } else {
            this.VISU.navTo({
                cas: cas._id,
                rubrique: 'info',
                sousRubrique: 'patient'
            });
        }
    }
    private async editCas(cas, patientData) {
        // update cas
        await this.DB.update('cas', cas)
        // update list
        await this.LIST.editListItem('cas', cas)

        //add new patient if needed
        if (!cas.patient) {

            // add patient
            await this.DB.put('patient', {
                ...patientData,
                cas: [cas._id]
            }).then(newPatientId => {
                patientData._id = newPatientId;
                cas.patient = newPatientId
            })

            // update cas (associate patient)
            await this.DB.update('cas', cas);

        }

        // edit patient if changes
        else if (Object.keys(patientData).length > 0) {
            // patientData.cas = [...this.patient.cas, cas._id]
            patientData.cas = [...this.CAS.currentObjects.patient.cas, cas._id]
            await this.DB.update('patient', patientData)
        }

        // maj disaplay patient
        this.CAS.defineCurrent('patient', patientData)

        if (!cas.poste) {
            return;
        }

        // close if poste changes : close passage
        if (cas.position && cas.position.split(':')[1] !== cas.poste) {
            await this.CAS.closeCasPassage(cas);
        }

        // if no passage : open new passage
        if (!cas.position) {
            // bugfix: check for unlinked open passages
            const existingPassageId = await this.PASSAGE.bugfixFindPassageAndCloseExtraneous(this.cas._id, this.LIST.listsRef.passage);
            if (existingPassageId) {
                await this.PASSAGE.forceClosePassage(existingPassageId, 'Close passage before creating a new one');
            }
            await this.CAS.openCasPassage(cas, cas.poste);
        }

        return;
    }
    private async addCas(cas, patientData) {
        // create & add id to cas
        let ID = this.DB.generateID('cas');
        cas._id = ID;

        let passage: PassageModel;
        let passageID: string;

        if (cas.poste) {
            // create passage & get id
            passage = this.CAS.generatePassage(cas, cas.poste);
            passageID = this.DB.generateID('passage', cas.poste);

            // add passage id
            cas.position = passageID;
        }

        // update list
        this.LIST.addItemToList('cas', cas);

        // add new patient if data
        if (Object.keys(patientData).find(k => k !== 'cas' && patientData[k])) {
            // add patient to DB & update cas & list
            const patient = { ...patientData, cas: [ID] };
            await this.DB.put('patient', patient)
                .then(newPatientID => {
                    patient._id = newPatientID;
                    cas.patient = newPatientID;
                    if (passage) {
                        passage.patient = newPatientID;
                        passage.pathologie = cas.pathologie;
                    }
                });

            await this.LIST.editListItem('cas', cas);
            // maj disaplay patient
            this.CAS.defineCurrent('patient', patient);
        }

        // add cas to db
        await this.DB.putWithID('cas', cas, ID);

        // add passage to db
        if (passage && cas.position) {
            await this.DB.putWithID('passage', passage, cas.position);
            passage._id = cas.position;
            this.LIST.addItemToList('passage', passage);
        }
    }

  }

export class CasModel {
    _id: string;
    timeCode: Date;
    niv: string;
    sinus: string;
    urgence: string;
    nom: string;
    prenom: string;
    sexe: string;
    age: number;
    pathologie: string;
    patient: any;
    currentIntervention: string = null;
    action: any;
    observation: any;
    conclusion: any;
    regulation: any;
    historique: any[] = [];
    clos: any;
    position: any;
    notification: any[] = [];
    note: string;
    poste: string = null;
    typeprecision: string;
    types: string;
    modes: string;
    lat: any;
    lng: any;

    // virtual fields added in service to simplify views
    positionCreated?: Date;
    currentPosition?: string;
    ageYears?: number;
    isMinor?: boolean;
    lastAction?: Date;
    lastObservation?: Date;
    theconclusion?: any;
    primaryNotification?: { type: string, created: Date };

    constructor(
        niv: string = '',
        urgence: string = 'u3',
        nom: string = '',
        prenom: string = '',
        sexe: string = '',
        age: number = 0,
        pathologie: string = '',
        typeprecision: string = '',
        types: string = '',
        modes: string = '',
    ) {
        this.niv = niv;
        this.urgence = urgence;
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.age = age;
        this.pathologie = pathologie;
        this.types = types;
        this.typeprecision = typeprecision;
        this.modes = modes;
    }

}

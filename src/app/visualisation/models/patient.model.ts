export class PatientModel {
    _id: string;
    cas: any[];
    niv: string;
    nom: string;
    prenom: string;
    sexe: string;
    age: number;
    nationalite: string;
    adresse: string;
    telephone: string;
    cp: number;
    ville: string;
    typeprecision: string;
    types:string;

    prevenir: any[];

    passif: any = {
        antecedent: [],
        traitement: [],
        allergie: []
    };
    constructor(
        cas: any[] = [],
        nom: string = '',
        niv: string = '',
        prenom: string = '',
        sexe: string = '',
        typeprecision: string = '',
        types: string = '',

    ) {

        this.cas = cas;
        this.niv = niv;
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.types = types;
        this.typeprecision = typeprecision;



    }

}

export class PassageModel {
    _id: string;
    cas: string;
    lieu: string;
    entree: any;
    sortie: any;
    patient: string;
    pathologie: string;
    lat: any;
    lng: any;

    constructor() { }

}

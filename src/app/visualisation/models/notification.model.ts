export class NotificationModel {
    _id: string;
    demande: 'intervention' | 'discussion' | 'regulation';
    cas: string;
    auteur: string;
    timeCode: Date;

    close: any;

    constructor(
        cas: string,
        demande: 'intervention' | 'discussion' | 'regulation'

    ) {

        this.cas = cas;
        this.demande = demande;



    }

}
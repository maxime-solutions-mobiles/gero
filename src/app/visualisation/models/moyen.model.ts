
export class MoyenModel {
    public _id: string;
    public nom: string;
    public type: string;// <-- découle picto, places dispos...
    public personnel: string[];//ref id personnel
    public cas: string[];// ref id patient
    //public interventions: string[];//ref interventions
    public disponible: boolean = true;
    public capacite: number;
    public geoloc: boolean;
    public balise: string;
    public famille: string;
    public chef: string;
    public telephone: string;
    public currentStates: any[] = [];
    public currentIntervention: string = null;
    public historique: any[] = [];
    public lat: string;
    public lng: string;
    public standBy: boolean;
    public etat: string = 'disponible';

    public updating:any;
    /*private stateRef: any[] = [
        'disponible',
        'alerte',
        'sll',
        'transport',
        'retour'
    ]*/
    constructor(
        nom: string = '',
        type: string = '',
        disponible: boolean = true

    ) {

        this.nom = nom;
        this.type = type;
        this.disponible = disponible;
        this.etat = 'disponible';

    }







}
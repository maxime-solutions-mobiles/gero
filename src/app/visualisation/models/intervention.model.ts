import { MoyenModel } from './moyen.model';
import { CasModel } from './cas.model';
export class InterventionModel {
    _id: string;
    timeCode: Date;
    cas: any[];
    auteur: string;
    motif: string;
    lieu: string;
    telephone: number;
    requerant: string;
    adresse: any;
    observation: string;
    moyen: any[];
    numero: number;
    clos: any;
    lat:any;
    lng:any;
    started: any;
    historique: any[] = [];
    discussion: any[] = [];
    origine: string;
    updating: any;
    position: any;
    urgence: any;
    notificationsSentAt?: Date;

    constructor(
        motif: string = '',
        lieu: string = '',
        cas: any[] = [],
        moyen: any[] = [],
    ) {
        this.motif = motif;
        this.lieu = lieu;
        this.cas = cas;
        this.moyen = moyen;
    }

}

export class ConclusionModel {
    _id: string;
    cas: any;
    discussion: any[];
    regulation: any[];
    document: any[];
    cloture: any[];
    diagnostic: any[];
    sortie: any[];
    ordonnance: any[];
    courier: any[];
    theconclusion: any;
    constructor(
        cas: any = '',
        regulation: any[] = [],
        discussion: any[] = [],
        diagnostic: any[] = [],
        document: any[] = [],
        ordonnance: any[] = [],
        courier: any[] = []
    ) {

        this.cas = cas;
        this.regulation = regulation;
        this.discussion = discussion;
        this.document = document;
        this.diagnostic = diagnostic;
        this.ordonnance = ordonnance;
        this.courier = courier;
    }

}

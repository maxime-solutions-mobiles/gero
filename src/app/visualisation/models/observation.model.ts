
export class ObservationModel {

    _id: string;
    circonstancielle: any[];
    vitale: any[];
    surveillance: any[];
    clinique: any[];
    soap: any[];
    exposure: any[];

    cas: string;

    constructor(
        cas: any = '',
        circonstancielle: any[] = [],
        vitale: any[] = [],
        surveillance: any[] = [],
        clinique: any[] = [],
        soap: any[] = [],
        exposure: any[] = [],



    ) {

        this.cas = cas;
        this.soap = soap;
        this.clinique = clinique;
        this.vitale = vitale;
        this.surveillance = surveillance;
        this.circonstancielle = circonstancielle;
        this.exposure = exposure;



    }

}
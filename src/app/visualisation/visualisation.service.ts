import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Subscription, ReplaySubject } from 'rxjs';
import { MoyenService } from './../services/moyen.service';
import { CasService } from './../services/cas.service';
import { ListService } from '../services/list.service';
import { DBService } from './../services/db.service';
import { UserService } from './../services/user.service';
import { CasMode } from './visualisation.service';
import { InterventionService } from '../services/intervention.service';


@Injectable()
export class VisualistationService implements OnDestroy {

    private logStyle = 'color: #16a085;';
    public currentUser: { prenom: any; nom: any; poste: string; fonction: string; };
    private _URL: any = new Subject<any>();
    private _voletDisplay: any = new ReplaySubject<any>();
    public get voletDisplay() { return this._voletDisplay }

    private _currentURL: any = {
        cas: null,
        moyen: null,
        intervention: null,
        rubrique: null,
        sousRubrique: null,
        action: null
    };
    private _selectPatient: any = new Subject<any>();
    private _display: any = new Subject<any>();
    private _currentDisplay: any = {
        list: 'cas',
        listMod: 'display'
    };
    private _defaultCasMode: CasMode = 'medium';

    public tempListMode: 'display' | 'select';

    public tempCasMode: CasMode;

    private rubbriquesRef = {
        info: ['pec', 'patient', 'atcd'],
        action: ['geste', 'traitement'],
        conclusion: ['conclusion', 'discussion', 'document'],
        observation: ['circonstanciel', 'vital', 'clinique', 'soap', 'surveillance']
    };

    private urlElements = ['moyen', 'cas', 'intervention', 'rubrique', 'sousRubrique', 'map', 'passage'];

    private subscription: Subscription;

    constructor(
        private router: Router,
        private DB: DBService,
        private USER: UserService,
        private LIST: ListService,
        private CAS: CasService,
        public INTER: InterventionService,
        public MOYEN: MoyenService
    ) {
        this.subscription = this.USER.profile.subscribe((profle) => {
            this.currentUser = profle;
            if (profle.fonction === 'administrateur') {
                this._defaultCasMode = 'large';
            } else {
                this._defaultCasMode = 'medium';
            }
            this.currentDisplay = { casMode: this._defaultCasMode }
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public get URL() {
        return this._URL.asObservable();
    }

    public set URL(params) {
        let display;
        if (params.moyen) {
            display = { list: 'moyen', listMod: 'display' };
        }
        if (params.intervention) {
            display = { list: 'intervention', listMod: 'display' };
        }
        if (params.cas && !params.passage) {
            display = { list: 'cas', listMod: 'display' };
        }
        if (params.passage) {
            display = { list: 'passage', listMod: 'display' };
        }
        this.currentDisplay = display;
        this._currentURL = params;
        this._URL.next(params);
    }

    public get currentURL() {
        return this._currentURL;
    }

    public set selectPatient(patientID: any) {
        if (!patientID || typeof patientID !== 'string') {
            return;
        }
        this.DB.getById('patient', patientID).then(doc => this._selectPatient.next(doc));
    }

    public get selectPatient() {
        return this._selectPatient.asObservable();
    }

    public get display() {
        return this._display.asObservable();
    }

    public set currentDisplay(obj: {
        list?: 'cas' | 'intervention' | 'moyen' | 'mcno' | 'statistiques' | 'map' | 'passage',
        listMod?: 'display' | 'select',
        casMode?: CasMode
    }) {
        this.LIST.stopAllListenChanges()
        if (obj.list === 'cas') {
            if (this.currentUser.fonction === 'user') {
                this.LIST.startListenChanges('cas', 'USER');
            } else {
                this.LIST.startListenChanges('cas');
            }
        }
        if (obj.list === 'passage') {
            if (this.currentUser.fonction === 'user') {
                this.LIST.startListenChanges('passage', 'USER');
            } else {
                this.LIST.startListenChanges('passage');
            }
        }
        if (obj.list === 'intervention' || obj.list === 'moyen') {
            this.LIST.startListenChanges('intervention');
            this.LIST.startListenChanges('moyen');
        }
        this._currentDisplay = { ...this._currentDisplay, ...obj }
        this._display.next(this._currentDisplay);
    }

    public get currentDisplay() { return this._currentDisplay }

    public selectListMode(listName: string, crits: any) {
        let key = Object.keys(crits)[0];
        if (crits[key] && crits[key] !== '') {
            this.LIST.filtreList(listName, crits);
            this.changeListMode('select');
        } else {
            this.changeListMode();
        }
    }

    public changeListMode(mode?: 'display' | 'select') {
        if (!this.tempListMode) {
            this.tempListMode = this.currentDisplay.listMod;
        }

        if (mode) {
            this.currentDisplay = { listMod: mode };
        } else {
            this.currentDisplay = { listMod: this.tempListMode }
            this.tempListMode = null;
            this.LIST.filtreList(this.currentDisplay.list);
        }
    }

    public autoCompleteSearch(crits: any) {
        const key = Object.keys(crits)[0];
        const str = crits[key].$regex.source;

        let listName = 'cas';
        if (this._currentDisplay.list === 'passage') {
            listName = 'passage';
            const casIds = this.LIST.listsRef.cas
                .filter((cas) => {
                  const prop = cas[key];
                  const regex = new RegExp(str, 'gi');
                  return prop && regex.test(prop);
                })
                .map((cas) => cas._id);
            crits = { cas: casIds };
        }

        if (str !== '(?:)') {
            if ((key === 'nom' || key === 'prenom') && str.length < 3) {
                return this.changeCasMode();
            }
            if (key === 'niv' && str.length < 3) {
                return this.changeCasMode();
            }

            this.LIST.filtreList(listName, crits).then(list => {
                if (list.length < 1 || str.length < 1) {
                    this.changeCasMode();
                } else {
                    this.changeCasMode('autocomplete');
                }
            });
        } else {
            this.changeCasMode();
        }
    }

    public changeCasMode(mode?: CasMode) {
        if (!this.tempCasMode) {
            this.tempCasMode = this.currentDisplay.casMode;
        }

        if (mode) {
            this.currentDisplay = { casMode: mode };
        } else {
            this.currentDisplay = { casMode: this.tempCasMode || this._defaultCasMode }
            this.tempCasMode = null;
            this.LIST.filtreList('cas');
        }
    }

    public reset() {
        this.router.navigate(['visualisation'])
        this.changeCasMode();
    };

    private _voletParams;

    public set voletDisplay(params) {

        let compile = { ...this.currentURL, ...this._voletParams, ...params }
        this._voletDisplay.next(compile);
        this._voletParams = compile;
        //console.timeEnd("voletShow");

    }

    public voletShow(params) {

        //console.time("voletShow");
        const templates = ['moyen', 'cas', 'intervention'];
        let details: any = {};

        // 1. definir  le template

        let template: string;
        template = params.template || templates.find(t => params[t]);
        if (!template) template = this._voletParams.template || templates.find(t => this._voletParams[t]);

        if (params[template] && params[template] == 'new') template = template == 'cas' ? 'nouveauCas' : 'nouvelleIntervention';

        details.template = template;


        // 2. definir les rubriques

        if (this._voletParams && details.template != this._voletParams.template) {
            details.rubrique = null;
            details.sousRubrique = null;
        }
        if (params.rubrique) details.rubrique = params.rubrique;
        if (params.sousRubrique) details.sousRubrique = params.sousRubrique;


        // 3. définir l'element en cours

        if (params.moyen) this.MOYEN.setCurrentByID(params.moyen);
        else if (params.cas && params.cas != 'new') this.CAS.currentCas = params.cas;
        else if (params.intervention && params.intervention != 'new') this.INTER.setCurrentByID(params.intervention);


        this.voletDisplay = details;


        /*
                console.groupCollapsed('%c voletShow', this.logStyle)
                console.log(' currentURL ==> ', this.currentURL)
                console.log(' _voletParams ==> ', this._voletParams)
                console.log(' params ==> ', params)
                console.log(' details ==> ', details)
                console.groupEnd();
                */

    }

    public navTo(params) {
        if (this.currentURL === params) {
            return;
        }
        if (this.currentDisplay.casMode === 'autocomplete') {
            this.changeCasMode();
        }

        let oldURL = { ...this.currentURL };

        // navTo is display only method
        if (oldURL.action) {
            oldURL.action = 'show';
        }

        // init path array
        let path = ['visualisation'];
        let ref;

        if (params.moyen) {
            ref = 'moyen';
        }
        if (params.cas) {
            ref = 'cas';
        }
        if (params.intervention) {
            ref = 'intervention';
        }
        if (params.passage) {
            ref = 'passage';
        }

        if (ref) {
            this.urlElements.forEach(element => {
                if (element !== ref ) {
                    oldURL[element] = null;
                }
            });
        }



        if (params.rubrique == 'conclusion' && params.sousRubrique == 'conclusion') {

            let urlToGo = { ...oldURL, ...params };

            if (urlToGo[ref]) {
                path.push(urlToGo[ref]);
            }

            this.URL = urlToGo;
            //this.router.navigate(path);

            this.voletShow({ ...urlToGo, template: ref });

        }

        else {

            path.push(ref);


            let urlToGo = { ...oldURL, ...params };

            if (urlToGo[ref]) {
                path.push(urlToGo[ref]);
            }

            this.URL = urlToGo;
            this.router.navigate(path);

            this.voletShow({ ...urlToGo, template: ref });

        }


    }

}

export type CasMode = 'short' | 'medium' | 'large' | 'autocomplete';

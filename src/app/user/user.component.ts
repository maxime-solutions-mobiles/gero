import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { ListService } from './../services/list.service';
import { UserService } from './../services/user.service';
import { ParametreService } from '../services/parametre.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-user',
  template: `
    <div *ngIf="display" class="modal">
      <div class="wait" *ngIf="!show">
        <i class="fas fa-spinner-third fa-spin"></i>
        <p>{{ feedBack }}</p>
      </div>
      <div *ngIf="show">
        <i (click)="close()" class="fal fa-times closeBtn"></i>
        <h3>Est-ce bien vous {{ prenom }} {{ nom }}?</h3>
        <p>
          Si vous n'êtes pas {{ prenom }} {{ nom }}
          <span class="cancel" (click)="logout()">connectez-vous</span> :)
        </p>
        <h3 *ngIf="!pc && !administrateur && !auditeur && postes">
          Vous êtes dans le poste &nbsp;
          <mat-form-field>
            <mat-select
              [value]="currentPoste"
              (selectionChange)="selectPoste($event)"
              placeholder="Choisir"
            >
              <mat-option *ngFor="let poste of postes" value="{{ poste }}">{{
                poste
              }}</mat-option>
            </mat-select>
          </mat-form-field>
        </h3>

        <div class="action">
          <button
            color="success"
            [disabled]="
              !postes || currentPoste || administrateur
                ? false
                : true || pc
                ? false
                : true
            "
            mat-raised-button
            (click)="close()"
          >
            Continuer
          </button>
        </div>
      </div>
    </div>
  `,
})
export class UserComponent implements OnInit, OnDestroy {
  public display = true;
  public administrateur: boolean;
  public auditeur: boolean;
  public pc: boolean;
  public nom: string;
  public prenom: string;
  public show = true;
  public currentPoste: string;
  public postes: string[];
  public feedBack = 'Chargement des informations utilisateur';
  private subscription: Subscription;

  constructor(
    private USER: UserService,
    private PARAM: ParametreService,
    private LIST: ListService,
    private authService: AuthService,
  ) {}

  public ngOnInit() {
    this.feedBack = 'Chargement des paramètres';
    if (this.PARAM.currentParametre.poste) {
      this.postes = this.PARAM.currentParametre.poste.map(p => p.nom);
    }

    this.subscription = this.USER.profile
      .pipe(
        map(profile => {
          this.prenom = profile.prenom;
          this.nom = profile.nom;
          this.currentPoste = profile.poste;
          this.administrateur = profile.fonction === 'administrateur';
          this.auditeur = profile.fonction === 'auditeur';
          this.pc = profile.fonction === 'pc';
        }),
        mapTo(this.PARAM.current),
        map(param => {
          if (param.poste) {
            this.postes = param.poste.map(p => p.nom);
          }
        }),
      )
      .subscribe(() => {
        this.feedBack = 'Chargement des bases de données';
        this.LIST.READY.then(() => (this.show = true));
      });
  }

  public ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public selectPoste(evt) {
    const poste = evt.value;
    if (poste === this.currentPoste) {
      return;
    }
    this.currentPoste = poste;
    this.USER.selectPoste(poste);
    this.LIST.resetList('cas');
  }

  public close() {
    this.display = false;
    if (!this.currentPoste) {
      this.LIST.resetList('cas');
    }
  }

  public logout(): void {
    this.authService.logout();
  }
}

import { DBService } from './../services/db.service';
import { AbstractControl } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomValidatorService {
    public nivExist(DB: DBService) {
        return (control: AbstractControl) => {
            if (!control.value) {
                return null;
            }
            /**
            if (!/^niv\d{5}$/i.test(control.value)) {
                return Promise.resolve({ invalidFormat: true });
            }
            **/
            const niv = control.value.toLowerCase();
            return DB.getFieldsIn('cas', ['niv']).then(list => list.find(c => c.niv.toLowerCase() === niv) ? { nivExist: true } : null);
        };
    }
}

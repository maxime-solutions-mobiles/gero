
import { ValidatorFn } from '@angular/forms';

export interface FieldConfig {
    disabled?: boolean,
    label?: string,
    name: string,
    options?: string[],
    placeholder?: string,
    type: string,
    validation?: ValidatorFn[],
    feedBacks?: Object,
    value?: any,
    inputType?: string,
    class?: string,
    asyncValidator?:any,
    valueChanges?:any,
    focused?: boolean
}
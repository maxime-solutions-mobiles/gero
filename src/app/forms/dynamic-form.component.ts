import { DBService } from './../services/db.service';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';


import { FieldConfig } from './field-config.interface';

@Component({
    exportAs: 'dynamicForm',
    selector: 'dynamic-form',
    template: `
    <form
    class="dynamic-form"
    [formGroup]="form"
    (submit)="handleSubmit($event)">
   <ng-container
      *ngFor="let field of config"
      dynamicField
      [config]="field"
      [group]="form">
    </ng-container>
  </form>
  `
})
export class DynamicFormComponent implements OnChanges, OnInit {
    @Input() config: FieldConfig[] = [];

    @Output() submit: EventEmitter<any> = new EventEmitter<any>();

    public form: FormGroup;
    private _tableName: string;
    public set tableName(val: string) { this._tableName = val }
    @Input() initialValues: any;
    public get controls() { return this.config.filter(({ type }) => type !== 'button') }
    public get changes() { return this.form.valueChanges }
    public get valid() { return this.form.valid }
    public get value() { return this.form.value }

    constructor(public fb: FormBuilder, private DB: DBService) { }

    ngOnInit() {
        this.form = this.createGroup();
        this.form.statusChanges.subscribe(result => {
             if (result == 'VALID') this.setDisabled('submit', false);
             else this.setDisabled('submit', true);
         });
        if (this.initialValues) this.setValues(this.initialValues);

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.config && changes.config.currentValue) this.config = changes.config.currentValue;
        if (this.form) {
            const controls = Object.keys(this.form.controls);
            const configControls: string[] = this.controls.map((item) => item.name);

            controls
                .filter((control) => !configControls.includes(control))
                .forEach((control) => this.form.removeControl(control));

            configControls
                .filter((control) => !controls.includes(control))
                .forEach((name) => {
                    const config = this.config.find((control) => control.name === name);
                    this.form.addControl(name, this.createControl(config));
                });
        }

    }

    private createGroup() {
        const group = this.fb.group({});
        this.controls.forEach(control => group.addControl(control.name, this.createControl(control)));
        return group;
    }

    private createControl(config: FieldConfig) {
        const { disabled, validation, value, asyncValidator } = config;
        return this.fb.control({ disabled, value }, validation, asyncValidator);
    }

    public handleSubmit(event: Event) {
        event.preventDefault();
        event.stopPropagation();


        // Add new values to initial (edit)
        if (!this.initialValues) this.initialValues = {};
        var editedItem = { ...this.initialValues, ...this.value };

        // #exeption traitement des document
        let files = this.controls.filter(l => l.type == 'file');
        files.forEach(file => {
            editedItem[file.name] = file.value;
            file.value = null;
        });

        // si tablename pas déclaré => retour des datas
        if (!this._tableName) {

            this.submit.emit(editedItem);
            this.form.reset();

            return;
        }

        // si tablename déclaré => edit or add to bdd
        if (editedItem._id) {

            this.DB.update(this._tableName, editedItem).then(doc => {

                this.submit.emit(doc)
            });

        } else {

            this.DB.put(this._tableName, editedItem).then(id => {
                editedItem._id = id;
                this.submit.emit(editedItem)

            });
        }
        this.form.reset();
    }
    public configLine(name) {
        let index = this.config.findIndex(l => l.name == name);
        return this.config[index]
    }
    public setDisabled(name: string, disable: boolean) {
        if (this.form && this.form.controls[name]) {
            const method = disable ? 'disable' : 'enable';
            this.form.controls[name][method]();
            return;
        }

        this.config = this.config.map((item) => {
            if (item.name === name) {
                item.disabled = disable;
            }
            return item;
        });
    }

    public setValue(name: string, value: any) {
        if (!this.form || !this.form.controls || !this.form.controls[name]) return;
        this.form.controls[name].setValue(value, { emitEvent: true });

    }

    public setValues(datas: any): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (!this.form || !this.form.controls) return resolve(false);
            this.initialValues = datas;
            Object.keys(this.form.controls).forEach(key => {
                if (datas[key]) this.form.controls[key].setValue(datas[key], { emitEvent: true })
                else this.form.controls[key].setValue(null, { emitEvent: true })
            })

            resolve(true);
        });
    }

    public reset() {
        this.initialValues = {}
        if (this.form) this.form.reset();
    }

}

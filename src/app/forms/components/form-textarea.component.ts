import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
  selector: 'form-input',
  host: { '[class]': 'classNames' },
  template: `
    <mat-form-field [formGroup]="group">
      <textarea
        matInput
        type="{{config.inputType}}"
        placeholder="{{config.placeholder}}"
        [formControlName]="config.name"
      ></textarea>

      <mat-error *ngIf="group?.controls[config.name].invalid">
        {{errorMessage(group?.controls[config.name].errors)}}
      </mat-error>
    </mat-form-field>
  `
})
export class FormTextAreaComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames = '';
    public error: any = null;
    private defaultMessages = {
        required: 'Ce champ est obligatoire'
    };

    ngOnInit() {
        this.classNames = this.config.class;
        this.defaultMessages = { ...this.defaultMessages, ... this.config.feedBacks };
    }

    public errorMessage(errorType: string) {
        let control = this.group.controls[this.config.name];
        for (let err of Object.keys(control.errors)) {
            if (control.hasError(err)) {
              return this.defaultMessages[err];
            }
        }
        return '';
    }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
  selector: 'form-separator',
  host: { '[class]': 'classNames' },
  template: `
    <h2>{{config.placeholder}}</h2>
  `
})
export class FormSeparatorComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames = '';

    ngOnInit() {
        // affect class for dedicated render
        this.classNames = this.config.class;
    }
}

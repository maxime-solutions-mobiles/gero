import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-date',
    host: { '[class]': 'classNames' },
    template: `
        <div [formGroup]="group">
            {{ config.placeholder }}
            <input atp-time-picker [formControlName]="config.name" value="{{currentTime}}" />
        </div>
    `
})
export class FormTimeComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames: string = null;
    public currentTime: string;

    ngOnInit() {
        // affect class for dedicated render
        this.classNames = this.config.class;

        const now = new Date();
        this.currentTime = now.getHours() + ':' + now.getMinutes();
    }
}

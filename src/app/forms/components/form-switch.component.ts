import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-switch',
    host: { '[class]': 'classNames' },
    template: `

    <div  *ngIf="config.placeholder">{{config.placeholder}}</div>

    <mat-button-toggle-group value="{{group?.value[config.name]}}">
        <mat-button-toggle
            *ngFor="let option of config.options"
            (change)="optionSelected = option"
            [value]="option"
        >
            {{option}}
        </mat-button-toggle>

    </mat-button-toggle-group>
    <form [formGroup]="group">
    <input  type="hidden" [formControlName]="config.name" [ngModel]="optionSelected || group?.value[config.name]">
    </form>
  `
})
export class FormSwitchComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames = '';
    public optionSelected: string;

    ngOnInit() {
        // affect class for dedicated render
        this.classNames = this.config.class;
    }
}

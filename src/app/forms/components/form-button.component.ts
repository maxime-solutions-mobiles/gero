import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
  selector: 'form-button',
  host: { '[class]': 'classNames' },
  template: `
    <div class="action" [formGroup]="group">
      <button
        [disabled]="config.disabled"
        color="success"
        mat-raised-button
        type="submit"
      >
        {{ config.label }}
      </button>
    </div>

  `
})
export class FormButtonComponent implements Field, OnInit {
  public config: FieldConfig;
  public group: FormGroup;
  public classNames = '';
  public label: string = null;

  ngOnInit() {
    this.classNames = this.config.class;
  }
}

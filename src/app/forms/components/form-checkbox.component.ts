import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-input',
    host: { '[class]': 'classNames' },
    template: `
        <div [formGroup]="group">
            <mat-checkbox [formControlName]="config.name">{{config.placeholder}}</mat-checkbox>
        </div>
    `
})
export class FormCheckboxComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames = '';
    public error: any = null;

    ngOnInit() {
        this.classNames = this.config.class;
    }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from './../field-config.interface';
import { Field } from '../field.interface';

@Component({
  selector: 'form-select',
  host: { '[class]': 'classNames' },
  template: `
  <mat-form-field [formGroup]="group">
    <mat-select
      [formControlName]="config.name"
      placeholder="{{ config.placeholder }}"
    >
        <mat-option *ngFor="let option of config.options" [value]="option">
          {{ option }}
        </mat-option>
    </mat-select>

    <mat-error *ngIf="group?.controls[config.name].invalid">
      {{errorMessage(group?.controls[config.name].errors)}}
    </mat-error>
  </mat-form-field>
  `
})
export class FormSelectComponent implements Field, OnInit {
  public config: FieldConfig;
  public group: FormGroup;
  public classNames = '';
  public defaultMessages: any = {
    required: 'Merci de selectioner une option'
  };

  ngOnInit() {
    // affect class for dedicated render
    this.classNames = this.config.class;
  }

  public errorMessage(errorType: string) {
    const control = this.group.controls[this.config.name];
    for (const err of Object.keys(control.errors)) {
      if (control.hasError(err)) {
        return this.defaultMessages[err];
      }
    }
    return '';
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-switch-multi-select',
    host: { '[class]': 'classNames' },
    template: `
    <div *ngIf="config.placeholder" >{{ config.placeholder }}</div>
    <mat-button-toggle-group [formGroup]="group" multiple>
        <mat-button-toggle
            *ngFor="let option of config.options"
            [value]="option"
            (change)="changeOption(option)"
        >
            {{option}}
        </mat-button-toggle>
    </mat-button-toggle-group>
    <form [formGroup]="group">
    <input  type="hidden" [formControlName]="config.name" [ngModel]="selectedOptions.join(',') || group?.value[config.name]">
    </form>
        `
})
export class FormSwitchMultiSelectComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames: string;
    public selectedOptions: any[] = [];

    ngOnInit() {
        // affect class for dedicated render
        this.classNames = `${this.config.class} ${this.config.placeholder}`;
    }

    changeOption(optionSelected: any) {
        // maintain selected otions list
        const index = this.selectedOptions.indexOf(optionSelected);

        if (index > -1) {
          this.selectedOptions.splice(index, 1);
        } else {
          this.selectedOptions.push(optionSelected);
        }

        this.selectedOptions = this.config.options.filter(
          option => this.selectedOptions.indexOf(option) > -1
        );

        // set value in form container
        this.group.value[this.config.name] = this.selectedOptions;
    }
}



import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
    selector: 'form-address',
    host: { '[class]': 'classNames' },
    template: `
    <mat-form-field class="example-full-width" [formGroup]="group">
        <input matInput ngx-google-places-autocomplete
            [options]="autocompleteOptions"
            #placesRef="ngx-places"
            placeholder="{{config.placeholder}}"
            value="{{value}}"
            [formControlName]="config.name"
            (onAddressChange)="handleAddressChange($event)"
        />
    </mat-form-field>
    `
})
export class FormAddressComponent implements Field, OnInit, OnDestroy {
    @ViewChild('placesRef') placesRef: GooglePlaceDirective;
    public value: string = null;
    public config: FieldConfig;
    public group: FormGroup;
    public classNames: string = null;
    public subscription: Subscription;
    public autocompleteOptions: any = {
        types: ['address'],
        componentRestrictions: { country: 'FR' }
    }

    ngOnInit() {
        this.classNames = this.config.class;
    }

    ngOnDestroy() {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
    }

    public handleAddressChange(address: Address) {

        let postcode = address.address_components.filter(component => {
            return component.types.indexOf('postal_code') !== -1;
        })

        let locality = address.address_components.filter(component => {
            return component.types.indexOf('locality') !== -1;
        })

        this.group.value[this.config.name] = address.formatted_address;

        this.group.value['postcode'] = postcode.length > 0 ? postcode[0].long_name : '';
        this.group.value['city'] = locality.length > 0 ? locality[0].long_name : '';

        this.group.value['lat'] = address.geometry.location.lat().toString();
        this.group.value['lng'] = address.geometry.location.lng().toString();


        this.subscription = this.group.valueChanges.subscribe(values => {
            this.group.value[this.config.name] = address.formatted_address;

            this.group.value['postcode'] = postcode.length > 0 ? postcode[0].long_name : '';
            this.group.value['city'] = locality.length > 0 ? locality[0].long_name : '';

            this.group.value['lat'] = address.geometry.location.lat().toString();
            this.group.value['lng'] = address.geometry.location.lng().toString();
        })
    }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NativeDateAdapter } from '@angular/material';
import { DateAdapter } from '@angular/material/core';

import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

export class CustomDateAdapter extends NativeDateAdapter {
    parse(value: any): Date | null {
        if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
            const str = value.split('/');
            return new Date(Number(str[2]), Number(str[1]) - 1, Number(str[0]), 12);
        }
        const timestamp = typeof value === 'number' ? value : Date.parse(value);
        return isNaN(timestamp) ? null : new Date(timestamp);
    }
}

@Component({
    selector: 'form-date',
    host: { '[class]': 'classNames' },
    providers: [
        { provide: DateAdapter, useClass: CustomDateAdapter }
    ],
    template: `
        <mat-form-field  [formGroup]="group">
            <input matInput [matDatepicker]="mtDatePicker" placeholder="{{config.placeholder}}" [formControlName]="config.name">
            <mat-datepicker-toggle matSuffix [for]="mtDatePicker"></mat-datepicker-toggle>
            <mat-datepicker #mtDatePicker></mat-datepicker>
        </mat-form-field>
    `
})
export class FormDateComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames: string = null;

    constructor(private dateAdapter: DateAdapter<Date>) {}

    ngOnInit() {
        this.dateAdapter.setLocale('fr-FR');
        // affect class for dedicated render
        this.classNames = this.config.class;
    }
}

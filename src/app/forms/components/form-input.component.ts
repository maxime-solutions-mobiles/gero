import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatInput } from '@angular/material';
import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-input',
    host: { '[class]': 'classNames' },
    template: `
      <mat-form-field [formGroup]="group">
        <input
          [formControlName]="config.name"
          matInput
          placeholder="{{config.placeholder}}"
          type="{{config.inputType}}"
          value="{{config.value}}"
        >
        <mat-error *ngIf="group?.controls[config.name].errors ">
          {{ errorMessage(group?.controls[config.name].errors)}}
        </mat-error>
      </mat-form-field>
    `
})
export class FormInputComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames = '';
    public error: any = null;
    public value: string = null;
    private errorMessages: any = {
        required: 'Ce champ est obligatoire',
        email: 'Email non valide',
        nivExist: 'Ce NIV est déjà utilisé'
    };

    @ViewChild(MatInput) input: MatInput;

    ngOnInit() {
        // affect class for dedicated render
        this.classNames = this.config.class;

        // set focus
        if (this.config.focused) {
          this.input.focus();
        }

        // set feedbacks
        this.errorMessages = { ...this.errorMessages, ... this.config.feedBacks };
    }

    public errorMessage(errorType: string) {
        let control = this.group.controls[this.config.name];
        for (let err of Object.keys(control.errors)) {
            if (control.hasError(err)) {
                return this.errorMessages[err];
            }
        }
        return '';
    }
}

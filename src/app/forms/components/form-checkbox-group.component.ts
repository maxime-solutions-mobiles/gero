import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-checkbox-group',
    host: { '[class]': 'classNames' },
    template: `
    <div *ngIf="config.placeholder">{{config.placeholder}}</div>
    <mat-checkbox
        *ngFor="let option of config.options"
        [value]="option"
        class="example-margin"
        (click)="changeOption(option)"
        [checked]="false"
    >
        {{option}}
    </mat-checkbox>
  `
})
export class FormCheckboxGroupComponent implements Field, OnInit, OnDestroy {
    public options: string[] = [];
    public config: FieldConfig;
    public group: FormGroup;
    private selectedOptions: any[] = [];
    public classNames = '';
    private subscription: Subscription;

    ngOnInit() {
      // affect class for dedicated render
      this.classNames = this.config.class;
    }

    ngOnDestroy() {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
    }

    changeOption(optionSelected: any) {
        const index = this.selectedOptions.indexOf(optionSelected);

        if (index > -1) {
          this.selectedOptions.splice(index, 1);
        } else {
          this.selectedOptions.push(optionSelected);
        }

        this.selectedOptions = this.config.options.filter(
          option => this.selectedOptions.indexOf(option) > -1
        );

        this.group.value[this.config.name] = this.selectedOptions;
        this.subscription = this.group.valueChanges.subscribe(values => {
            this.group.value[this.config.name] = this.selectedOptions;
        });
    }
}

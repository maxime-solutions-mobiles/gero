import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FieldConfig } from './../field-config.interface';
import { Field } from '../field.interface';

@Component({
    selector: 'form-autocomplete',
    host: { '[class]': 'classNames' },
    template: `
    <mat-form-field [formGroup]="group">
        <input matInput
            placeholder="{{config.placeholder}}"
            aria-label="State"
            [matAutocomplete]="auto"
            [formControlName]="config.name"
        >

        <mat-autocomplete #auto="matAutocomplete">
            <mat-option
                *ngFor="let option of filteredOptions"
                [value]="option"
            >
                {{option}}
            </mat-option>
        </mat-autocomplete>
    </mat-form-field>
    `
})
export class FormAutocompleteComponent implements Field, OnInit, OnDestroy {
    public options: any;
    public config: FieldConfig;
    public subscription: Subscription;
    public group: FormGroup;
    public classNames = '';
    public filteredOptions: any[];

    public ngOnInit() {
        // affect class for dedicated render
        this.classNames = this.config.class;

        // display option list & order by name
        this.filteredOptions = this.config.options.sort();

        // filter option list on change
        this.subscription = this.group.get(this.config.name).valueChanges.subscribe(o => {
            this.filteredOptions = this.config.options.filter(option => option.match(new RegExp('^' + o, 'i'))).sort();
            if (!this.filteredOptions || this.filteredOptions.length < 0) {
              this.filteredOptions = this.config.options;
            }
        });
    }

    public ngOnDestroy() {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
    }

    public errorMessage(errorType: string) {
        const control = this.group.controls[this.config.name];
        const defaultMessages = { required: 'Merci de selectioner une option' };
        for (const err of Object.keys(control.errors)) {
            if (control.hasError(err)) {
                return defaultMessages[err];
            }
        }
        return '';
    }
}

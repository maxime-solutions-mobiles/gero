import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-date',
    host: { '[class]': 'classNames' },
    template: `
        <div [formGroup]="group">
            {{config.placeholder}}
            <int-phone-prefix
                [locale]="locale"
                [defaultCountry]="locale"
                [onlyNumbers]="false"
                [formControlName]="config.name"
            ></int-phone-prefix>
        </div>
    `
})
export class FormPhoneComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames: string = null;
    public locale = 'fr';

    ngOnInit() {
        // affect class for dedicated render
        this.classNames = this.config.class;
    }
}

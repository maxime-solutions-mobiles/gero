import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { Field } from '../field.interface';
import { FieldConfig } from '../field-config.interface';

@Component({
    selector: 'form-file',
    host: { '[class]': 'classNames' },
    template: `
      <div [formGroup]="group">
        <div class="file-display" *ngIf="img"><img [src]="img" /></div>

        <label class="file-label" for="{{config.name}}">
            <span>{{ config.placeholder }}</span>
        </label>

        <input type="file" id="{{config.name}}" [formControlName]="config.name" (change)=change($event)>

        <mat-error *ngIf="group?.controls[config.name].errors">
          {{ errorMessage(group?.controls[config.name].errors) }}
        </mat-error>
      </div>
  `
})
export class FormFileComponent implements Field, OnInit {
    public config: FieldConfig;
    public group: FormGroup;
    public classNames = '';
    public img;
    private defaultMessages = {
        required: 'Ce champ est obligatoire'
    };

    constructor(private sanitizer: DomSanitizer) { }

    ngOnInit() {
        // affect class for dedicated render
        this.classNames = this.config.class;

        this.img = null;

        // set feedbacks
        this.defaultMessages = { ...this.defaultMessages, ... this.config.feedBacks };
    }

    change(e) {
        // récuperation du document
        var files = e.srcElement.files;
        var file = files[0];
        // TODO : traitement multiple files

        // ajout du doc à config pour récuperation au niveau du formulaire
        this.config.value = file;

        // affichage de l'image
        let url = URL.createObjectURL(file);
        this.img = this.sanitize(url);
        // TODO : affichage contextuel
    }

    public errorMessage(errorType: string) {
        let control = this.group.controls[this.config.name];
        for (let err of Object.keys(control.errors)) {
            if (control.hasError(err)) {
                return this.defaultMessages[err];
            }
        }
        return '';
    }

    public sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    }
}

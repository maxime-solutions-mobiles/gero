import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Field } from './field.interface';
import { FieldConfig } from './field-config.interface';

import { FormButtonComponent } from './components/form-button.component';
import { FormDateComponent } from './components/form-date.component';
import { FormInputComponent } from './components/form-input.component';
import { FormSelectComponent } from './components/form-select.component';
import { FormSwitchComponent } from './components/form-switch.component';
import { FormSwitchMultiSelectComponent } from './components/form-switch-multi-select.component';
import { FormSeparatorComponent } from './components/form-separator.component';
import { FormTextAreaComponent } from './components/form-textarea.component';
import { FormFileComponent } from './components/form-file.component';
import { FormCheckboxGroupComponent } from './components/form-checkbox-group.component';
import { FormCheckboxComponent } from './components/form-checkbox.component';
import { FormPhoneComponent } from './components/form-phone.component';
import { FormTimeComponent } from './components/form-time.component';
import { FormAddressComponent } from './components/form-address.component';
import { FormAutocompleteComponent } from './components/form-autocomplete.component';

const components = {
    button: FormButtonComponent,
    date: FormDateComponent,
    input: FormInputComponent,
    switch: FormSwitchComponent,
    select: FormSelectComponent,
    switch_multi_select: FormSwitchMultiSelectComponent,
    separator: FormSeparatorComponent,
    textArea: FormTextAreaComponent,
    file: FormFileComponent,
    checkbox_group: FormCheckboxGroupComponent,
    checkbox: FormCheckboxComponent,
    phone: FormPhoneComponent,
    time: FormTimeComponent,
    address: FormAddressComponent,
    autocomplete: FormAutocompleteComponent
};

@Directive({
    selector: '[dynamicField]'
})
export class DynamicFieldDirective implements Field, OnInit, OnDestroy {
    @Input() config: FieldConfig;
    @Input() group: FormGroup;
    private component: ComponentRef<Field>;
    public placeholder: string;
    public classNames: string;
    public subscription: Subscription[] = [];
    constructor(
        public resolver: ComponentFactoryResolver,
        public container: ViewContainerRef
    ) { }

    ngOnInit() {
        if (!components[this.config.type]) {
            const supportedTypes = Object.keys(components).join(', ');
            throw new Error(
                `Type inconnu : ${this.config.type}.
              Types supportés : ${supportedTypes}`
            );
        }
        const component = components[this.config.type];
        const factory = this.resolver.resolveComponentFactory<Field>(component);
        this.component = this.container.createComponent(factory);
        this.component.instance.config = this.config;
        this.component.instance.group = this.group;

        if (this.group.get(this.config.name) && this.config.valueChanges && typeof this.config.valueChanges == "function") {

            let sub = this.group.get(this.config.name).valueChanges.subscribe(val => this.config.valueChanges(val));
            this.subscription.push(sub)
        }
    }

    ngOnDestroy() {
        this.subscription.forEach((sub) => sub && sub.unsubscribe());
    }

}

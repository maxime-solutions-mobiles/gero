import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatButtonToggleModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule
} from '@angular/material';

import { DynamicFormComponent } from './dynamic-form.component';
import { DynamicFieldDirective } from './dynamic-field.directive';

import { FormButtonComponent } from './components/form-button.component';
import { FormDateComponent } from './components/form-date.component';
import { FormInputComponent } from './components/form-input.component';
import { FormSwitchComponent } from './components/form-switch.component';
import { FormSwitchMultiSelectComponent } from './components/form-switch-multi-select.component';
import { FormSelectComponent } from './components/form-select.component';
import { FormSeparatorComponent } from './components/form-separator.component';
import { FormTextAreaComponent } from './components/form-textarea.component';
import { FormFileComponent } from './components/form-file.component';
import { FormCheckboxGroupComponent } from './components/form-checkbox-group.component';
import { FormCheckboxComponent } from './components/form-checkbox.component';
import { FormPhoneComponent } from './components/form-phone.component';
import { FormTimeComponent } from './components/form-time.component';
import { FormAddressComponent } from './components/form-address.component';
import { FormAutocompleteComponent } from './components/form-autocomplete.component';


import { InternationalPhoneModule } from 'ng4-intl-phone';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

import { CustomValidatorService } from './custom-validator.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonToggleModule,
        MatAutocompleteModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCheckboxModule,
        InternationalPhoneModule,
        AmazingTimePickerModule,
        GooglePlaceModule
    ],
    declarations: [
        DynamicFormComponent,
        DynamicFieldDirective,
        FormButtonComponent,
        FormDateComponent,
        FormInputComponent,
        FormSelectComponent,
        FormSwitchComponent,
        FormSwitchMultiSelectComponent,
        FormSeparatorComponent,
        FormTextAreaComponent,
        FormFileComponent,
        FormCheckboxGroupComponent,
        FormCheckboxComponent,
        FormPhoneComponent,
        FormTimeComponent,
        FormAddressComponent,
        FormAutocompleteComponent


    ], exports: [
        DynamicFormComponent

    ],
    entryComponents: [
        FormButtonComponent,
        FormDateComponent,
        FormInputComponent,
        FormSelectComponent,
        FormSwitchComponent,
        FormSwitchMultiSelectComponent,
        FormSeparatorComponent,
        FormTextAreaComponent,
        FormFileComponent,
        FormCheckboxGroupComponent,
        FormCheckboxComponent,
        FormPhoneComponent,
        FormTimeComponent,
        FormAddressComponent,
        FormAutocompleteComponent

    ],
    providers: [CustomValidatorService]
})
export class DynamicFormModule { }

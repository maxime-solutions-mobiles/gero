import { Model } from './model';
import { ParticipantRole } from './participant-role';
import { ParticipantPermissions } from './participant-permissions';
import { EventModel } from './event.model';
import { UserModel } from './user.model';

export interface ParticipantModel extends Model {
  id: string;
  eventId: string;
  userId: string;
  organizationId: string;
  memberId?: string;
  role: ParticipantRole;
  permissions: ParticipantPermissions;
  startDate?: Date;
  endDate?: Date;
  user?: UserModel;
  event?: EventModel;
}

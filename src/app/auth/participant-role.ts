export enum ParticipantRole {
  Admin = 'participant.admin',
  Manager = 'participant.manager',
  User = 'participant.user',
}

export enum UserProfession {
  Doctor = 'doctor',
  Nurse = 'nurse',
  Other = 'other',
}

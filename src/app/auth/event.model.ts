import { Model } from './model';
import { AddressModel } from './address.model';
import { ParticipantModel } from './participant.model';

export interface EventModel extends Model {
  slug: string;
  name: string;
  type: string;
  address: AddressModel;
  description: string;
  startDate: Date;
  endDate: Date;
  allowMemberRegistration: boolean;
  allowPublicRegistration: boolean;
  databasePrefix: string;
  activatedAt: Date;
  participants?: ParticipantModel[];
}

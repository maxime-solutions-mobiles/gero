export class ParticipantPermissions {
  visits?: boolean;
  cases?: boolean;
  patients?: boolean;
  logbook?: boolean;
  interventions?: boolean;
  dispatch?: boolean;
  statistics?: boolean;
  settings?: boolean;
}

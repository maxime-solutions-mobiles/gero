import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public activationPending = false;
  public unauthorizedEvent = false;
  public returnUrl: string;
  public portalUrl = environment.portalUrl;

  public loginForm = this.fb.group({
    identifier: ['', Validators.required],
    password: ['', Validators.required],
  });

  private subscriptions: Subscription[] = [];

  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
  ) {}

  public ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    let isAuthenticated = false;

    const sessionSub = this.authService
      .isAuthenticated()
      .subscribe(state => (isAuthenticated = state));
    this.subscriptions.push(sessionSub);

    const eventSub = this.authService.isAuthorized().subscribe(isAuthorized => {
      this.unauthorizedEvent = isAuthenticated && !isAuthorized;
      if (isAuthorized) {
        this.router.navigateByUrl(this.returnUrl);
      }
    });
    this.subscriptions.push(eventSub);
  }

  public ngOnDestroy() {
    this.subscriptions.map(sub => sub && sub.unsubscribe());
  }

  public login() {
    this.loginForm.disable();
    this.authService.login(this.loginForm.value).subscribe({
      error: err => this.loginErrorHandler(err),
    });
  }

  private loginErrorHandler(err: HttpErrorResponse) {
    this.loginForm.enable();
    this.loginForm.patchValue({ password: '' });

    if (err.error.error === 'INVALID_IDENTIFIER') {
      this.loginForm.controls.identifier.setErrors({ invalid: true });
      return;
    }

    if (err.error.error === 'INVALID_PASSWORD') {
      this.loginForm.controls.password.setErrors({ invalid: true });
      return;
    }

    if (err.error.error === 'ACTIVATION_PENDING') {
      this.activationPending = true;
      return;
    }
  }
}

export interface AddressModel {
  street: string;
  town: string;
  postcode: string;
  country: string;
}

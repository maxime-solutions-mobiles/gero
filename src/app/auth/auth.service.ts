import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { of, Observable, ReplaySubject } from 'rxjs';
import { catchError, tap, map, mergeMap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { UserModel } from './user.model';
import { EventModel } from './event.model';
import { ParticipantModel } from './participant.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public userSession = new ReplaySubject<UserModel>(1);
  public participantSession = new ReplaySubject<ParticipantModel>(1);
  private baseUrl = environment.authServiceUrl;

  constructor(
    private readonly router: Router,
    private readonly http: HttpClient,
  ) {
    this.loadUser()
      .pipe(mergeMap(() => this.loadEvent()))
      .subscribe();
  }

  /**
   * Check if user is connected
   */
  public isAuthenticated(): Observable<boolean> {
    return this.userSession.pipe(map(user => !!user));
  }

  /**
   * Check if user is authorized to access event
   */
  public isAuthorized(): Observable<boolean> {
    return this.participantSession.pipe(map(participant => !!participant));
  }

  /**
   * Login to user account
   */
  public login(formData: {
    identifier: string;
    password: string;
  }): Observable<void> {
    return this.http.post<UserModel>(`${this.baseUrl}/login`, formData).pipe(
      tap(user => {
        this.userSession.next(user);
        localStorage.setItem('user', JSON.stringify(user));
      }),
      mergeMap(() => this.loadEvent()),
    );
  }

  /**
   * Terminate session
   */
  public logout(): void {
    localStorage.removeItem('user');
    this.userSession.next(null);
    localStorage.removeItem('event');
    this.participantSession.next(null);
    this.http.get<void>(`${this.baseUrl}/logout`).subscribe();
    this.router.navigate(['/login']);
  }

  /**
   * Fetch session data
   */
  private loadUser(): Observable<void> {
    return this.http.get<UserModel>(`${this.baseUrl}/session`).pipe(
      catchError(err => {
        if (err.status === 401) {
          return of(null);
        }

        // Restore session from local storage if possible
        const userJson = localStorage.getItem('user');
        if (userJson) {
          try {
            const user: UserModel = JSON.parse(userJson);
            return of(user);
          } catch (err) {
            console.error(err);
          }
        }

        console.error(err);
        return of(null);
      }),
      tap(user => {
        this.userSession.next(user);
        if (user) {
          localStorage.setItem('user', JSON.stringify(user));
        }
      }),
      map(() => null),
    );
  }

  /**
   * Fetch current event data
   */
  private loadEvent(): Observable<void> {
    const eventSlug = location.pathname
      .split('/')
      .splice(2)
      .shift();
    const url = `${this.baseUrl}/events/${eventSlug}`;
    let user: UserModel;

    return this.userSession.pipe(
      // Only try to load event if session is loaded
      mergeMap(u => {
        user = u;
        if (user) {
          return this.http.get<EventModel>(url);
        }
        return of(null);
      }),
      catchError(err => {
        if (err.status === 401 || err.status === 404) {
          return of(null);
        }

        // Restore event from local storage if possible
        const eventJson = localStorage.getItem('event');
        if (eventJson) {
          try {
            const event: EventModel = JSON.parse(eventJson);
            if (event.slug === eventSlug) {
              return of(event);
            }
          } catch (err) {
            console.error(err);
          }
        }

        console.error(err);
        return of(null);
      }),
      tap(event => {
        if (!event) {
          this.participantSession.next(null);
          return;
        }

        // Ensure user is a participant since some events are publicly viewable
        const participant = user.participations.find(
          ({ eventId }) => eventId === event.id,
        );
        if (!participant) {
          this.participantSession.next(null);
          return;
        }

        participant.event = event;
        participant.user = user;

        this.participantSession.next(participant);
        localStorage.setItem('event', JSON.stringify(event));
      }),
      map(() => null),
    );
  }
}

import { Model } from './model';
import { ParticipantModel } from './participant.model';
import { UserProfession } from './user-profession';

export interface UserModel extends Model {
  title: string;
  givenName: string;
  familyName: string;
  profession: UserProfession;
  specialty: string;
  RPPS: string;
  ADELI: string;
  driversLicence: string;
  language: 'en' | 'fr';
  participations?: ParticipantModel[];
}

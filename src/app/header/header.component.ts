import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { ParticipantModel } from '../auth/participant.model';
import { DBService, SyncStatus } from './../services/db.service';
import { ListService } from './../services/list.service';
import { UserService } from '../services/user.service';
import { VisualistationService } from './../visualisation/visualisation.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  public participant: ParticipantModel;
  public showList: any;
  public currentUser: any;
  public openUserSettings: boolean;
  public menuByUser = {
    admin: ['visualisation', 'statistiques', 'mcno', 'map'],
  };
  public menuDetails = {
    visualisation: {
      picto: 'fal fa-procedures',
      label: 'visualisation',
      url: '/visualisation',
    },
    intervention: {
      picto: 'fal fa-ambulance',
      label: 'intervention',
      url: '/intervention',
    },
    statistiques: {
      picto: 'fal fa-chart-pie',
      label: 'statistiques',
      url: '/statistiques',
    },
    mcno: { picto: 'fal fa-list-ul', label: 'mcno', url: '/mcno' },
    map: { picto: 'fal fa-map', label: 'map', url: '/map' },
    parametres: {
      picto: 'fal fa-cog',
      label: 'parametres',
      url: '/parametres',
    },
  };

  public syncStatuses = SyncStatus;
  public syncStatus: SyncStatus = SyncStatus.Syncing;

  public counts = { passage: 0, intervention: 0 };

  public notifs = { regulation: 0, discussion: 0, intervention: 0 };
  private notifLists = { regulation: [], discussion: [], intervention: [] };

  private subscription: Subscription[] = [];

  constructor(
    private router: Router,
    private authService: AuthService,
    private USER: UserService,
    public LIST: ListService,
    private VISU: VisualistationService,
    private DB: DBService,
  ) {}

  ngOnInit() {
    // TODO : limiter en focntion du role user ?!

    this.subscription.push(
      this.USER.profile.subscribe(profile => {
        this.currentUser = profile;
        this.getTotalCurentPassage('passage');
        if (profile.fonction === 'administrateur') {
          this.getTotalCurrent('intervention');
        }
      }),
    );

    // Récuperation, affichage des notification ouvertes par type
    this.subscription.push(
      this.LIST.listObservables['notification'].subscribe(updatedList => {
        this.notifLists = updatedList;
        ['regulation', 'intervention', 'discussion'].forEach(type => {
          this.notifs[type] = updatedList[type].length;
        });
      }),
    );

    this.showList = this.VISU.currentDisplay.list;

    this.subscription.push(
      this.VISU.display.subscribe(display => {
        this.showList = display.list;
      }),
    );

    this.subscription.push(
      this.LIST.change.subscribe(doc => {
        let table = doc._id.split(':')[0];
        if (table === 'cas' || table === 'intervention') {
          this.getTotalCurrent(table);
        } else if (table === 'passage') {
          this.getTotalCurentPassage(table);
        }
      }),
    );

    const syncSub = this.DB.getSyncStatus().subscribe(
      status => (this.syncStatus = status),
    );
    this.subscription.push(syncSub);

    const participantSub = this.authService.participantSession.subscribe(
      participant => (this.participant = participant),
    );
    this.subscription.push(participantSub);
  }

  ngOnDestroy() {
    this.subscription.forEach(sub => sub && sub.unsubscribe());
  }

  private getTotalCurrent(table: string) {
    let filters: any = this.LIST.defaultFilters[table]();
    this.getCount(table, filters);
  }

  private getTotalCurentPassage(table: string) {
    let filters: any = {
      sortie: { $exists: false },
      clos: { $exists: false },
      _id: { $regex: new RegExp('cas', 'gi') },
    };

    if (this.currentUser.poste) {
      filters.poste = this.currentUser.poste;
    }
    this.getCount(table, filters);
  }

  private getCount(table, filters) {
    if (!filters) {
      return 0;
    }
    filters = this.LIST.convertMongo(filters);
    return this.DB.getAll(table).then(list => {
      return (this.counts[table] = list.filter(l => eval(filters)).length);
    });
  }

  public displayNotif(notifName: string) {
    if (!this.notifs[notifName]) {
      return;
    }

    // afficher la liste des cas
    this.router.navigate(['visualisation']).then(() => {
      this.VISU.currentDisplay = { list: 'cas', listMod: 'display' };
      const request: any = { _id: { $in: this.notifLists[notifName] } };

      if (this.currentUser.poste && this.currentUser.fonction === 'user') {
        request.poste = this.currentUser.poste;
      }
      this.LIST.filtreList('cas', request);
    });
  }

  public selectList(
    listName: 'cas' | 'intervention' | 'moyen' | 'mcno' | 'passage',
  ) {
    if (listName === 'mcno') {
      this.router.navigate(['/mcno']);
    }

    if (!this.router.url.includes('visualisation')) {
      this.router.navigate(['visualisation']).then(() => {
        this.VISU.currentDisplay = { list: listName, listMod: 'display' };
      });
    }

    this.VISU.currentDisplay = { list: listName, listMod: 'display' };
  }

  public selectMap() {
    this.showList = 'map';
    this.router.navigate(['/map']);
  }

  public selectCharts() {
    this.showList = 'statistiques';
    this.router.navigate(['/statistiques']);
  }
}

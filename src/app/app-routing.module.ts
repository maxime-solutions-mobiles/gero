import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

import { Mcno } from './mcno/mcno.page';
import { Parametres } from './parametres/parametres.page';
import { Statistiques } from './statistiques/statistiques.page';
import { Visualisation } from './visualisation/visualisation.page';
import { GeroMap } from './map/map.page';

const appRoutes: Routes = [
  {
    path: 'mcno',
    component: Mcno,
    canActivate: [AuthGuard],
  },
  {
    path: 'parametres',
    component: Parametres,
    canActivate: [AuthGuard],
  },
  {
    path: 'statistiques',
    component: Statistiques,
    canActivate: [AuthGuard],
  },
  {
    path: 'map',
    component: GeroMap,
    canActivate: [AuthGuard],
  },
  {
    path: 'visualisation',
    component: Visualisation,
    canActivate: [AuthGuard],
  },
  {
    path: 'visualisation/cas/:cas',
    component: Visualisation,
    canActivate: [AuthGuard],
  },
  {
    path: 'visualisation/passage/:passage',
    component: Visualisation,
    canActivate: [AuthGuard],
  },
  {
    path: 'visualisation/passage/:passage/:rubrique',
    component: Visualisation,
    canActivate: [AuthGuard],
  },
  {
    path: 'visualisation/passage/:passage/:rubrique/:sousRubrique',
    component: Visualisation,
    canActivate: [AuthGuard],
  },
  {
    path: 'visualisation/moyen/:moyen',
    component: Visualisation,
    canActivate: [AuthGuard],
  },
  {
    path: 'visualisation/intervention/:intervention',
    component: Visualisation,
    canActivate: [AuthGuard],
  },
  { path: '', redirectTo: '/visualisation', pathMatch: 'full' },
  { path: '**', redirectTo: '/visualisation' },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

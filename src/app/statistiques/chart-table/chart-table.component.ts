import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'chart-table',
  templateUrl: './chart-table.component.html'
})
export class ChartTableComponent implements OnInit {
  @Input() chart;
  constructor() { }


  ngOnInit() {
    console.log("chart table data : ", this.chart.tabledata);
  }

}

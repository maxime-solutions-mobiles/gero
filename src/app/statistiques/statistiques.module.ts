
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Statistiques } from './statistiques.page';

import { MatDatepickerModule, MatInputModule, MatTableModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DynamicFormModule } from '../forms/dynamic-form.module';
import { HttpClientModule } from '@angular/common/http';

import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { PDFService } from '../services/pdf.service';
import { ChartBlockComponent } from './chart-block/chart-block.component'
import { ChartTableComponent } from './chart-table/chart-table.component';

@NgModule({
    imports: [
        CommonModule,
        MatDatepickerModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        DynamicFormModule,
        MatInputModule,
        MatTableModule,
        ChartsModule,
        MatMomentDateModule,
        NgxChartsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule
    ],
    declarations: [
        Statistiques,
        ChartBlockComponent,
        ChartTableComponent
    ],
    exports: [
        Statistiques
    ],
    providers: [
        PDFService
    ]
})
export class StatistiquesModule { }

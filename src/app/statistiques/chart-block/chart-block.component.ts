import { Component, OnInit, Input } from '@angular/core';
import * as shape from 'd3-shape';
@Component({
  selector: 'chart-block',
  templateUrl: './chart-block.component.html'
})
export class ChartBlockComponent implements OnInit {
  @Input() chart;
  curve = shape.curveLinear;
  customColors = [
    {
      name: 'homme',
      value: '#2597de'
    },
    {
      name: 'femme',
      value: '#d926a5'
    },
    {
      name: 'indéterminé',
      value: '#cccccc'
    }
  ];

  constructor() { }

  ngOnInit() {
    console.log('chart : ', this.chart);
  }

}

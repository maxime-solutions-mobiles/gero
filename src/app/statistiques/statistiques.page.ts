
import { VisualistationService } from './../visualisation/visualisation.service';
import { Component, NgZone } from '@angular/core';
import { ParametreService } from './../services/parametre.service';


import { DBService } from '../services/db.service';
import { PDFService } from '../services/pdf.service';

import * as moment from 'moment';
import 'moment/locale/fr';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { UserService } from '../services/user.service';
import * as shape from 'd3-shape';

import { DateTimeAdapter } from 'ng-pick-datetime';

let jsPDF = require('jspdf');
let canvg = require('canvg');

let sortByName = (data: any[]) => {

    function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();

        let comparison = 0;
        if (nameA > nameB) {
            comparison = 1;
        } else if (nameA < nameB) {
            comparison = -1;
        }
        return comparison;
    }

    return data.sort(compare);

}
abstract class Chart {

    id: string;
    name: string;
    title: string;
    type: string;
    scheme: string;

    chartdata: Array<any>;
    tabledata: Array<any>;
    cols: Array<any>;
    rows: Array<any>;

    loading: boolean;

    abstract setDataItem(...params): void;

    constructor(type: string, scheme: string) {

        this.type = type;
        this.scheme = scheme;


        this.cols = [];
        this.rows = [];

        this.chartdata = [];
        this.tabledata = [];

        this.loading = false;
    }

    startLoading(): void {
        this.loading = true;
        this.chartdata = [];
    }

    stopLoading(): void {
        this.loading = false;
        this.createTableData();
    }

    createTableData(): void {

        this.rows = Array.from(new Set(this.rows));

        for (let row of this.chartdata) {
            delete row.patients;
        }

        this.tabledata = this.chartdata;
    }

    setData(data: any[]): void {
        //this.chartdata = data;
        this.chartdata = Array.from(new Set(data));

    }
}

class OneDataItemChart extends Chart {

    setDataItem(patient): void {

        let index = this.chartdata.findIndex(obj => obj.name === patient);

        if (index === -1) {

            this.chartdata.push({
                name: patient,
                value: 1
            });

        } else {

            this.chartdata[index].value += 1;
        }

        this.rows.push(patient);
    }
}

class TwoDataItemsChart extends Chart {

    setDataItem(param: string, patient: string): void {

        let index = this.chartdata.findIndex(obj => obj.name === param);

        if (index === -1) {

            this.chartdata.push({
                name: param,
                patients: [patient],
                value: 1
            });

        } else if (this.chartdata[index].patients.indexOf(patient) === -1) {

            this.chartdata[index].patients.push(patient);
            this.chartdata[index].value += 1;
        }

        this.rows.push(param);
    }
}

class PatientSexChart extends OneDataItemChart {
    cols: Array<string> = ['Sex', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'sexe';

    }
}

class PatientAgeChart extends OneDataItemChart {
    cols: Array<string> = ['Age', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'age';
    }
}

class PatientNationalityChart extends OneDataItemChart {
    cols: Array<string> = ['Nationality', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'nationality';
    }
}

class PatientTypeChart extends OneDataItemChart {
    cols: Array<string> = ['Type', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'type';
    }
}

class PosteComingChart extends TwoDataItemsChart {
    cols: Array<string> = ['Poste', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'coming';
    }
}

class ReasonComingChart extends TwoDataItemsChart {
    cols: Array<string> = ['Reason', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'reason';
    }
}

class PosteActualChart extends OneDataItemChart {
    cols: Array<string> = ['Poste', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'actual';
    }
}

class PosteActivityChart extends Chart {

    //cols: Array<string> = ['-', ...Array.from(Array(24).keys()).map(d => d.toString())];
    cols: Array<string> = [...Array.from(Array(24).keys()).map(d => d.toString())];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'hour';
    }

    setDataItem(patient: string, lieu: string, hour: string) {

        let index = this.chartdata.findIndex(obj => obj.name === lieu);

        if (index === -1) {

            this.chartdata.push({
                name: lieu,
                series: [{
                    name: hour,
                    patients: [patient],
                    value: 1
                }]
            });

        } else {

            let ind = this.chartdata[index].series.findIndex(obj => obj.name === hour);

            if (ind === -1) {

                this.chartdata[index].series.push({
                    name: hour,
                    patients: [patient],
                    value: 1
                });

            } else if (this.chartdata[index].series[ind].patients.indexOf(patient) === -1) {

                this.chartdata[index].series[ind].patients.push(patient);
                this.chartdata[index].series[ind].value += 1;
            }
        }

        this.rows.push(lieu);
    }

    createTableData(): void {

        this.tabledata = [];

        this.rows = Array.from(new Set(this.rows));

        for (let row of this.chartdata) {

            let i = this.rows.findIndex(obj => obj === row.name);

            for (let hours of row.series) {

                let j = this.cols.findIndex(obj => obj === hours.name.toString()) - 1;

                if (!this.tabledata[i]) {
                    this.tabledata[i] = Array(this.cols.length - 1).fill(0);
                }

                this.tabledata[i][j] = hours.value;

                delete hours.patients;
            }
        }
    }
}

class PosteReasonComingChart extends Chart {

    //cols: Array<string> = ['-'];
    cols: Array<string> = [];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'reseonperposte';
    }

    setDataItem(patient: string, poste: string, motif: string) {

        let index = this.chartdata.findIndex(obj => obj.name === poste);

        if (index === -1) {

            this.chartdata.push({
                name: poste,
                series: [{
                    name: motif,
                    patients: [patient],
                    value: 1
                }]
            });

        } else {

            let ind = this.chartdata[index].series.findIndex(obj => obj.name === motif);

            if (ind === -1) {

                this.chartdata[index].series.push({
                    name: motif,
                    patients: [patient],
                    value: 1
                });

            } else if (this.chartdata[index].series[ind].patients.indexOf(patient) === -1) {

                this.chartdata[index].series[ind].patients.push(patient);
                this.chartdata[index].series[ind].value += 1;
            }
        }

        this.cols.push(poste);
        this.rows.push(motif);
    }

    createTableData(): void {

        this.tabledata = [];

        this.cols = Array.from(new Set(this.cols));
        this.rows = Array.from(new Set(this.rows));

        for (let row of this.chartdata) {

            let i = this.cols.findIndex(obj => obj === row.name) - 1;

            for (let series of row.series) {

                let j = this.rows.findIndex(obj => obj === series.name);

                if (!this.tabledata[j]) {
                    this.tabledata[j] = Array(this.cols.length - 1).fill(0);
                }

                this.tabledata[j][i] = series.value;

                delete series.patients;
            }
        }
    }
}

class BecomingPatientChart extends OneDataItemChart {
    cols: Array<string> = ['Type', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'becoming';
    }
}

class EvacuationHospitalChart extends OneDataItemChart {
    cols: Array<string> = ['Hospital', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'hospital';
    }
}

class EvacuationVehicleChart extends OneDataItemChart {
    cols: Array<string> = ['Vehicle', 'Patients'];

    constructor(type: string, scheme: string) {
        super(type, scheme);
        this.name = 'vehicule';
    }
}

class ChartList {

    private list: Map<string, Chart>;

    constructor() {
        this.list = new Map<string, Chart>();
    }

    set(name: string, chart: Chart): void {
        this.list.set(name, chart);
    }

    get(name): Chart {
        return <Chart>this.list.get(name);
    }

    getArray(): Array<any> {
        return Array.from(this.list.values());
    }

    get length(): number {
        return Array.from(this.list.values()).length;
    }

    startLoading(): void {
        this.list.forEach(chart => {
            chart.startLoading();
        })
    }

    stopLoading(): void {
        this.list.forEach(chart => {
            chart.stopLoading();
        })
    }
}

@Component({
    selector: 'statistiques',
    templateUrl: './statistiques.page.html',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
        { provide: DateAdapter, useClass: MomentDateAdapter },
    ]
})
export class Statistiques {

    public startDate;
    public endDate;
    public customColor = {
        domain: ['#111111', '#222222', '#333333', '#444444', '#444444', '#555555']
    }
    charts: ChartList;

    curve = shape.curveLinear;

    constructor(
      private DB: DBService,
      private PDF: PDFService,
      private VISU: VisualistationService,
      private USER: UserService,
      private zone: NgZone,
      private PARAM: ParametreService,
      public dateTimeAdapter: DateTimeAdapter<any>
    ) {
        this.createDesignDocuments();
        this.setCharts();

        dateTimeAdapter.setLocale('fr');

        this.charts.get('sexe').title = 'Genre';
        this.charts.get('sexe').cols = ['Sexe', 'Patients'];

        this.charts.get('age').title = 'Âge';
        this.charts.get('age').cols = ['Age', 'Patients'];

        this.charts.get('nationality').title = 'Nationalité';
        this.charts.get('nationality').cols = ['Nationalité', 'Patients'];

        this.charts.get('type').title = 'Typologie';
        this.charts.get('type').cols = ['Typologie', 'Patients'];

        this.charts.get('actual').title = 'Patients présents dans les postes';
        this.charts.get('actual').cols = ['Poste', 'Patients'];

        this.charts.get('coming').title = 'Nombre de patients vus par poste';
        this.charts.get('coming').cols = ['Poste', 'Patients'];

        this.charts.get('hour').title = 'Nombre de patients vus par poste et par heure';

        this.charts.get('reason').title = 'Motifs de passages';
        this.charts.get('reason').cols = ['Motif', 'Patients'];

        this.charts.get('reseonperposte').title = 'Motifs de passages par poste';

        this.charts.get('becoming').title = 'Devenir des patients';
        this.charts.get('becoming').cols = ['Typologie', 'Patients'];

        this.charts.get('hospital').title = 'Destination hospitalière des patients évacués';
        this.charts.get('hospital').cols = ['Hospitalière', 'Patients'];

        this.charts.get('vehicule').title = 'Vecteurs d\'évacuations des victimes';
        this.charts.get('vehicule').cols = ['Vehicule', 'Patients'];

        this.VISU.currentDisplay = { list: 'statistiques', listMod: 'display' };

        this.DB.TABLE.passage.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', (passage: any) => {
            this.showPosteActualChart();
        });
    }

    ngOnInit() {
        //this.startDate = moment().startOf('day').toDate();
        this.startDate = moment().add(-1, 'days').startOf('day').toDate();
        this.endDate = moment().add(1, 'days').startOf('day').toDate();
    }

    createDesignDocuments() {

        const ddocPassage = {
            _id: '_design/charts',
            views: {
                'cas': {
                    'map': 'function (doc) { if (doc.cas) emit(Date.parse(doc.timeCode), doc.cas); }'
                },
                'poste_patient': {
                    'map': 'function (doc) { if (doc.patient) emit(Date.parse(doc.timeCode), doc.patient); }'
                },
                'poste_actual': {
                    'map': 'function (doc) { if (doc.patient && !doc.sortie) emit([doc.lieu ? doc.lieu : \'Indéterminé\', doc.patient], 1); }',
                    'reduce': '_sum'
                },
                'poste_coming': {
                    'map': 'function (doc) { if (doc.patient) emit(Date.parse(doc.timeCode), [doc.lieu ? doc.lieu : \'Indéterminé\', doc.patient]); }'
                },
                'poste_activity': {
                    'map': 'function (doc) { if (doc.patient) emit(Date.parse(doc.timeCode), [doc.lieu ? doc.lieu : \'Indéterminé\', doc.patient]); }'
                },
                'pathologie': {
                    'map': 'function (doc) { if (doc.patient && doc.pathologie) emit(Date.parse(doc.timeCode), [doc.pathologie, doc.patient]); }'
                },
                'pathologie_poste': {
                    'map': 'function (doc) { if (doc.patient && doc.pathologie) emit(Date.parse(doc.timeCode), [doc.poste ? doc.poste : \'Indéterminé\', doc.pathologie, doc.patient]); }'
                }
            }
        };

        const ddocPatient = {
            '_id': '_design/charts',
            'views': {
                'stats': {
                    'map': 'function (doc) { emit(doc._id, [doc.sexe ? doc.sexe[0].toUpperCase() + doc.sexe.substr(1) : \'Indéterminé\', doc.age ? ((new Date().getTime() - new Date(doc.age).getTime()) / (1000 * 3600 * 24 * 31 * 12) >= 18 ? \'Majeurs\' : \'Mineurs\') : \'Indéterminé\', doc.nationalite ? doc.nationalite[0].toUpperCase() + doc.nationalite.substr(1) : \'Indéterminé\', doc.types ? doc.types : \'Indéterminé\']); };'
                }
            }
        };

        const ddocConclusion = {
            '_id': '_design/charts',
            'views': {
                'type': {
                    'map': 'function (doc) { var last = doc.regulation.reduce(function (prev, current) { return (prev.timeCode > current.timeCode) ? prev : current }); emit(Date.parse(doc.timeCode), last.type); }'
                },
                'destination': {
                    'map': 'function (doc) { var last = doc.regulation.reduce(function (prev, current) { return (prev.timeCode > current.timeCode) ? prev : current }); if (last.hopital) emit(Date.parse(doc.timeCode), last.hopital); }'
                },
                'moyen': {
                    'map': 'function (doc) { var last = doc.regulation.reduce(function (prev, current) { return (prev.timeCode > current.timeCode) ? prev : current }); if (last.moyen) emit(Date.parse(doc.timeCode), last.moyen); }'
                }
            }
        };

        this.DB.TABLE['passage'].put(ddocPassage).catch(err => {
            if (err.name !== 'conflict') {
                throw err;
            }
        }).then(() => {
            return this.DB.TABLE['patient'].put(ddocPatient);
        }).catch(err => {
            if (err.name !== 'conflict') {
                throw err;
            }
        }).then(() => {
            return this.DB.TABLE['conclusion'].put(ddocConclusion);
        }).catch(err => {
            if (err.name !== 'conflict') {
                throw err;
            }
        }).then(() => {
            this.showDateCharts();
            this.showPosteActualChart();
        }).catch(err => {
            console.log(err);
        });
    }

    setCharts() {

        this.charts = new ChartList();


        // info / indicatif
        this.charts.set('sexe', new PatientSexChart('pie', 'horizon')); // h || f
        this.charts.set('age', new PatientAgeChart('pie', 'horizon')); // majeur || mineur
        this.charts.set('nationality', new PatientNationalityChart('pie', 'horizon')); // Patients/nationalité
        this.charts.set('type', new PatientTypeChart('pie', 'horizon')); // typo Patients

        // en cours / live
        this.charts.set('actual', new PosteActualChart('number-card', 'neons')); // Patients/poste
        //this.charts.set('hour', new PosteActivityChart('line', 'neons')); //  Patients / poste /heure
        this.charts.set('hour', new PosteActivityChart('line', 'neons')); //  Patients / poste /heure



        this.charts.set('reason', new ReasonComingChart('bar', 'horizon')); // motifs
        this.charts.set('reseonperposte', new PosteReasonComingChart('bar-stacked', 'horizon')); // motifs/poste

        this.charts.set('hospital', new EvacuationHospitalChart('bar', 'aqua')); // destination évac Patient

        // somme / bilan
        this.charts.set('coming', new PosteComingChart('bar', 'neons')); // Patient / poste : total
        this.charts.set('becoming', new BecomingPatientChart('bar', 'picnic')); // devenir Patients
        this.charts.set('vehicule', new EvacuationVehicleChart('bar', 'aqua')); // vecteur évac Patient





    }

    public changeStartDate() {
        this.showDateCharts();
    }

    public changeEndDate() {
        this.showDateCharts();
    }

    generatePDF() {

        const pdf = new jsPDF('l', 'pt', 'a4');

        let font = this.PDF.fonts.get('Arimo');

        pdf.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        pdf.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);
        pdf.addFileToVFS(font.italic.postScriptName, font.italic.base64string);
        pdf.addFont(font.italic.postScriptName, font.italic.fontName, font.italic.fontStyle);
        pdf.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        pdf.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);





        const logo = this.PARAM.currentParametre['logo'];
        const imgwidth = 2 * Number(logo[0].width);
        const imgheight = 2 * Number(logo[0].height);
        const imgData = 'data:image/' + logo[0].format + ';base64,' + logo[0].base64 + '';

        const tableXOffset = 530, tableYOffset = 115;
        const cellWidth = 75, cellHeight = 12;
        const chartXOffset = 50, chartYOffset = 250;
        const chartWidth = 500, chartHeight = 300;

        this.charts.getArray().forEach((chart, chartIndex: number) => {

            (function addHeader(event, startDate, endDate) {
                pdf.setFillColor(37, 151, 222);
                pdf.rect(0, 0, 900, 70, 'FD');
                pdf.addImage(imgData, logo[0].format2, 10, (70 - imgheight) / 2, imgwidth, imgheight);
                pdf.setFont(font.regular.fontName, font.regular.fontStyle);
                pdf.setFontSize(20);
                pdf.setTextColor(255, 255, 255);
                pdf.text('' + logo[0].nom + ', ' + event + '', 150, 24);
                pdf.text('Statistiques du ' + (moment(startDate).format('DD-MM-YYYY à HH:mm')) + ' au ' + (moment(endDate).format('DD-MM-YYYY à HH:mm')) + ' ', 150, 50);
                pdf.setTextColor(0, 0, 0);
                pdf.setFontSize(7);
                pdf.text(20, 580, 'statistiques générées par GERO' + '\u00A9' + ' -Version Gero3-  édité par AGS - www.gestion-secours.com ');
                pdf.setFontSize(20);
                pdf.text(chart.title, 150, 500);
            })(this.USER.INFO.event, this.startDate, this.endDate);

            if (chart.tabledata.length > 0) {

                (function addChartToPDF(chartName) {


                    let element = document.getElementById(chartName);
                    if (chart.type !== 'line' && element) {

                        const canvas = document.createElement('canvas');
                        let svg;
                        if (chart.type == 'pie') svg = document.getElementById(chartName).getElementsByClassName('ngx-charts')[0].outerHTML;
                        else svg = element.getElementsByClassName('ngx-charts')[0].outerHTML;

                        if (svg) svg = svg.replace(/\r?\n|\r/g, '').trim();
                        let size = 350;

                        canvg(canvas, svg, { scale: size });

                        var imgData = canvas.toDataURL('image/png');
                        if (chart.type == 'pie') pdf.addImage(imgData, 'PNG', chartXOffset, chartXOffset);
                        else pdf.addImage(imgData, 'PNG', 100, 100, size, size);



                    }
                })(chart.name);

                (function addTableHeader(x, y) {
                    pdf.setFont(font.bold.fontName, font.bold.fontStyle);
                    pdf.setFontSize(8);
                    pdf.setLineWidth(0.1);
                    if (chart.type === 'line') {
                        chart.cols.forEach((col, colIndex) => {
                            pdf.setFillColor(37, 151, 222);
                            pdf.rect(x, y, colIndex > 0 ? cellWidth / 3 : cellWidth, cellHeight, 'F');
                            pdf.cell(x, y, colIndex > 0 ? cellWidth / 3 : cellWidth, cellHeight, col);
                            x += colIndex > 0 ? cellWidth / 3 : cellWidth;
                        });
                    } else {
                        chart.cols.forEach((col, colIndex) => {
                            pdf.setFillColor(37, 151, 222);
                            pdf.rect(x, y, cellWidth, cellHeight, 'F');
                            pdf.cell(x, y, cellWidth, cellHeight, col);
                            x += cellWidth;
                        });
                    }
                })(chart.type === 'line' ? chartXOffset : tableXOffset, tableYOffset);

                (function addTableBody(x, y) {
                    chart.tabledata.forEach((row, rowIndex) => {
                        (function addTableRow(x, y) {
                            pdf.setFont(font.regular.fontName, font.regular.fontStyle);
                            pdf.setFontSize(7);
                            pdf.setFillColor(0);
                            if (chart.type === 'line' || chart.type == 'bar2d') {
                                pdf.cell(x, y, cellWidth, cellHeight, chart.rows[rowIndex]);
                                chart.cols.forEach((col, colIndex) => {
                                    if (colIndex > 0) {
                                        pdf.cell(x, y, chart.type === 'line' ? cellWidth / 3 : cellWidth, cellHeight, String((<any>Object).values(row)[colIndex - 1]));
                                    }
                                    x += colIndex > 0 ? chart.type === 'line' ? cellWidth / 3 : cellWidth : cellWidth;
                                });
                            } else {
                                chart.cols.forEach((col, colIndex) => {
                                    pdf.cell(x, y, cellWidth, cellHeight, String((<any>Object).values(row)[colIndex]));
                                    x += cellWidth;
                                });
                            }
                        }(x, y))
                        y += cellHeight;
                    });
                })(chart.type === 'line' ? chartXOffset : tableXOffset, tableYOffset + cellHeight);
            }

            if (chartIndex < this.charts.length - 1) {
                pdf.addPage();
            }
        })

        pdf.save('stats.pdf');
    }

    /*
    private getUniqueCases() {
        return this.DB.TABLE['passage'].query('charts/cas', { startkey: Date.parse(this.startDate), endkey: Date.parse(this.endDate) })
            .then( result => {
                return Array.from(new Set(result.rows.map(row => row.value)));
            });
    }
    */

    private showDateCharts() {
        this.showPatientCharts();

        this.showPosteComingChart();
        this.showPosteActivityChart();
        this.showReasonComingChart();
        this.showPosteReasonComingChart();

        this.showBecomingPatientChart();
        this.showEvacuationHospitalChart();
        this.showEvacuationVehicleChart();

        /*
        this.getUniqueCases().then( cases => {
            this.showBecomingPatientChart(cases);
            this.showEvacuationHospitalChart(cases);
            this.showEvacuationVehicleChart(cases);
        });
        */
    }

    private showPatientCharts() {
        const sexChart = this.charts.get('sexe');
        const ageChart = this.charts.get('age');
        const nationalityChart = this.charts.get('nationality');
        const typesChart = this.charts.get('type');

        sexChart.startLoading();
        ageChart.startLoading();
        nationalityChart.startLoading();
        typesChart.startLoading();

        let currentPoste = this.USER.INFO.poste;

        this.DB.TABLE['passage'].query('charts/poste_patient', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate)
        }).then(result => {

            let patients = Array.from(new Set(result.rows.map(row => row.value)));

            if (patients.length > 0) {

                this.DB.TABLE['patient'].query('charts/stats', {
                    keys: patients
                }).then(stats => {
                    stats.rows.forEach(row => {
                        const [sex, age, nationality, types] = row.value;
                        sexChart.setDataItem(sex);
                        ageChart.setDataItem(age);
                        nationalityChart.setDataItem(nationality);
                        typesChart.setDataItem(types);
                    });
                    sexChart.stopLoading();
                    ageChart.stopLoading();
                    nationalityChart.stopLoading();
                    typesChart.stopLoading();
                }).catch(function (err) {
                    sexChart.stopLoading();
                    ageChart.stopLoading();
                    nationalityChart.stopLoading();
                    typesChart.stopLoading();
                });

            } else {

                sexChart.stopLoading();
                ageChart.stopLoading();
                nationalityChart.stopLoading();
                typesChart.stopLoading();
            }
        }).catch(function (err) {
            sexChart.stopLoading();
            ageChart.stopLoading();
            nationalityChart.stopLoading();
            typesChart.stopLoading();
        });
    }

    private showPosteActualChart() {
        const posteActualChart = this.charts.get('actual');

        posteActualChart.startLoading();

        this.zone.run(() => {
            this.DB.TABLE['passage'].query('charts/poste_actual', {
                group: true,
                reduce: true,
                group_level: 2
            }).then(stats => {
                stats.rows.forEach(row => {
                    const [lieu] = row.key;
                    posteActualChart.setDataItem(lieu);
                });
                sortByName(posteActualChart.chartdata);
                sortByName(posteActualChart.tabledata);
                posteActualChart.stopLoading();
            }).catch(function (err) {
                posteActualChart.stopLoading();
            });
        });
    }

    private showPosteComingChart() {
        const posteComingChart = this.charts.get('coming');

        posteComingChart.startLoading();

        this.DB.TABLE['passage'].query('charts/poste_coming', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate)
        }).then(stats => {

            stats.rows.forEach(row => {
                const [lieu, patient] = row.value;
                posteComingChart.setDataItem(lieu, patient);
            });
            sortByName(posteComingChart.chartdata);
            sortByName(posteComingChart.tabledata);
            posteComingChart.rows.sort();
            posteComingChart.stopLoading();
        }).catch(function (err) {
            posteComingChart.stopLoading();
        });
    }

    private showPosteActivityChart() {
        const posteActivityChart = this.charts.get('hour');

        posteActivityChart.startLoading();

        this.DB.TABLE['passage'].query('charts/poste_activity', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate),
        }).then(stats => {

            stats.rows.forEach(row => {
                const [lieu, patient] = row.value;
                let d = new Date(row.key);
                posteActivityChart.setDataItem(patient, lieu, d.getHours());
            });
            sortByName(posteActivityChart.chartdata);
            sortByName(posteActivityChart.tabledata);
            posteActivityChart.stopLoading();
        }).catch(function (err) {
            posteActivityChart.stopLoading();
        });
    }

    private showReasonComingChart() {
        const reasonComingChart = this.charts.get('reason');

        reasonComingChart.startLoading();

        this.DB.TABLE['passage'].query('charts/pathologie', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate)
        }).then(stats => {
            stats.rows.forEach(row => {
                const [pathologie, patient] = row.value;
                reasonComingChart.setDataItem(pathologie, patient);
            });
            sortByName(reasonComingChart.chartdata);
            sortByName(reasonComingChart.tabledata);
            reasonComingChart.stopLoading();
        }).catch(function (err) {
            reasonComingChart.stopLoading();
        });
    }

    private showPosteReasonComingChart() {
        const posteReasonComingChart = this.charts.get('reseonperposte');

        posteReasonComingChart.startLoading();

        this.DB.TABLE['passage'].query('charts/pathologie_poste', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate)
        }).then(stats => {
            stats.rows.forEach(row => {
                const [poste, pathologie, patient] = row.value;
                posteReasonComingChart.setDataItem(patient, poste, pathologie);
            });
            sortByName(posteReasonComingChart.chartdata);
            posteReasonComingChart.chartdata.forEach((posteData, index) => {
                sortByName(posteReasonComingChart.chartdata[index].series);

            })
            sortByName(posteReasonComingChart.tabledata);
            posteReasonComingChart.stopLoading();
        }).catch(function (err) {
            posteReasonComingChart.stopLoading();
        });
    }

    // private showBecomingPatientChart(cases) {
    private showBecomingPatientChart() {
        const becomingPatientChart = this.charts.get('becoming');

        becomingPatientChart.startLoading();

        this.DB.TABLE['conclusion'].query('charts/type', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate)
        }).then(stats => {
            stats.rows.forEach(row => {
                const type = row.value;
                becomingPatientChart.setDataItem(type);
            });
            becomingPatientChart.stopLoading();
        }).catch(function (err) {
            becomingPatientChart.stopLoading();
        });
    }

    // private showEvacuationHospitalChart(cases) {
    private showEvacuationHospitalChart() {
        const evacuationHospitalChart = this.charts.get('hospital');

        evacuationHospitalChart.startLoading();

        this.DB.TABLE['conclusion'].query('charts/destination', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate)
        }).then(stats => {
            stats.rows.forEach(row => {
                const hospital = row.value;
                evacuationHospitalChart.setDataItem(hospital);
            });
            evacuationHospitalChart.stopLoading();
        }).catch(function (err) {
            evacuationHospitalChart.stopLoading();
        });
    }

    // private showEvacuationVehicleChart(cases) {
    private showEvacuationVehicleChart() {
        const evacuationVehicleChart = this.charts.get('vehicule');

        evacuationVehicleChart.startLoading();

        this.DB.TABLE['conclusion'].query('charts/moyen', {
            startkey: Date.parse(this.startDate),
            endkey: Date.parse(this.endDate)
        }).then(stats => {
            stats.rows.forEach(row => {
                const moyen = row.value;
                evacuationVehicleChart.setDataItem(moyen);
            });
            evacuationVehicleChart.stopLoading();
        }).catch(function (err) {
            evacuationVehicleChart.stopLoading();
        });
    }
}

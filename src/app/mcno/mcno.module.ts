import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';

import { Mcno } from './mcno.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '../forms/dynamic-form.module';
import { MatTableModule, MatSortModule, MatPaginatorModule, MatInputModule } from '@angular/material';

import { PDFService } from '../services/pdf.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DynamicFormModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatInputModule
    ],
    declarations: [
        Mcno
    ],
    exports: [
        Mcno
    ],
    providers: [
        PDFService,
        DatePipe
    ]
})
export class McnoModule { }

import { Component, OnInit, ViewChild } from '@angular/core';

import { FieldConfig } from '../forms/field-config.interface';
import { DBService } from '../services/db.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';

import { ParametreService } from '../services/parametre.service';

import { DatePipe } from '@angular/common';
import { PDFService } from '../services/pdf.service';

let jsPDF = require('jspdf');

class McnoPDF {
    pdf: any;
    font: any;
    logo: any;
    startX = 0;
    startY = 0;
    w = 180;
    fontSize = 8;
    headerFontSize = 8;
    rowHeight = 10;
    headerHeight = 12;
    currentLine = 0;
    x = 0;
    y = 0;
    topMargin = 10;
    leftMargin = 10;
    columns: any[];
    data: any[];
    datePipe: any;

    addLogo(logo) {
        this.logo = logo[0];
    }

    setBoldFont(fontSize: number) {
        this.pdf.setFont(this.font.bold.fontName, this.font.bold.fontStyle);
        this.pdf.setFontSize(fontSize);
    }

    setNormalFont(fontSize: Number) {
        this.pdf.setFont(this.font.regular.fontName, this.font.regular.fontStyle);
        this.pdf.setFontSize(fontSize);
    }

    addReportHeader() {
        const imgwidth = 2 * Number(this.logo.width);
        const imgheight = 2 * Number(this.logo.height);
        const imgData = 'data:image/' + this.logo.format + ';base64,' + this.logo.base64 + '';

        this.pdf.setFillColor(100, 100, 255);
        this.pdf.rect(0, 0, 900, 60, 'FD');
        this.pdf.addImage(imgData, this.logo.format2, 3, 3, imgwidth, imgheight);

        this.setNormalFont(20);
        this.pdf.setTextColor(255, 255, 255);

        this.pdf.text('' + this.logo.nom, 150, 24);
        this.pdf.text('Main courante non opérationnelle', 150, 50);

        this.y += 70;
        this.currentLine += 7;
    }

    addPageFooter() {
        this.setNormalFont(7);
        this.pdf.setTextColor(0, 0, 0);
        this.pdf.text('statistiques générées par GERO' + '\u00A9' + ' -Version Gero3-  édité par AGS - www.gestion-secours.com ', 20, 580);
    }

    getMaxRowLines(row: any, fontSize: number) {

        this.setNormalFont(fontSize);

        let max = 0;

        let splitLines = [];

        this.columns.forEach(column => {

            if (row[column.name] !== undefined && row[column.name] !== null) {

                let lines = [];

                if (column.name === 'timeCode') {
                    lines = this.pdf.splitTextToSize(this.datePipe.transform(row[column.name], 'dd/MM/yy HH:mm'), column.width - 5);
                } else {
                    lines = this.pdf.splitTextToSize(row[column.name], column.width - 5);
                }

                if (lines.length > max) {
                    max = lines.length;
                }

                splitLines.push(lines);

            } else {

                splitLines.push([]);
            }
        });

        return {
            'max' : max,
            'lines': splitLines
        };
    }

    addTableHeader() {
        this.setBoldFont(this.headerFontSize);
        this.pdf.setLineWidth(0.1);
        this.columns.forEach(col => {
            this.pdf.setFillColor(100, 100, 255);
            this.pdf.rect(this.x, this.y, col.width, this.headerHeight, 'F');
            this.pdf.cell(this.x, this.y, col.width, this.headerHeight, col.name[0].toUpperCase() + col.name.substr(1));
            this.x += col.width;
        });
        this.y += this.headerHeight;
        this.currentLine++;
    }

    addTableCell(width, height, text, fontSize) {
        this.setNormalFont(fontSize);
        this.pdf.setTextColor(0, 0, 0);
        this.pdf.cell(this.x, this.y, width, height, text);
        this.x += width;
    }

    addTable() {
        this.x = this.leftMargin;
        // this.y = this.topMargin;

        this.addTableHeader();
        this.addPageFooter();

        let rowsQty = Math.round((this.pdf.internal.pageSize.getHeight() - this.topMargin * 3) / this.rowHeight) - 2;

        // this.currentLine = 0;

        this.data.forEach((d, dIndex) => {

            this.x = this.leftMargin;

            let maxRowLines = this.getMaxRowLines(d, this.fontSize);

            if (maxRowLines.max < rowsQty) {

                this.columns.forEach((c, cIndex) => {
                    let lines = maxRowLines.lines[cIndex];
                    this.addTableCell(c.width, this.rowHeight * maxRowLines.max, lines.length > 0 ? lines : ' ', this.fontSize);
                });

                this.currentLine += maxRowLines.max;

                this.y += this.rowHeight * maxRowLines.max;

            } else {

                if (this.currentLine > 0) {

                    this.columns.forEach((c, cIndex) => {
                        let lines = maxRowLines.lines[cIndex].splice(0, rowsQty - this.currentLine);

                        this.addTableCell(c.width, this.rowHeight * (rowsQty - this.currentLine), lines.length > 0 ? lines : ' ', this.fontSize);
                    })

                    maxRowLines.max -= rowsQty - this.currentLine;

                    this.addNewPage();
                }

                let pages = Math.ceil(maxRowLines.max / rowsQty);

                for (let i = 0; i < pages; i++) {

                    this.x = this.leftMargin;

                    this.columns.forEach((c, cIndex) => {

                        let lines = [];

                        lines = maxRowLines.lines[cIndex].slice(i * rowsQty, (i + 1) * rowsQty);

                        if (i < pages - 1) {
                            this.addTableCell(c.width, this.rowHeight * (rowsQty - 1), lines.length > 0 ? lines : ' ', this.fontSize);
                        } else {
                            this.addTableCell(c.width, this.rowHeight * (maxRowLines.max - rowsQty * (pages - 1)), lines.length > 0 ? lines : ' ', this.fontSize);
                        }

                    });

                    if (i < pages - 1) {
                        this.addNewPage();
                    }
                }

                this.currentLine += maxRowLines.max - rowsQty * (pages - 1);

                this.y += this.rowHeight * (maxRowLines.max - rowsQty * (pages - 1));
            }

            if (this.currentLine === rowsQty) {
                this.addNewPage();
            }
        });
    }

    addNewPage() {
        this.pdf.addPage();
        this.y = this.topMargin;
        this.x = this.leftMargin;
        this.currentLine = 0;
        this.addTableHeader();
        this.addPageFooter();
    }

    createPDF() {
        this.pdf.save('mcno.pdf');
    }

    setDatePipe(datePipe: any) {
        this.datePipe = datePipe;
    }

    setColumns(columns: any[]) {
        this.columns = columns;
    }

    setData(data: any[]) {
        this.data = data;
    }

    run() {
        this.addReportHeader();
        this.addTable();
        this.createPDF();
    }

    constructor(font: any) {
        this.x = this.startX;
        this.y = this.startY;

        this.pdf = new jsPDF('l', 'pt', 'a4');

        this.font = font;

        this.pdf.addFileToVFS(this.font.regular.postScriptName, this.font.regular.base64string);
        this.pdf.addFont(this.font.regular.postScriptName, this.font.regular.fontName, this.font.regular.fontStyle);
        this.pdf.addFileToVFS(this.font.italic.postScriptName, this.font.italic.base64string);
        this.pdf.addFont(this.font.italic.postScriptName, this.font.italic.fontName, this.font.italic.fontStyle);
        this.pdf.addFileToVFS(this.font.bold.postScriptName, this.font.bold.base64string);
        this.pdf.addFont(this.font.bold.postScriptName, this.font.bold.fontName, this.font.bold.fontStyle);
    }
}

@Component({
    selector: 'mcno',
    templateUrl: './mcno.page.html'
})
export class Mcno implements OnInit {
    public mcnoList: any[] = [];
    public showVolet: boolean;
    dataSource = new MatTableDataSource(this.mcnoList);
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    private origineList = [];
    private destinataireList = [];
    private canalList = [];    

    public config: FieldConfig[] = [
        {
            type: 'select',
            name: 'origine',
            placeholder: "Origine",
            valueChanges: (val) => {
                let origine = this.origineList.find(l => l.nom == val);
            },
            class: "grid-demi"
        },
        {
            type: 'select',
            name: 'destinataire',
            valueChanges: (val) => {
                let destinataire = this.destinataireList.find(l => l.nom == val);
            },
            placeholder: "Destinataire",
            class: "grid-demi"
        },
        {
            type: 'select',
            name: 'canal',
            valueChanges: (val) => {
                let canal = this.canalList.find(l => l.nom == val);
            },
            placeholder: "Canal",
            class: "grid-demi"
        },
        {
            type: 'input',
            name: 'action',
            placeholder: "Action",
            class: "grid-demi"
        },
        {
            type: 'textArea',
            name: 'observation',
            placeholder: 'Observation',
            class: "grid-full"
        },
        {
            type: 'checkbox_group',
            name: 'type',
            options: ['Type1', 'Type3', 'Type4'],
            class: "grid-full"
        },
        {
            label: 'Ajouter',
            name: 'submit',
            type: 'button'
        }
    ];
    private changeListener: any;

    columnsToDisplay: any[];
    columnsToDisplayPDF: any[];

    constructor(private datePipe: DatePipe, private DB: DBService, private PDF: PDFService, private PARAM: ParametreService) {
        this.columnsToDisplay = ['poste', 'timeCode', 'auteur', 'observation', 'origine', 'destinataire', 'canal', 'action'];
        this.columnsToDisplayPDF = [{
            name: 'poste',
            width: 60
        }, {
            name: 'timeCode',
            width: 60
        }, {
            name: 'auteur',
            width: 100
        }, {
            name: 'observation',
            width: 180
        }, {
            name: 'origine',
            width: 100
        }, {
            name: 'destinataire',
            width: 100
        }, {
            name: 'canal',
            width: 100
        }, {
            name: 'action',
            width: 100
        }];
    }

    ngOnInit() {
        this.DB.getAll('mcno').then(list => {
            this.mcnoList = list.reverse()
            this.dataSource = new MatTableDataSource(this.mcnoList);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        });

        this.changeListener = this.DB.getTable('mcno').changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', (change) => {
            // delete from Array if exist
            this.mcnoList = this.mcnoList.filter(l => l != change.doc);
            // if added/edited => add
            if (!change.deleted) this.mcnoList.unshift(change.doc);

            this.dataSource = new MatTableDataSource(this.mcnoList);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;

        });

        this.initOrigineList(this.PARAM.currentParametre);
        this.PARAM.current.subscribe(r => { if (r.origine) this.initOrigineList(r) });

        this.initDestinataireList(this.PARAM.currentParametre);
        this.PARAM.current.subscribe(r => { if (r.destinataire) this.initDestinataireList(r) });

        this.initCanalList(this.PARAM.currentParametre);
        this.PARAM.current.subscribe(r => { if (r.canal) this.initCanalList(r) });
    }

     private initOrigineList(r) {
        if (!r || !r.origine || !this.configLine('origine')) return;
        this.origineList = r.origine;
        this.configLine('origine').options = r.origine.map(l => l.nom);
    }

    private initDestinataireList(r) {
        if (!r || !r.destinataire) return;
        this.destinataireList = r.destinataire;
        this.configLine('destinataire').options = r.destinataire.map(l => l.nom);
    }

    private initCanalList(r) {
        if (!r || !r.canal) return;
        this.canalList = r.canal;
        this.configLine('canal').options = r.canal.map(l => l.nom);
    }
    
    private configLine(name) {
        let index = this.config.findIndex(l => l.name == name);
        return this.config[index]
    }

    ngOnDestroy() {
        if (this.changeListener) this.changeListener.cancel();
    }
    public toggleVolet() {
        this.showVolet = !this.showVolet;

    }
    public submitMcno(value: any) {
        this.DB.put('mcno', value);
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    generatePDF() {
        const pdf = new McnoPDF(this.PDF.fonts.get('Arimo'));
        pdf.addLogo(this.PARAM.currentParametre['logo']);
        pdf.setDatePipe(this.datePipe);
        pdf.setColumns(this.columnsToDisplayPDF);
        pdf.setData(this.mcnoList);
        pdf.run();
    }
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

/**
 * Notification Service
 *
 * Provides interface to SMS sending backend service.
 *
 * Warning: backend implementation is unsafe and allows any text to be sent to
 * anyone. Safe implementation should 1) limit possible recipients by accepting
 * user IDs instead of phone numbers and 2) prevent arbitrary messages by
 * forming the text with content fetched directly from database or using
 * predefined templates. Alternatively it could also be possible to simply
 * trigger notification events from the frontend and leave the backend handle
 * choosing recipients and forming message contents.
 */
@Injectable()
export class NotificationService {

  constructor(private http: HttpClient) { }

/**
 * Send a notification
 *
 * @param recipient - recipients phone number
 * @param content - text to send
 *
 * @returns nothing
 */
  public send(recipient: string, message: string) {
    const url = `${environment.notificationServiceUrl}/`;
    return this.http.post<any>(url, { message, receiver: recipient });
  }

}

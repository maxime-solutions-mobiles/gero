import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { InterventionModel } from '../visualisation/models/intervention.model';
import { MoyenModel } from '../visualisation/models/moyen.model';
import { UserService } from './user.service';
import { DBService } from './db.service';

export interface CurrentState {
  intervention?: string;
  timeCode?: Date;
  auteur?: string;
  etat: string;
}

@Injectable()
export class MoyenService {

    private _states = {
        'alerte': { label: 'Alerté', color: 'danger', ref: 'alerte' },
        'sll': { label: 'Sur les lieux', color: 'warning', ref: 'sll' },
        'transport': { label: 'Transport', color: 'primary', ref: 'transport' },
        'retour': { label: 'Retour', color: 'accent', ref: 'retour' },
        'disponible': { label: 'Disponible', color: 'success', ref: 'disponible' },
    };
    public get stateRef(): string[] { return Object.keys(this.states) };
    private _currentMoyen: MoyenModel = new MoyenModel();
    private _current: any = new Subject<MoyenModel>();
    public get current() { return this._current.asObservable(); }

    public set currentMoyen(moyen: MoyenModel) {
        this._currentMoyen = moyen;
        this._current.next(moyen);
    }
    public setCurrentByID(id: string) {
        if (typeof id != "string") return;
        this.DB.getById('moyen', id).then((doc) => {
            this.currentMoyen = doc;
        });
    }
    public get currentMoyen() { return this._currentMoyen }

    constructor(
        private DB: DBService,
        private USER: UserService,
    ) { }

    // STATES
    public get states() { return this._states }
    public getNextState(curState): any {
        let sid = this.stateRef.indexOf(curState);
        let next = this.stateRef[sid + 1] || this.stateRef[0];
        return this.states[next];
    }
    public resetMoyenList(listIDMoyen: string[], interventionID?: string) {
        return this.DB.getByIdArray('moyen', listIDMoyen).then(arr => arr.forEach(m => this.resetStates(m)));
    }

    public async resetStates(moyen: MoyenModel, interventionID?: string) {
        // transfer current states to historique
        if (!moyen.historique) {
            moyen.historique = [];
        }

        moyen.historique.push({
            etats: moyen.currentStates,
            timeCode: this.USER.CONTEXT.timeCode,
            auteur: this.USER.CONTEXT.auteur,
            intervention: interventionID || moyen.currentIntervention
        });

        // reset attributs
        moyen.disponible = true;
        moyen.currentStates = [];
        moyen.currentIntervention = null;
        moyen.etat = 'disponible';
        moyen.standBy = false;
        moyen.updating = null;

        // update
        await this.DB.update('moyen', moyen);
        return moyen;
    }

    public async affectToInter(moyen: MoyenModel, inter: InterventionModel) {
        if (moyen.currentStates.length > 1) {
            moyen = await this.resetStates(moyen);
        }
        moyen.updating = null;
        moyen.currentIntervention = inter._id;
        if (!inter.started) {
            moyen.standBy = true;
        }
        return this.DB.update('moyen', moyen);
    }

    public async startInter(moyen: MoyenModel) {
        return this.goToState(moyen, 'alerte');
    }
    public async closeInter(moyen: MoyenModel) {
        return this.goToState(moyen, 'disponible');
    }
    public async goToState(moyen: MoyenModel, stateRef: string) {
        if (moyen.etat == stateRef) return moyen;
        // define next state index
        let newIndex = this.stateRef.indexOf(stateRef);
        if (moyen.currentStates[newIndex] && moyen.currentStates[newIndex].timeCode) return moyen;

        //console.log(`%c${moyen.nom} goToState ${stateRef} [${newIndex}]`, 'color:green');
        moyen.standBy = false;
        moyen.etat = stateRef;

        if (!moyen.currentStates) moyen.currentStates = [];
        for (let i = 0; i < newIndex; i++) {
            if (!moyen.currentStates[i])
                moyen.currentStates.push({
                    etat: this.stateRef[i],
                    timeCode: '-',
                    auteur: this.USER.CONTEXT.auteur
                });
        }

        moyen.currentStates.push({
            etat: this.stateRef[newIndex],
            timeCode: this.USER.CONTEXT.timeCode,
            auteur: this.USER.CONTEXT.auteur
        });

        if (stateRef == 'disponible') return this.resetStates(moyen, moyen.currentIntervention).then(r => moyen)
        else return this.DB.update('moyen', moyen)


    }


    public nextState(moyen: MoyenModel) {
        if (!moyen.currentIntervention) return;

        let index = this.stateRef.indexOf(moyen.etat);
        if (!moyen.etat || index < 0) return;
        let nextIndex = index < this.stateRef.length - 1 ? index + 1 : 0;
        this.goToState(moyen, this.stateRef[nextIndex]);
    }


    public toggleDispo(moyen: MoyenModel) {
        moyen.disponible = !moyen.disponible;
        return this.DB.update('moyen', moyen);
    }


    // CRUD
    // TODO : transfer vers admin

    public addMoyen(moyen: MoyenModel) {
        if (moyen._id) {
            return this.editMoyen(moyen);
        }
        return this.DB.put('moyen', moyen);
    }

    public editMoyen(moyen: MoyenModel) {
        return this.DB.update('moyen', moyen);
    }

}

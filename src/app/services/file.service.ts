import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

/**
 * File Service
 *
 * Provides interface to document service server which stores encrypted files.
 */
@Injectable()
export class FileService {
  constructor(private http: HttpClient) {}

  /**
   * Upload file
   *
   * @param file - blob from file input field
   *
   * @returns file info required to download the file later
   */
  public upload(file: File) {
    const url = `${environment.docServiceUrl}/`;

    const formData = new FormData();
    formData.append('file', file);

    interface FileUploadResponse {
      fileId: string;
      key: string;
      fileName: string;
    }

    return this.http.post<FileUploadResponse>(url, formData);
  }

  /**
   * Download file
   *
   * @param fileId   - file ID used to find the file
   * @param key      - key to decrypt file
   * @param fileName - original file name used to set name on download
   */
  public download(fileId: string, key: string, fileName: string) {
    const url = `${environment.docServiceUrl}/${fileId}/${fileName}`;

    this.http.post(url, { key }, { responseType: 'blob' }).subscribe(blob => {
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import PouchFind from 'pouchdb-find';
import PouchDebug from 'pouchdb-debug';
import PouchDB from 'pouchdb';

import { environment } from 'src/environments/environment';
import { UserService } from './user.service';
import { AuthService } from '../auth/auth.service';
import { ParticipantModel } from '../auth/participant.model';
import { EventModel } from '../auth/event.model';

PouchDB.plugin(PouchFind);
PouchDB.plugin(PouchDebug);

export enum SyncStatus {
  Error = 3,
  Syncing = 2,
  Done = 1,
}

@Injectable()
export class DBService {
  public ready: Promise<void>;
  private resolveReady;
  private rejectReady;

  private tableNames: string[] = [];

  public TABLE: any = {};

  private RemoteTABLE: any = {};
  private SyncTABLE: any = {};
  private currentResync: Promise<void> = Promise.resolve();

  // Current PouchDB sync status
  private syncStatus = new BehaviorSubject(SyncStatus.Syncing);

  // Sync status of each table
  private tableSyncStatuses: { [key: string]: SyncStatus } = {};

  private _tableLists: any = {
    common: [
      'cas',
      'patient',
      'bilan',
      'action',
      'observation',
      'conclusion',
      'mcno',
      'passage',
      'parametre',
      'document',
      'notification',
    ],
    user: [],
    pc: ['moyen', 'intervention', 'notification'],
    administrateur: ['moyen', 'intervention', 'notification'],
    auditeur: ['moyen', 'intervention', 'notification'],
  };

  private participant: ParticipantModel;

  private logStyle = 'color: #c0392b;';

  constructor(private authService: AuthService, private USER: UserService) {
    this.ready = new Promise((resolve, reject) => {
      this.resolveReady = resolve;
      this.rejectReady = reject;
    });

    this.authService.participantSession.subscribe(participant => {
      if (participant) {
        this.participant = participant;
        this.initDB();
      }
    });
  }

  private async getEvent(): Promise<EventModel> {
    try {
      const curEventDb = new PouchDB('current-event');
      return await curEventDb.get('currentEvent');
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  private async setEvent(tableNames: string[]) {
    const curEventDb = new PouchDB('current-event');
    let curEvent;
    try {
      curEvent = await curEventDb.get('currentEvent');
    } catch (err) {
      // not found
      curEvent = { _id: 'currentEvent' };
    }
    const eventInfo = this.participant.event;
    Object.assign(curEvent, eventInfo);
    curEvent.tableNames = tableNames;
    await curEventDb.put(curEvent);
    console.log(`Current event DOC is set to ${curEvent.databasePrefix}`);
  }

  private async checkEvent(): Promise<boolean> {
    const storedEvent: any = await this.getEvent();
    if (!storedEvent) {
      return false;
    }
    const activeEvent = this.participant.event;
    if (activeEvent.id === storedEvent.id) {
      return true;
    }
    console.log(`Current event DOC is set to ${storedEvent.databasePrefix}`);
    console.log(`while actual event is ${activeEvent.databasePrefix}`);
    return false;
  }

  private async sync(tableName: string) {
    if (this.SyncTABLE[tableName]) {
      this.SyncTABLE[tableName].cancel();
      delete this.SyncTABLE[tableName];
    }
    const isCorrectEvent = await this.checkEvent();
    if (!this.RemoteTABLE[tableName] || !isCorrectEvent) {
      return;
    }

    const syncOpts: PouchDB.Replication.SyncOptions = {
      live: true,
      retry: true,
    };

    this.SyncTABLE[tableName] = this.TABLE[tableName]
      .sync(this.RemoteTABLE[tableName], syncOpts)
      // replication paused (e.g. replication up to date, user went offline)
      .on('paused', () =>
        this.updateTableSyncStatus(tableName, SyncStatus.Done),
      )
      // handle complete
      .on('complete', () =>
        this.updateTableSyncStatus(tableName, SyncStatus.Done),
      )
      // handle change
      .on('change', () =>
        this.updateTableSyncStatus(tableName, SyncStatus.Syncing),
      )
      // replicate resumed (e.g. new changes replicating, user went back online)
      .on('active', () =>
        this.updateTableSyncStatus(tableName, SyncStatus.Syncing),
      )
      // a document failed to replicate (e.g. due to permissions)
      .on('denied', () =>
        this.updateTableSyncStatus(tableName, SyncStatus.Error),
      )
      // handle error
      .on('error', err => {
        console.error('PouchDB sync error', err);
        this.updateTableSyncStatus(tableName, SyncStatus.Error);
      });
  }

  private updateTableSyncStatus(tableName: string, status: SyncStatus) {
    this.tableSyncStatuses[tableName] = status;
    const combinedStatus = Math.max(...Object.values(this.tableSyncStatuses));
    this.syncStatus.next(combinedStatus);
  }

  public getSyncStatus() {
    const syncStatusSmoothing = 200;
    return this.syncStatus.pipe(debounceTime(syncStatusSmoothing));
  }

  private async createRemoteTable(tableName) {
    delete this.RemoteTABLE[tableName];
    const isCorrectEvent = await this.checkEvent();
    if (!isCorrectEvent) {
      return null;
    }
    const { databasePrefix } = this.participant.event;
    return (this.RemoteTABLE[tableName] = new PouchDB(
      `${environment.databaseUrl}/${databasePrefix}--${tableName}`,
      {
        fetch: (url, opts: any) => {
          opts.credentials = 'include';
          return PouchDB.fetch(url, opts);
        },
      },
    ));
  }

  public async initDB() {
    let tableList = this._tableLists.common;
    tableList = tableList.concat(this._tableLists[this.USER.INFO.fonction]);

    try {
      const checkEvent = await this.checkEvent();
      await this.initTables(tableList, checkEvent);
      console.log('Init table done');
      await this.syncAllTables();

      this.resolveReady();
    } catch (err) {
      console.error(err);
      this.rejectReady(err);
      throw err;
    }
  }

  public async initTables(tableNames: string[], keepData: boolean) {
    // definition/creation des tables locales
    console.log('INIT TABLES : ', tableNames.join(', '));
    this.tableNames = tableNames;
    if (!keepData) {
      console.log('Destroy all local tables');
      const oldEvent: any = await this.getEvent();
      if (oldEvent && oldEvent.tableNames) {
        await Promise.all(
          oldEvent.tableNames.map(async (tname: string) => {
            const toDestroy = new PouchDB(tname);
            await toDestroy.destroy();
          }),
        );
      }
    }

    const { event } = this.participant;
    const result = tableNames.map(tableName => {
      const fullName = `${event.databasePrefix}--${tableName}`;
      this.TABLE[tableName] = new PouchDB(fullName);
      return fullName;
    });
    await this.setEvent(result);
  }

  /**
   * Creation et synchro des tables distantes
   */
  public async syncAllTables() {
    this.currentResync = this.currentResync.then(() => {
      return this.doSyncAllTables();
    });
    return this.currentResync;
  }

  private async doSyncAllTables() {
    await Promise.all(
      this.tableNames.map(async tableName => {
        await this.createRemoteTable(tableName);
        await this.sync(tableName);
      }),
    );
    console.dir(Object.keys(this.RemoteTABLE));
  }

  public destroyAllLocalTables() {
    console.log(
      '%cDESTROY LOACAL tables :' + this.tableNames.join(','),
      this.logStyle,
    );
    for (const tname of this.tableNames) {
      this.TABLE[tname]
        .destroy()
        .then(() => console.log('%cLOCAL TABLE ', tname, ' DESTROYED'))
        .catch(err => console.error(err));
    }
  }

  public generateID(tableName: string, prefix: string = null) {
    const PREFIX = prefix ? tableName + ':' + prefix : tableName;
    const name = PREFIX + ':' + new Date().getTime();
    return name;
  }

  public putWithID(tableName: string, doc: any, ID: string) {
    console.groupCollapsed(
      '%cDB > put new doc in ' + tableName + '| With ID ' + ID,
      this.logStyle,
    );

    const docToSave = { ...this.USER.CONTEXT, ...doc };
    console.log('new doc To Save %O', docToSave);
    return this.add(tableName, docToSave, ID);
  }

  public put(
    tableName: string,
    doc: any = {},
    prefix: string = null,
  ): Promise<string> {
    console.groupCollapsed('%cDB > ADD new doc in ' + tableName, this.logStyle);
    const context = this.USER.CONTEXT;
    const docToSave = { ...context, ...doc };
    console.log('new doc To Save %O', docToSave);
    const PREFIX = prefix ? tableName + ':' + prefix : tableName;
    const name = PREFIX + ':' + new Date().getTime();
    return this.add(tableName, docToSave, name);
  }

  public add(tableName: string, docToSave: any, id: string): Promise<string> {
    return this.TABLE[tableName]
      .put({
        _id: id,
        ...docToSave,
      })
      .then((result: any) => {
        console.log('result %O', result);
        console.groupEnd();
        return result.id;
      })
      .catch((err: any) => {
        console.log('%cerror put in DB', this.logStyle);
        if (err.name === 'conflict') {
          console.log('%cCONFLICT', this.logStyle);
        }
        console.error(err);
        console.groupEnd();
      });
  }

  public update(tableName: string, doc: any = {}, id?: any): Promise<string> {
    console.groupCollapsed(`%cDB > UPDATE doc in ${tableName}`, this.logStyle);
    console.log('id? :', id);

    const table = this.TABLE[tableName];
    const updatedDoc = doc;
    const ID = id || doc._id;

    console.log(`%cID : ${ID}`, this.logStyle);
    return table
      .get(ID)
      .then(theDoc => {
        updatedDoc._rev = theDoc._rev;
        console.log('old doc : %O', theDoc);
        let docContext: any = {};
        Object.keys(this.USER.CONTEXT).forEach(
          k => (docContext[k] = theDoc[k]),
        );
        let docTOSend = {
          _id: ID,
          ...docContext,
          ...updatedDoc,
        };
        console.log('doc TO Send : %O', docTOSend);
        return table.put(docTOSend);
      })
      .then((result: any) => {
        console.log('result %O', result);
        console.groupEnd();
        return result.id;
      })
      .catch((err: any) => {
        console.log('%cerror update in DB', this.logStyle);
        if (err.name === 'conflict') {
          console.log('%cCONFLICT', this.logStyle);
        }
        console.error(err);
        console.groupEnd();
      });
  }

  public delete(tableName: string, doc: any = {}, id?: string) {
    const table = this.TABLE[tableName];
    const updatedDoc = doc;
    const ID = id || doc._id;
    return table
      .get(ID)
      .then(theDoc => {
        updatedDoc._rev = theDoc._rev;
        return table.remove(theDoc);
      })
      .catch(err => console.error(err));
  }

  public getTable(tableName) {
    return this.TABLE[tableName];
  }

  public getTableLength(tableName: string): Promise<number> {
    return this.filter(tableName, {
      _id: { $regex: new RegExp(tableName, 'gi') },
    }).then(r => r.list.length);
  }

  public getNivList() {
    return this.getAll('cas').then(list =>
      list
        .filter(l => l.niv)
        .map(l => l.niv)
        .sort(),
    );
  }

  public async getAll(tableName: string): Promise<any[]> {
    await this.ready;
    return this.TABLE[tableName]
      .allDocs({
        include_docs: true,
        startkey: tableName,
        endkey: tableName + '\ufff0',
      })
      .then(db => db.rows.map(row => row.doc))
      .catch(err => console.error(err));
  }

  public async getAllRef(tableName: string): Promise<any[]> {
    await this.ready;
    return this.TABLE[tableName]
      .allDocs({
        startkey: tableName,
        endkey: tableName + '\ufff0',
      })
      .then(db =>
        db.rows.map(row => {
          return { _id: row.id, _rev: row.value.rev };
        }),
      )
      .catch(err => console.error(err));
  }

  public getAttachmentById(id: string): Promise<any> {
    return this.TABLE['document'].get(id, { attachments: true });
  }

  public getById<T = any>(tableName: string, id: any): Promise<T> {
    return this.TABLE[tableName].get(id);
  }

  public async getByIdArray(tableName: string, ids: any[]): Promise<any[]> {
    if (!ids || !Array.isArray(ids) || ids.length < 1) {
      return [];
    }

    return this.TABLE[tableName]
      .allDocs({
        include_docs: true,
        keys: [...ids],
      })
      .then(db => db.rows.map(row => row.doc))
      .catch(err => {
        console.error(err);
        console.log(
          '%c|=> get ids ' + ids.join('/') + 'in table' + tableName,
          'color:red',
        );
      });
  }

  public getFieldsIn(tableName: string, fields: string[]) {
    let selector = {};
    fields.forEach(key => (selector[key] = { $gt: null }));
    return this.filter(tableName, selector).then(retour => retour.list);
  }

  public async filter(tableName: string, filter: any): Promise<any> {
    await this.ready;

    let OPTIONS: any = {
      selector: {},
    };

    // get DB table
    let TABLE = this.TABLE[tableName];

    OPTIONS.selector = filter;
    OPTIONS.sort = [{ _id: 'desc' }];

    if (!OPTIONS.selector._id) {
      OPTIONS.selector._id = { $exists: true };
    }

    // DEFINE FIELDS
    let INDEXFIELDS = Object.keys(OPTIONS.selector);
    let INDEXNAME = INDEXFIELDS.join('_');

    if (
      !OPTIONS ||
      !OPTIONS.selector ||
      !INDEXFIELDS ||
      !Array.isArray(INDEXFIELDS) ||
      !tableName
    ) {
      console.log(
        '!!!! DB.filter >',
        OPTIONS,
        OPTIONS.selector,
        INDEXFIELDS,
        tableName,
      );

      return false;
    }

    return this.TABLE[tableName]
      .createIndex({
        index: { fields: INDEXFIELDS },
        ddoc: INDEXNAME,
        name: INDEXNAME,
      })
      .then(() => {
        return this.TABLE[tableName]
          .find(OPTIONS)
          .then(filtredList => ({ list: filtredList.docs, options: OPTIONS }))
          .catch(err => console.error(err));
      })
      .catch(err => console.error(err));
  }

  public getTableList() {
    return this._tableLists;
  }

  public async load_file_from_local(getFile: File) {
    let currentJsonFile = {};
    const curEventDb = new PouchDB('current-event');
    ////// remove already exist file
    try {
      let doc = await curEventDb.get('json');
      curEventDb.remove(doc);
    } catch (err) {
      console.log(err);
    }
    console.log(getFile.type);

    // attachment file
    Object.assign(currentJsonFile, {
      _id: 'json',
      _attachments: {
        file: {
          type: 'application/json',
          data: getFile,
        },
      },
    });

    return await curEventDb
      .put(currentJsonFile)
      .then(() => curEventDb.getAttachment('json', 'file'))
      .then(blob => window.URL.createObjectURL(blob))
      .catch(err => {
        console.log(err);
        return null;
      });
  }
}

import { InterventionService } from './intervention.service';
import { MoyenService } from './moyen.service';
import { MoyenModel } from './../visualisation/models/moyen.model';
import { CasService } from './cas.service';
import { UserService } from './user.service';
import { Subject } from 'rxjs/Subject';
import { InterventionModel } from './../visualisation/models/intervention.model';
import { DBService } from './db.service';
import { CasModel } from './../visualisation/models/cas.model';
import { Injectable } from '@angular/core';
import PouchFind from 'pouchdb-find';
import PouchDB from 'pouchdb-browser';
PouchDB.plugin(PouchFind);
import { NotificationModel } from '../visualisation/models/notification.model';
import { Observable, pipe } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { PassageModel } from '../visualisation/models/passage.model';
import { PassageService } from './passage.service';

@Injectable()
export class ListService {
    private logStyle = 'color: #8e44ad;';

    private _currentFilters: { cas: any, passage:any, moyen: any, intervention: any } = {
        cas: null,
        passage: null,
        moyen: null,
        intervention: null
    };
    private _currentOrder: { cas: any, passage:any, moyen: any, intervention: any } = {
        cas: null,
        passage: null,
        moyen: null,
        intervention: null
    };
    private pagination: { cas: any, passage:any, moyen: any, intervention: any } = {
        cas: { limit: 10, page: 0, next: false, last: false },
        passage: { limit: 10, page: 0, next: false, last: false },
        moyen: { limit: 16, page: 0, next: false, last: false },
        intervention: { limit: 10, page: 0, next: false, last: false }
    };
    public lists: {
        cas: CasModel[], passage: PassageModel[], intervention: InterventionModel[], moyen: MoyenModel[],
        notification: { regulation: any[], discussion: any[], intervention: any[], conc_regulation_type: any[], notif_regulation_type:any[] }
    } = {
            cas: [],
            intervention: [],
            passage: [],
            moyen: [],
            notification: { regulation: [], discussion: [], intervention: [], conc_regulation_type: [], notif_regulation_type: [] }
        };

    public pageList: {
        cas: CasModel[], passage:PassageModel[], intervention: InterventionModel[], moyen: MoyenModel[]
    } = {
            cas: [],
            passage: [],
            intervention: [],
            moyen: []
        };
    public pageListObservables = {
        cas: new Subject<CasModel[]>(),
        passage: new Subject<PassageModel[]>(),
        intervention: new Subject<InterventionModel[]>(),
        moyen: new Subject<MoyenModel[]>()
    }
    public listObservables = {
        cas: new Subject<CasModel[]>(),
        passage: new Subject<PassageModel[]>(),
        intervention: new Subject<InterventionModel[]>(),
        moyen: new Subject<MoyenModel[]>(),
        notification: new Subject<{ regulation: any[], discussion: any[], intervention: any[], conc_regulation_type: any[], notif_regulation_type: any[] }>()
    }

    public listsRef: { cas: CasModel[], passage: PassageModel[], intervention: InterventionModel[], moyen: MoyenModel[] } = {
        cas: [],
        passage: [],
        intervention: [],
        moyen: []
    };
    public defaultFilters: { cas: any, passage:any, moyen: any, intervention: any } = {
        cas: () => {
            let filter: any = {
                'clos': { $exists: false },
                '_id': { $regex: new RegExp('cas', 'gi') }
            };

            if (this.USER.INFO && this.USER.INFO.poste) {
                filter.poste = this.USER.INFO.poste
            } else filter.poste = { $gt: null };

            if (this.USER.INFO && this.USER.INFO.isOmnipotent) {
                filter.poste = { $gt: null };
            }
            return filter;
        },
        passage: () =>  {
            let filter: any = {
                'sortie': { $exists: false },
                '_id': { $regex: new RegExp('passage', 'gi') }
            };
            if (this.USER.INFO && this.USER.INFO.poste) {
                filter.lieu = this.USER.INFO.poste;
            }
            return filter;
        },
        moyen: () => {
            let filter: any = {
                '_id': { $regex: new RegExp('moyen', 'gi') }
            };
            return filter;
        },
        intervention: () => {
            let filter: any = {
                'clos': { $exists: false },
                '_id': { $regex: new RegExp('intervention', 'gi') }
            };
            return filter;
        }
    };

    private _currentFiltersObservable: any = new Subject<{ cas: any, passage:any, moyen: any, intervention: any }>();
    private _filtering: any = new Subject<boolean>();
    private _change: any = new Subject<boolean>();
    public get change() { return this._change.asObservable(); }
    private _currentOrderObservable: any = new Subject<{ cas: any, passage:any, moyen: any, intervention: any }>();
    public get currentPagination() { return this.pagination }
    private _paginationObservable: any = new Subject<{ cas: any, passage:any, moyen: any, intervention: any }>();
    /*private getChangeKeys(before: any, after: any): string[] {
        if (!before || !after) return;
        return Object.keys(after).reduce((diff, key) => {
            if (before[key] === after[key] || after[key] == '_rev') return diff
            return [...diff, key]
        }, []);
    }*/
    private curCas: CasModel;
    constructor(
        private DB: DBService,
        private USER: UserService,
        private CAS: CasService,
        private PASSAGE: PassageService,
        private MOYEN: MoyenService,
        private INTER: InterventionService) { }

    public get READY() {

        let userFilter = {
            "_id": "_design/USER_FONCTION_FILTER",
            "filters": {
                "USER": function (doc, req) {
                    return !doc.clos && (doc.poste === null || doc.poste === req.query.poste);
                }.toString()
            }
        };
        if (this.USER.INFO.fonction == 'user') {
            return this.DB.getById('cas', '_design/USER_FONCTION_FILTER').then(doc => {
                if (doc) return this.initSync();
                else return this.DB.TABLE['cas'].put(userFilter).then(r => { return this.initSync() });
            })
        }
        else return this.initSync();
    }

    private initSync() {
        let syncP: any[] = [];
        if (this.USER.INFO.fonction == "user") {
            if (this.USER.INFO.poste) syncP.push(this.initList('cas', 'USER'));
            this.USER.poste.subscribe(poste => syncP.push(this.initList('cas', 'USER')));
        } else {
            syncP.push(this.initList('cas'));
            syncP.push(this.initList('passage'));
            syncP.push(this.initList('moyen'));
            syncP.push(this.initList('intervention'));
            syncP.push(this.setNotificationList());
        }

        return Promise.all([...syncP]).then(arr => { return arr })
    }

    private initList(listName: string, userFilter?: any) {
        this._currentFilters[listName] = this.defaultFilters[listName]();
        return this.DB.getAll(listName).then(async r => {
            this.listsRef[listName] = r.sort((a, b) => new Date(b.timeCode).getTime() - new Date(a.timeCode).getTime());
            //if (this.lists[listName].length < 1) {}
            this.listObservables[listName].next(this.listsRef[listName]);
            this.filtreList(listName, this.defaultFilters[listName]());

            return this.listsRef[listName];
        });
    }
    private changeListener: any = { cas: null, intervention: null, moyen: null };

    public stopListenChanges(listName) {
        if (this.changeListener && this.changeListener[listName]) this.changeListener[listName].cancel()
    }
    public stopAllListenChanges() {
        if (this.changeListener) Object.keys(this.changeListener).forEach(k => this.stopListenChanges(k));
    }
    public async startListenChanges(listName, userFilter?) {
        let OPTIONS: any = {
            since: 'now',
            live: true,
            include_docs: true
        }
        if (userFilter) {

            OPTIONS.filter = 'USER_FONCTION_FILTER/' + userFilter;
            OPTIONS.query_params = { "poste": this.USER.INFO.poste }
        }

        await this.DB.ready;
        this.changeListener[listName] = this.DB.TABLE[listName].changes(OPTIONS)
            .on('change', (change: any) => {
                //console.log('%cLIST SERVICE > change on ' + listName, this.logStyle);

                if (change.id.includes('_design')) return;
                if (change.doc.updating) change.doc.updating = false;
                // Diffuse to listeners
                this._change.next(change.doc);

                // update current if needed
                if (this.CAS.currentObjects['cas'] && this.CAS.currentObjects['cas']._id == change.id) {
                    if (this.CAS.currentObjects[listName] != change.doc) this.CAS.currentCas = change.doc
                }

                if (this.PASSAGE.currentObjects['passage'] && this.PASSAGE.currentObjects['passage']._id == change.id) {
                    if (this.PASSAGE.currentObjects[listName] != change.doc) this.PASSAGE.currentCas = change.doc
                }

                if (this.MOYEN.currentMoyen && this.MOYEN.currentMoyen._id == change.id) {
                    if (this.MOYEN.currentMoyen != change.doc) {
                        this.MOYEN.currentMoyen = change.doc
                    }
                }
                if (this.INTER.currentInter && this.INTER.currentInter._id == change.id) {
                    if (this.INTER.currentInter != change.doc) {
                        this.INTER.currentInter = change.doc
                    }
                }

                // if edited &&  is in  the current list => change item
                let indexInRefList = this.listsRef[listName].findIndex(l => l._id == change.id);
                let indexInList = this.lists[listName].findIndex(l => l._id == change.id);

                if (this.listsRef[listName][indexInRefList] == change.doc) return;

                if (change.deleted) {
                    this.listsRef[listName].splice(indexInRefList, 1);
                    this.lists[listName].splice(indexInList, 1);
                    return this.goPage(listName, this.pagination[listName].page);
                }

                // filter if order may have changed
                if (indexInRefList > -1) this.listsRef[listName].splice(indexInRefList, 1, change.doc)
                else this.listsRef[listName].unshift(change.doc);

                this.listObservables[listName].next(this.listsRef[listName]);


                if (indexInList > -1) {
                    // edit list if order is the same
                    this.lists[listName].splice(indexInList, 1, change.doc);
                    this.refreshList(listName)
                } else {
                    // if added
                    this.lists[listName].unshift(change.doc);
                    this.refreshList(listName, 0)
                }


            })
            .on('error', (err: any) => console.error('err on change TABLE ' + listName + ' : ', err));



    }
    public updateItemInList(item: any, listName: string) {
        let indexInRefList = this.listsRef[listName].findIndex(l => l._id == item._id);
        this.listsRef[listName].splice(indexInRefList, 1, item)
        this.listObservables[listName].next(this.listsRef[listName]);

    }
    public addItemToList(listName: string, doc: any) {
        this.listsRef[listName].unshift(doc);
        this.lists[listName].unshift(doc);
        this.listObservables[listName].next(this.listsRef[listName]);
        this.goPage(listName, 0)
    }
    public refreshListITem(listName: string, docID: any) {
        this.DB.getById(listName, docID).then(doc => this.editListItem(listName, doc))
    }

    public updatingListItem(listName: string, doc: any) {
        let indexInRefList = this.listsRef[listName].findIndex(l => l._id == doc._id);
        if (indexInRefList > -1) this.listsRef[listName].splice(indexInRefList, 1, doc)
        this.listObservables[listName].next(this.listsRef[listName]);
    }

    public editListItem(listName: string, doc: any) {
        let indexInRefList = this.listsRef[listName].findIndex(l => l._id == doc._id);
        let indexInList = this.lists[listName].findIndex(l => l._id == doc._id);
        if (indexInRefList > -1) this.listsRef[listName].splice(indexInRefList, 1, doc)
        if (indexInList > -1) this.lists[listName].splice(indexInList, 1, doc);
        this.listObservables[listName].next(this.listsRef[listName]);
        this.goPage(listName, this.pagination[listName].page)
    }
    public removeItemFromList(listName: string, doc: any) {
        let indexInRefList = this.listsRef[listName].findIndex(l => l._id == doc._id);
        let indexInList = this.lists[listName].findIndex(l => l._id == doc._id);
        if (indexInRefList > -1) this.listsRef[listName].splice(indexInRefList, 1)
        if (indexInList > -1) this.lists[listName].splice(indexInList, 1);
        this.listObservables[listName].next(this.listsRef[listName]);
        this.goPage(listName, this.pagination[listName].page)
    }

    private setNotificationList() {

        this.currentFilters['notification'] = { 'close': { $exists: false } };
        return this.DB.getAll('notification').then(async r => {
            this.listsRef['notification'] = r;
            this.filtreNotifList();
            await this.DB.ready;
            this.DB.TABLE.notification.changes({
                since: 'now',
                live: true,
                include_docs: true
            })
                .on('change', (change: any) => {
                    let indexInRefList = this.listsRef['notification'].findIndex(l => l._id == change.id);
                    if (change.deleted || change.doc.close) {
                        this.listsRef['notification'].splice(indexInRefList, 1);
                        return this.filtreNotifList();
                    }

                    if (indexInRefList > -1) this.listsRef['notification'].splice(indexInRefList, 1, change.doc)
                    else this.listsRef['notification'].unshift(change.doc);
                    this.filtreNotifList();

                })
                .on('error', (err: any) => console.error('err on change TABLE notification : ', err));


            return this.listsRef['notification'];
        });
    }
    public filtreNotifList() {

        let formatList = this.getNotifIdByType(this.listsRef['notification']);
        this.lists['notification'] = formatList;
        this.listObservables['notification'].next(formatList);
    }
    public resetList(tableName: string): void {

        this._filtering.next(true);
        this.initList(tableName).then(r => this._filtering.next(false));

        /*this.DB.getAll(tableName).then(r => this.listsRef[tableName] = r);//update ref list
        this._filtering.next(true);
        this.filtreList(tableName, this.defaultFilters[tableName]())*/
    }


    ////////////////////////////////////////////////////
    public get filtering() { return this._filtering.asObservable(); }
    public get currentFiltersObservable() { return this._currentFiltersObservable.asObservable(); }
    public get currentFilters() { return this._currentFilters }
    public get paginationObservable() { return this._paginationObservable.asObservable(); }
    public get casListObservable(): Observable<CasModel[]> { return this.pageListObservables['cas'].asObservable(); }
    public get passageListObservable() { return this.pageListObservables['passage'].asObservable(); }
    public get moyenListObservable(): Observable<MoyenModel[]> { return this.pageListObservables['moyen'].asObservable(); }

    public get interventionListObservable(): Observable<InterventionModel[]> { return this.pageListObservables['intervention'].asObservable(); }
    public get lastCasList(): CasModel[] { return this.pageList['cas']; }
    public get lastPassageList() { return this.pageList['passage']; }
    public get moyenList(): MoyenModel[] { return this.pageList['moyen'] }
    public get interventionList(): InterventionModel[] { return this.pageList['intervention'] }

    public moyensListById(moyenIds: string[]) {
        if (!moyenIds || !moyenIds.length) {
          return [];
        }
        return this.listsRef.moyen.filter((moyen) => moyenIds.includes(moyen._id));
    }

    public casListById(casIds: string[]) {
        if (!casIds || !casIds.length) {
          return [];
        }
        return this.listsRef.cas.filter((cas) => casIds.includes(cas._id));
    }

    ////////////////////////////////////////////////////

    public convertMongo(filters: any[]) {
        // !!!  Non exhaustif
        let keys = Object.keys(filters);
        let cond = keys.map(key => {
            if (!filters[key]) return 'true';
            if (typeof filters[key] == 'string') return 'l["' + key + '"] && l["' + key + '"] =="' + filters[key] + '"';
            if (filters[key].$regex) return 'l["' + key + '"] && l["' + key + '"].match(' + filters[key].$regex + ')';
            if (filters[key].$exists === false) return '!l["' + key + '"]';
            if (filters[key].$exists) return 'l["' + key + '"]';
            if (filters[key].$gt === null) return 'l["' + key + '"]';
            if(filters[key].$in) {
                if(key == '_id') {
                    return 'l["' + key + '"] && ["' + filters[key].$in.join('","') + '"].indexOf(l["' + key + '"]) >= 0';
                } else {
                    return 'l["' + key + '"] && l["' + key + '"].some(r=> ["' + filters[key].$in.join('","') + '"].indexOf(r) >= 0)';
                }
            }
            if (Array.isArray(filters[key])) {
              return 'l["' + key + '"] && ["' + filters[key].join('","') + '"].includes(l["' + key + '"])';
            }
            return 'true';
        });
        return cond.filter(l => l && l.trim).join('&&');


    }
    ////////////////////////////////////////////////////

    public refreshList(listName: string, page?: number) {
        return this.filtreList(listName, this._currentFilters[listName], page || this.pagination[listName].page);
    }

    public filtreList(tableName: string, crits?: any, page: number = 0): Promise<any[]> {
        if (!crits || !Object.keys(crits)[0]) crits = this.defaultFilters[tableName]();

        this._filtering.next(true);
        let list = this.listsRef[tableName].filter(l => eval(this.convertMongo(crits)));
        this.lists[tableName] = list;

        this.listObservables[tableName].next(list);
        this._filtering.next(false);
        this.goPage(tableName, page);
        this._currentFilters[tableName] = crits;
        this._currentFiltersObservable.next(this._currentFilters);
        return new Promise(resolve => {
            resolve(list);
        });
    }

    public goPage(tableName: string, pageNumber: number) {
        this._filtering.next(true);
        const { limit } = this.pagination[tableName];
        const skip = limit * pageNumber;
        const list = this.lists[tableName].slice(skip, skip + limit);
        this.pageList[tableName] = list;
        this.pageListObservables[tableName].next(list);
        this.definePagination(tableName, pageNumber);
        this._filtering.next(false);
    }

    private definePagination(tableName: string, pageNumber: number = 0) {
        const totalItems = this.lists[tableName].length;
        const limit = this.pagination[tableName].limit;
        const totalPages = Math.ceil(totalItems / limit);

        this.pagination[tableName].page = pageNumber;
        this.pagination[tableName].last = false;
        this.pagination[tableName].next = false;

        if (pageNumber > 0) {
            this.pagination[tableName].last = true;
        }

        if (pageNumber < (totalPages - 1)) {
            this.pagination[tableName].next = true;
        }

        this._paginationObservable.next(this.pagination);
    }

    public getNotifIdByType(notifs) {
        const openNotifs = notifs.filter(n => !n.close);
        const list: {
            regulation: string[],
            intervention: string[],
            discussion: string[],
            conc_regulation_type: string[]
            notif_regulation_type: string[]
        } = {
            regulation: [],
            discussion: [],
            intervention: [],
            conc_regulation_type: [],
            notif_regulation_type: []
        };

        openNotifs.forEach((notif: NotificationModel) => {
            if (list[notif.demande] && !list[notif.demande].includes(notif.cas)) {
                list[notif.demande].push(notif.cas);
            }
        });

        notifs.forEach((notif: NotificationModel) => {
            if (notif.demande === 'regulation' && list.notif_regulation_type) {
                list.notif_regulation_type.push(notif.cas);
            }
        });

        this.DB.filter('conclusion', { cas: { $gt: 'cas' } }).then((doc) => {
            const reg = [];
            doc.list.forEach((element) => {
                if (element.regulation && element.regulation.length > 0) {
                    if (element.regulation[element.regulation.length - 1].type === 'regulation' && element.cas) {
                        reg.push(element.cas);
                    }
                }
            });
            this.lists.notification.conc_regulation_type = reg;
        });
        return list;
    }

    async getDemadeType(openNotifs) {
        let result = null;
        let filter = {
            '_id': {
                $regex: new RegExp('conclusion', 'gi')
            },
            'cas': openNotifs
        };
        const func = this.DB.filter('conclusion', filter);
        result = await func;
        if(result && result.list && result.list.length > 0){
            let conclusion =  result.list[0];
            if(conclusion.regulation && conclusion.regulation.length > 0)  {
                return conclusion.regulation[conclusion.regulation.length - 1].type;
            }
        }
        return null;
    }
}

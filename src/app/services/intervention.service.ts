import { UserService } from './user.service';
import { InterventionModel } from './../visualisation/models/intervention.model';
import { DBService } from './db.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { NotificationService } from './notification.service';
import * as moment from 'moment';

@Injectable()
export class InterventionService {

    private _currentInter: InterventionModel = new InterventionModel();
    private _current: any = new Subject<InterventionModel>();
    public get current() { return this._current.asObservable(); }
    public set currentInter(inter: InterventionModel) {
        this._currentInter = inter;
        this._current.next(inter);
    }
    public setCurrentByID(id: string) {
        if (typeof id != "string") return;
        this.DB.getById('intervention', id).then((doc) => {
            this.currentInter = doc;
        });
    }

    private _currentInterChange: any = new Subject<any>();
    public get currentInterChange() { return this._currentInterChange.asObservable(); }

    public get currentInter() { return this._currentInter }

    private _currentUpdate: Subject<any> = new Subject<any>();
    public get currentUpdate() { return this._currentUpdate }
    public setCurrentUpdate(val) { this._currentUpdate.next(val) }

    constructor(
        private DB: DBService,
        private USER: UserService,
        private notification: NotificationService,
    ) { }

    public async affect(type: 'moyen' | 'cas' | 'passage', id: string) {
        const inter = this._currentInter;
        inter.updating = type;

        if (inter[type] && inter[type].includes(id)) {
            inter.updating = false;
            return;
        }

        if (!inter[type]) {
          inter[type] = [];
        }
        inter[type].push(id);
        this._currentInterChange.next(inter);

        return this.DB.update('intervention', inter)
            .then(r => {
                inter.updating = false;
                this.currentInter = inter;
                this._currentInterChange.next(inter);
            });
    }

    public async cancel(type: 'moyen' | 'cas', id: string) {

        let inter = this._currentInter;
        inter.updating = type;

        if (!inter[type].includes(id)) return;
        else {

            if (!inter.historique) inter.historique = [];
            inter.historique.push({ cancel: type, id: id, ...this.USER.CONTEXT })

            inter[type] = inter[type].filter(l => l != id);
            this._currentInterChange.next(inter);


            return this.DB.update('intervention', inter)
                .then(r => {
                    inter.updating = false;
                    this.currentInter = inter;
                    console.log(`Cancel ${id} TO ${inter._id}`)
                });
        }
    }
    public cancelInter() {
        if (!this.currentInter.historique) this.currentInter.historique = [];
        this.currentInter.historique.push({ start: this.currentInter.started });
        this.currentInter.historique.push({ cancel: this.USER.CONTEXT });
        this.currentInter.clos = this.USER.CONTEXT;
        return this.DB.update('intervention', this.currentInter);
    }
    public async start() {
        this.currentInter.started = this.USER.CONTEXT;
        return this.DB.update('intervention', this.currentInter);
    }
    public async close() {
        this.currentInter.clos = this.USER.CONTEXT;
        return this.DB.update('intervention', this.currentInter);
    }

    public addComment(comment: any) {
        if (!this.currentInter.discussion) this.currentInter.discussion = [];
        this.currentInter.discussion.push({ ...comment, ...this.USER.CONTEXT });
        return this.DB.update('intervention', this.currentInter);
    }

/**
 * Send SMS to each moyens assigned to intervention
 */
    public notifiyMoyens(intervention, moyenList, casList) {
        let casText = casList.map((cas) => cas.niv).join(', ');
        if (casList.length === 1) {
            casText = `pour la victime ${casText}`;
        }
        if (casList.length > 1) {
            casText = `pour les victimes ${casText}`;
        }

        moyenList.forEach((moyen) => {
            const ts = moment(intervention.timeCode).format('D[/]M [à] H[h]mm');
            const place = intervention.adresse || intervention.lieu;
            const message = `
              Déclenchement ${moyen.nom}, le ${ts}, inter n° ${intervention.numero}.
              Motif : ${intervention.motif}. Lieu : ${place}
              ${casText}.`.replace(/\s+/gs, ' ').trim();

            if (!moyen.telephone) {
                console.error('Unable to send message, phone number required');
                return;
            }

            this.notification.send(moyen.telephone, message).subscribe();
        });

        this.currentInter.notificationsSentAt = new Date();
        this._current.next(this.currentInter);
        return this.DB.update('intervention', this.currentInter);
    }

}

import { Injectable } from '@angular/core';
import { from, ReplaySubject } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { UserService } from './user.service';
import { CasModel } from '../visualisation/models/cas.model';
import { PatientModel } from '../visualisation/models/patient.model';
import { ActionModel } from '../visualisation/models/action.model';
import { ObservationModel } from '../visualisation/models/observation.model';
import { ConclusionModel } from '../visualisation/models/conclusion.model';
import { PassageModel } from '../visualisation/models/passage.model';
import { OrdonnanceModel } from '../visualisation/detail-cas/forms/conclusion-ordonnance.component';
import { DBService } from './db.service';
import { InterventionModel } from '../visualisation/models/intervention.model';
import { NotificationModel } from '../visualisation/models/notification.model';
import { CourierModel } from '../visualisation/detail-cas/forms/conclusion-courier.component';
import { FileService } from './file.service';

const modelsRef = {
    patient: PatientModel,
    cas: CasModel,
    observation: ObservationModel,
    action: ActionModel,
    conclusion: ConclusionModel,
    passage: PassageModel,
    ordonnance: OrdonnanceModel,
    courier: CourierModel
};
export type Cloture = 'lsp' | 'décédé' | 'décharge' | 'confié' | 'autre' | 'transport';

@Injectable()
export class CasService {
    private logStyle = "color: #d35400;";
    private modelRef: any = {
        cas: new CasModel(),
        patient: new PatientModel(),
        action: new ActionModel(),
        observation: new ObservationModel(),
        conclusion: new ConclusionModel(),
        passage: new PassageModel()
    };

    private _currentObjects: any = { ...this.modelRef };

    public set currentObjects(obj: any) {
        Object.keys(obj).forEach(k => {
            if (this._currentObjects[k] != obj[k]) {
                this._currentObjects[k] = obj[k];
                this.currentSubject[k].next(obj[k]);
            }
        });
    }
    public get currentObjects() {

        let retour = {
            cas: new CasModel(),
            patient: new PatientModel()
        };
        retour = { ...retour, ...this._currentObjects };
        return retour;
    }

    public currentSubject = {
        cas: new ReplaySubject<CasModel>(),
        patient: new ReplaySubject<PatientModel>(),
        action: new ReplaySubject<ActionModel>(),
        observation: new ReplaySubject<ObservationModel>(),
        conclusion: new ReplaySubject<ConclusionModel>(),
        passage: new ReplaySubject<PassageModel>()
    }
    public get casObservable() { return this.currentSubject['cas'].asObservable(); }
    public get patientObservable() { return this.currentSubject['patient'].asObservable(); }
    public get actionObservable() { return this.currentSubject['action'].asObservable(); }
    public get observationObservable() { return this.currentSubject['observation'].asObservable(); }
    public get conclusionObservable() { return this.currentSubject['conclusion'].asObservable(); }
    public get passageObservable() { return this.currentSubject['passage'].asObservable(); }
    private dependencieList = ['patient', 'action', 'observation', 'conclusion', 'passage']

    constructor(
        private DB: DBService,
        private USER: UserService,
        private fileService: FileService
    ) { }

    // maintain & update current cas
    public set currentCas(idOrCas) {
        if (idOrCas === 'new') {
            let newCas = new CasModel();
            this.currentObjects = { cas: newCas }
            this.defineCurrent('patient', null)
            return;
        }
        if (idOrCas === null) {
            this.currentObjects = {};
            this.defineAllDependencies(new CasModel())
            return;

        }
        if (idOrCas.urgence) {
            this.currentObjects = { cas: idOrCas };
            this.defineAllDependencies(idOrCas)
            return;
        }

        let ID = idOrCas instanceof CasModel ? idOrCas._id : idOrCas;
        if (typeof ID != "string") return;

        this.DB.getById('cas', ID).then((doc) => {
            if (!doc) return;
            this.currentObjects = { cas: doc }
            this.defineAllDependencies(doc)

        }).catch((err) => console.error(err));
    }

    public async addPatient(patient) {
        // create patient
        let newPatient = new PatientModel();
        newPatient = { ...this.currentObjects.patient, ...patient };

        // add current cas to patient
        if (!newPatient.cas || !Array.isArray(newPatient.cas)) {
            newPatient.cas = [];
        }
        if (!newPatient.cas.includes(this.currentObjects.cas._id)) {
            newPatient.cas.push(this.currentObjects.cas._id);
        }

        let ID: string;

        // add || edit
        if (newPatient._id) {
            ID = await this.DB.update('patient', newPatient);
        } else {
            ID = await this.DB.put('patient', newPatient);
        }

        // update patient cas list
        const casData = {
            nom: newPatient.nom,
            prenom: newPatient.prenom,
            age: newPatient.age,
            sexe: newPatient.sexe,
            patient: ID,
        };

        this.currentObjects = { patient: newPatient };

        const casList = await this.DB.getByIdArray('cas', newPatient.cas);
        const updates = casList.map((cas) => {
            return this.DB.update('cas', { ...cas, ...casData });
        });
        await Promise.all(updates);
        this.defineCurrent('patient', newPatient);
    }


    public defineAllDependencies(cas: CasModel = this.currentObjects.cas) {
        this.dependencieList.forEach(dependency => this.defineCurrent(dependency, cas[dependency] || null));
    }




    public defineCurrent(objName: string, idOrObj: string | Object) {

        if (this.currentObjects[objName]) {
            if (typeof idOrObj == 'string' && idOrObj == this.currentObjects[objName]._id) return;
            if (idOrObj instanceof modelsRef[objName] && idOrObj == this.currentObjects[objName]) return;
        }

        if (!idOrObj) {
            this.currentObjects = { [objName]: new modelsRef[objName] };
            return;
        }
        if (idOrObj instanceof modelsRef[objName]) {
            if (!this.currentObjects[objName] || this.currentObjects[objName] != idOrObj) this.currentObjects = { [objName]: idOrObj };
            return;
        }

        if (typeof idOrObj != "string") return;

        this.DB.getById(objName, idOrObj).then((doc) => {
            if (!this.currentObjects[objName] || this.currentObjects[objName] != doc) this.currentObjects = { [objName]: doc };
        }).catch((err) => console.error(err));


    }

    public addFile(file) {
        let doc = {
            titre: file.titre,
            type: file.type,
            ...this.USER.CONTEXT,
        };
        let docId: string;

        return this.fileService.upload(file.doc).pipe(
            map((upload) => Object.assign(doc, upload)),
            mergeMap(() => from(this.DB.put('document', doc))),
            mergeMap((id: string) => {
                docId = id;
                return from(this.setConclusionDoc(docId));
            }),
            map(() => docId),
        );
    }

    private setConclusionDoc(docId) {
        if (!this.currentObjects.conclusion.document) {
            this.currentObjects.conclusion.document = [];
        }
        this.currentObjects.conclusion.document.push(docId);

        if (this.currentObjects.conclusion._id) {
            return this.DB.update('conclusion', this.currentObjects.conclusion);
        }

        return this.DB.put('conclusion', this.currentObjects.conclusion).then((conclusionId) => {
            this.currentObjects.conclusion._id = conclusionId
            this.currentObjects.cas['conclusion'] = conclusionId;
            this.currentObjects = { conclusion: this.currentObjects.conclusion };
            return this.DB.update('cas', this.currentObjects.cas);
        });
    }

    public addNewItem(categorie, nouvelItem) {

        //init vars
        let cat = categorie[0];
        if (!cat) return;

        let catObj = this.currentObjects[cat];
        if (!catObj) catObj = this.modelRef[cat];

        let context = this.USER.CONTEXT;
        nouvelItem = { ...context, ...nouvelItem }

        // add to current list
        if (!catObj[categorie[1]]) catObj[categorie[1]] = [];
        catObj[categorie[1]].push(nouvelItem);

        // update DB
        if (catObj._id) {
            this.currentObjects = { [cat]: catObj }
            return this.DB.update(cat, catObj);
        }
        else {
            catObj.cas = this.currentObjects.cas._id;
            this.currentObjects = { [cat]: catObj }

            this.DB.put(cat, catObj).then(id => {
                catObj._id = id;
                this.currentObjects.cas[cat] = id;
                this.currentObjects = { [cat]: catObj }
                return this.DB.update('cas', this.currentObjects.cas);
            });




        }
    }

    public addNewItem2(categorie, nouvelItem) {

        //init vars
        let cat = categorie[0];
        let catObj = this.currentObjects[cat];
        if (!cat || !catObj) return;


        let context = this.USER.CONTEXT;

        nouvelItem = { ...context, ...nouvelItem }

        // add to current list
        if (!catObj[categorie[1]]) catObj[categorie[1]] = [];
        catObj[categorie[1]].push(nouvelItem);

        // update DB
        if (catObj._id) { this.DB.update(cat, catObj); }
        else {
            catObj.cas = this.currentObjects.cas._id;
            this.DB.put(cat, catObj).then(id => {
                catObj._id = id;

                this.currentObjects.cas[cat] = id;
                this.currentObjects = { [cat]: catObj }
                this.DB.update('cas', this.currentObjects.cas);


            });




        }
    }



    ////////////////////////// passage / cloture /////////////////////////
    public cloreCurrentCas(type: Cloture, info: any = null) {
        //return this.cloreCas(this._currentObjects.cas, type, info);
    }

    public cloreCas(cas: CasModel, type: Cloture, info?: any) {
        // TODO : cloture cas ...
        //return new Promise((resolve, reject) => resolve());

        let cloture = {
            type: type,
            ...this.USER.CONTEXT
        }
        if (info) cloture = { ...cloture, ...info };
        /**
        if (cas.clos) {
            if (!cas.historique) cas.historique = [];
            cas.historique.push(cas.clos);
        }
        **/
        if (cas.currentIntervention) cas.currentIntervention = null;

        //cas.clos = cloture;
        this.clearNotif(['discussion', 'regulation', 'intervention'], cas);

        if (cas.position) this.closeCasPassage(cas);
        else {
            // le cas a deja Г©tГ© clos ou sortit
            this.DB.update('cas', cas);
            this.addCasNewItem(cas, ['conclusion', 'sortie'], cloture).then(r => this._currentObjects = { cas: cas });
        }

        return new Promise((resolve, reject) => resolve(cloture));

    }

    public async closePassage(passageID: string = null, info: any = null) {
        const cas = this._currentObjects.cas;
        await this.closeCasPassage(cas, passageID, info);
        this.currentCas = this._currentObjects.cas;
        return this.currentCas;
    }

    public closeCasPassage(cas: CasModel, passageID?: string, info?: any) {
        if (!passageID) {
            passageID = cas.position;
        }
        cas.position = null;
        if (!cas.historique) {
            cas.historique = [];
        }
        cas.historique.push(passageID);
        cas.poste = null;

        return this.DB.update('cas', cas).then(() => {
            if (!passageID) {
                return;
            }
            return this.DB.getById('passage', passageID).then(doc => {
                doc.sortie = this.USER.CONTEXT;
                if (info) {
                    doc = { ...doc, ...info };
                }
                return this.DB.update('passage', doc).then(() => {
                    this.addCasNewItem(cas, ['conclusion', 'sortie'], doc.sortie).then(() => this._currentObjects = { cas });
                    return doc;
                });
            });
        });
    }

    public generatePassage(cas: any, poste?: string) {
        poste = poste || this.USER.CONTEXT.poste;

        const newPassage = new PassageModel();
        newPassage.cas = cas._id;
        newPassage.lieu = poste;
        newPassage.entree = this.USER.CONTEXT;
        newPassage.patient = cas.patient;
        newPassage.pathologie = cas.pathologie;

        return newPassage;
    }

    public async openCasPassage(cas: CasModel, poste?: string) {
        if (!cas) {
            cas = this.currentObjects.cas;
        }
        const newPassage = this.generatePassage(cas, poste);

        const passageID = await this.DB.put('passage', newPassage, poste);
        cas.position = passageID;
        cas.poste = poste;
        await this.DB.update('cas', cas).then(r => this.currentCas = cas);
        this._currentObjects = { cas };
    }

    ////////////////////////// - /////////////////////////
    public cancelIntervention(cas: CasModel) {
        cas.currentIntervention = null;
        return this.DB.update('cas', cas);
    }

    public affectToInter(cas: CasModel, inter: InterventionModel) {
        cas.currentIntervention = inter._id;
        return this.DB.update('cas', cas);

    }


    public addCasNewItem(cas: CasModel, categorie: string[], nouvelItem: any) {

        //init vars
        let cat = categorie[0];
        if (!cat || !cas[cat] || typeof cas[cat] != "string") return new Promise(r => r());

        return this.DB.getById(cat, cas[cat]).then(catObj => {
            if (!catObj) return;
            let context = this.USER.CONTEXT;
            nouvelItem = { ...context, ...nouvelItem }

            // add to current list
            if (!catObj[categorie[1]]) catObj[categorie[1]] = [];
            catObj[categorie[1]].push(nouvelItem);

            // update DB
            if (catObj._id) { return this.DB.update(cat, catObj); }
            else {
                catObj.cas = cas._id;
                let ID = this.DB.generateID(cat);
                //TODO : passage prefix?
                cas[cat] = ID;


                this.DB.putWithID(cat, catObj, ID).then(id => { });
                return this.DB.update('cas', cas);
            }
        });

    }


    public addNotif(type: 'discussion' | 'regulation' | 'intervention') {
        let notif = new NotificationModel(this.currentObjects.cas._id, type);

        this.DB.put('notification', notif, type).then(id => {
            if (!this.currentObjects.cas.notification) this.currentObjects.cas.notification = []
            this.currentObjects.cas.notification.push(id);
            return this.DB.update('cas', this.currentObjects.cas);

        }).then(r => {
            console.log('notification : ', type);

        });

    }
    public async startInter(cas: CasModel) {
        return this.clearNotif(['intervention'], cas);
    }
    public async closeInter(cas: CasModel, interventionID) {
        return this.cloreCas(cas, 'transport', { intervention: interventionID })
    }
    public async clearNotif(
        notifType: string[] = ['discussion', 'regulation', 'intervention'],
        cas: CasModel = this.currentObjects.cas
    ) {

        if (!cas || !cas.notification || cas.notification.length == 0) return;

        let notifToRemoveFromCas: string[] = [];

        notifType.forEach(type => {
            // get cas notif for type
            let notifID = cas.notification.filter(ID => ID.includes(type));
            if (!notifID || notifID.length < 1) return;
            // close notif
            this.DB.getByIdArray('notification', notifID).then((list: NotificationModel[]) => {
                list.forEach(notif => {
                    notif.close = this.USER.CONTEXT;
                    this.DB.update('notification', notif);
                });
            });

            notifToRemoveFromCas.push(...notifID);
        });

        // archive notif for cas
        if (!cas.historique) cas.historique = [];
        cas.historique.push(...notifToRemoveFromCas);

        // clear cas notifs
        cas.notification = cas.notification.filter((idNotif) => !notifToRemoveFromCas.includes(idNotif));
        return this.DB.update('cas', cas);
    }



}



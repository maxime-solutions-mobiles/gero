import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import * as moment from 'moment';

import 'moment/locale/fr';


@Injectable()
export class documentService {




    public pdfDetail(data) {
        console.log(data);
        var doc = new jsPDF();
		

		var initY = 5;
        var initX = 5;
        doc.setFontSize(5);
        doc.setFontType("normal");

        // metadata
        doc.setProperties({
            title: 'Fiche bilan GERO',
            subject: 'Fiche bilan secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        var print = data; 

            var header = function (posX, posY, text, size, tgle) {


                var dec = 0;
                if (size == undefined)
                    size = 285
                if (tgle == undefined) {
                    tgle = false;
                }
                doc.setFillColor(200, 200, 200);
                doc.rect(posX, posY, size, 5, 'FD');
                doc.setFillColor(255, 255, 255);

            }

            var bloc2 = function () {
                initY = 3;
                doc.setFontSize(14);
                doc.setFontType("bold");
                doc.text(initX + 5, initY + 10, "FICHE BILAN N°");
                doc.text(initX + 45, initY + 10, " ");
                initX = 190 / 3 + initX;
            }
            var bloc1 = function () {
                initY = 3;
                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABvCAYAAABWxv0DAAAgAElEQVR4nOy9ebQkR3Xn/7kRmVX13uvXu1pqqSWBQICEkZEAsRizGTAYG2xsbJ+fNxjsGQYv/GyPzwy2h+Ofx9gGL3gYYx8vgPE2gLFZjFlssS8akAQYkFiEJLQgtLS6X2/vVVVm3Pv7IzKrMrOylve6tcAQfV5XVeQ3I27ciLhx741NVNUARAQzQ0QApn6f9XuruGaY9/xU4P5vjWvWK9D6+1u4b3xc+aytLVTf/0bCicUwKmyz4GXcrNBkXDWNWbgmQc24tgKdCtyscixS7m9k3LRBqIz7Fu6bA9cW7glF4O7GiRWlnSWlq89LzLRMZo0KiwiZNoJPNW7ae/Pw3yy4Jv5buP87cPekUnB34ZJqRFPYVEPbqC0iM7WmatptuGnCxYr/R3ECVsRi8/NoY0ZbOZtl+2bHTdPM2gaZb+G+cXAgxF5TfoIw3eqZprl9I+CS8kuzoVcTKOM2o9U0GbzI+6O8iv8NQ7TyjkzSt0h6bbhFyvbNiit/N0Nb3X0Ld9/Eqepo8B4N5lT6MDqRblvfqGLa2tF9DZe0jcrVUJXyzbhqYtM0sGm4qQJMQEwQBcwRfFalBoevpTeNxmZh5+HmlfmbBTePR9+K+8aIEwRzEADFcDT6LQ5nRKEG4OYPbLMEyH0FN+F0r77cFteWSFMoTRsZ2lTdZjDNQTyKEDC8hXEaCM75MbZFQLaFWfTMovmbDVd9Po1H035/C3ffwqkZUggkg5H1EcEgZiACbhx9XyzHZnEjp3sJKgu2GSEwT7AVxh3ODFAQP5b85Kg4jpjn5mNDrj2m3LWRc3A942g/MLRx/ql37F9J2N11nL2ty/23O87sCd7ASZG0M7AAlqDOorYmECTgrK6dtdHe5MM03Hg0UK44JuhgLFi/WcL2Jc+FK4aJw1AcHrEh15wQDg48qd47ZTYUMUPMWO52OdCD7T2HR8EycClGQM1HTQMQAmjR9pyrp7eAtnp347T4wwwPSDDUg4rhLfqnDIfrC/mNR7EbDhO+fpj8ruP0Dx0h7VeEmTNkR5d0xzbcvlX8OTtJz98Ne3uAIhgmYOJwQVCnKIJHwARxk7TeV0KrwBo9nCGspuEmhJWBqiG2QZAU9Q6nxu19z0fuOMYHbuvz77cFrlsbsO5WMF1HMYIkqCUggbFtLoUAyvGSkZCwp7PMo/Yqj9+f8ORzVnngkmPJO5SAzz2S5KhkOF1B3GSZttoISx/CUJVnv+cm3n8omZvGN1r4zw/s8j8fuxtTAW84EnILvPTKI/z5Z46w3r2Xyux81C5USAzoGPvSwLdt9zx8/w6eckbKY3akLDklc0YiQuYUR4KYkLr2ER4W00TvDhyFmyT6zRUVCCb4NSO//CY2Pnwd2RU3Eq6+nexoxkBytg0Fr8IQwReqlBUeLQd4AhuJQuLoCegZK/jH3Y/upeex/J0Pwi5YJaSKA1KFoQjeOdKGFVMtx7RwT+FqyxpmmWqLZjipXYESyBS+ui584KY+b7vpOB8+eIRsuA2HoknALMomdd34luWIxNG9mpiIx/KAx2MSUBcQ5yAozgvnb3c858yU591/OxfsMhKXgksQlAR3ygRWKLSL4yo8/T13ceWd/blpfKOFFz1kiVc/Zld0lLhiVDb4xSuP8idf6CP3loYlhc5uIOJIrIOimGSo5SRhmTP2rPP9Zzp+8vwzuHCb0JMkOqK9w1c65H0lmCpBFVWHHBqQ/du19P/hs+iHroNjSmaO3IGYsmJGXxRvDkVQ5xBVohbGyDz0ZiBRNzZLMBfomJFTdLb7b2fbcx5O5wcuQC8+g7TncZbjkk5rv7gvxC2sYU0zm0rcNN/JRpZzxcGMv7punXfecoKDuUHuccGhaUGEWjE+aKGwljyPpmQcTovegsaHDpyCs4RginiPDhVJPGYZPe959BmOF563jWee0WV1OaHjZGLE26z/q8RlpnjL6Kvjye85xBUHB63vfyOHn37wEq957F4kB8jBe5IQ+MUrDvG/vngclfTeIUwV8Q5Fo59GM8DjSBFJENtAQw+P0HOBp56d8vMXrHDp6V1S5+hUTMJmO54W7m5cPgwMrjnI8G+uYviWz5Pfchw1jyMg5IVhC+Y8Q4wUKbR8R4KPvIiI6JAnCnZxYBpIXAJqDCXgRCEIKgl4j/iMbY86gP7HR7H6PReQ7lmq0Tivn9yTuKQa0RQ80/xUTWZrAASCKT4IIVHykPPxg8ofX3OUd38tp58bRCs5mokeQMEUA0JBtxQjpzGKiP+VUkwEVJDMxTydxpFfM0jLwiYMNOfDX1c+ctsRvn1Vef63bedHDiyzd8kDgZwUbwFvZa7UytQm6WsMNMMkNhVnseF8swUxD2bRdytxHspwBBKC9KJj996gSxIIRa2JIJaARH8PlgFJoREqxyTwtpuU99864AcfkPJLD13h/O0reAwVITGpO6y5+8ybHMERcBb9R2hgaIZ89k76r76co2//PP5IjphHEJaADMglJZjhECQIPTzR6eEwpFjCUNBS8ghwJrgAhieEaCqm5sklGpBdA8kDIU/Y+OjX0I9+lfxhB9j24kvxz3soya4OIFG5cGCi+MLKKcvV1kfayn+qcElVOJWAZkVM+ywzyVygE6KzLvicLx4W/ujq4/zDzRscHySxIUnpOCy5Wk5vOEZSDLBSKDWmaUeCqxhIzOtYqEF0ppY4UcwEJxkqKZ8+Klzz0UO87owhv3b+Es84r8OKKSZCXkpuNcS7GuPGjvXJUdObEfAUdmplLUyF4LIYIw2RyXLdk7iSh5TvSeOz8rP0F44ii8fOilqMA8Vm0jtVOJNQtBEKOqq4MoSi2LEMR0LKG67L+cjXjvHyRwrff0BwSQ8V8A0vc5tJ0ha/WZzPYegDCYZagt64RvjjT3D07/4dd3BAoowWKAjCkGjmJVU7D0MJRGNcCs7UhVXJKyMKyRJXxvmiPYwN+rgwQugSPncHx37+nfD6K1h+6RNZevoDGfYSPODygDk/7moLuJBONW5TakHT3CszSdWTy5BBH/70i8d51vsO8oYvH+NE1gFbZ7IxjRKsJlRphER8OUO4RZzqEpILPhi59Ljm9sBPfHyN539ojavXhmgY4MMGJvkEfTW7ecakQo0OqfxVBUlJXylA7i1cZcZ1DKuMyWXHsDovpoatpHeP46IAT6xPEOX6DeMFl9/Bb119jI1guHvQDReckVjKsO/I/u7zHHn237P26ivhYFZoq1L8qxWsGJjjX/l9Ho4RjhGuFG5j8daGA1NlcNVBjvzEP3HwF96Nu+44CuQi5PfyFOJE7mVHrfp6Jmf+rPbMZMCNxz0v/Pid/MoVx7lpY4D5FLUB6pbKtyZzFxlHj/Koak0nhxMxxDnMO0yG5MkGIfO885Y+z3nfXfzxtQM2SHDUW221/FWezB0lzMb0lbhqSyi/3Fs4qeBqdBdpyBxcM5xsevcEDitm3FLIEwIZWX+Z3/1Mzi9/4jYO6WQyE8VcYFJmEZwjwMF1jv/Kuzny4neiX7yTjg4I5CjjGdexUCnSBdpXs0/HVT+rOGmIrUmcoJKS2IDe8T7215/ijh94HYN/vgZnQnO6YpqWOY83W8VNzEu3LtZqJoIRdIhZCsH40J1DfvmTR/n8mmBOICQEMcCB5fWRf9SgSu2gJa7IpWYmbAFnxNnH+I6H4Mh9AE24YcP49SvXuer2nN959DIHenlct1M0YBWHk0mf3XShVRUYJU0VgdJQfu4dnNTkPJXoqJHYbFzbiyeT3j2BKxpHXIJnoAlDFzXq134lJw8HedVjdrHNG0Yal2m1dJotLX0JWTFDJygZg2sOc/SF/0Dy6cN0gpLhol/LSj9UpHGsO40rWCYKPBvX/GSElFpcKy4oRkKG4TKBLxzh8PPfzuAX72T5l76TXs8TLRgDLySVZSLzJq9OFteq31U1qKaPyyzavi5PEHL++sYT/NQHj/LZo8v40InTq6MRsDLajUb5xu+yU1Xjyu8iJ4+r5eeK39FJOWSdN914gmdddoSPH+0TLCNzgopiZBPCqmRg++ggk/nWtJ7y+b2IiwwaC7WyjmpmVQW3UNhEevcKriyKgRVtE0CMYJ6//eoGL//cnVF4+EHUMhas83k4xSESMFHsX7/Kie97LatXHiJTo18IzdIPG0tQ13pqeY20IlkI1/w3YsNURJvWFXeb9J2SrBn9l7+f9Re/FVs3TIZ4Aa91PsyzyE4W59ochW0zglV/jlNl4DL+8Op1XnL5MW7NQPQwYn1MPHVBMSdIo3HdIzgBA6+rBFK+cEj50fes856bAy4bkAePryi/bb6sqeku9PvewhXfa4JfpiSzYP1tJr37IC7kHV5zTYc3Xn8E0TizVoa2TtUWpuHUCSEkZH/zWe76kTfgblZOIDgxOhpFlIz+lWbCWAMae5a2gqv/Y/SGVN6ej/MIy7mh3nBZl+zvvsDB7/8Lwp2B3BT1k7KjyZe2/rNVnKv+qH62VUr5eTwoL7+6z8s+fYiNEPDqMecYdrqYFVOs5ag2+mMcZ5X4Znaj/Atc5Wc1qfH7BWZilIXJfMfRAqhkgBAkcOtA+Q8fXuN1Nw5wMkBlXGYtfXUNxjYIr3+vanrVMtTotmLPV0ty94U4AczG7IWJYo4eGJPp1fherfcWx9Gm6LPGH0y2KVuIPic5G1mf//6ZPv9+WIk7K6pKarsAa3YwkYrmU+BcljP8yys48nPvJAyWUVFggEcZuoRyvnVMnjXYO9Z0xqUrxYpS1Zao/F+NizOAYw3LKunRkleT2QEICGnIwIaYOeQDt3HHD78evfUEASuacsGbhr+3KYSmrd9cFOfmaQ9SOLL7GjA1TmTKH1yzxh99+jZCWAUTgsvBUrAhcSNSOYpJ5Q9GuzU90Rg1xUseV5NY3A8opGAuOsyTHJGcOPTFZBKBREHM4y1F8DgPgovrhkZCwhr51kdWEyEzI677dWAZazn8yuWH+bOvnEAHGUEDQXMGmsVqbmFmhVEjIVTrGVWBJBEnKCIG4nDE3W5yH/wzyYAcxMV1blitaGV56qHCm7gUHQQchneCExfX8pwUbeDM8Go4M6TYciMIoq6W7zz61ARNc25eT/mjzx7ixDB2vMyM6kL+tqU9Tc3KTFHNCSg6yDj2uqs48mvvwZ1wpPkgLnglxdThtNRmWikrno2fqjNElAQhdxIHVHTkb5Ny6ZBELSRBcFBscwsoSgrRrTyRr9Xyqoq9yFWHFp1WrE8/gc5HbufWF/4N4aajZKaEYOTDuHy1bVBvG+y3gpt5HlY1gR5GZoHXXneMP/xsnxN+JS4JKNYujTvmFFOicISXHd+yHHE+CgznwBKwAYk7xvZOh92dDivSZSm1QgY4NrKcowiH1jOOD63YQ61IMBIZEgyMzqi6xzUjZT00BNoYpy6mdVy38eufvJNukvCCc7ajyYCeerDx+Bcba0v5qvmWQWQi37jlKMGFIeqXEYaILTBddQ8HpykmHq8Bcy37BktNpjoRUi7ELOu7EOCeDoQNgu8BDmG4ZbpMfBzpBUyUJGRgcRFl7uKq9/HAMZu+4FycaNGcd9465H13bPB9Z3bj3LFPokCsuEamzRjHfuIwM3I18rd8kfDSy5AjngwjrWhAFYOHauMof9GCS9UQcQwEnPm48n3Fw86UdHsKHvKVDuSBZCMnHyisZSRHcySP29wyAmKGKxacVnW6MbfGCyeoPBkvhpBi43nKtg/eyZFf/md2veYHkb2C+MmdD22rDmYtGp2HS6ZJsppK5gLDkPCOW47xm58+QZZ1SSxHnBZbAqQxkrXo3zL+YkFx4kCVhA77UuHivTmPOH2Vx+zby+k9z76eZ7szlrrlLAv0hznHhsItgyGfP6Jc9fUhl9+e85VjG5xgqSAhNPJroaHmlB6HXqb0fcZ62MV/u+Iu9qUJzzprieAEL+2jaj39qmY1I1917E2NH7twlV0OcDunifl7NVyyO3Z8cw6xancqQnUwKMs+0ialhlNyfurCFe63lJDhSG3blunKfYaK59jQcfPhY3zuRM7X1jI2dAmnhrqyfhegTwDxOCccDUv8xReP8ZQzeiwl9QPwmkKq7bdTJTPIP3ILG//tX8iOxEEwkbjlLGZXFQRjguqiaxI3dIL1hOULT8ddej/kiftZOns37rQl/I4lQuLwyx0sV7SfY8MMO3gC+9pxBl+8ncEHbiD9zO0MbzkWhZ1ZJZ/JZQ/jIHWceLwa/biEnuRt19Lf/a+s/MGzkOXJbrfICvdN4azS+2qOdgOVeCRMTsZn1zJ++L2HuaHvwWVRDlshe0fbGypMrmo3xXMxRTRgkpJIziP3dfnR+6/y1ANdzlsxOiIEKVT+4vW4BaYcDRhpciaGmnJoQ/nUEeN/X3uCf73lOHduKOq7YGGUvyDRt9bUAMtRWAAUcYaooQ66wx5nrWb8/ZN3cfFpPVJ8fUTVnCCeEAJPes9B/s/BvJ7eSKNsY7pw3jb452eexoOXAyKddmy9TU8Pi+I2GXINxS61TjwayBmo8YufXOPVX9oo8i3L28y/EmFGYo5/feZ2nrS3h7oE52zr9JohFmfYcoQ71jP+z6Ehf/Wlo1z29RP0dRuQM/batNhBRYSYYU6ilm6O5QTe+6zdXLTb0SGZvVHaIMeKY1mie2Hw5cOsP+/v6F9zBDHFm5IJLFvCoHipyHmku4AUm7fjEoekcFSYOBDFnbvK0g9/OyvPvhB56G7cqgPzhdlbbs8ZT/mXVWViFOojeQ7csk744M0cectnyD94Pcl6KbSs2OZjODHqC4frGpbi6ACBIbkTOpoQloXl33oGvZ+7hK6PdJWlKrfxTFZh3Uc1varruCnng0QzTAJkibKWO1764du4YbiEJ5AHHzlC2UdbpHI5gJmRGIgqufdoJ+GRu5SXPmwPTzk9ZVu3Q1LR6tKmuj1OqpA3UswWRH/GaT3lGb3A4/bt4oajq7z+y8f56+uPc3TgMAEnhuWhYGJprpWtt5K4CRYKoahC7vvcMAj83OVrvOW79nHOkkTTdSpnG8SOmMPYLKwo3rnAkgkm3Th62bjiRgMHdXOkWnFbwU2QPMenkBblFYsLcLW6LKCskWrl1IS0VMwyIYjiJMESwWn0340Vz03SV6m6FNi9LDy3m/D0M/Zx2S0b/PoVd/KFjRUSjqN0os49hT6L6h/mhAzjmMJ7b17nEXu2j47nnkofcfWUV6MvgfRIxuH//m70msMkGkWIkuDNGG+NLzWa0m8U000kntlmdOgnRhoGyNmrrP7nJ9D9kYfhD6zikgo9bnq9jfreaOiHTgrcvwP334n/oQfBlbdz+FUfhPdej+ZCpsJ2POuujwtVsVDXDD3llp6URCFzSrphrP3ee9l30WmEJ5+LyzQWSBQmlpoWqS64imDuOiwrGpkJ4IYMzXjVZ47w/rVOZLMF6iuNqw1zMpgYuTOypMvp/hh/eInjHd+9j+fsX2ZHksZD1Ub5MtHp2ukrKgkQiU7WVXIuWk34g0u28f6nncb3HkhI1BNCB01TrFwRGl8aNfqao7zaOSTBZzu46rjnZVfeipaddxqj56RXy3dOaJod0/LdLG6WOdt0BywynV+Aqxk1M55b3kXznYXrmiN4Ty9Vnn0/zz8+8yy+c++g8GfWPOfT6SvSU4GP3zpgPZv0VU042SGaoOTkFhi8/nPo266LgtkJWvEPjd+p+4hKzSUA4gQlBxuw8oJHsu+yF5H+l0fC2UvT+v1U+qatcQJIVlJ4whlsf9Pz2PnXz8M9YAcrLueoz2rKR3NtVnUmsfTIOTXAs3Kbsvarl5HfvoEmSk4ghKTVHz6Pvlm4CYEllRHIcFxxh/InX1grNvoK6qOaWvcBTG+VzhxOlEfv7vPOZz6An3vwLvaRYImhPuDLZAqHW9Xx1haqGCsO0TNLEXGIKM7DQ3b3+PvH7+EvHrPM+f4oqCMxV9FyqGs/ZSVVRmB1gvNHeeLuLr904f5R3lM78pz0JvKdE6qzJbPy3QyuLEP1dxma8TNnRMepjss3EtQTGVPvsvWwaL6zcAJ4PD4kuNDjwcvK65+8j4duy8fWzTz6ynI448q7hKN5iAeUzs1XUQfLnz3O8VdehuBJQ04otJsyx6rTujSxqrNz3hxBhO4ZKXve/FMsveY55OftIMETFav2DjyNvmk+obKtpKQk3Q780PnseP8LkR+9AGcZnZBQ1aqaa7NolCMhIZM4G8mVNzN41cfI8vLon8l8F6VvGs6VoKpEMyCQsRE8v/apO1jPOiSWgxYzdnW7Z6I9igQScsQZqXh+8twl3vZdZ/NtexyJEyRRvHggjcfvzpC21biJynKlv0swScg9QEIv5PS6nh8/v8fffv/9eNzpIVreopRnMToLCBZXTIRYGQlJLI/k7JUBL73kNN7+tFUeukdqkr1doDZH72bcdE1UaN+72Fappwo3bWBYFFdJvKWM1WeLq/5bpc9cND/EB7IUcuc5Z0n4rUftZcmqM1cz6CvjTTgyDNw8gOp5Bq30IWSABuG233wP3D5ENGPowWtzM02lPoh1bgIiUfDlYrhLzmD7ZS8mffZ5+FSjq8QlIEmtAy/Cl3ERJwWcihbjqid1HfxZS+x47fNYfcX3kq/EOUQnRu7iFRe19BrpZxT6ixlOhcGfX4G/6k46KpUzuqZbJtME8DRcq1NGUJIg/O8bj/KZO/vkJHGXdrmeqSJpixfGxTFI8x65M3rB8TMXCr/7+D3sS7LiEDKPE48IeIn7tpqaQetM5TSck8JXBSkJzjkkTePZQR4esc3xj0/Yxw8+YEgiPcTW48l/ksRlK04wH30CZvHY5UftEP7umfv5rxcusYrCQidUNjSotr4qDb41U2ioxc24k8VNW7rSfGcRHFDRJhk3i5oZXJR3ohG2d6yt0hfbkEckIbW4bkhUeNQZPS7YXclyFn0VTVGccssxifcDzMhXUZLg0Pd+BXnv1XEmUOPhlNWStpV+ZGBJiiUGTzuH0/72J5CHbCcNHrEULzLS0UYm5IJ8KePaBIDHgxOSot8klmBdpfeSS1n9/e+FfSmGw1uCJJOCthoccd1XKL7pkcBdr/hnbCNu25kVptE3CzfibF0FE9YG8JprjjNEqDlaazwpemXV1BLIvNLTZV70UM9vPfw09gqoT6OLUqb7Ktqk7cnggqU4y9mxJPzppQd48XmCd9tx1kckHruvopjkGIHVToefvTDhrc/Yz9N2JnRNCKNTNWetch8RNv5coCKaoekzqZZ3QqvYAq75bBYdi+BqZpZV4iaeLWYGnwr6BCM3EDy7k8Ale0q65tBXmlSm5JJw8Oj6RB1OaoBGUDj62+/H9R3lBuZoZBYDSWWipfxd+rFS58lN6Tzxfuz7s+ch5y2RGljcp39SfNnUQAYkwWE+0P3ph7Hyqu/D7/SYeSRUyzG537Bapvhd0XffwPGPfXW8vGRKvovSV/2sbc2pdvy/vbXPl9YEo4OX8m5AqwwXlbFD6r9Tp/zQuYGXXbSXbT7ERZnO8OSUToVFTJuTxanlZDi6QdjWyfkfj97Of3xAB5/3ig0LAihJCFy0Q3jt47fxiot3cHrXEzyoN5Bi2pqxhtc+ysl4pJbC9GjCpphIxjjNpnCpapbVvLeKq5MzWY5FcWWRaxpWtXgi43YxkeTW810E15FA8IIzz4EdK4vRNyqHYS5hPShi+cx8czOy911LdsUdDF2n9qyqiVgl47GzXcg0I33MXpb+8rm4c5YRc6i5eHit5ifFl6qvdxZWJFoo6jw9S0hESH7oQjqv/G78ckZe3gNa+dcsXzUegSTv0f+fH0GyyWObtkJfFedGmVvANBBQDg2NN3/5EAN1qATUisoYezCr7MNpSkJOEjxO4PGnCb976WlsSz3iPAlSuOyTtv46YQJOk76bxXWcI3UJ4gXvElZ9wssf3eMH7tct9gEp28zzMw/expufuofnnLtCJ01wCJ4EL1JMCkSB1RQC9Uwbmkb5WtN0lpbGxqS52/a9zSzeLK5pUk/zH07F1apexr9HK8jHplV82aiv66H+/FTTB2jwGANUHKE8e3sefaPXPS4oQw/FmTSj/EZ/weKlJxls/MXluEzoaDwHtBRHZXJVZ3XujERyzOUEH3Dn7WLHH30/nL0cafAO5+OCncSNjyHeCl+m9Y22545CXjsH4kg99H7sIvxLvgPv4jVguQ94seLYqFjGaknLWIeAJWQfuIHBVV9HQx63OWnc7rQV+qqfrszUIncRzbjizj6fvVOjpDewivScGB0NcCEexeoT9neG/OGlezkz1VIvIe4vdIWfYb5KO0sr2BwuKTpvzN+cspL0eOWjtvO4XcoDl3L+7PG7eOWle7j/8hJeov/DSXm7Tkm3FIrTLPOoLa4YWat1PMM6avPdtZVvq7g2M3LaCD4VV4uuaNzlUpeqBtN0F9QT3Fy+C+IM4g4MSyFs8KXDw8Xoq47FouxK0/a8iwnyDWeEqw/S/9h1hQ5io3PvocqmsfDqmHHCe5Y1IVnusP3/eyqdi0+nG8Ak3mle+nSr/NkKX6a5Dab1nVG+CIgn6Xp2veQJ2HcfYEUTvCXkUtzEUynheAFsDMGU3ALdIRx9/ZVk4vBq8RBmlS3R1+7DAoIZuTrect1hjrEEBFxu5SHQjaooep8I6oZ0QhfjCC97xF4u2BXIU8gdYGPfzywHri0gfU8WJ0FQ63NgxfOGJ5zFPz79DH743C7LGteNVN9tS2sW/YxmoKwyejc6edkgpoRF890qrho3i3eL40o7q/guVU2lipmvYZ1S+iTHieO2TLjycFmv8+grNUjDW59ze0tog2wDCIqSozgGb7+Gpbvi8tF4E5qrCar4/3h5aDBFgpClQvKjF7H0gw8h80YuguipqI/JwWkRDaaKEwwJsCEgexJ2/MYzOHZmB1EhnuA1Pmgw/l+qM/G3L1bne0vgH64hWcsJFlfbC3bS9FXvOyITx8ENz0fuMlKLI+wT+I8AACAASURBVFNKSrznqSm0xg3R5Y5hCk84bTs/dv8OqfXwoUOqgSD1qc1pjK7aqbM0p5PB5RKNPVXh3BXlITssOjhTR1fr7zbTmWZvj4ONBVKJa5a1FGhTwqL5bhU363OzuMob9fLVTN4Z5d1ivovgXOjgTXnrjRtcf7Ry/dpM+kajCt4pB7YJecu0vBVXjHc2jBPv+jwnJCFIvFXIUT22RUb/lzqWI8HhyM4UVn71CdBJcepxzmGFUrDV+thsvc3CBYZ0gpCLJ3n4fpZ+9lLEZ8V+0tJMHmtY1d+5BRxKH3DHPMff83mCM7xYvFfxJOlzZVZBHKn1+dyxY1x3zJG5OBoN4hkVI4JiHVvNzHEIHbfBb337DpIkQZzEI1+cx1fWkJQZz+74d19InCeVlCQRvO/gpUviPIkkOD+5dGGaIGwNbdpUE9sW15rUYvluNm5Rvi9cP03YhAZZlHciudmNdLP05aaEPCfTnKFBrhmXHRzwis8ORprLovQ5CZy2vMKZywEL9dMHhPJ6uoRwzW1w7Tq+3K2kVhwDUHFAj94rTxZVXBLY9l+eQXL2Kk6EpDhi2NO6wuik+LIljECSdEm9o+OENIHVFz4SvWAv6lI82QjarnM7Sj+hWkZ41/V0KA86OPkyuDLjoQJ0+NCtG9GpWPNbVRIpf1cEa/A9vmtfl0v2F5tkBShH/RkmUDPMNrlOHhflRV0jGa3eXaCyF833VIe7my9bxlnjc9r3TbJt0/RZIG5vNTTPeM+dGS++7DC39uNaqYXpMwiScPHuDttST+oyqiGKnLiqO7viZsKReCOUjF6vJ2iNzHKElf09lv+fi0mK43FG49wpbH/zXCWzcVXLhHjO2J4Vdv3kpfTIyMSNyic1DbU0D8eaJRjr778GW4fcOUJ5Rd9J0DcS66lAPzg+fihgXphey9Xfhd2qfX7mgp0kZDUzqDazMofZTQfcLBv9voCbHzbXS++Jcoz8FKcIV3NkW71NxOcV3GQmp4w+MYfPN7hlqPzOp4/x/H87xPVDR6oniCdhLEBfUQ5Hzvee1aUjtV2IozcdCqroB67Hjy7PnTSPys5bpd6Jx37qYsLO4kSDU1wfE3yZom1vBlfelJb+wIPRM1eIM3GlOCr/6tuMGJVbWL5L0WtuI6AjYXMy9I20NCfGwfWc646VanNDdR6RWAwLZsWHcN4qPG5vhxxIW46/bRI2q/PHI5YFsRDHrpbjKWpVJZPHvZ4S3Gj0jsd+VMeNxcJ0rAGJaqFcF3O0d1c5GjSZxYaVoyQWEDc2e8p6mdeQxi9UzamRqjBqJlWckyE5RnkQolRm1KKwkHaaq3FW8ikeoDg040g/58snBrz7xg3edMNRblgXkHj6bfDb4m5mCTPoKxzeLh6TtHdFeMqBBKPYM0u1vQqJKsMTgf41dxAKkdQ8bC8mXx30iw6eGMs/cSnecoZ06M0x6efVxzxcWz/bbFzczGbo/VdIH3cO+Zu+QOMK41FpqzplyZl+JvirbmT5kafjGsXYCn0Vs9Jxx2DAwf46hNW4fWWi5RXfzeEkFCQlfOd+2JOAc57q2pVpmZaNv42woHk8jlUz3n/HgIRua3p3d1ALBBKEIcvO89j9nsS687WssvM1f1fjPRzswws+tEYvCZFn95S16Ye43LjwtGV+4+Jt7JyyonrqoFK3HirCpvq88HFW/HpOe/zqJ46zJznIRifF58nWy+xyRHMGJNwxcNy0oWwMs4qIcCA+HoczGmTb6RNzeIunkIgkPPd+Xc5acuTO8OqgcsmCUFwm+rVjcHCjMmNGRUjFX+XvKgfTi0/Dn7eKw4863qzBe6H6mIGbJegWxcXz6OLdnUvfdz5H//FayLOaYCpLLBMcAEzIPndXcX4doxNct0rfSGAJxnXHIWTJaGQps25qC6KKeYlL77XPU/bvxMsQQofgfDw/aUbHnjVTqJKQhsC67/Dj/3YHh6edq3E3B3M5ab7MMAlcut346LP3F/sN55iGUukZVnNSjDHBccwcn7h9A01CXDO0Ke1ta0FMSE3JXIfg+iS2o/Z8XiOOD5q/m+WtgsZtR2XAlWsZZilpJgzTrR+RjBhJMJwOscSRmcYLc2tmX5Pv7fQleU6WGmIp+7t9/sMDT6NroObB5YAfT60XnVBvO0p+dECcqncjbaruw6l25Bh6T3lAPAPRxZtzoF17KsNC9bEJXBW7MI5ofRGgc+l5sOqQw+XQIKPyNQVXyQNDGX7x69F8lpOnL4kJx/y/cmIAtgQ+FCPRNIkeD+/HHNtcziV7luIBiFqYio2D7iad3+MtJJOMNlSFJa/kiWFhdgHvtuAcSE6SJwQ/ZPK06inBIPpEmCqDxA1RVSxJEUvjIYNtN8mc6uCEoQRc5lDxiNXraSEfXZs7s3xNpj2AQIdEE1weCGmGtPm1FgxmRi4peAcSz3Ays3i2mhv7WBahLysO7kxV+ekHbePh2yWe/KkQfBzPm7Pc/pYN3EAxsdoGh6rjoKpxlCR0HncOAw9dKw3H2eFkZwfbBtc27GycoWbRNXPuKuGsZfzh4ahMbYKqOdmQ3HAElzsak65boi+JScbI2zes2KUwpVWWPgQRcOACnL2jy+7UEfAkYpSLw5pSsk2ralNlPYoljmAOlQ6m+cR790hQI7gMl0PAEySuQJnfiBomSCMawOUdRDwhi5vB9Z4QVsTG1TElSwbgFJFdTD0ZbnoilVD10UyoXjWcE8jZiL3VXFwf0LZlZ5Hgh8AwnmgQIBei1Jk4VaONvuhpjb8D+AQJcMkex3960A6cF3IcaXUpTyUVQ/C3HouXpFpe6bjtY1TclGYEn9O56Cz6xbS/YqOz4O7JsOjk0VirjMekB4272VYu2k//84ehRVCVYWyaF4gTGfmxHLfbM8mhzdGXlDkYwqETirp87HOpkUBFvXaIDBFJ2LOSspIKaeE0ndb8m36rtt8x5RRzWRz9zYjbeqYWby4DpuImHMNN31M86Trzhgt+dJ3STN9O2XHa0qtYJsHHyfEyPWalN42+LeAMI/NgmiAhzD7yeZTcrAbUIphb6Su9Pcn4eCWBiVNAFq3K4owrk7hKvPXF0oc28SiF0MHJECMg5ti7Evjtx+1j71JxAJLE0alt3ZACevA48YQ1GV0uUeoadROp0LscpDt6sLfHsvhijKinvllBskg4Nbh4NZslcWsN5+1AoFjbP+Z926boMgyygN11HLd750nTFxeOFuZ930skKpQTlu3BRBGNSu3urtBNFhulF1H/NhcWfb8prE7Ow73l9ViLvnd34uawbJr5PgU9J7H7Js7JEE0C1ltiW8f4/Udu4wk7hVQKv8u0JQOFDBocOxH9uDT1yPKzHHxjvzINpGfsRLyrp1cJ0/i8Vdy0uLYwCyeUVpegDty+HTVrI/bhqj41fg/AnOCDkh8/QbM+tkKfM4sG4ECVwxsnikP2Sj/AFMHlCn+VeFbI8VMIaS4CW2Qt1vSHo/+mPNsMrqIFjaKkBTdJ30yH5dz0Fs33bsLV4qaHZr1NL2813baE7T6IC8AQfIczsqO84tLt/Og521GfEKQwcKb6hIoU1vstlkTsKzL6Hk2oIIqgDLclE5rvonzeCg7aFYKtxBUX+eEVuttXyYuV+WKTd3i3eUI65kmM4vark6NldPOzUdwKYzS2RFdsmVEJDF8c81mOM20LRJuLwKrO9ukCrBFXYmT03yRONomTIr7VdLJJri8U5qS3aL53Jw6mlq1aH00nc7OYrWGqttx44Z7GlSeQSgByxDwqXfaGjJc/9nRe+MAOPk1IxCEyaQTW2mlhYXorttLIpOvcSiCQmRZrlmJfaaY3i88ni2srR9v3ubjCPeQKKyxd6qGF800Lk749tULLNDAUcYKE6b7aRekb1ZAXoYPDLBSdvymoxjY5Es+gFrXoVJSx7VodAUaOu4bQauLaCjo/nAyuqcxX4bPT3TTdNeyi+d6NuCltoVovC/tJapAGLSOBKfcyLkdcjst7cVBOjQf3hvz+Y3bz9P1LJFLOBLpW/2uVL+XYEK+Jh7jI2VMXU+PgkaiFiJGUPp4F+Xx34mbV7QTOotCRQpfRwQAvxcbw8r7RqX3RiglzKWYaN5HvlDDa/JyKsJqmBNeQguXafBh/iqA+IBpYDwl55WiMpo296GrWaiEbJZmCOQncRD+3KabFJsK89BbN9x7BzSjGPEE1RXkdH4xXxstYaNybOPPIcAlLjJQNnru/y5u++0yeeUZ03DePkJla7HJyyAS6KQGjPEzGRn/V3XXxf28QRJDj/Vrd3BOO81Phw4LxindR6N91DIYZo3PiRu+Wpa7+g4BGU9s5mrlshb7x1hyFnnOYxBMItWwItQYapaRoPMbVMO4c5vTzQNqZVKdroxOTgqr5e7qDt6ohlOp5ReMbSe8FcSPlsUWhH0VNNoBmeSbJnJHeovnenbhWkmebHTMblVT4Xc74CUxkeC/ixITEK/dfzfnFb9vJj5y9jZVuQq7xJiQzJi4lrSVR5UvxX3fHKutOivXVzWn98T66oTPS4kYZvf0wplpHzuDzovUxC9emtTRn5+fioBA8UUDna8fomMNEMAu4uBCJqbOEIuAcSad+hPRW6RutwxKBvUs53rqYK6fdy4ZRbRQGlgNLGMrtxzLWcmUpjWefm2VIcXFDmz9r1qpcs2J3vXrUKYkmiPQbDGAyLBjnnCNkhnPxng9tLpkY+XtkQjEr6Zs54k34eyrpVRMsj+xxHiQQj7FcrAwnF+fo5I6MgHMpWI5IOtEJpi3omzsijprLpPov5nEoYgGnXbJ0SLlYVoh+ITWKC2+jwDWXxrTmmBOz8jXneNI+48++80zOWgJxxRHYbrSiZ+HyjnSIvcskBHIBV+6NbGG4L2bSxRzhYCBby5AuCCnOxc6+UL6L0jej4zffbaYzDRcsLiKPez8N/crhyAcDiuvtJ8+vr9DkjOXUk+/bRnd0zd7W6RurRQ4OLHUR1sfaVdMULDUsFNOAeuGWjQ3W+oGzekZwHZzG4yea/qpq5s1QnX1THEkeUAnkMsBCZwK/1aAG3mUEG4BvrAkqv8wwn+qNYaIQ4xdb0yuEllncn+UTJOSkmhZriU7SJF0gmANNhqhLUB22NpppI/govmaOUdHyKkyTysOy7hkCRt7r4cMxJF8pDoSLQV1AnY/3VVogyJAkDMA8QTwm9fQWzRfN+fihlMvvGvDDZ60ghNGAOq+8bXwRE/yBHcU+xcoyhRZfb7kBuKQzv+Y2uvvOiZeyNNrZvHw3i6ti23zKbZpZG86JRLNZDRQGV986KnFZ3qbQqq1+V8hWElZ3dCoD+NbpS0q1T0V50EoP7BCwNGWEjpFqEvfVaWAoHS4/mHHBjg6iATVIXJ2AmbNOzSxMUe9Yx7OHnJCculXgvSD0nSfTHjbMydNaj4vla7akRpiqZTVN0mnpieC0g1nKavcwAehowslsVVk0GEKSdbA8sANhoI7lpF6WWU5bazS4ItEYVxUoIvXvZqjvsdfW6fczXKeDpZXbw82ADTQ4EuuSZcLQr5A7BRlE3mg9vUXzRTzHcuO/fuwwFzyjw4W7OrVtVvOc1LXfBqBwoId1HG5ArbNW7ZFmKzKMwQdvovvkc2JNmK+ZojPzXZS+RpjmcpnnU67iSjnoRJHb+iQ3ro+u2ijLVS17VUgLQmKe7Nzt8QYq6gvLt0LfyCRUU87d5klSCFnB8trIX2oIEi8v1TwubdAO7/96n588b5mOxXUni6i0TWlafrqov7HNTvDeHzyXpVPYj4Mqv33NHbzuCzna7YJmTDStUSdqvDzXLKnyqy29cScKbsCDun3e9j3n8JCluKewKueQGZ9sHadiqCoexzrCimWYdVp9H1NH8CpfagK5wRurPBehkx3nb75nD0/bt4Li8KOrWiJvVI1D6rhlo89XDh3hsls3eN/NcMtAyEIgHjMk9XQXyFcskKhwU9bhpVfcwd886Sx2dUohU2hCU8o78RshKMi+Vbrbl8ju7MPIvTzurELZkceeHcMIH76ezB5Pj6jtTs1nwfqYh2vzB7W9PxMXq4cMI/vUVwnH8wmzr1reMR9inBNYvuAAwWK7WyTfkTYbQTVcMvItScLe3gb7l1a5IR80OqeMP0dfhSAC5Hz8lsDBfuC0FU9a7HCs+qyqoal1NYl3zhWK9ioPWqY1LDrtPoEz2JN2UeljYVhyZPSsWdRaqJizrb6cprCaSE9Gn2LCIIElAU2KW6qb5mTlsyosRuXZAs7BaL/dKkwVVpHM9s7QxpdRpqOlBFUNJz7PXIcl6SJOSFRAxhc2GEbihX0e9qXLXLy6zHPPDqxdYrz5luO87po+nzt4ApUlUCEvt48tkC/RCAQ1PnjTBr//uTVednEPpwleMpxfjmJvIWEAzhmdc3YSTl+GOwcVIVU3jRrDM4Iw+OT17Lz5BJy9AyxA4/jwWUJps7hp1k3TjzwP5xCQnMQCg3ddi2VjoVSKqapB2Nz0PfQJ2y/YS0KOSHdmviAEjQpPcEKvIqhKnKt2wr29lPN3ZC17qKz+Weq8xbu35h3+9Wt9Ug2YhKnq6SynbbVT3GM4q3wpG3xpbkwm2Np4Fk/PaE+4GJFaaGybpDgZXBXfLMe0dJu4Vs2zVkZqbWNeaM0XxSSwK4H/dO4S7336Dn75EbvZnjpwWcUsnJ+vSbG5XIVhuoM/+fwRrrsLkiTDZBlnkzyYxhcj3n1gSwnuIbuLHR6lSdTQSkb/xl05HTrW3nQVWZSQC+e7FVybRVO+O80P1o4zwCO39jnxb1eTyLi8pQ4p0CjtWOvsuIB71JnEqylm02cWUzAnrA0GxbhTx1UuUhU6Dh67uxMzGzGiqnaPtYTRH0Yw+MsvH2Et+KItjZk4zdaepX1NFOJU4WiEmvlXKdM0eTRPs5uZXpWPba9OCte2hnUyuCa+GbeQ/6RNwxIZl7dW7hm8mpWvgTOPeo8mKTtcl9+8sMcbv2s7D9vtQfLF8zWir0hyFOOo9fiNzxxlw7o4pXDmL8YXEcOrEMTRfeJ55C4ULBn/K1lU/V3m4MQzeN1V+KODGhtPpj42gxuxpG0gmoozUCX/py9it6yD5RPlHWtV4zZe/h6c1qFz0X68pCNGTC1HIfACwpET/YqwH4fRNIe3gIny5DO3kSbxsC1GN47U7yJrZIXTjE/eFfjorUOkPPRfbXxUdsHU2ergJNOa7500ru3hgprAwmFqejY3r7IMs7SjU4GbNXrPi2sgWrTH4rs146ak0JavxG1i3pTUwCWC+R5P3dvjjU8+jWef2cMREHWMGtnUfAUzgUQx7eMZ8I5blXfdfAKTMNIFzOK5T5MKZCWmaNe5KL1HnYNf7ZLFHKjqWFUdREYGk5CbkV5/nPU3fwq1aGJaMHJT4qr5OXzZZFzTZ1WGaT6j8rtZcf4BQo7RX+tz4vWXg3oqHBuVsq5jjeNVhG2PP5+wVBjMwsx8VaJwPN7PcJ20WOJSx432EnpxqKQ8ZGeHB64CKniEaGtXNaxqxysqShxqHX7zqtu4Y+jJMTKMgGI2nMq4JhOnCZxTjauFUgOa1zkXFBIz01uEHuoj0CzhtBVc2+8yrvnufP5VtEdpxktFTs3XLKvBiZCIxCO3ncM5IXUCifCg7R3+5PG7+MkHbMcnQ1JttM9mvuUjFXAJQYRcE37tk1/n68PYQSzEbqYtg0qVPhNBnZAA/oIz8OetFgc9xgWNpbZW1TGqZRcgdwlHXv0x7OsbWFAIkLeMZYvWx2ZwMNv6KalUNUwV8oCZMXjDFegX7irOsC9L0rC6Sp2q8E0KipjSe9b5lGverbIGq40+QXAObjyh7Ol0aJs5H53WoC7uxt6VKk873YGkmIAXo35Oe7VxRtLMCY4+nz66xKu/dARPhqNPwCHmaubJIvZzU6idSlx7qDT0GdC5JuHM9BYTVpv1SWwGN8t0XgQXHzZ+VH1Jo3Gs2QPbmbpovs24M73xu49a4scfsEJw6ex8G/Q58TgGXLdxGr/2mTsLDSGPtxpb3QiZML3N8GqkOPJtnt73PZSuZWTiix2FDS2z5t8ygnlMHb0vHWXt9z5EJkqWZKSFJniyfKniqm2/+rvpkmniwEDAE8iTAF9ao/+az9AfelJKjao6M1rhdSGYVBxdHGE1kD7jAkwcTgXPbPoEYUMDB7M+OzvxRNwmLmpYUqivJnjneO4Dd7FXjqM+HvQ2eSqlTAwJZgnOUl599V382+2GZJAYaHHrzSxH/LzKOGW4CQpGD0cVFdf1tHXUZsXOCK3pTX9npEbPUe9nCeBFcUBtAKnGtWliE+VtalKtvjoavqTZGtYi+VZxmiRs73R41SU7+ZkHLo3OR2/Pt06fWsSIwj9eP+DNN+XkeBwZVppps/jiAYVEjOQHvo1sZ0KiCUPiyZzjLh3zH1MmeDG85mTm6L/+SjbecS15qXXMy3cBvjTj6v63uqCahou0GJkzbM24/TfeS7hpjVQ0ttNyszgyEsRV8WUEzJS+V3rP/Xbc3pREonZbnu4wjT5DueWEsTcVgrkJLJSzhFYQ4ARwPHxXl0tOT3CqqO/grHFMsVnRKCIBLleEhDzd4Hi/x//7sUN8qe9RG6LkmCqqoW5NTmF2W/xWcWpgGre+mOQQhvHcbxOcwQRBBdPnaUPTtaxZ6c3otI1n0wTOohrkNFwbv5qYebgJuStlpNUBc7XazeVb92NAD1jtGr936U5+6KwOzinOEiZOqG3SZ0ZwQDhOP0v53avu4qb1gGQhSg2Zni9C9MN4wamRPGwPK48+ByEed1xuNBLGdTr+33AWGEgAVdKjyolffQ/pZ9dGrMqJN8uYadwSs0m+NP3E0/hcTcPM6KOYjjfjGTl+6Dj+J5+g99av4LQQViFaDWOxPK71sQNeEdchd7D9+Y8caa0qhdFYFZwQT3EwyMxQDfz77es8eHcPr4UgbNA98mE5iTN8TmDFC89/0C58Yg0t28rSjmsQUC8EVxTIwfUnMn72o3dw24YjCR5FGIojiAJhguE1Kduivm4V50wJxFkdNGE9T7h+LRQaZXksGWPzLSbAtDDTHLQxP9rTKzrNlOTbytccHU8W12zczbK1aacLm8DV7+VPm17ezeZbL4NHnMO7Dqsdx6uetIun74l5e11vyVeqCYEZ6hyK8fljQ379qjWOSIfCizU9XwMnxSkF3iFeSF70eOhEp3lIkopZWIZSWAo5QlcdVk7xf+kIh1/yFoZfO06QnCQPlIsFnG2eL/Nw076nGnkhpogGghnrb76a4e+8j34IxXHlHlfhYymIq8I5xiQk5PSe+ED8Iw/gXNwvOTp6ukZDwS2FRAM3rxu5N3okcT+zTJajtg6ryuRnHujxiJ3GctDJs79L7aRZOWZgglrKhw+m/MInD3JLFnB5oJsXMtmmL8CsdrJZps2iOAQMwYcNsjzn9798lLff1AcveFcKFalojKOXpliFM3rfaLiZlZ7U+k7tdanzZdYoulVcm4O2+m5V/Z5r9sa3GDGqdrpnVUBAKzM3me8s3D4xXvOkM7hopxJkuZHvbPqUhLfeqLzxy8egZQ/6rHzFHJ2nP4D04dvJnaOTV7fp2Oj/aqpW+eaBwUdvY+NF/8Dga8cZekHU4ixpw2zaar3N6kNVopwEsCF9E/SfruXEz78NXTe6GhpUW6UcNoobldc8IRnQ/aXHYstuZr6GIgrmFc2Vd90y5LH7tqMMqR9rNX5vfLZoQ5JtTx2/cuEOfDetmIRF5xt57xvVWwqRoBhD3nFz4Bc+dgdfGChWLh2zekVUmVzVCNr+NoszMZwZ6+p45Rc2eMWnjserm0Lpm7PCWpOxJjTDgpuvbcxKrxI3JTRV+jYN8mRw08qwKG5UnkaR618qPJjDrlNFn6PD/m7gtd+xg/2dUM93Dn1OPDk5r/zsCS4/lNU0pHn5CkLaCWz79WeQeyk0kOpoV84UxrixNhL9P1mS0THH8L03c+LFb8ffuM5QFJfnI13vZPgyU9hWcRLbULAO9obPc/uL34ocj5dMZNRvuhnPfY79dGMNSzA8+tQHsPod98MKa2pavmqCBMgZclNIWOtvcGCb4NWjEuIx7I1yuGkN03B8z4Flnnx2vBYpsQAS4u5DM0YqxUQnNIKP9xYq8M5blJ/7wB1cdVQJKlF1DoEQcnJVtEVbajqQy795uKA5GnJQRU0JlrM2yPnVT63zh5/aYMO6aFCwQO6ShhCRkvziZwbSZSlzmF+PTa6h0TSKPTs9rG4qtiWxgJl7MrhZWtOiuAlTq8yvplHauI2UmuVJ5jsLZxLoknDRacv88XessM0BLiEJymhqfAp9YkowuLmvvPQTd3DjekauSh5y8jCbfyZAEIbPPJ/lZzwIh5E5xYvGblIxB0vWWUVwJbknmCLq0HfdwF0veBN69WE2vOHU2AhGZsQ7LGdoWNPom9Y2TCE3I9e4h9OZoQPo//GV3PXL/0LnroCNxvPGHaMV3XGkTYrGW3Sc4nYKO37pu2A5LnGY1WfEjKE3fG787ReP8JQDqzgB71O885WmVRmc2qWfgAouzXnZhWeyJ80g69HJBBfK630KXJMYGasoRrwz7oOH4cffd5A3fnWDE0UDFqQ2IdfUkqr0NOOm4aJrUMkLdl57WHjhxw7z5188xlHXB8lQ57HiyLExvfUixZDg8gEbScL+FRePiZ2rYc1Kb/671fI0R8dThZuVd3NgaAe2RVrjeyms55uVi+Y7C2dOEElwQXjGgV287BE76IUMfLfYejOdPkWjtqApV9wp/I9/P0o/yxFyvIaZ+SpgiWdJjNXffCrZHsFbhw1xDJ02/FlVs7B4YlEnUxQ1GH7kq6w9769wb7uOoRqpZrgwjMJgCi+n0TdNeIkIGQHyHGc5uQ0ZHlznyEvewYlf+xfs+KCgz40F0pjiOt8xLHYMungUx8rzH073sWeBM6Q4lbSaf5U+F8UMN2/AZ9YGPGr3XWL4OAAAIABJREFUEl7iKRZOfDG21PN1zUQiIXGZl88dD93reMlFq1h3QO7AqxFnYqzxR8sniIKXhC8PEn7+w7fzkssP8qW+gcRGUZ3Sb9rbbfb3TJxAsJxB3udPv7TG9152G//8NWXoAhK6EWAVmq3y1wje4uF62/wJXnD+7pHaPF0LmJ1eLd9pKbQMHm0O1ZPBzQuL4hpv1YslVjEJ5wutzeTbahrhoj/EGUvAi85f5gUP6mAacKWAmkJf1JwVExhKlzdeO+TPvrqOalIciTIjX4uX1QcB//+3d+7xlhxVvf+uqureZ5/nnHllJkwSEpIQkhASEgN5EMJDXvIReeXeeAH1+rhX/SCiXPANKD4+vlAuiiICH598FL2iHxGC4Ed5GFBIAkQICZDXhGQeyZw5r713d9W6f1T33r379N7nTGYmk5nMms+Z3V396+qq6q5Va1WtWuuCeWbf+Dy6rks7OJIghZRV3jdQoFiTFvOyJHD7Knu+/y/Jfu4jmD0ZuU1QMevGnG0arJq+VVXFoaw6oeMtnY99k30v+2Py930Rv+JwhTBRSlH98g0mafv1EAQbPLmN5ZPLdzD5k9ewOhHAm0rdmxmox5AGz1u+2OV/nj0FZm3/qtfD1C9SNJ4BvHFA4HVPmuLFO4TgBhsYK3dV6lFXg4ge7H0PkymLMsn778x49t/fzVtuXmRvz9CUY7U8TZLDKJxmjn/+Frzg4wv8xH+scvfqBOgSrpsiFHMbpVom1b+1+XubIep45dnTvHhnwexo/nD7VR+T39Bz16tHA2Mcmjs5ArhRz9/YZHuVtFavyvewTn0P9bkjcUFRY8hNIIhn2hp+7tItXL0rkJt0nfIl0XON7WJCl16u/PKNy3x8b6cfkXvUc40ErIIJcQXN/tBTSV9wFoGcrrGFZbgOnts/qgwqxbEhGlZK6OIWLKu/eRN7r/0Duu/5HGYhR8dwrEN5byJCJtC65X5WX/NXLL3ir3GfWcL7ALbXNycomdRgkn3ty1QUhwWv+E3CzM9eg54yTVvT6IwxhKHnrmk/lM/ugTsWc563K8HoBvp6CEGrKoNq1b4iHgXNuXvR8J037ObWJUWdQXo5mhhcXsRzG3qOMggEoMWvFIWEEAMbss11ePm5m7j+7Fkum45ipRqPGNPv90Gl3/9VFaMhSniieHLyAPcvW/7ung7vu2OJWxfy2PgilTJURvvqnEq/fLGeogqSIqGHinDlFssHn7eNbWkPw2Tf4ZqqEr1iWrz3XPuRfdy4Nx/OTytt0W/RspEMZ7Zz3vnMHZwxyUA9fpSRc3DWhMcYRwggNkqor//sAd5x2ypDA1T/o6l20OKCgKjyLy/cyjNOSeIeQDNaXa1+iyVtFAfg8Rgv3Lyc8aob9nL7YtzGkfQM3YlAyOvl0349BIcYz2lO+Lvnz/Lk+Uk0GNR6LA5jRj1XCPTIvrrIgZe8D719kUDAIhhxdAi4sisMyR5V2atkC/EoSMCIkp29jdnvv5TZV16M7mpjRcmMQQrFTQNx1buSY7lNqFQ3DRaz7Mn+/W72v/tmVj9yK3PLOV4MXge9ndrRgKUOzo1IEffZxj2RaY+ZX3wuMz9xJcaVk/RFnxp6X4EeSuI7KC0WgvLSG/byAxfM8d9OF1ImoBZsdg1v0kqr119E9binns890OPl//oQ+7oG4+NMvtpsEPdo6Dto4JRaMAVjUWMwQSEPYDxnzglXn5pyzSycu22OXVOOGRud9hsjBB/nnTrBcF8mfGmf5yv7O/z7AxmfP9Ch6wF8fzlYtcIgyvKUVe0zUO2ni0KqSmYSgkk4p73M3zx7B+dtdiABhwLJIO8mhlXJbyBRaa1tBKuWYHNS7eI1xcvANvrRRD/2hDa/fvVWnObk6uK3FJTXf65gWEOMCkrmBFTqXny4MMywbPQTPGoVqf4NbhhHjNSiQTC+x8fvt1z/qXs52HEEl+J9VvlGtfIdlPUQMDmYFpfOrPDhF5zB5gRUNMYYcKPLl2mO6Xp6/7abB1/zV+i+DkIgR2iHOMG+3lsuixGjRitWQ1R4JcFPO8yVp5I+6yzSy89g4sythDnBTxjSiulRUMiyQLLi0T09lr94D/mnv8nqh7+CvbuLFYMWMRODxAA0TbKg1spVpgQLGhQjioqhff35zP3+S8mnLGnNcHcNY/eBzIDS5XduCnz0/v186Hk7SU1CaoZ9vlfbuSQ3btm0Dw5gJOeKbSlvv2KO1/3rPh60KRIckvViYEWpfLk1yaVS+vjp+hwRS1CHuAywfH1hgrsP9PgT20LDAhPaYX7S4oyjlaZkWUY3eB7KM3wmGE0JmqOJgLeIyYjBHFzcuFk8b6gTVatYMpJy/stauvTAwS6/n/97zWlcsCkho0fiLZjoFXTQeWqNVctv6Hn154bIoDKmo393GbNt6BjSkqTE7m9qa0Ul1d8vw+cwzBSqwHXm15o+2A3jgpAXAcyv3ZXzs0/dzC98tkMnX0HEDZewVr5UFfWGLAifX5jktZ+5n3c9/RSmJizYuGI+6rmKwdscec5pzPzKizjwE3/LpgVl0ca51bX6cVXpqs4SaVwKUFg1gjFxscsuKuGGe8hvuJcl6cJsQmvHHMn2WZbaldgHPqAHVujev0C2Z5k0tzgVpohOCXLNUVWsGFpBiEPtWvZUDjQD+SuabuQhMKOBngT0hWcy85svxk8LttjFN2pwQZVcwAbDZ/YZ3nnHft799HlSa0m0S2ACW2uieju7oQxHUJBAIMWZHted2mb5ik381Kf38ZCZKebfywIVUWBipmsqH08NGMWELkqA4KK7WLNMtD7pIsaQqWNvR/DGw8oyIkJAIKREsdRj1EIGIemhucXg0CFvlOVzy/JVmWrBZENhLRgUax2bQsavXLmL52yDYBTnE3JrSMvoKCPnC6TxcKCCDp6rVuNksAbUdjGYsXMUx4qcZAieIC2seqizLen/V6nzkIg1SBpqMmXI4+iIQXPUytc4nGo0vEwDqGmhqvyvMy13L6zy+1+ZJjcrcad/daqgUo/M5CgJaAbk/L97ptjZWuJtT3MkMoGY6AZFgkQ7v4pEkXjBi8OKkvyP8+g99GIO/NI/YJcUbzwulN7khybRGDCGAYPwIrQx2BCiNAN4UZCMHjmJOlgIhAML9L56MK4kVt6AEPc7GvUgOSrCqhoQh2j8/lSVjgXj4xxalWmVqmnMr1xNjSzLYOgaz+I1p3Hq21+O3dZGQ3SPbsrnV95H9fWLBvb0erzl8we4dMsU1+5sYQTEp6j1qJqxA9Na56I1UFx+NET+3cI4uP7MGXo9w8/ftMhiEU1D6UGYJtgcMAORe6gJoTQn9poOJDAFxKIaq6sSd/lAlEYQMxiQTSAuRwvBFpJUsCBR5+87yy6lvLqE1VdX48tUE0AtJji2tjr8+hWbue70CYwYjAg44qbWcmG03y5NDVb89qVLBr8VNTmgBLWAB02G86jee8TTivbQ4rgqAfdfVb+RYselhQlKDPNU5zzVwaBBom5Ka6Chj7u2aHCoOBEZjNKqiBFaDn7m4q184+Ae/nF3AiGGTs9DvB79LpV5x28pVtXhWeVdt/fY3J7iJy/0tOLEKrk4khCG5lzExY0pqoq0hM0/ejGu7Vl90z8h3UBXlVYx8MUJjkHU6AGDiOdWA71Bzviyn6jFRL8HpYJWbSFKWUkQ+tbiRTcyRDtI+k8RnC+Vvfo7GkyzR2YZVdsgSisEsqtO54z3XIc5c6oImwe2QQ5XzQmFZqUqZB5+88sHuXUh44PPnaedtiIwoebxoplM/WU3SQ/1lbop4/mh8yZ5x1WzbHVxSVktGOkhWh9FmkiGO1GVoayRmo8CrlQDHQhtJrxh12yXd125netPT3EhGsnWR/QNL/k3wapzaMcaN4SVYezDpcPIb6MrXA8HF3mzY971+O3Lt3L5JsGbBFHBGF9s+K9QrR5GAz16/NaXV/mjr3ajwz6Jdnn1+q2Z/G8ZJr//UqZ/9YWQJrRRLIHcCIm44vMcSDH1lbjy+sPBlbZT9Y31Uvm3MVwRK5QWk9rCiCN/5uPY+uffTThjunGyYFgd9AS1WB8ltA/c2eEPbhde+oQJLt9S9wLT3MeGzBoOfRkbgjhE4PrHTfDuZ85zXhpApoqVg8r80dBgrMO/gxJSGd7X4o8GrlAHpBdjpT1x6yp/fOVWXnS6xYUEbwtrlJr+vK55gG7guccUR03yLRNrpM3JI3EbyW+ImiWnjZivHDpOQXPy4Ng1m/C7z5zjvHSBzJUb4MfXI5BgSOn4Dm/74gofuG2RniqYHCPDJjn18qkKWKX1g5cx9+6XsXpKgkpWMFHtM4vyaXWmQY2pHAquTB1p8LlhHORGEAyrE6u0Xn4m2/7y1fC4NibEPY9D99T6h5JgFbwGPr0/45du2c9ZrcDrz52rCqej76+dNxqOjpqhL695BKuCOsuLdrT5s+dv4xlzXSAvHA5Z0ABGEF/oUqWK1v+IqnmPUGP0cHFa+StShSgGG3A25zvP8Hzg2jN4xikWR0JwSiKevmrJsOQ53gq8oXzlb3XV8ljiqvx86FrlvhK3Hh1KfsMF3EDmR4YEQ3AJqYkKy1NnWrzrOWcwGzKsOFAfTSysqXwulXpoHl0DS4sHfc4bv3CQD3xjORqWqkMzjat/6td0+gQQY3GpYK57Elv+8vvQc+dJNNAxBguFyWuIwUprjKTcBDMsYcmGcPTRUsMNrjThYh4x8nvAowhGUtKJjMkfv5KJP/wuzCkpLoBxZg3DophLy7Wc+ggEybi9a3jtp/fxrdWEH3mK4/ET5bRI7X2NmAYoaY3haHWUqKuCZcdNAWMtiTE467h4vs3fPO9Ufuz8FjYN2NCLdh/eo2lprzPcQIO5nTEfrxwuTqHYbS4GnLfYPCe0AptF+IVLp3n31Ts4a1ZJTIo1ErcGSDJsc1Vrm0ZqVLtk+O9Y46qSWKnS9JkbDbh16HDyq6ptOtpKv2mXw6HgRCARQYwhkbjidtlWx589ZwtpYjFBMaGHzVfjIFu2WZFfEEuQvGBKcMBbfvLGffzFHYt4VXIL6oVVDOTDUriYwtWzGFJraV2zg80f+1Har7yQRHrR5VIxid3SusxTSk0Mpa+VjZpxw2nS/xt+E004sMQQfqltgQitHZb2+65j9s3PZ3J2AmMcJrFYUzotHFDQmJdBwWeICgtdz//+9IPc9lDGSx/nuP7xc5DaIXcz9fc5ippXq8dQ1Xq6pK5RNrU6vP2SzfzDc7Zw4SYhEY8ah82GZqvLYq0dffsjW5mmh48rzkUEDYHcGcRN8+1bLP/0HZv46Qsm2WIDNiR0RvCiqqX4uuqz6nD5+vhapz1WuKFrVCTUfmWbceNoo/nVqdKu8bQZW7fUP1ycitDKlBeekvD7l80wnQSCmcdoK4ZsLJlVQz0kCOqVh0KL1392H+//+kGyEDDkTORdsGM2+orgcsGemuL+9BXM/N4rMLumUBdIjKFjo8HlgIVQkXmGpaON4epaRjwepDbjFGE5EdqqLKUZEy85j9mP/wDtVzwJm8aFsXF9IYhDAohXRCzLXvi+T+7nkw9YHjdp+JlL55hRARlIpBvdiaGqa1XCeudsSqvj057gdYo8Tfj2UyyfeNFO3vSUWXZO+YoFbq0zDbWT0l+Z6ZMcPg4QExvYOseTp1d571WODz13O5fMt/CSgiZ4CxPFjvimujYto6+hElMtX3U+TR4FuP5fcaF/b8PxRibMDyW/xtubN7GXx3Xp9kjgAh5x0Svmy85u8VvfNs92lshdggYdMKuGeqgBi8WqsqIpb/jcg7z3q8ssY6ItYmBs+bqJIWCYVMX94IVs++irsa8+j+4sfdu8QYyd8rhkSvF4+NpoXGROVfmqMoiNwQEkCJ3zZzjl3S9l/i9ehj1rE5kExHt6BsZpG1GdzAkC+3LlRz/5EP+4x7IpW+atT53nnDkhWI8NKQZPueK73rxkiXPVk2pjN1m7jzIIy1qQ+mg7otJirtXjzRdNct2ZM7znvx7kb+7psnsloOKi2G0CJkAQU2whEER9tKk01RG7aMaB5BsPiw/PGFNY3JYfI+As5AETDMEYrBHOn/W86uwpvvfcWbY6IWrXJr5qC0lQgpFoxlBpoPpxtY28KjmKqMEGE639S9Wo9gqbXuuxwdVonUHNqCLqi2CkgmjcsBp9PwVCqFnFjMtP8sgMvETral2/iBtdlT0UnFPBW4chMB0SXnVOfG8/9fkDPJQ7JMQuZzXgrQeNW5ijzVagZwImd3ibsdyb5A03HeBAL/Dj508zmWaEkAJ5zEMcptIZUwUViyRKSzx67jw73nU9q6++m8Xf/QR84n50sVPYROV4AplxGDVYjdKhL1iNKazmB4xnWMYqU0vpK5e43uc0Og4MUtoZejyGBKFnID1tM/IjFzD36qtgRwtVj4hjQpXghIn+NjeKpwG6CprE+ayiqfZ2Pb9w434+cNcqzrV56ROnecXjJ2iJJRgtvGisz6jq79g1JVapbpTXtHKWQHQXC6goXh0Gw3kz8Lanb+GHL+jyV9/I+Nu7lrj9YMayN1gE1RwjEn1iGUMoXNf0tzsYQUMhoheMS41Hg8FoEhVmW0xTYXGqBO9Rq8xb4arNU1x3ruN5p6bMpynWlEw3Gi32jf5sZIJ1xt2kXvTTgqFlM1YRcreKNC15HOfUMzYuSYtG7x2qSPC4XJDQQmy24bxM0eGDyfAkpLXrdduqo5dmi5AqBiwk3vLfz5mgZeZ50xcOsndZ0SRg8hjlZki9JkpRwcRFGTUZmVfe9uVFdve6vOWSOeaTLi6kiOaoC4Dtl8X0tQ0h2nsZfAr2maex5dJX073pXpbe/1myG+7EP9AlCZa25sQJfUAVkRwRJWirohZWJGpKZlU9z3HiCSoYLMEAajAhoScJbiKQX7CF1ndfxMzLLsadOoVawQQwJHHiSKTYVjfcphIgNwmIjTtitMO9PfjhTz3EJ+72qJ3g0pmcX7x4CxPWIRKtzxBiGxwiDTGscZbETdQkhUSK81aGyJHPmk742Sdbfuj8SW7e2+Ff7lrhn/d3+MpBx0pmC9ctGabYo6SFWK5xx+2gcRRsHkf4OEo7TKaoCTiTM9cyXLTJ8R27Jrnm1CnOnRHaJsEUVrhgGplutc6DrTfjcbkRRB09EUJoozow8ztRyOARjYqINRmKQ40hWA+mh+rGmXSgjeQJNrPkdW7F2kFhlJpwxHHGM+kd158ZmJuY5Q03LnFHJ8YPtEbxpYvgob2nUE452DCBDxnv++oK9y1afu1pMzx+pkfi04JJj5YARQI2FDZdU0Lr6tNIrzoDvWM//sN3sHzDV8m/cB+yL5CLwyg4deTl3E9fMawYixZXqlupAxbRBFHIxGDxhJbCOdNMX3M2rRecS/q008k3p4h4AnGxIIjGzco6RopVQC02ZKgEvrwkvO4z9/OpvQ6fOLanOb991Wa2t5WgGZa0cTBpzLoBJyHE8M5NRqOjRq1xONXoxVBNRXfW2KSq0lcvlnpw11KX/3ywxy17lvnKYuDelYTd3cBKp4vYODJFUVz6jWMxBNtjk4XT2gm7Zg0Xz8IVO2Y5Z044tZ3QVkFdXCpOcKhEr5JJLeTYKIa1kTTv4/rOaujyazft4falqXVfwPFGz94Z+IGzp8lsi0Q9XoSg8EffWOTGew+S6/SG8zJmiTc+eZLzNs1jg8G5+F4fOcmqOS0LvagGBoO3yi37O7zhsw9w44NTEDr4oRB3VXUohq1XGwgSsFmCOsulmwK/8fRNXLG1RWKi183GsqgSQoaXOKXQlejbMwGQYpN0V8nvfAh/8/2sfuoO9Mt70dv2k+3rIL6cvRqmuqZdfrlmKkFOm8ZdsA130SlMXHM28sStsH2iEJqiyh+nJMt+qnHLmhk28SnrESmQK4hXPrKnx8/cuMBtB7sEY0i88r5rZ3n56TMYEz2vuLJfj8ivSZuppg8xrDqonr6eFNLEzCoZFQNTfYNHbPZOgJWespwHFnqePd2c/d0e3dz2X0I7dWx3yvxUwqZEmHLCtLMkfeZYMHzRyq4gAa2OQ4N6NdWjqaGacDHKSDzymLhE219dqkzcxgyH2uGY4arpTee1dK/R2VvZLZQoO+do3IYhG89PFWIU8fjxl3vXRrV/0/nRwNW7uKrn7tXAL9/0IH/y9Yxctai44LylZ8pgWP07ilYRkB7Wt9nZ7vLzl0zzmidsIhVBbC/OhYkdlK+4s/p0gWIeSPsfc/yR6Ep5JYeFVbr7Fwl3LpPvfpDkgVV6DxzE5AMj1oBiZ9qwbZKwcwb3hM20Tt+ETCbYuUk0aTY9XatcVtLLgToERGJU54AhIHS84Q9v28fvfKnLfR2LU09uLL94ySQ/fdFsMV9ZyIDrvJ9Rx/3z0h9WNZFKATfy8vuVGyPmjWICa4FxuVN9iC3lXL/ZtFrxcpVkRDnrz14Pt9HyHe+4+gLKqLweqzivPcQ7cu/58/tWedO/L/Fgz4O1BK0YFPcHRB30dBOwvRTvEmbsCq88p8UvX7SZLYkDcpxzD7t8vmByUho6SQcVi1cBLSTWolyqkIfCQYAEomvA+Oz1JJpx5YPoX54AmY3beu896HnjF7/Fh+6agEzxbgUJgdc+cSu/cvkEbdcam99Gn9vHlRJWtdHqx9Ubmxhalda7ryl9qFA+DiseQJTS+YVQ+E/SqFqKTYqBr1nSq1d+vQ+iXsbHCm7s4PEYxHVUSUOXXByuk/G5hcD33vgt7jg4C34VbyoqYl8kKdpbDDbLMU7JnGByx3lzLX71ioQXbJsmsc2eCDZSvqCKasBIdPocVPsr7ArYyrYi1TjtYorjqgeFEALWDk92j+qvo5gIeY9eED5w3ypv+cIB7lpMMOpQemASXnVW4B1P286MFUyxGDU2v40+F5o9jpYZjOJyG01bbzSpFrY818KNRfS/XrHMLX1GybBX7CYax3ibMI8VXEnjBpnHOi7QQXFYVTIc+B57Oo43ff4B/u4uy0ooQt4J0A/GG/MwQaIkJgETAkYDQR1J4vnBc1u89eKtzKdSuZ9h/WtM+UIIkeFQqHxBENF+OLDqInXQyNCMmMI2TPoBsct+WWeMQ8yyXywZSJCxMGQh49YlePN/7OdjdwZywEhGZpXUG15yxiR/9IwZ2i4uEhhjRj5rzXM3gBtiWPXGqp43YcalNRVoXP7r0dHAleUbVacTEVdix10/iatIK15Rm7OSW95z+wHe+aVV7lyxoDli88I+zRFXTEPhGq4QvVQLCSyygCfNBt586Xa+Y6chTWwRMa+cC5Qhi4EjVY+m/lje14QDyIMvVoYNIXikmMW8bQXe+7UF/vzWBe7XGdAuRnOEBOMML9+V8ntXbmE+XSvZr/fcDeOqKmGVHskRb9SzjyauqYFG1eFEwTV9IE10Ejcg7wNqFNfLyZzhP/f1eOst+/nkfS06KjG8Oz28lBuBdVhVFKE0gxBt0TI9XrDL8roLp3na1hYJ0exGbcCaZrukR7K+qoqGDHAg0KPHXYuev74j44/vWOaulR5qUvBxF4sCLe94zfnKr14yz3xrItb5KJVvjVlDE5PZiMRVxzWlb0QaaGJsRxq3Xt3G1eF4xj2sEe0xjlOFnoBVwXgFCRz0yvtvO8jbv7bA7qWcYNqVrTUV3azPuOKJVSUkCeK7zLlJvuv0lO89P+WyeUNLYvDQI12PEjvuvioOoOcDWehxywHhQ9/o8td3LXF3J0OlhckNXjPEBVQDk87w+vM28aYLpkha0BIz9KyNPnfDuFF2WE0PrVesiq0/9FA7zbgGPIk7MrhRaU10ElekBy08koa4NUnAhoCXhJsWVnjHTQf44D3QUYshi7s2VAvHgMWfRtXQiMGEQI7FFotJsy3Dc7cbvu+8LVy5vcVsEVonKmFx6jzyvaJ8pRdRIZrplAyxZI4FCVpJrm7gqbj/q2isoooX4d5Oxsd3d/jbOxa5cZ9yMM8JavHGYSRDQhdvDBJabEty3nzZPN9z1hRtG0BdtOZvaOtxgsSh4NaYNdRvbDqvM6J+ZrUHrYd7uIU+XNw4bIk/kXHj2r5+/0ncaFwWAl0f+Oi9y/zaLQvcdEAxueCT6BhSi32Yg4l17c9XDbhMPJtMHBdsEl5xZsqLd7Y5Z9YiYuNEPoYgkaEokYkm4uMKuRQOZ0KMlNxfBiiYUFw8jGHVVDOQAMHgjcXjscHwjaWcz+1d5aP35nzygZzdvYDP8+g4ABCx4LMiek/A+AnO3QLvvHyOq3e2+5FyNtKnDxc31nD0cEfmjRZsVOFGSQiHi6uWr6lOJyqufu14YAqPZpyXwtTAC3t68Gd3LPLOW/dzb6cFSD8QK1BRDalIQgOGZTSqWCQps8Zz+WbLs0+b4epTWlyyOWVCuogYytXzgC3mzHwRVF7QEFATt8UJoYg5aDCaRy/w4vC5snuly837V/mPB4VP3dfh9iXDgUzoGoEQw7eJMcXWOEMMbBAXFSxw3a6EX79skm0zkPp2P/TZI9H+jXEJy+Mh4DpqYR036nod2yQlPRJp4+p4ouKa6EiNfI9FnOYa42Wm0ftHCD0e6MDvfmWZP/3aMns7RcDYYg4LqR4DRUAIMaXPOIfJBDE53gBYJo2ydSLhgs2GC7d4Lpx2nLNpktPbhrnJBKfRoNoWxlZeYXHVs79r2dvtcd/yMvetGm5bcPzX0ipfW+mwuCz43JAHxScJJmTYoJjgwUFPYiANClMKUYuScLZb4LVXbOd7zpxiQgSrBmM1SmAj2u5Ip61hWFVqUutGdfxx6mAT7limHevnH8u0UXQ8SDSPNpxHEQ0YpVDNTPRooMrdXeGdtx3kL/7rAN/KpoAOSo4JLYIIkJGokImJjCwEMA7EF66WpD93ZQh4k6DVSObEcHYSQvR4QvTnJsZGSQ0BccQIVh6RACqUlzCRWZpcwBiCiYzHZAafdLAeAvH+Ged55eMS/s+3beWcKY8TfqU2AAAE6klEQVQQ41WqKEKz9f5GvsGHg9vQ5uc682mSXqq46gPHdaSmQj0SuFFM+ETHNb2XepudxB05nM8z7lkNvPebi3zwm8t8c6FFT2P0TafgbRZFInUgbrDXtfZeGcVYx+G0nC+DgSpaTZAaLo/zW2KwWRu1wjQdnrIz5Q1PmedZ21Om8Wjh42tcuxzNNNFIzQ0y1BajM2liaKMksaY8H879h4sb1dkfC2nrMfqTuCODC6pI3kUd7Msc/3zXIu/9+hL/ud+wnHkI04hkBOOL+a6SiRT/iQx4i9bO18VpWdhh3KCgfZwJoC46w5TgmJ5Y5aLZhB954iwvOaPNlCPOikmc2B/li33U4HgkcX2GVe/YTS+l/oKaXliZXtI4Ma9Ko55XpyOBG1fm9RrzeMeNks5O4o48LpCRq8WFuIrWNYGgPW7eH/j7u1b52O4D3LaQsKoJohnaD6xa5NNnTFXm0+dM6+BgiJkNSja4v7g2oULHCdvtKs/a4bj+7G08c0fKdNKFMIGIQUyOqIGau5z1BIEjjWtkWGWDN920HkM7HNzRlKjWw42TAk80XJ0OZSA6ids4TpXozrvgGQaNNloiBIS9Xc9Nezt8ePcy/7Y34/YF6GU+Gp+KFtt+DEZLV+LFSqIR6DM3LfbflozIFI4CBMVDcU1FKouUEuefJDBrEp40C88/bYKXnD7Nk6ZTWmkpgJWMEUqDib7r5RFCzHD9j8L0TpOle/3GUaPJ4XLMcZ1oXKWOBK5+/FjAnaRHDylKFnKs5gRNWOhavr7S49MPrPKp/YEv7etx33KXbp4j1iE+2kT5InyiCWXEi/gX2VX5v4IJSDAQXGRWRhAfcGI4dcpy4ZbAtdtTnrVjirOmHNMtgJzcOtq4DX83jzTukO2w6tj1JLNREkBTIZvE7VHi+OHg1hs5R5X3eMcdq8HhJG4tDohW9AaUuCexXPnLfGChE9jbddzyUODmfR1uW+7xrY7nnsXAQx3BaxZlJxPDsRYZIqIQAg7DnINTJhN2TTnOmLZcNK9csq3NaZOOrS1LS3K8yRBaRMksMkJxdqg+69EjiVvDsMob+oAGRjQu01HMayOFeqTTxn1gJyLuJD16SFXJQ5wXEiNIOemuZshaPZoP5GRq6QWh5xXvlQeyjJVMWOoKK5kSvNJOhem2oWUD84ljysKEVVpGSYyJEXwQci23/AgiORqS6E9LAyqCNW5kmY81Axuyw3q49GhgPoeS9mgpx7FMO0nHmGoT4VFqKE+LzjkE00YfcFoDSv1aP7EAVefc68UowCKPrm91LMMaJ0mN+/AfDu7RRI+G0eMk7iTuSOFOVGpkWOvNlxyJtFF0LF/wOBX2JO4k7njDnYhkYNAAVd6lOhyuvmQ8TRpknTGN0jIPJZ96GY40rn5cv+9ExjXRSdyJh6v356bj4w3XuDWniek0MaS6JPZw0zZy7Wjg6oy2mnYi49ajk7gTA7fRb+B4SluzHFAFNzGaano1o0NNayrQRhjQkcKNKtO4xjpRcOPoJO7Exh3vtCbWeClaVplNk4hWl7bK9OpvedyEa2KATXS0cONU1xMVV087iXvs4Y73tEZ/WPXOME6lasI1iaZ13LEeidbDP9ZwdfxJ3ImJO97JNElQVRqlwpXXNirN1HF1hjaKwx4tXNmJH0u4avooOok7sXFVOh5x/x8rHmI96RmaVQAAAABJRU5ErkJggg=='
                doc.addImage(imgData, 'PNG', 3, 3, 27, 16);
                doc.setFontSize(12);
                doc.setFontType("bold");
                doc.text(initX + 30, initY + 8, "AGS  ");
                doc.text(initX + 30, initY + 14, "GERO");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 1 * 190 / 3 + initX;
            }

            var bloc3 = function () {
                //Bloc3
                initY = 3;
                doc.setFontType("italic");
                doc.setFontSize(8);
                doc.text(initX + 15, initY + 5, "ADRESSE");
                doc.text(initX + 15, initY + 10, "CODE POST VILLE");
                doc.text(initX + 15, initY + 15, "tél. +33 (0)805 696 443 ");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 190 / 3 + initX;
            }

            var cadre1 = function () {
                //Bloc3
                initY = 25;
                initX = 5;
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.rect(initX, initY, 200, 12);
                doc.text(initX + 1, initY + 3, "Date : " + (moment().format('DD-MM-YYYY') || "") +"" );
                doc.text(initX + 50, initY + 3, "Heure : " + (moment().format('HH:mm') || "") +"" );
                doc.text(initX + 140, initY + 3, "Poste : " );
                initY = initY + 15
            }


            var cadreIdentification = function () {

                initX = 5;
                var newY =  24;
                var i;
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "IDENTIFICATION");
                doc.text( "NIV :  " + print.cas.niv , initX + 40, initY + 4);
		        doc.rect(initX + 80, initY + 1, 3, 3, ((print.patient.sexe == "homme") ? "F" : ""));
                doc.text(initX + 85, initY + 4, "Homme ");
                doc.rect(initX + 150, initY + 1, 3, 3, ((print.patient.sexe == "femme") ? "F" : ""));
                doc.text(initX + 155, initY + 4, "Femme ");
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.text( "Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
                doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);
                /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/

                // Test calcul Age
                if (print.patient.age != undefined){
                    var formattedAge = moment().diff(moment(print.patient.age), "years");
                }


                doc.text(initX + 140, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") +" " +(formattedAge) || " ");
                /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

                doc.text( "Adresse : " + (print.patient.adresse || " ") +" "+ (print.patient.cp || "")+"  "  +(print.patient.ville || ""), initX + 1, initY + 12);
                doc.text( "Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
                doc.text( "Personne à prévénir : " , initX + 1, initY + 20);
                    if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
		            doc.setFontSize(8);
		            
		            for (i = 0; i < print.patient.prevenir.length; i++) 
		            	{
		                doc.text(initX + 1,  initY + 24 + i * 4, "" +  (print.patient.prevenir[i].nom));
		                doc.text(initX + 40, initY + 24 + i * 4, "antécédent : " + (print.patient.prevenir[i].prenom || ""));
		                doc.text(initX + 80, initY + 24 + i * 4, "" + (print.patient.prevenir[i].tel || ""));
		            	}
		            	newY = i * 4 + 24;
		        	}
		           	
		        doc.rect(initX, initY, 200, newY);
                initY = initY + newY
            }

        var cadreAntecedent = function () {
                initX = 5;
                var newY = initY;
                
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "INFORMATIONS PATIENT");
                doc.setFontType("normal");
                doc.setFontSize(9);
                var i  = 0;
                var ii = 0;
                var z  = 0;
		      
	                if (print.patient.passif.antecedent != undefined && print.patient.passif.antecedent.length > 1) {
		            doc.setFontSize(8);
		            
		            for (i = 0; i < print.patient.passif.antecedent.length; i++) 
		            	{
		                doc.text(initX + 1,  newY + 8 + i * 5, "" +  moment(print.patient.passif.antecedent[i].timeCode).format('DD-MM-YYYY à HH:mm')) ;
		                doc.text(initX + 80, newY + 8 + i * 5, "antécédent : " + (print.patient.passif.antecedent[i].antecedent || ""));
		                doc.text(initX + 40, newY + 8 + i * 5, "" + (print.patient.passif.antecedent[i].auteur || ""));
		            	}
		            	newY = initY + i * 5 ;
		        	}
		        	if (print.patient.passif.allergie != undefined && print.patient.passif.allergie.length > 1) {
		            doc.setFontSize(8);
		            
		            for (ii = 0; ii < print.patient.passif.allergie.length; ii++) 
		            	{
		                doc.text(initX + 1,  newY + 8 + ii * 5, "" + moment(print.patient.passif.allergie[ii].timeCode).format('DD-MM-YYYY à HH:mm')) ;
		                doc.text(initX + 80, newY + 8 + ii * 5, "allergie : " + (print.patient.passif.allergie[ii].allergie || ""));
		                doc.text(initX + 40, newY + 8 + ii * 5, "" + (print.patient.passif.allergie[ii].auteur || ""));
		            	}
		            	newY = newY + ii * 5;
		        	}
		        	if (print.patient.passif.traitement != undefined && print.patient.passif.traitement.length > 1) {
		            doc.setFontSize(8);
		            
		            for (z = 0; z < print.patient.passif.traitement.length; z++) 
		            	{
		                doc.text(initX + 1,  newY + 8 + z * 5, "" + moment(print.patient.passif.traitement[z].timeCode).format('DD-MM-YYYY à HH:mm')) ;
		                doc.text(initX + 80, newY + 8 + z * 5, "traitement :" + (print.patient.passif.traitement[z].traitement || ""));
		                doc.text(initX + 40, newY + 8 + z * 5, "" + (print.patient.passif.traitement[z].auteur || ""));
		            	}

		        	}

                doc.rect(initX, initY, 200, i*5 + ii*5 + z*5 + 10);
                initY = initY + i*5 + ii*5 + z*5 + 10
            }

        var cadreCirconstances = function () {
                initX = 5;
                var orig = initY;
                
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "CIRCONSTANCES DE LA DETRESSE");
                doc.setFontType("normal");
                doc.setFontSize(9);

                doc.text(initX + 120, initY + 4, "");
                var size = 9;
                var i    = 0;
		        var cin  = 0;
                var line = 0;
	                if (print.observation.circonstancielle != undefined && print.observation.circonstancielle.length > 1) {
		            doc.setFontSize(8);
		            
		            for (i = 0; i < print.observation.circonstancielle.length; i++ ) 
		            	{
		                doc.text(initX + 1,   initY + 8 +  line * 4 , "" + moment(print.observation.circonstancielle[i].timeCode).format('DD-MM-YYYY à HH:mm')) ;
		                doc.text(initX + 40,  initY + 8 +  line * 4 , "" + (print.observation.circonstancielle[i].auteur || ""));
		                doc.text(initX + 80,  initY + 8 +  line * 4 , "" + (print.observation.circonstancielle[i].type   || ""));
                        doc.text(initX + 100,  initY + 8 + line * 4 , "" + (print.observation.circonstancielle[i].plainte || ""));
	                			if (print.observation.circonstancielle[i].type == "signe" ) {
                                    doc.text(initX + 100, initY + 8 + line * 4 , "" + (print.observation.circonstancielle[i].signe || "")); 
                                    var nb_ligne= print.observation.circonstancielle[i].signe.split('\n');
                                    var nbline = nb_ligne.length; 
                                    line  = line + nbline - 1;
                                }
                                if (print.observation.circonstancielle[i].type == "intox" ) {
                                    doc.text(initX + 100, initY + 8 + line * 4 , "" + (print.observation.circonstancielle[i].produit || "") );
                                    line ++
                                    doc.text(initX + 100, initY + 8 + line * 4 , "" + (print.observation.circonstancielle[i].intoxdetails || "")); 
                                    var nb_ligne= print.observation.circonstancielle[i].intoxdetails.split('\n');
                                    var nbline = nb_ligne.length; 
                                    line  = line + nbline - 1;
                                }
	                			if (print.observation.circonstancielle[i].type == "cinetique" ) {
	                				//doc.text(initX + 105,  initY + 8 + line * 4 , "");
	                				doc.text(initX + 100,  initY + 8 + line * 4 , ((print.observation.circonstancielle[i].chute == "oui") ? "Chute de Grande hauteur " : ""));
	                				line ++
                                    //doc.text(initX + 105, initY + 8 + line * 4 , "Accident à grande vitesse ");
	                				doc.text(initX + 100, initY + 8 + line * 4 ,  ((print.observation.circonstancielle[i].vitesse == "oui") ? "Accident à grande vitesse " : ""));
	                				line ++
                                    doc.text(initX + 100, initY + 8 + line * 4 , "Victime d'accident :  "+ (print.observation.circonstancielle[i].avp   || ""));
	                				if (print.observation.circonstancielle[i].cinetique != "" ) {
	                					doc.text(initX + 100, initY + 8 + line * 4 + 4 ,  (print.observation.circonstancielle[i].cinetique  || ""));
    	                				var nb_ligne= print.observation.circonstancielle[i].cinetique.split('\n');
                                        var nbline = nb_ligne.length; 
                                        line  = line + nbline - 1;
                                    }
                                line++
	                			}
                        line ++
		            	}
		        	}

                doc.rect(initX, initY, 200,  line*4 + 10);
                initY = initY +  line*4 + 10
            }



        var exposure = function () {
            header(initX, initY, "BIAN LÉSIONNEL", 201, true);
            //columns(initX, initY + 5, 0, 5);
            doc.setFontSize(6);
            doc.text(initX + 1, initY + 8, "Horaires");
            doc.text(initX + 30, initY + 8, "par qui ");
            doc.text(initX + 70, initY + 8, "type ");
            doc.text(initX + 110, initY + 8, "produit");
           


            if (print.observation.exposure != undefined && print.observation.exposure.length > 1) {
                doc.setFontSize(8);
                var i;
                for (i = 0; i < print.observation.exposure.length; i++) {
                     
                    //columns(initX, initY+i*5+5, 1, 5);
                    doc.text(initX + 1, initY + 13 + i * 5, "" +  moment(print.observation.exposure[i].timeCode).format('DD-MM-YYYY à HH:mm')) ;
                    doc.text(initX + 30, initY + 13 + i * 5, "" + (print.observation.exposure[i].auteur || ""));
                    doc.text(initX + 70, initY + 13 + i * 5, "" + (print.observation.exposure[i].etype || ""));
                    doc.text(initX + 100, initY + 13 + i * 5, "" + (print.observation.exposure[i].elocalisation || ""));
                    doc.text(initX + 125, initY + 13 + i * 5, "" + (print.observation.exposure[i].elateralisation || ""));
                    doc.text(initX + 155, initY + 13 + i * 5, "" + (print.observation.exposure[i].edescription || ""));
                    
                }


            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 + i * 5;
        }

        var cadreBilanuv = function (){
                initX = 5;
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "BILAN D'URGENCE VITALE");
                doc.setFontType("normal");
                doc.setFontSize(9);

                


                if (print.observation.vitale != undefined && print.observation.vitale.length > 1) {
                doc.setFontSize(8);
                var i;
                var line = 0;

                for (i = 0; i < print.observation.vitale.length; i++) {

                    doc.text(initX + 1, initY  + 8  , "" +  moment(print.observation.vitale[i].timeCode).format('DD-MM-YYYY à HH:mm')) ;
                    doc.text(initX + 40, initY + 8  , "" +  (print.observation.vitale[i].auteur)) ;

                    doc.text(initX + 3,  initY + 13 , "Airways ");
                    doc.text(initX + 20, initY + 13 , "Liberté des VAS" + (print.observation.vitale[i].airways.aliberte || "") + " Atteinte du rachis : " + (print.observation.vitale[i].airways.arachis || "") );
                    doc.text(initX + 20, initY + 18 , "Respiration présente : " + (print.observation.vitale[i].airways.arespiration || "") + " Vomissement : " + (print.observation.vitale[i].airways.avomissement || "") );
                    doc.text(initX + 3,  initY + 28 , "Breathing ");
                    doc.text(initX + 20, initY + 28 , "Respiration efficace" + (print.observation.vitale[i].breathing.befficace || "") + " le patient peut parler ? : " + (print.observation.vitale[i].breathing.bparler || "") );
                    doc.text(initX + 20, initY + 33 , "Bruits respiratoires" + (print.observation.vitale[i].breathing.bbruits || "")  );
                    doc.text(initX + 20, initY + 38 , "Signes de détresse" + (print.observation.vitale[i].breathing.bdetresse || "")  );
                    doc.text(initX + 20, initY + 43 , "Qualification de la respiration" + (print.observation.vitale[i].breathing.bqualification || ""));
                    doc.text(initX + 3,  initY + 53 , "Circulation ");
                    doc.text(initX + 20, initY + 53 , "Présence d'un pouls radial" + (print.observation.vitale[i].circulation.cpouls || "") + " TRC < 3 secondes : " + (print.observation.vitale[i].circulation.ctrc || "")  + " Hémorragie : " + (print.observation.vitale[i].circulation.chemorragie || ""));
                    doc.text(initX + 20, initY + 58 , "Signes de détresse" + (print.observation.vitale[i].circulation.cdetresse || "")  );
                    doc.text(initX + 20, initY + 63 , "Qualification de la circulation" + (print.observation.vitale[i].circulation.cqualification || ""));
                    doc.text(initX + 3,  initY + 73 , "Disabilities ");
                    doc.text(initX + 20, initY + 73 , "AVPU " + (print.observation.vitale[i].disabilities.davpu || "")  );
                    doc.text(initX + 20, initY + 78 , "Conscience" + (print.observation.vitale[i].disabilities.dconscience || "") + " Convulsions : " + (print.observation.vitale[i].disabilities.dconvulsions || "")  + " durée convulsions : " + (print.observation.vitale[i].disabilities.dconvulsionsduree || ""));
                    doc.text(initX + 20, initY + 83 , "PCI" + (print.observation.vitale[i].disabilities.dpci || "") + " Durée de la PCI : " + (print.observation.vitale[i].disabilities.dpciduree || "")  + " orientation dans temps et espace : " + (print.observation.vitale[i].disabilities.dorientation || ""));
                    doc.text(initX + 20, initY + 88 , "Pupilles droite" + (print.observation.vitale[i].disabilities.dpupilledroite || "") + " Pupille gauche : " + (print.observation.vitale[i].disabilities.dpupillegauche || "") );
                    doc.text(initX + 20, initY + 93 , "Trouble de la motricité " + (print.observation.vitale[i].disabilities.dmotricite || "") + " Trouble de la sensibilite : " + (print.observation.vitale[i].disabilities.dsensibilite || "") );
                   
                   initY = initY + 100;

                }

            }

        }

        var surveillance = function () {
            header(initX, initY, "EVOLUTIONS DES PARAMÈTRES VITAUX", 201, true);
            //columns(initX, initY + 5, 0, 5);
            doc.setFontSize(6);
            doc.text(initX + 1, initY   + 8, "Horaires");
            doc.text(initX + 20, initY  + 8, "PA d||g");
            doc.text(initX + 55, initY  + 8, "glasgow");
            doc.text(initX + 65, initY  + 8, "FC");
            doc.text(initX + 75, initY  + 8, "TRC");
            doc.text(initX + 85, initY  + 8, "FR | SaO2");
            doc.text(initX + 105, initY + 8, "ENS  EVA");
            doc.text(initX + 120, initY + 8, "EtCO2");
            doc.text(initX + 135, initY + 8, "glycémie");
            doc.text(initX + 150, initY + 8, "température");
            doc.text(initX + 170, initY + 8, "hemocue");


        if (print.observation.surveillance != undefined && print.observation.surveillance.length > 1) {
            doc.setFontSize(8);
            var y = 0;
            var i;
            for (i = 0; i < print.observation.surveillance.length; i++) {
               /**
                var bnote = '';
                var notes;

                bnote = print.surveillance[i].note;
                if (bnote != undefined) {
                    bnote = bnote.replace(/\n/g, ' - ');
                }
                else {
                    bnote = "-";
                }
                notes = doc.setFontSize(8)
                    .splitTextToSize(bnote, 68)
                  **/
                //columns(initX, initY+i*5+5, 1, 5);
                doc.text(initX + 1, initY   + 13 + y * 5, moment(print.observation.surveillance[i].timeCode).format("HH:mm"));
                doc.text(initX + 20, initY  + 13 + y * 5, "" + (print.observation.surveillance[i].pasd || "") + "/" + (print.observation.surveillance[i].padd || "") + " || " + (print.observation.surveillance[i].pasg || "")+ "/" + (print.observation.surveillance[i].padg || ""));
                doc.text(initX + 55, initY  + 13 + y * 5, "" + (print.observation.surveillance[i].glasgow || ""));
                doc.text(initX + 65, initY  + 13 + y * 5, "" + (print.observation.surveillance[i].fc || ""));
                doc.text(initX + 75, initY  + 13 + y * 5, "" + (print.observation.surveillance[i].trc || "") );
                doc.text(initX + 85, initY  + 13 + y * 5, "" + (print.observation.surveillance[i].fr || "") + " | " + (print.observation.surveillance[i].spo2 || ""));
                doc.text(initX + 105, initY + 13 + y * 5, "" + (print.observation.surveillance[i].ens || ""));
                doc.text(initX + 120, initY + 13 + y * 5, "" + (print.observation.surveillance[i].etco2 || ""));
                doc.text(initX + 135, initY + 13 + y * 5, "" + (print.observation.surveillance[i].glycemie || ""));
                doc.text(initX + 150, initY + 13 + y * 5, "" + (print.observation.surveillance[i].temperature || ""));
                doc.text(initX + 170, initY + 13 + y * 5, "" + (print.observation.surveillance[i].hemocue || ""));
                y ++
                // y = y + notes.length;
            }

        }
        doc.setFontSize(8);
        initX = 5;
        initY += 16 + i * 5;
    }





        var cadreSuite = function () {
                initX = 5;
                doc.rect(initX, initY, 140, 25);
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "SUITE DONNÉE");
                doc.setFontType("normal");
                doc.setFontSize(9);

                doc.rect(initX + 3, initY + 5, 3, 3, "F" );
                doc.text(initX + 15, initY + 8, "LSP");
                doc.rect(initX + 33, initY + 5, 3, 3, "");
                doc.text(initX + 40, initY + 8, "Décharge");
                doc.rect(initX + 63, initY + 5, 3, 3,  "F" );
                doc.text(initX + 70, initY + 8, "Transfert");
                doc.rect(initX + 93, initY + 5, 3, 3, "F" );
                doc.text(initX + 150, initY + 8, "Evacuation");
                doc.text(initX + 3, initY + 12, "Destination  : ");
                doc.text(initX + 3, initY + 16, "Service  : " );
                doc.text(initX + 3, initY + 20, "Vecteur  : " );


                initY = initY + 22
        }
    

        var traitement = function () {
	        header(initX, initY, "traitement", 201, true);
	        //columns(initX, initY + 5, 0, 5);
	        doc.setFontSize(6);
	        doc.text(initX + 1, initY + 8, "Horaires");
	        doc.text(initX + 30, initY + 8, "par qui ");
	        doc.text(initX + 70, initY + 8, "type ");
            doc.text(initX + 110, initY + 8, "produit");
	       


	        if (print.action.traitement != undefined && print.action.traitement.length > 1) {
	            doc.setFontSize(8);
	            var i;
	            for (i = 0; i < print.action.traitement.length; i++) {
	                 
	                //columns(initX, initY+i*5+5, 1, 5);
	                doc.text(initX + 1, initY + 13 + i * 5, "" +  moment(print.action.traitement[i].timeCode).format('DD-MM-YYYY à HH:mm')) ;
                    doc.text(initX + 30, initY + 13 + i * 5, "" + (print.action.traitement[i].auteur || ""));
                    doc.text(initX + 70, initY + 13 + i * 5, "" + (print.action.traitement[i].typeTraitement || ""));
                    if (print.action.traitement[i].typeTraitement == "Medicaments" ) {
                        doc.text(initX + 110, initY + 13 + i * 5, "" + (print.action.traitement[i].produit || "") + "  " + (print.action.traitement[i].posologie || "") + " voie :  " + (print.action.traitement[i].voie || ""));
                    }
                    if (print.action.traitement[i].typeTraitement == "Solutes" ) {
                        doc.text(initX + 110, initY + 13 + i * 5, "" + (print.action.traitement[i].solute || "") + "  " + (print.action.traitement[i].quantite || "") + " vitesse : " + (print.action.traitement[i].vitesse || ""));
                    }
	            }


	        }
	        doc.setFontSize(8);
	        initX = 5;
	        initY += 16 + i * 5;
	    }

        var soap = function () {
            header(initX, initY, "EXAMENS CLINIQUES et OBSERVATIONS", 201, true);
          

            if (print.observation.soap != undefined && print.observation.soap.length > 1) {
                doc.setFontSize(8);
                var i;
                var line = 0;
                for (i = 0; i < print.observation.soap.length; i++) {
                     
                    //columns(initX, initY+i*5+5, 1, 5);
                    doc.setFontSize(6);
                    doc.text(initX + 1, initY + 8 + line * 5, "" +  moment(print.observation.soap[i].timeCode).format('DD-MM-YYYY à HH:mm')) ;
                    doc.text(initX + 1, initY + 10 + line * 5, "" + (print.observation.soap[i].auteur || ""));
                    
                    doc.setFontSize(8);
                    if (print.observation.soap[i].hdm != undefined ) {
                            
                        
                        var hdm   = print.observation.soap[i].hdm;
                            hdm   = doc.setFont("Helvetica")
                                        .setFontSize(8)
                                        .splitTextToSize(hdm, 160);

                        doc.text(initX + 40, initY + 8 + line * 5, hdm );

                        var nbline1   = hdm.length; 
                        line          = line + nbline1 - 2 ;
                    }
                    if (print.observation.soap[i].examen != undefined ) {

                        var examen;
                            examen = print.observation.soap[i].examen;
                            examen = doc.splitTextToSize(examen, 160);
                       
                        doc.text(initX + 40, initY + 8 + line * 5, examen );

                        var nbline2   = examen.length; 
                        line          = line + nbline2 - 2;
                    }
                }


            }

            doc.setFontSize(8);
            initX = 5;
            initY += 0 + line * 4;

            //if (initY >> 280)  doc.addPage();

        }


        var geste = function () {
            header(initX, initY, "GESTES EFFECTUÉS", 201, true);
            //columns(initX, initY + 5, 0, 5);
            doc.setFontSize(6);
            doc.text(initX + 1, initY + 8, "Horaires");
            doc.text(initX + 30, initY + 8, "par qui ");
            doc.text(initX + 70, initY + 8, "type ");
            doc.text(initX + 100, initY + 8, "détail");
           


            if (print.action.geste != undefined && print.action.geste.length > 1) {
                doc.setFontSize(8);
                var i;
                var line = 0;
                for (i = 0; i < print.action.geste.length; i++) {
                     
                    //columns(initX, initY+i*5+5, 1, 5);
                    doc.text(initX + 1, initY + 13 + line * 5, "" +  moment(print.action.geste[i].timeCode).format('DD-MM-YYYY à HH:mm')) ;
                    doc.text(initX + 30, initY + 13 + line * 5, "" + (print.action.geste[i].auteur || ""));
                    doc.text(initX + 70, initY + 13 + line * 5, "" + (print.action.geste[i].typeGeste || ""));
                    if (print.action.geste[i].typeGeste == "atelle" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "" + (print.action.geste[i].type || "") + " position : " + (print.action.geste[i].position || "") + "    " + (print.action.geste[i].lateralite || ""));
                    }
                    if (print.action.geste[i].typeGeste == "iot" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, " sonde de " + (print.action.geste[i].tube || "") + " repère " + (print.action.geste[i].repere || "") + " pression ballonet " + (print.action.geste[i].pression || ""));
                        line ++
                        doc.text(initX + 100, initY + 13 + line * 5, " etCO2 à intubation : " + (print.action.geste[i].etco2 || "") + " Cormack :  " + (print.action.geste[i].cormack || ""));
                    }
                    if (print.action.geste[i].typeGeste == "position" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "" + (print.action.geste[i].position || ""));
                    }
                    if (print.action.geste[i].typeGeste == "pansement" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "Localisation " + (print.action.geste[i].localisation || "") + " type : " + (print.action.geste[i].type || "") );
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " commentaire :  " + (print.action.geste[i].commentaire || ""));
                    }
                    if (print.action.geste[i].typeGeste == "rcp" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "Ventilation artificielle : " + (print.action.geste[i].compression || "") + " Compression thoraciques : " + (print.action.geste[i].compression || "") + " Défibrillation :  " + (print.action.geste[i].defibrillateur || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "aspiration" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, " " + (print.action.geste[i].aspiration || "") + " Sécrétions abondantes  " + (print.action.geste[i].abondant || "") + " Sécrétions sales :  " + (print.action.geste[i].aspect || ""));
                    }
                    if (print.action.geste[i].typeGeste == "vvp" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "Type : " + (print.action.geste[i].catheter || "")  +" Position : " + (print.action.geste[i].position || "") + " Latéralité :   " + (print.action.geste[i].lateralite || "") ); 
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "vvc" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, " Position" + (print.action.geste[i].position || "") + " nombre de voies " + (print.action.geste[i].voies || "") + " pose echoguidée :  " + (print.action.geste[i].echo || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "suture" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, " Site " + (print.action.geste[i].site || "") + " Fil  " + (print.action.geste[i].fil || "") + " type de fil :  " + (print.action.geste[i].typefil || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "sng" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "Position " + (print.action.geste[i].position || "") + " Taille " + (print.action.geste[i].taille || "") + " usage :  " + (print.action.geste[i].usage || "") + " vérification :  " + (print.action.geste[i].verification || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "dio" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "Position  : " + (print.action.geste[i].position || "") + " latéralite " + (print.action.geste[i].lateralite || "") + " type :  " + (print.action.geste[i].type || "") + " taille :  " + (print.action.geste[i].taille || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "conditionnement" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, " Couverture " + (print.action.geste[i].couverture || "") + " immobilisation " + (print.action.geste[i].immobilisation || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "bu" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "leucocytes " + (print.action.geste[i].leucocytes || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "nitrites " + (print.action.geste[i].nitrites || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "pH " + (print.action.geste[i].ph || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "proteines " + (print.action.geste[i].proteines || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "glucoses " + (print.action.geste[i].glucoses || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "cetones " + (print.action.geste[i].cetones || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "urobilinogene " + (print.action.geste[i].urobilinogene || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "bilirubine " + (print.action.geste[i].bilirubine || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "densite " + (print.action.geste[i].densite || "")); line ++
                        doc.text(initX + 100, initY + 13 + line * 5, "remarques " + (print.action.geste[i].remarques || "")); line ++
                    }
                    if (print.action.geste[i].typeGeste == "dc" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, "" + (print.action.geste[i].remarques || ""));
                    }
                    if (print.action.geste[i].typeGeste == "su" ) {
                        doc.text(initX + 100, initY + 13 + line * 5, " Type " + (print.action.geste[i].type || "") + " Sonde " + (print.action.geste[i].sonde || "") + " taille :  " + (print.action.geste[i].taille || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " remarques :  " + (print.action.geste[i].remarques || ""));
                    }



                line++
                }


            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 + i * 5;
        }


    function footer() {
                doc.setFontSize(7);
                doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");

     };


        var pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        var page_1 = function () {
            
            cadreAntecedent();
            //cadreBilanuv();
            cadreCirconstances();
            exposure();
            cadreSuite();
            //traitement();
            footer();
            initY = 3;
            initX = 5;
        }

        var page_2 = function () {
            
            cadreBilanuv();
            //traitement();
            footer();
            initY = 3;
            initX = 5;
        }

        var page_3 = function () {
            //cadreCirconstances();
            //cadreAntecedent();
            //cadreBilanuv();
            //cadreSuite();
            surveillance();
            footer();
            initY = 3;
            initX = 5;
        }

        var page_4 = function () {
            //cadreCirconstances();
            //cadreAntecedent();
            //cadreBilanuv();
            //cadreSuite();
            traitement();
            footer();
            initY = 3;
            initX = 5;
        }

        var page_5 = function () {
            //cadreCirconstances();
            //cadreAntecedent();
            //cadreBilanuv();
            //cadreSuite();
            geste();
            footer();
            initY = 3;
            initX = 5;
        }

        var page_6 = function () {
            //cadreCirconstances();
            //cadreAntecedent();
            //cadreBilanuv();
            //cadreSuite();
            soap();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

        doc.addPage();

        pdf_header();
        page_2();

        doc.addPage();

        pdf_header();
        page_3();
        
        doc.addPage();

        pdf_header();
        page_4();

        doc.addPage();

        pdf_header();
        page_5();

        doc.addPage();

        pdf_header();
        page_6();


       


        doc.save('Test.pdf');
        doc.autoPrint();

    }


    public pdfConseilTC(data) {
        console.log(data);
        var doc = new jsPDF();
        

        var initY = 5;
        var initX = 5;
        doc.setFontSize(5);
        doc.setFontType("normal");

        // metadata
        doc.setProperties({
            title: 'Conseil aux traumatisés Cranien sur GERO',
            subject: 'Fiche TC secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        var print = data; 

            var header = function (posX, posY, text, size, tgle) {


                var dec = 0;
                if (size == undefined)
                    size = 285
                if (tgle == undefined) {
                    tgle = false;
                }
                doc.setFillColor(200, 200, 200);
                doc.rect(posX, posY, size, 5, 'FD');
                doc.setFillColor(255, 255, 255);

            }

            var bloc2 = function () {
               
            }
            var bloc1 = function () {
                initY = 3;
                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABvCAYAAABWxv0DAAAgAElEQVR4nOy9ebQkR3Xn/7kRmVX13uvXu1pqqSWBQICEkZEAsRizGTAYG2xsbJ+fNxjsGQYv/GyPzwy2h+Ofx9gGL3gYYx8vgPE2gLFZjFlssS8akAQYkFiEJLQgtLS6X2/vVVVm3Pv7IzKrMrOylve6tcAQfV5XVeQ3I27ciLhx741NVNUARAQzQ0QApn6f9XuruGaY9/xU4P5vjWvWK9D6+1u4b3xc+aytLVTf/0bCicUwKmyz4GXcrNBkXDWNWbgmQc24tgKdCtyscixS7m9k3LRBqIz7Fu6bA9cW7glF4O7GiRWlnSWlq89LzLRMZo0KiwiZNoJPNW7ae/Pw3yy4Jv5buP87cPekUnB34ZJqRFPYVEPbqC0iM7WmatptuGnCxYr/R3ECVsRi8/NoY0ZbOZtl+2bHTdPM2gaZb+G+cXAgxF5TfoIw3eqZprl9I+CS8kuzoVcTKOM2o9U0GbzI+6O8iv8NQ7TyjkzSt0h6bbhFyvbNiit/N0Nb3X0Ld9/Eqepo8B4N5lT6MDqRblvfqGLa2tF9DZe0jcrVUJXyzbhqYtM0sGm4qQJMQEwQBcwRfFalBoevpTeNxmZh5+HmlfmbBTePR9+K+8aIEwRzEADFcDT6LQ5nRKEG4OYPbLMEyH0FN+F0r77cFteWSFMoTRsZ2lTdZjDNQTyKEDC8hXEaCM75MbZFQLaFWfTMovmbDVd9Po1H035/C3ffwqkZUggkg5H1EcEgZiACbhx9XyzHZnEjp3sJKgu2GSEwT7AVxh3ODFAQP5b85Kg4jpjn5mNDrj2m3LWRc3A942g/MLRx/ql37F9J2N11nL2ty/23O87sCd7ASZG0M7AAlqDOorYmECTgrK6dtdHe5MM03Hg0UK44JuhgLFi/WcL2Jc+FK4aJw1AcHrEh15wQDg48qd47ZTYUMUPMWO52OdCD7T2HR8EycClGQM1HTQMQAmjR9pyrp7eAtnp347T4wwwPSDDUg4rhLfqnDIfrC/mNR7EbDhO+fpj8ruP0Dx0h7VeEmTNkR5d0xzbcvlX8OTtJz98Ne3uAIhgmYOJwQVCnKIJHwARxk7TeV0KrwBo9nCGspuEmhJWBqiG2QZAU9Q6nxu19z0fuOMYHbuvz77cFrlsbsO5WMF1HMYIkqCUggbFtLoUAyvGSkZCwp7PMo/Yqj9+f8ORzVnngkmPJO5SAzz2S5KhkOF1B3GSZttoISx/CUJVnv+cm3n8omZvGN1r4zw/s8j8fuxtTAW84EnILvPTKI/z5Z46w3r2Xyux81C5USAzoGPvSwLdt9zx8/w6eckbKY3akLDklc0YiQuYUR4KYkLr2ER4W00TvDhyFmyT6zRUVCCb4NSO//CY2Pnwd2RU3Eq6+nexoxkBytg0Fr8IQwReqlBUeLQd4AhuJQuLoCegZK/jH3Y/upeex/J0Pwi5YJaSKA1KFoQjeOdKGFVMtx7RwT+FqyxpmmWqLZjipXYESyBS+ui584KY+b7vpOB8+eIRsuA2HoknALMomdd34luWIxNG9mpiIx/KAx2MSUBcQ5yAozgvnb3c858yU591/OxfsMhKXgksQlAR3ygRWKLSL4yo8/T13ceWd/blpfKOFFz1kiVc/Zld0lLhiVDb4xSuP8idf6CP3loYlhc5uIOJIrIOimGSo5SRhmTP2rPP9Zzp+8vwzuHCb0JMkOqK9w1c65H0lmCpBFVWHHBqQ/du19P/hs+iHroNjSmaO3IGYsmJGXxRvDkVQ5xBVohbGyDz0ZiBRNzZLMBfomJFTdLb7b2fbcx5O5wcuQC8+g7TncZbjkk5rv7gvxC2sYU0zm0rcNN/JRpZzxcGMv7punXfecoKDuUHuccGhaUGEWjE+aKGwljyPpmQcTovegsaHDpyCs4RginiPDhVJPGYZPe959BmOF563jWee0WV1OaHjZGLE26z/q8RlpnjL6Kvjye85xBUHB63vfyOHn37wEq957F4kB8jBe5IQ+MUrDvG/vngclfTeIUwV8Q5Fo59GM8DjSBFJENtAQw+P0HOBp56d8vMXrHDp6V1S5+hUTMJmO54W7m5cPgwMrjnI8G+uYviWz5Pfchw1jyMg5IVhC+Y8Q4wUKbR8R4KPvIiI6JAnCnZxYBpIXAJqDCXgRCEIKgl4j/iMbY86gP7HR7H6PReQ7lmq0Tivn9yTuKQa0RQ80/xUTWZrAASCKT4IIVHykPPxg8ofX3OUd38tp58bRCs5mokeQMEUA0JBtxQjpzGKiP+VUkwEVJDMxTydxpFfM0jLwiYMNOfDX1c+ctsRvn1Vef63bedHDiyzd8kDgZwUbwFvZa7UytQm6WsMNMMkNhVnseF8swUxD2bRdytxHspwBBKC9KJj996gSxIIRa2JIJaARH8PlgFJoREqxyTwtpuU99864AcfkPJLD13h/O0reAwVITGpO6y5+8ybHMERcBb9R2hgaIZ89k76r76co2//PP5IjphHEJaADMglJZjhECQIPTzR6eEwpFjCUNBS8ghwJrgAhieEaCqm5sklGpBdA8kDIU/Y+OjX0I9+lfxhB9j24kvxz3soya4OIFG5cGCi+MLKKcvV1kfayn+qcElVOJWAZkVM+ywzyVygE6KzLvicLx4W/ujq4/zDzRscHySxIUnpOCy5Wk5vOEZSDLBSKDWmaUeCqxhIzOtYqEF0ppY4UcwEJxkqKZ8+Klzz0UO87owhv3b+Es84r8OKKSZCXkpuNcS7GuPGjvXJUdObEfAUdmplLUyF4LIYIw2RyXLdk7iSh5TvSeOz8rP0F44ii8fOilqMA8Vm0jtVOJNQtBEKOqq4MoSi2LEMR0LKG67L+cjXjvHyRwrff0BwSQ8V8A0vc5tJ0ha/WZzPYegDCYZagt64RvjjT3D07/4dd3BAoowWKAjCkGjmJVU7D0MJRGNcCs7UhVXJKyMKyRJXxvmiPYwN+rgwQugSPncHx37+nfD6K1h+6RNZevoDGfYSPODygDk/7moLuJBONW5TakHT3CszSdWTy5BBH/70i8d51vsO8oYvH+NE1gFbZ7IxjRKsJlRphER8OUO4RZzqEpILPhi59Ljm9sBPfHyN539ojavXhmgY4MMGJvkEfTW7ecakQo0OqfxVBUlJXylA7i1cZcZ1DKuMyWXHsDovpoatpHeP46IAT6xPEOX6DeMFl9/Bb119jI1guHvQDReckVjKsO/I/u7zHHn237P26ivhYFZoq1L8qxWsGJjjX/l9Ho4RjhGuFG5j8daGA1NlcNVBjvzEP3HwF96Nu+44CuQi5PfyFOJE7mVHrfp6Jmf+rPbMZMCNxz0v/Pid/MoVx7lpY4D5FLUB6pbKtyZzFxlHj/Koak0nhxMxxDnMO0yG5MkGIfO885Y+z3nfXfzxtQM2SHDUW221/FWezB0lzMb0lbhqSyi/3Fs4qeBqdBdpyBxcM5xsevcEDitm3FLIEwIZWX+Z3/1Mzi9/4jYO6WQyE8VcYFJmEZwjwMF1jv/Kuzny4neiX7yTjg4I5CjjGdexUCnSBdpXs0/HVT+rOGmIrUmcoJKS2IDe8T7215/ijh94HYN/vgZnQnO6YpqWOY83W8VNzEu3LtZqJoIRdIhZCsH40J1DfvmTR/n8mmBOICQEMcCB5fWRf9SgSu2gJa7IpWYmbAFnxNnH+I6H4Mh9AE24YcP49SvXuer2nN959DIHenlct1M0YBWHk0mf3XShVRUYJU0VgdJQfu4dnNTkPJXoqJHYbFzbiyeT3j2BKxpHXIJnoAlDFzXq134lJw8HedVjdrHNG0Yal2m1dJotLX0JWTFDJygZg2sOc/SF/0Dy6cN0gpLhol/LSj9UpHGsO40rWCYKPBvX/GSElFpcKy4oRkKG4TKBLxzh8PPfzuAX72T5l76TXs8TLRgDLySVZSLzJq9OFteq31U1qKaPyyzavi5PEHL++sYT/NQHj/LZo8v40InTq6MRsDLajUb5xu+yU1Xjyu8iJ4+r5eeK39FJOWSdN914gmdddoSPH+0TLCNzgopiZBPCqmRg++ggk/nWtJ7y+b2IiwwaC7WyjmpmVQW3UNhEevcKriyKgRVtE0CMYJ6//eoGL//cnVF4+EHUMhas83k4xSESMFHsX7/Kie97LatXHiJTo18IzdIPG0tQ13pqeY20IlkI1/w3YsNURJvWFXeb9J2SrBn9l7+f9Re/FVs3TIZ4Aa91PsyzyE4W59ochW0zglV/jlNl4DL+8Op1XnL5MW7NQPQwYn1MPHVBMSdIo3HdIzgBA6+rBFK+cEj50fes856bAy4bkAePryi/bb6sqeku9PvewhXfa4JfpiSzYP1tJr37IC7kHV5zTYc3Xn8E0TizVoa2TtUWpuHUCSEkZH/zWe76kTfgblZOIDgxOhpFlIz+lWbCWAMae5a2gqv/Y/SGVN6ej/MIy7mh3nBZl+zvvsDB7/8Lwp2B3BT1k7KjyZe2/rNVnKv+qH62VUr5eTwoL7+6z8s+fYiNEPDqMecYdrqYFVOs5ag2+mMcZ5X4Znaj/Atc5Wc1qfH7BWZilIXJfMfRAqhkgBAkcOtA+Q8fXuN1Nw5wMkBlXGYtfXUNxjYIr3+vanrVMtTotmLPV0ty94U4AczG7IWJYo4eGJPp1fherfcWx9Gm6LPGH0y2KVuIPic5G1mf//6ZPv9+WIk7K6pKarsAa3YwkYrmU+BcljP8yys48nPvJAyWUVFggEcZuoRyvnVMnjXYO9Z0xqUrxYpS1Zao/F+NizOAYw3LKunRkleT2QEICGnIwIaYOeQDt3HHD78evfUEASuacsGbhr+3KYSmrd9cFOfmaQ9SOLL7GjA1TmTKH1yzxh99+jZCWAUTgsvBUrAhcSNSOYpJ5Q9GuzU90Rg1xUseV5NY3A8opGAuOsyTHJGcOPTFZBKBREHM4y1F8DgPgovrhkZCwhr51kdWEyEzI677dWAZazn8yuWH+bOvnEAHGUEDQXMGmsVqbmFmhVEjIVTrGVWBJBEnKCIG4nDE3W5yH/wzyYAcxMV1blitaGV56qHCm7gUHQQchneCExfX8pwUbeDM8Go4M6TYciMIoq6W7zz61ARNc25eT/mjzx7ixDB2vMyM6kL+tqU9Tc3KTFHNCSg6yDj2uqs48mvvwZ1wpPkgLnglxdThtNRmWikrno2fqjNElAQhdxIHVHTkb5Ny6ZBELSRBcFBscwsoSgrRrTyRr9Xyqoq9yFWHFp1WrE8/gc5HbufWF/4N4aajZKaEYOTDuHy1bVBvG+y3gpt5HlY1gR5GZoHXXneMP/xsnxN+JS4JKNYujTvmFFOicISXHd+yHHE+CgznwBKwAYk7xvZOh92dDivSZSm1QgY4NrKcowiH1jOOD63YQ61IMBIZEgyMzqi6xzUjZT00BNoYpy6mdVy38eufvJNukvCCc7ajyYCeerDx+Bcba0v5qvmWQWQi37jlKMGFIeqXEYaILTBddQ8HpykmHq8Bcy37BktNpjoRUi7ELOu7EOCeDoQNgu8BDmG4ZbpMfBzpBUyUJGRgcRFl7uKq9/HAMZu+4FycaNGcd9465H13bPB9Z3bj3LFPokCsuEamzRjHfuIwM3I18rd8kfDSy5AjngwjrWhAFYOHauMof9GCS9UQcQwEnPm48n3Fw86UdHsKHvKVDuSBZCMnHyisZSRHcySP29wyAmKGKxacVnW6MbfGCyeoPBkvhpBi43nKtg/eyZFf/md2veYHkb2C+MmdD22rDmYtGp2HS6ZJsppK5gLDkPCOW47xm58+QZZ1SSxHnBZbAqQxkrXo3zL+YkFx4kCVhA77UuHivTmPOH2Vx+zby+k9z76eZ7szlrrlLAv0hznHhsItgyGfP6Jc9fUhl9+e85VjG5xgqSAhNPJroaHmlB6HXqb0fcZ62MV/u+Iu9qUJzzprieAEL+2jaj39qmY1I1917E2NH7twlV0OcDunifl7NVyyO3Z8cw6xancqQnUwKMs+0ialhlNyfurCFe63lJDhSG3blunKfYaK59jQcfPhY3zuRM7X1jI2dAmnhrqyfhegTwDxOCccDUv8xReP8ZQzeiwl9QPwmkKq7bdTJTPIP3ILG//tX8iOxEEwkbjlLGZXFQRjguqiaxI3dIL1hOULT8ddej/kiftZOns37rQl/I4lQuLwyx0sV7SfY8MMO3gC+9pxBl+8ncEHbiD9zO0MbzkWhZ1ZJZ/JZQ/jIHWceLwa/biEnuRt19Lf/a+s/MGzkOXJbrfICvdN4azS+2qOdgOVeCRMTsZn1zJ++L2HuaHvwWVRDlshe0fbGypMrmo3xXMxRTRgkpJIziP3dfnR+6/y1ANdzlsxOiIEKVT+4vW4BaYcDRhpciaGmnJoQ/nUEeN/X3uCf73lOHduKOq7YGGUvyDRt9bUAMtRWAAUcYaooQ66wx5nrWb8/ZN3cfFpPVJ8fUTVnCCeEAJPes9B/s/BvJ7eSKNsY7pw3jb452eexoOXAyKddmy9TU8Pi+I2GXINxS61TjwayBmo8YufXOPVX9oo8i3L28y/EmFGYo5/feZ2nrS3h7oE52zr9JohFmfYcoQ71jP+z6Ehf/Wlo1z29RP0dRuQM/batNhBRYSYYU6ilm6O5QTe+6zdXLTb0SGZvVHaIMeKY1mie2Hw5cOsP+/v6F9zBDHFm5IJLFvCoHipyHmku4AUm7fjEoekcFSYOBDFnbvK0g9/OyvPvhB56G7cqgPzhdlbbs8ZT/mXVWViFOojeQ7csk744M0cectnyD94Pcl6KbSs2OZjODHqC4frGpbi6ACBIbkTOpoQloXl33oGvZ+7hK6PdJWlKrfxTFZh3Uc1varruCnng0QzTAJkibKWO1764du4YbiEJ5AHHzlC2UdbpHI5gJmRGIgqufdoJ+GRu5SXPmwPTzk9ZVu3Q1LR6tKmuj1OqpA3UswWRH/GaT3lGb3A4/bt4oajq7z+y8f56+uPc3TgMAEnhuWhYGJprpWtt5K4CRYKoahC7vvcMAj83OVrvOW79nHOkkTTdSpnG8SOmMPYLKwo3rnAkgkm3Th62bjiRgMHdXOkWnFbwU2QPMenkBblFYsLcLW6LKCskWrl1IS0VMwyIYjiJMESwWn0340Vz03SV6m6FNi9LDy3m/D0M/Zx2S0b/PoVd/KFjRUSjqN0os49hT6L6h/mhAzjmMJ7b17nEXu2j47nnkofcfWUV6MvgfRIxuH//m70msMkGkWIkuDNGG+NLzWa0m8U000kntlmdOgnRhoGyNmrrP7nJ9D9kYfhD6zikgo9bnq9jfreaOiHTgrcvwP334n/oQfBlbdz+FUfhPdej+ZCpsJ2POuujwtVsVDXDD3llp6URCFzSrphrP3ee9l30WmEJ5+LyzQWSBQmlpoWqS64imDuOiwrGpkJ4IYMzXjVZ47w/rVOZLMF6iuNqw1zMpgYuTOypMvp/hh/eInjHd+9j+fsX2ZHksZD1Ub5MtHp2ukrKgkQiU7WVXIuWk34g0u28f6nncb3HkhI1BNCB01TrFwRGl8aNfqao7zaOSTBZzu46rjnZVfeipaddxqj56RXy3dOaJod0/LdLG6WOdt0BywynV+Aqxk1M55b3kXznYXrmiN4Ty9Vnn0/zz8+8yy+c++g8GfWPOfT6SvSU4GP3zpgPZv0VU042SGaoOTkFhi8/nPo266LgtkJWvEPjd+p+4hKzSUA4gQlBxuw8oJHsu+yF5H+l0fC2UvT+v1U+qatcQJIVlJ4whlsf9Pz2PnXz8M9YAcrLueoz2rKR3NtVnUmsfTIOTXAs3Kbsvarl5HfvoEmSk4ghKTVHz6Pvlm4CYEllRHIcFxxh/InX1grNvoK6qOaWvcBTG+VzhxOlEfv7vPOZz6An3vwLvaRYImhPuDLZAqHW9Xx1haqGCsO0TNLEXGIKM7DQ3b3+PvH7+EvHrPM+f4oqCMxV9FyqGs/ZSVVRmB1gvNHeeLuLr904f5R3lM78pz0JvKdE6qzJbPy3QyuLEP1dxma8TNnRMepjss3EtQTGVPvsvWwaL6zcAJ4PD4kuNDjwcvK65+8j4duy8fWzTz6ynI448q7hKN5iAeUzs1XUQfLnz3O8VdehuBJQ04otJsyx6rTujSxqrNz3hxBhO4ZKXve/FMsveY55OftIMETFav2DjyNvmk+obKtpKQk3Q780PnseP8LkR+9AGcZnZBQ1aqaa7NolCMhIZM4G8mVNzN41cfI8vLon8l8F6VvGs6VoKpEMyCQsRE8v/apO1jPOiSWgxYzdnW7Z6I9igQScsQZqXh+8twl3vZdZ/NtexyJEyRRvHggjcfvzpC21biJynKlv0swScg9QEIv5PS6nh8/v8fffv/9eNzpIVreopRnMToLCBZXTIRYGQlJLI/k7JUBL73kNN7+tFUeukdqkr1doDZH72bcdE1UaN+72Fappwo3bWBYFFdJvKWM1WeLq/5bpc9cND/EB7IUcuc5Z0n4rUftZcmqM1cz6CvjTTgyDNw8gOp5Bq30IWSABuG233wP3D5ENGPowWtzM02lPoh1bgIiUfDlYrhLzmD7ZS8mffZ5+FSjq8QlIEmtAy/Cl3ERJwWcihbjqid1HfxZS+x47fNYfcX3kq/EOUQnRu7iFRe19BrpZxT6ixlOhcGfX4G/6k46KpUzuqZbJtME8DRcq1NGUJIg/O8bj/KZO/vkJHGXdrmeqSJpixfGxTFI8x65M3rB8TMXCr/7+D3sS7LiEDKPE48IeIn7tpqaQetM5TSck8JXBSkJzjkkTePZQR4esc3xj0/Yxw8+YEgiPcTW48l/ksRlK04wH30CZvHY5UftEP7umfv5rxcusYrCQidUNjSotr4qDb41U2ioxc24k8VNW7rSfGcRHFDRJhk3i5oZXJR3ohG2d6yt0hfbkEckIbW4bkhUeNQZPS7YXclyFn0VTVGccssxifcDzMhXUZLg0Pd+BXnv1XEmUOPhlNWStpV+ZGBJiiUGTzuH0/72J5CHbCcNHrEULzLS0UYm5IJ8KePaBIDHgxOSot8klmBdpfeSS1n9/e+FfSmGw1uCJJOCthoccd1XKL7pkcBdr/hnbCNu25kVptE3CzfibF0FE9YG8JprjjNEqDlaazwpemXV1BLIvNLTZV70UM9vPfw09gqoT6OLUqb7Ktqk7cnggqU4y9mxJPzppQd48XmCd9tx1kckHruvopjkGIHVToefvTDhrc/Yz9N2JnRNCKNTNWetch8RNv5coCKaoekzqZZ3QqvYAq75bBYdi+BqZpZV4iaeLWYGnwr6BCM3EDy7k8Ale0q65tBXmlSm5JJw8Oj6RB1OaoBGUDj62+/H9R3lBuZoZBYDSWWipfxd+rFS58lN6Tzxfuz7s+ch5y2RGljcp39SfNnUQAYkwWE+0P3ph7Hyqu/D7/SYeSRUyzG537Bapvhd0XffwPGPfXW8vGRKvovSV/2sbc2pdvy/vbXPl9YEo4OX8m5AqwwXlbFD6r9Tp/zQuYGXXbSXbT7ERZnO8OSUToVFTJuTxanlZDi6QdjWyfkfj97Of3xAB5/3ig0LAihJCFy0Q3jt47fxiot3cHrXEzyoN5Bi2pqxhtc+ysl4pJbC9GjCpphIxjjNpnCpapbVvLeKq5MzWY5FcWWRaxpWtXgi43YxkeTW810E15FA8IIzz4EdK4vRNyqHYS5hPShi+cx8czOy911LdsUdDF2n9qyqiVgl47GzXcg0I33MXpb+8rm4c5YRc6i5eHit5ifFl6qvdxZWJFoo6jw9S0hESH7oQjqv/G78ckZe3gNa+dcsXzUegSTv0f+fH0GyyWObtkJfFedGmVvANBBQDg2NN3/5EAN1qATUisoYezCr7MNpSkJOEjxO4PGnCb976WlsSz3iPAlSuOyTtv46YQJOk76bxXWcI3UJ4gXvElZ9wssf3eMH7tct9gEp28zzMw/expufuofnnLtCJ01wCJ4EL1JMCkSB1RQC9Uwbmkb5WtN0lpbGxqS52/a9zSzeLK5pUk/zH07F1apexr9HK8jHplV82aiv66H+/FTTB2jwGANUHKE8e3sefaPXPS4oQw/FmTSj/EZ/weKlJxls/MXluEzoaDwHtBRHZXJVZ3XujERyzOUEH3Dn7WLHH30/nL0cafAO5+OCncSNjyHeCl+m9Y22545CXjsH4kg99H7sIvxLvgPv4jVguQ94seLYqFjGaknLWIeAJWQfuIHBVV9HQx63OWnc7rQV+qqfrszUIncRzbjizj6fvVOjpDewivScGB0NcCEexeoT9neG/OGlezkz1VIvIe4vdIWfYb5KO0sr2BwuKTpvzN+cspL0eOWjtvO4XcoDl3L+7PG7eOWle7j/8hJeov/DSXm7Tkm3FIrTLPOoLa4YWat1PMM6avPdtZVvq7g2M3LaCD4VV4uuaNzlUpeqBtN0F9QT3Fy+C+IM4g4MSyFs8KXDw8Xoq47FouxK0/a8iwnyDWeEqw/S/9h1hQ5io3PvocqmsfDqmHHCe5Y1IVnusP3/eyqdi0+nG8Ak3mle+nSr/NkKX6a5Dab1nVG+CIgn6Xp2veQJ2HcfYEUTvCXkUtzEUynheAFsDMGU3ALdIRx9/ZVk4vBq8RBmlS3R1+7DAoIZuTrect1hjrEEBFxu5SHQjaooep8I6oZ0QhfjCC97xF4u2BXIU8gdYGPfzywHri0gfU8WJ0FQ63NgxfOGJ5zFPz79DH743C7LGteNVN9tS2sW/YxmoKwyejc6edkgpoRF890qrho3i3eL40o7q/guVU2lipmvYZ1S+iTHieO2TLjycFmv8+grNUjDW59ze0tog2wDCIqSozgGb7+Gpbvi8tF4E5qrCar4/3h5aDBFgpClQvKjF7H0gw8h80YuguipqI/JwWkRDaaKEwwJsCEgexJ2/MYzOHZmB1EhnuA1Pmgw/l+qM/G3L1bne0vgH64hWcsJFlfbC3bS9FXvOyITx8ENz0fuMlKLI+wT+I8AACAASURBVFNKSrznqSm0xg3R5Y5hCk84bTs/dv8OqfXwoUOqgSD1qc1pjK7aqbM0p5PB5RKNPVXh3BXlITssOjhTR1fr7zbTmWZvj4ONBVKJa5a1FGhTwqL5bhU363OzuMob9fLVTN4Z5d1ivovgXOjgTXnrjRtcf7Ry/dpM+kajCt4pB7YJecu0vBVXjHc2jBPv+jwnJCFIvFXIUT22RUb/lzqWI8HhyM4UVn71CdBJcepxzmGFUrDV+thsvc3CBYZ0gpCLJ3n4fpZ+9lLEZ8V+0tJMHmtY1d+5BRxKH3DHPMff83mCM7xYvFfxJOlzZVZBHKn1+dyxY1x3zJG5OBoN4hkVI4JiHVvNzHEIHbfBb337DpIkQZzEI1+cx1fWkJQZz+74d19InCeVlCQRvO/gpUviPIkkOD+5dGGaIGwNbdpUE9sW15rUYvluNm5Rvi9cP03YhAZZlHciudmNdLP05aaEPCfTnKFBrhmXHRzwis8ORprLovQ5CZy2vMKZywEL9dMHhPJ6uoRwzW1w7Tq+3K2kVhwDUHFAj94rTxZVXBLY9l+eQXL2Kk6EpDhi2NO6wuik+LIljECSdEm9o+OENIHVFz4SvWAv6lI82QjarnM7Sj+hWkZ41/V0KA86OPkyuDLjoQJ0+NCtG9GpWPNbVRIpf1cEa/A9vmtfl0v2F5tkBShH/RkmUDPMNrlOHhflRV0jGa3eXaCyF833VIe7my9bxlnjc9r3TbJt0/RZIG5vNTTPeM+dGS++7DC39uNaqYXpMwiScPHuDttST+oyqiGKnLiqO7viZsKReCOUjF6vJ2iNzHKElf09lv+fi0mK43FG49wpbH/zXCWzcVXLhHjO2J4Vdv3kpfTIyMSNyic1DbU0D8eaJRjr778GW4fcOUJ5Rd9J0DcS66lAPzg+fihgXphey9Xfhd2qfX7mgp0kZDUzqDazMofZTQfcLBv9voCbHzbXS++Jcoz8FKcIV3NkW71NxOcV3GQmp4w+MYfPN7hlqPzOp4/x/H87xPVDR6oniCdhLEBfUQ5Hzvee1aUjtV2IozcdCqroB67Hjy7PnTSPys5bpd6Jx37qYsLO4kSDU1wfE3yZom1vBlfelJb+wIPRM1eIM3GlOCr/6tuMGJVbWL5L0WtuI6AjYXMy9I20NCfGwfWc646VanNDdR6RWAwLZsWHcN4qPG5vhxxIW46/bRI2q/PHI5YFsRDHrpbjKWpVJZPHvZ4S3Gj0jsd+VMeNxcJ0rAGJaqFcF3O0d1c5GjSZxYaVoyQWEDc2e8p6mdeQxi9UzamRqjBqJlWckyE5RnkQolRm1KKwkHaaq3FW8ikeoDg040g/58snBrz7xg3edMNRblgXkHj6bfDb4m5mCTPoKxzeLh6TtHdFeMqBBKPYM0u1vQqJKsMTgf41dxAKkdQ8bC8mXx30iw6eGMs/cSnecoZ06M0x6efVxzxcWz/bbFzczGbo/VdIH3cO+Zu+QOMK41FpqzplyZl+JvirbmT5kafjGsXYCn0Vs9Jxx2DAwf46hNW4fWWi5RXfzeEkFCQlfOd+2JOAc57q2pVpmZaNv42woHk8jlUz3n/HgIRua3p3d1ALBBKEIcvO89j9nsS687WssvM1f1fjPRzswws+tEYvCZFn95S16Ye43LjwtGV+4+Jt7JyyonrqoFK3HirCpvq88HFW/HpOe/zqJ46zJznIRifF58nWy+xyRHMGJNwxcNy0oWwMs4qIcCA+HoczGmTb6RNzeIunkIgkPPd+Xc5acuTO8OqgcsmCUFwm+rVjcHCjMmNGRUjFX+XvKgfTi0/Dn7eKw4863qzBe6H6mIGbJegWxcXz6OLdnUvfdz5H//FayLOaYCpLLBMcAEzIPndXcX4doxNct0rfSGAJxnXHIWTJaGQps25qC6KKeYlL77XPU/bvxMsQQofgfDw/aUbHnjVTqJKQhsC67/Dj/3YHh6edq3E3B3M5ab7MMAlcut346LP3F/sN55iGUukZVnNSjDHBccwcn7h9A01CXDO0Ke1ta0FMSE3JXIfg+iS2o/Z8XiOOD5q/m+WtgsZtR2XAlWsZZilpJgzTrR+RjBhJMJwOscSRmcYLc2tmX5Pv7fQleU6WGmIp+7t9/sMDT6NroObB5YAfT60XnVBvO0p+dECcqncjbaruw6l25Bh6T3lAPAPRxZtzoF17KsNC9bEJXBW7MI5ofRGgc+l5sOqQw+XQIKPyNQVXyQNDGX7x69F8lpOnL4kJx/y/cmIAtgQ+FCPRNIkeD+/HHNtcziV7luIBiFqYio2D7iad3+MtJJOMNlSFJa/kiWFhdgHvtuAcSE6SJwQ/ZPK06inBIPpEmCqDxA1RVSxJEUvjIYNtN8mc6uCEoQRc5lDxiNXraSEfXZs7s3xNpj2AQIdEE1weCGmGtPm1FgxmRi4peAcSz3Ays3i2mhv7WBahLysO7kxV+ekHbePh2yWe/KkQfBzPm7Pc/pYN3EAxsdoGh6rjoKpxlCR0HncOAw9dKw3H2eFkZwfbBtc27GycoWbRNXPuKuGsZfzh4ahMbYKqOdmQ3HAElzsak65boi+JScbI2zes2KUwpVWWPgQRcOACnL2jy+7UEfAkYpSLw5pSsk2ralNlPYoljmAOlQ6m+cR790hQI7gMl0PAEySuQJnfiBomSCMawOUdRDwhi5vB9Z4QVsTG1TElSwbgFJFdTD0ZbnoilVD10UyoXjWcE8jZiL3VXFwf0LZlZ5Hgh8AwnmgQIBei1Jk4VaONvuhpjb8D+AQJcMkex3960A6cF3IcaXUpTyUVQ/C3HouXpFpe6bjtY1TclGYEn9O56Cz6xbS/YqOz4O7JsOjk0VirjMekB4272VYu2k//84ehRVCVYWyaF4gTGfmxHLfbM8mhzdGXlDkYwqETirp87HOpkUBFvXaIDBFJ2LOSspIKaeE0ndb8m36rtt8x5RRzWRz9zYjbeqYWby4DpuImHMNN31M86Trzhgt+dJ3STN9O2XHa0qtYJsHHyfEyPWalN42+LeAMI/NgmiAhzD7yeZTcrAbUIphb6Su9Pcn4eCWBiVNAFq3K4owrk7hKvPXF0oc28SiF0MHJECMg5ti7Evjtx+1j71JxAJLE0alt3ZACevA48YQ1GV0uUeoadROp0LscpDt6sLfHsvhijKinvllBskg4Nbh4NZslcWsN5+1AoFjbP+Z926boMgyygN11HLd750nTFxeOFuZ930skKpQTlu3BRBGNSu3urtBNFhulF1H/NhcWfb8prE7Ow73l9ViLvnd34uawbJr5PgU9J7H7Js7JEE0C1ltiW8f4/Udu4wk7hVQKv8u0JQOFDBocOxH9uDT1yPKzHHxjvzINpGfsRLyrp1cJ0/i8Vdy0uLYwCyeUVpegDty+HTVrI/bhqj41fg/AnOCDkh8/QbM+tkKfM4sG4ECVwxsnikP2Sj/AFMHlCn+VeFbI8VMIaS4CW2Qt1vSHo/+mPNsMrqIFjaKkBTdJ30yH5dz0Fs33bsLV4qaHZr1NL2813baE7T6IC8AQfIczsqO84tLt/Og521GfEKQwcKb6hIoU1vstlkTsKzL6Hk2oIIqgDLclE5rvonzeCg7aFYKtxBUX+eEVuttXyYuV+WKTd3i3eUI65kmM4vark6NldPOzUdwKYzS2RFdsmVEJDF8c81mOM20LRJuLwKrO9ukCrBFXYmT03yRONomTIr7VdLJJri8U5qS3aL53Jw6mlq1aH00nc7OYrWGqttx44Z7GlSeQSgByxDwqXfaGjJc/9nRe+MAOPk1IxCEyaQTW2mlhYXorttLIpOvcSiCQmRZrlmJfaaY3i88ni2srR9v3ubjCPeQKKyxd6qGF800Lk749tULLNDAUcYKE6b7aRekb1ZAXoYPDLBSdvymoxjY5Es+gFrXoVJSx7VodAUaOu4bQauLaCjo/nAyuqcxX4bPT3TTdNeyi+d6NuCltoVovC/tJapAGLSOBKfcyLkdcjst7cVBOjQf3hvz+Y3bz9P1LJFLOBLpW/2uVL+XYEK+Jh7jI2VMXU+PgkaiFiJGUPp4F+Xx34mbV7QTOotCRQpfRwQAvxcbw8r7RqX3RiglzKWYaN5HvlDDa/JyKsJqmBNeQguXafBh/iqA+IBpYDwl55WiMpo296GrWaiEbJZmCOQncRD+3KabFJsK89BbN9x7BzSjGPEE1RXkdH4xXxstYaNybOPPIcAlLjJQNnru/y5u++0yeeUZ03DePkJla7HJyyAS6KQGjPEzGRn/V3XXxf28QRJDj/Vrd3BOO81Phw4LxindR6N91DIYZo3PiRu+Wpa7+g4BGU9s5mrlshb7x1hyFnnOYxBMItWwItQYapaRoPMbVMO4c5vTzQNqZVKdroxOTgqr5e7qDt6ohlOp5ReMbSe8FcSPlsUWhH0VNNoBmeSbJnJHeovnenbhWkmebHTMblVT4Xc74CUxkeC/ixITEK/dfzfnFb9vJj5y9jZVuQq7xJiQzJi4lrSVR5UvxX3fHKutOivXVzWn98T66oTPS4kYZvf0wplpHzuDzovUxC9emtTRn5+fioBA8UUDna8fomMNEMAu4uBCJqbOEIuAcSad+hPRW6RutwxKBvUs53rqYK6fdy4ZRbRQGlgNLGMrtxzLWcmUpjWefm2VIcXFDmz9r1qpcs2J3vXrUKYkmiPQbDGAyLBjnnCNkhnPxng9tLpkY+XtkQjEr6Zs54k34eyrpVRMsj+xxHiQQj7FcrAwnF+fo5I6MgHMpWI5IOtEJpi3omzsijprLpPov5nEoYgGnXbJ0SLlYVoh+ITWKC2+jwDWXxrTmmBOz8jXneNI+48++80zOWgJxxRHYbrSiZ+HyjnSIvcskBHIBV+6NbGG4L2bSxRzhYCBby5AuCCnOxc6+UL6L0jej4zffbaYzDRcsLiKPez8N/crhyAcDiuvtJ8+vr9DkjOXUk+/bRnd0zd7W6RurRQ4OLHUR1sfaVdMULDUsFNOAeuGWjQ3W+oGzekZwHZzG4yea/qpq5s1QnX1THEkeUAnkMsBCZwK/1aAG3mUEG4BvrAkqv8wwn+qNYaIQ4xdb0yuEllncn+UTJOSkmhZriU7SJF0gmANNhqhLUB22NpppI/govmaOUdHyKkyTysOy7hkCRt7r4cMxJF8pDoSLQV1AnY/3VVogyJAkDMA8QTwm9fQWzRfN+fihlMvvGvDDZ60ghNGAOq+8bXwRE/yBHcU+xcoyhRZfb7kBuKQzv+Y2uvvOiZeyNNrZvHw3i6ti23zKbZpZG86JRLNZDRQGV986KnFZ3qbQqq1+V8hWElZ3dCoD+NbpS0q1T0V50EoP7BCwNGWEjpFqEvfVaWAoHS4/mHHBjg6iATVIXJ2AmbNOzSxMUe9Yx7OHnJCculXgvSD0nSfTHjbMydNaj4vla7akRpiqZTVN0mnpieC0g1nKavcwAehowslsVVk0GEKSdbA8sANhoI7lpF6WWU5bazS4ItEYVxUoIvXvZqjvsdfW6fczXKeDpZXbw82ADTQ4EuuSZcLQr5A7BRlE3mg9vUXzRTzHcuO/fuwwFzyjw4W7OrVtVvOc1LXfBqBwoId1HG5ArbNW7ZFmKzKMwQdvovvkc2JNmK+ZojPzXZS+RpjmcpnnU67iSjnoRJHb+iQ3ro+u2ijLVS17VUgLQmKe7Nzt8QYq6gvLt0LfyCRUU87d5klSCFnB8trIX2oIEi8v1TwubdAO7/96n588b5mOxXUni6i0TWlafrqov7HNTvDeHzyXpVPYj4Mqv33NHbzuCzna7YJmTDStUSdqvDzXLKnyqy29cScKbsCDun3e9j3n8JCluKewKueQGZ9sHadiqCoexzrCimWYdVp9H1NH8CpfagK5wRurPBehkx3nb75nD0/bt4Li8KOrWiJvVI1D6rhlo89XDh3hsls3eN/NcMtAyEIgHjMk9XQXyFcskKhwU9bhpVfcwd886Sx2dUohU2hCU8o78RshKMi+Vbrbl8ju7MPIvTzurELZkceeHcMIH76ezB5Pj6jtTs1nwfqYh2vzB7W9PxMXq4cMI/vUVwnH8wmzr1reMR9inBNYvuAAwWK7WyTfkTYbQTVcMvItScLe3gb7l1a5IR80OqeMP0dfhSAC5Hz8lsDBfuC0FU9a7HCs+qyqoal1NYl3zhWK9ioPWqY1LDrtPoEz2JN2UeljYVhyZPSsWdRaqJizrb6cprCaSE9Gn2LCIIElAU2KW6qb5mTlsyosRuXZAs7BaL/dKkwVVpHM9s7QxpdRpqOlBFUNJz7PXIcl6SJOSFRAxhc2GEbihX0e9qXLXLy6zHPPDqxdYrz5luO87po+nzt4ApUlUCEvt48tkC/RCAQ1PnjTBr//uTVednEPpwleMpxfjmJvIWEAzhmdc3YSTl+GOwcVIVU3jRrDM4Iw+OT17Lz5BJy9AyxA4/jwWUJps7hp1k3TjzwP5xCQnMQCg3ddi2VjoVSKqapB2Nz0PfQJ2y/YS0KOSHdmviAEjQpPcEKvIqhKnKt2wr29lPN3ZC17qKz+Weq8xbu35h3+9Wt9Ug2YhKnq6SynbbVT3GM4q3wpG3xpbkwm2Np4Fk/PaE+4GJFaaGybpDgZXBXfLMe0dJu4Vs2zVkZqbWNeaM0XxSSwK4H/dO4S7336Dn75EbvZnjpwWcUsnJ+vSbG5XIVhuoM/+fwRrrsLkiTDZBlnkzyYxhcj3n1gSwnuIbuLHR6lSdTQSkb/xl05HTrW3nQVWZSQC+e7FVybRVO+O80P1o4zwCO39jnxb1eTyLi8pQ4p0CjtWOvsuIB71JnEqylm02cWUzAnrA0GxbhTx1UuUhU6Dh67uxMzGzGiqnaPtYTRH0Yw+MsvH2Et+KItjZk4zdaepX1NFOJU4WiEmvlXKdM0eTRPs5uZXpWPba9OCte2hnUyuCa+GbeQ/6RNwxIZl7dW7hm8mpWvgTOPeo8mKTtcl9+8sMcbv2s7D9vtQfLF8zWir0hyFOOo9fiNzxxlw7o4pXDmL8YXEcOrEMTRfeJ55C4ULBn/K1lU/V3m4MQzeN1V+KODGhtPpj42gxuxpG0gmoozUCX/py9it6yD5RPlHWtV4zZe/h6c1qFz0X68pCNGTC1HIfACwpET/YqwH4fRNIe3gIny5DO3kSbxsC1GN47U7yJrZIXTjE/eFfjorUOkPPRfbXxUdsHU2ergJNOa7500ru3hgprAwmFqejY3r7IMs7SjU4GbNXrPi2sgWrTH4rs146ak0JavxG1i3pTUwCWC+R5P3dvjjU8+jWef2cMREHWMGtnUfAUzgUQx7eMZ8I5blXfdfAKTMNIFzOK5T5MKZCWmaNe5KL1HnYNf7ZLFHKjqWFUdREYGk5CbkV5/nPU3fwq1aGJaMHJT4qr5OXzZZFzTZ1WGaT6j8rtZcf4BQo7RX+tz4vWXg3oqHBuVsq5jjeNVhG2PP5+wVBjMwsx8VaJwPN7PcJ20WOJSx432EnpxqKQ8ZGeHB64CKniEaGtXNaxqxysqShxqHX7zqtu4Y+jJMTKMgGI2nMq4JhOnCZxTjauFUgOa1zkXFBIz01uEHuoj0CzhtBVc2+8yrvnufP5VtEdpxktFTs3XLKvBiZCIxCO3ncM5IXUCifCg7R3+5PG7+MkHbMcnQ1JttM9mvuUjFXAJQYRcE37tk1/n68PYQSzEbqYtg0qVPhNBnZAA/oIz8OetFgc9xgWNpbZW1TGqZRcgdwlHXv0x7OsbWFAIkLeMZYvWx2ZwMNv6KalUNUwV8oCZMXjDFegX7irOsC9L0rC6Sp2q8E0KipjSe9b5lGverbIGq40+QXAObjyh7Ol0aJs5H53WoC7uxt6VKk873YGkmIAXo35Oe7VxRtLMCY4+nz66xKu/dARPhqNPwCHmaubJIvZzU6idSlx7qDT0GdC5JuHM9BYTVpv1SWwGN8t0XgQXHzZ+VH1Jo3Gs2QPbmbpovs24M73xu49a4scfsEJw6ex8G/Q58TgGXLdxGr/2mTsLDSGPtxpb3QiZML3N8GqkOPJtnt73PZSuZWTiix2FDS2z5t8ygnlMHb0vHWXt9z5EJkqWZKSFJniyfKniqm2/+rvpkmniwEDAE8iTAF9ao/+az9AfelJKjao6M1rhdSGYVBxdHGE1kD7jAkwcTgXPbPoEYUMDB7M+OzvxRNwmLmpYUqivJnjneO4Dd7FXjqM+HvQ2eSqlTAwJZgnOUl599V382+2GZJAYaHHrzSxH/LzKOGW4CQpGD0cVFdf1tHXUZsXOCK3pTX9npEbPUe9nCeBFcUBtAKnGtWliE+VtalKtvjoavqTZGtYi+VZxmiRs73R41SU7+ZkHLo3OR2/Pt06fWsSIwj9eP+DNN+XkeBwZVppps/jiAYVEjOQHvo1sZ0KiCUPiyZzjLh3zH1MmeDG85mTm6L/+SjbecS15qXXMy3cBvjTj6v63uqCahou0GJkzbM24/TfeS7hpjVQ0ttNyszgyEsRV8WUEzJS+V3rP/Xbc3pREonZbnu4wjT5DueWEsTcVgrkJLJSzhFYQ4ARwPHxXl0tOT3CqqO/grHFMsVnRKCIBLleEhDzd4Hi/x//7sUN8qe9RG6LkmCqqoW5NTmF2W/xWcWpgGre+mOQQhvHcbxOcwQRBBdPnaUPTtaxZ6c3otI1n0wTOohrkNFwbv5qYebgJuStlpNUBc7XazeVb92NAD1jtGr936U5+6KwOzinOEiZOqG3SZ0ZwQDhOP0v53avu4qb1gGQhSg2Zni9C9MN4wamRPGwPK48+ByEed1xuNBLGdTr+33AWGEgAVdKjyolffQ/pZ9dGrMqJN8uYadwSs0m+NP3E0/hcTcPM6KOYjjfjGTl+6Dj+J5+g99av4LQQViFaDWOxPK71sQNeEdchd7D9+Y8caa0qhdFYFZwQT3EwyMxQDfz77es8eHcPr4UgbNA98mE5iTN8TmDFC89/0C58Yg0t28rSjmsQUC8EVxTIwfUnMn72o3dw24YjCR5FGIojiAJhguE1Kduivm4V50wJxFkdNGE9T7h+LRQaZXksGWPzLSbAtDDTHLQxP9rTKzrNlOTbytccHU8W12zczbK1aacLm8DV7+VPm17ezeZbL4NHnMO7Dqsdx6uetIun74l5e11vyVeqCYEZ6hyK8fljQ379qjWOSIfCizU9XwMnxSkF3iFeSF70eOhEp3lIkopZWIZSWAo5QlcdVk7xf+kIh1/yFoZfO06QnCQPlIsFnG2eL/Nw076nGnkhpogGghnrb76a4e+8j34IxXHlHlfhYymIq8I5xiQk5PSe+ED8Iw/gXNwvOTp6ukZDwS2FRAM3rxu5N3okcT+zTJajtg6ryuRnHujxiJ3GctDJs79L7aRZOWZgglrKhw+m/MInD3JLFnB5oJsXMtmmL8CsdrJZps2iOAQMwYcNsjzn9798lLff1AcveFcKFalojKOXpliFM3rfaLiZlZ7U+k7tdanzZdYoulVcm4O2+m5V/Z5r9sa3GDGqdrpnVUBAKzM3me8s3D4xXvOkM7hopxJkuZHvbPqUhLfeqLzxy8egZQ/6rHzFHJ2nP4D04dvJnaOTV7fp2Oj/aqpW+eaBwUdvY+NF/8Dga8cZekHU4ixpw2zaar3N6kNVopwEsCF9E/SfruXEz78NXTe6GhpUW6UcNoobldc8IRnQ/aXHYstuZr6GIgrmFc2Vd90y5LH7tqMMqR9rNX5vfLZoQ5JtTx2/cuEOfDetmIRF5xt57xvVWwqRoBhD3nFz4Bc+dgdfGChWLh2zekVUmVzVCNr+NoszMZwZ6+p45Rc2eMWnjserm0Lpm7PCWpOxJjTDgpuvbcxKrxI3JTRV+jYN8mRw08qwKG5UnkaR618qPJjDrlNFn6PD/m7gtd+xg/2dUM93Dn1OPDk5r/zsCS4/lNU0pHn5CkLaCWz79WeQeyk0kOpoV84UxrixNhL9P1mS0THH8L03c+LFb8ffuM5QFJfnI13vZPgyU9hWcRLbULAO9obPc/uL34ocj5dMZNRvuhnPfY79dGMNSzA8+tQHsPod98MKa2pavmqCBMgZclNIWOtvcGCb4NWjEuIx7I1yuGkN03B8z4Flnnx2vBYpsQAS4u5DM0YqxUQnNIKP9xYq8M5blJ/7wB1cdVQJKlF1DoEQcnJVtEVbajqQy795uKA5GnJQRU0JlrM2yPnVT63zh5/aYMO6aFCwQO6ShhCRkvziZwbSZSlzmF+PTa6h0TSKPTs9rG4qtiWxgJl7MrhZWtOiuAlTq8yvplHauI2UmuVJ5jsLZxLoknDRacv88XessM0BLiEJymhqfAp9YkowuLmvvPQTd3DjekauSh5y8jCbfyZAEIbPPJ/lZzwIh5E5xYvGblIxB0vWWUVwJbknmCLq0HfdwF0veBN69WE2vOHU2AhGZsQ7LGdoWNPom9Y2TCE3I9e4h9OZoQPo//GV3PXL/0LnroCNxvPGHaMV3XGkTYrGW3Sc4nYKO37pu2A5LnGY1WfEjKE3fG787ReP8JQDqzgB71O885WmVRmc2qWfgAouzXnZhWeyJ80g69HJBBfK630KXJMYGasoRrwz7oOH4cffd5A3fnWDE0UDFqQ2IdfUkqr0NOOm4aJrUMkLdl57WHjhxw7z5188xlHXB8lQ57HiyLExvfUixZDg8gEbScL+FRePiZ2rYc1Kb/671fI0R8dThZuVd3NgaAe2RVrjeyms55uVi+Y7C2dOEElwQXjGgV287BE76IUMfLfYejOdPkWjtqApV9wp/I9/P0o/yxFyvIaZ+SpgiWdJjNXffCrZHsFbhw1xDJ02/FlVs7B4YlEnUxQ1GH7kq6w9769wb7uOoRqpZrgwjMJgCi+n0TdNeIkIGQHyHGc5uQ0ZHlznyEvewYlf+xfs+KCgz40F0pjiOt8xLHYMungUx8rzH073sWeBM6Q4lbSaf5U+F8UMN2/AZ9YGPGr3XWL4OAAAIABJREFUEl7iKRZOfDG21PN1zUQiIXGZl88dD93reMlFq1h3QO7AqxFnYqzxR8sniIKXhC8PEn7+w7fzkssP8qW+gcRGUZ3Sb9rbbfb3TJxAsJxB3udPv7TG9152G//8NWXoAhK6EWAVmq3y1wje4uF62/wJXnD+7pHaPF0LmJ1eLd9pKbQMHm0O1ZPBzQuL4hpv1YslVjEJ5wutzeTbahrhoj/EGUvAi85f5gUP6mAacKWAmkJf1JwVExhKlzdeO+TPvrqOalIciTIjX4uX1QcB//+3d+7xlhxVvf+uqureZ5/nnHllJkwSEpIQkhASEgN5EMJDXvIReeXeeAH1+rhX/SCiXPANKD4+vlAuiiICH598FL2iHxGC4Ed5GFBIAkQICZDXhGQeyZw5r713d9W6f1T33r379N7nTGYmk5nMms+Z3V396+qq6q5Va1WtWuuCeWbf+Dy6rks7OJIghZRV3jdQoFiTFvOyJHD7Knu+/y/Jfu4jmD0ZuU1QMevGnG0arJq+VVXFoaw6oeMtnY99k30v+2Py930Rv+JwhTBRSlH98g0mafv1EAQbPLmN5ZPLdzD5k9ewOhHAm0rdmxmox5AGz1u+2OV/nj0FZm3/qtfD1C9SNJ4BvHFA4HVPmuLFO4TgBhsYK3dV6lFXg4ge7H0PkymLMsn778x49t/fzVtuXmRvz9CUY7U8TZLDKJxmjn/+Frzg4wv8xH+scvfqBOgSrpsiFHMbpVom1b+1+XubIep45dnTvHhnwexo/nD7VR+T39Bz16tHA2Mcmjs5ArhRz9/YZHuVtFavyvewTn0P9bkjcUFRY8hNIIhn2hp+7tItXL0rkJt0nfIl0XON7WJCl16u/PKNy3x8b6cfkXvUc40ErIIJcQXN/tBTSV9wFoGcrrGFZbgOnts/qgwqxbEhGlZK6OIWLKu/eRN7r/0Duu/5HGYhR8dwrEN5byJCJtC65X5WX/NXLL3ir3GfWcL7ALbXNycomdRgkn3ty1QUhwWv+E3CzM9eg54yTVvT6IwxhKHnrmk/lM/ugTsWc563K8HoBvp6CEGrKoNq1b4iHgXNuXvR8J037ObWJUWdQXo5mhhcXsRzG3qOMggEoMWvFIWEEAMbss11ePm5m7j+7Fkum45ipRqPGNPv90Gl3/9VFaMhSniieHLyAPcvW/7ung7vu2OJWxfy2PgilTJURvvqnEq/fLGeogqSIqGHinDlFssHn7eNbWkPw2Tf4ZqqEr1iWrz3XPuRfdy4Nx/OTytt0W/RspEMZ7Zz3vnMHZwxyUA9fpSRc3DWhMcYRwggNkqor//sAd5x2ypDA1T/o6l20OKCgKjyLy/cyjNOSeIeQDNaXa1+iyVtFAfg8Rgv3Lyc8aob9nL7YtzGkfQM3YlAyOvl0349BIcYz2lO+Lvnz/Lk+Uk0GNR6LA5jRj1XCPTIvrrIgZe8D719kUDAIhhxdAi4sisMyR5V2atkC/EoSMCIkp29jdnvv5TZV16M7mpjRcmMQQrFTQNx1buSY7lNqFQ3DRaz7Mn+/W72v/tmVj9yK3PLOV4MXge9ndrRgKUOzo1IEffZxj2RaY+ZX3wuMz9xJcaVk/RFnxp6X4EeSuI7KC0WgvLSG/byAxfM8d9OF1ImoBZsdg1v0kqr119E9binns890OPl//oQ+7oG4+NMvtpsEPdo6Dto4JRaMAVjUWMwQSEPYDxnzglXn5pyzSycu22OXVOOGRud9hsjBB/nnTrBcF8mfGmf5yv7O/z7AxmfP9Ch6wF8fzlYtcIgyvKUVe0zUO2ni0KqSmYSgkk4p73M3zx7B+dtdiABhwLJIO8mhlXJbyBRaa1tBKuWYHNS7eI1xcvANvrRRD/2hDa/fvVWnObk6uK3FJTXf65gWEOMCkrmBFTqXny4MMywbPQTPGoVqf4NbhhHjNSiQTC+x8fvt1z/qXs52HEEl+J9VvlGtfIdlPUQMDmYFpfOrPDhF5zB5gRUNMYYcKPLl2mO6Xp6/7abB1/zV+i+DkIgR2iHOMG+3lsuixGjRitWQ1R4JcFPO8yVp5I+6yzSy89g4sythDnBTxjSiulRUMiyQLLi0T09lr94D/mnv8nqh7+CvbuLFYMWMRODxAA0TbKg1spVpgQLGhQjioqhff35zP3+S8mnLGnNcHcNY/eBzIDS5XduCnz0/v186Hk7SU1CaoZ9vlfbuSQ3btm0Dw5gJOeKbSlvv2KO1/3rPh60KRIckvViYEWpfLk1yaVS+vjp+hwRS1CHuAywfH1hgrsP9PgT20LDAhPaYX7S4oyjlaZkWUY3eB7KM3wmGE0JmqOJgLeIyYjBHFzcuFk8b6gTVatYMpJy/stauvTAwS6/n/97zWlcsCkho0fiLZjoFXTQeWqNVctv6Hn154bIoDKmo393GbNt6BjSkqTE7m9qa0Ul1d8vw+cwzBSqwHXm15o+2A3jgpAXAcyv3ZXzs0/dzC98tkMnX0HEDZewVr5UFfWGLAifX5jktZ+5n3c9/RSmJizYuGI+6rmKwdscec5pzPzKizjwE3/LpgVl0ca51bX6cVXpqs4SaVwKUFg1gjFxscsuKuGGe8hvuJcl6cJsQmvHHMn2WZbaldgHPqAHVujev0C2Z5k0tzgVpohOCXLNUVWsGFpBiEPtWvZUDjQD+SuabuQhMKOBngT0hWcy85svxk8LttjFN2pwQZVcwAbDZ/YZ3nnHft799HlSa0m0S2ACW2uieju7oQxHUJBAIMWZHted2mb5ik381Kf38ZCZKebfywIVUWBipmsqH08NGMWELkqA4KK7WLNMtD7pIsaQqWNvR/DGw8oyIkJAIKREsdRj1EIGIemhucXg0CFvlOVzy/JVmWrBZENhLRgUax2bQsavXLmL52yDYBTnE3JrSMvoKCPnC6TxcKCCDp6rVuNksAbUdjGYsXMUx4qcZAieIC2seqizLen/V6nzkIg1SBpqMmXI4+iIQXPUytc4nGo0vEwDqGmhqvyvMy13L6zy+1+ZJjcrcad/daqgUo/M5CgJaAbk/L97ptjZWuJtT3MkMoGY6AZFgkQ7v4pEkXjBi8OKkvyP8+g99GIO/NI/YJcUbzwulN7khybRGDCGAYPwIrQx2BCiNAN4UZCMHjmJOlgIhAML9L56MK4kVt6AEPc7GvUgOSrCqhoQh2j8/lSVjgXj4xxalWmVqmnMr1xNjSzLYOgaz+I1p3Hq21+O3dZGQ3SPbsrnV95H9fWLBvb0erzl8we4dMsU1+5sYQTEp6j1qJqxA9Na56I1UFx+NET+3cI4uP7MGXo9w8/ftMhiEU1D6UGYJtgcMAORe6gJoTQn9poOJDAFxKIaq6sSd/lAlEYQMxiQTSAuRwvBFpJUsCBR5+87yy6lvLqE1VdX48tUE0AtJji2tjr8+hWbue70CYwYjAg44qbWcmG03y5NDVb89qVLBr8VNTmgBLWAB02G86jee8TTivbQ4rgqAfdfVb+RYselhQlKDPNU5zzVwaBBom5Ka6Chj7u2aHCoOBEZjNKqiBFaDn7m4q184+Ae/nF3AiGGTs9DvB79LpV5x28pVtXhWeVdt/fY3J7iJy/0tOLEKrk4khCG5lzExY0pqoq0hM0/ejGu7Vl90z8h3UBXlVYx8MUJjkHU6AGDiOdWA71Bzviyn6jFRL8HpYJWbSFKWUkQ+tbiRTcyRDtI+k8RnC+Vvfo7GkyzR2YZVdsgSisEsqtO54z3XIc5c6oImwe2QQ5XzQmFZqUqZB5+88sHuXUh44PPnaedtiIwoebxoplM/WU3SQ/1lbop4/mh8yZ5x1WzbHVxSVktGOkhWh9FmkiGO1GVoayRmo8CrlQDHQhtJrxh12yXd125netPT3EhGsnWR/QNL/k3wapzaMcaN4SVYezDpcPIb6MrXA8HF3mzY971+O3Lt3L5JsGbBFHBGF9s+K9QrR5GAz16/NaXV/mjr3ajwz6Jdnn1+q2Z/G8ZJr//UqZ/9YWQJrRRLIHcCIm44vMcSDH1lbjy+sPBlbZT9Y31Uvm3MVwRK5QWk9rCiCN/5uPY+uffTThjunGyYFgd9AS1WB8ltA/c2eEPbhde+oQJLt9S9wLT3MeGzBoOfRkbgjhE4PrHTfDuZ85zXhpApoqVg8r80dBgrMO/gxJSGd7X4o8GrlAHpBdjpT1x6yp/fOVWXnS6xYUEbwtrlJr+vK55gG7guccUR03yLRNrpM3JI3EbyW+ImiWnjZivHDpOQXPy4Ng1m/C7z5zjvHSBzJUb4MfXI5BgSOn4Dm/74gofuG2RniqYHCPDJjn18qkKWKX1g5cx9+6XsXpKgkpWMFHtM4vyaXWmQY2pHAquTB1p8LlhHORGEAyrE6u0Xn4m2/7y1fC4NibEPY9D99T6h5JgFbwGPr0/45du2c9ZrcDrz52rCqej76+dNxqOjpqhL695BKuCOsuLdrT5s+dv4xlzXSAvHA5Z0ABGEF/oUqWK1v+IqnmPUGP0cHFa+StShSgGG3A25zvP8Hzg2jN4xikWR0JwSiKevmrJsOQ53gq8oXzlb3XV8ljiqvx86FrlvhK3Hh1KfsMF3EDmR4YEQ3AJqYkKy1NnWrzrOWcwGzKsOFAfTSysqXwulXpoHl0DS4sHfc4bv3CQD3xjORqWqkMzjat/6td0+gQQY3GpYK57Elv+8vvQc+dJNNAxBguFyWuIwUprjKTcBDMsYcmGcPTRUsMNrjThYh4x8nvAowhGUtKJjMkfv5KJP/wuzCkpLoBxZg3DophLy7Wc+ggEybi9a3jtp/fxrdWEH3mK4/ET5bRI7X2NmAYoaY3haHWUqKuCZcdNAWMtiTE467h4vs3fPO9Ufuz8FjYN2NCLdh/eo2lprzPcQIO5nTEfrxwuTqHYbS4GnLfYPCe0AptF+IVLp3n31Ts4a1ZJTIo1ErcGSDJsc1Vrm0ZqVLtk+O9Y46qSWKnS9JkbDbh16HDyq6ptOtpKv2mXw6HgRCARQYwhkbjidtlWx589ZwtpYjFBMaGHzVfjIFu2WZFfEEuQvGBKcMBbfvLGffzFHYt4VXIL6oVVDOTDUriYwtWzGFJraV2zg80f+1Har7yQRHrR5VIxid3SusxTSk0Mpa+VjZpxw2nS/xt+E004sMQQfqltgQitHZb2+65j9s3PZ3J2AmMcJrFYUzotHFDQmJdBwWeICgtdz//+9IPc9lDGSx/nuP7xc5DaIXcz9fc5ippXq8dQ1Xq6pK5RNrU6vP2SzfzDc7Zw4SYhEY8ah82GZqvLYq0dffsjW5mmh48rzkUEDYHcGcRN8+1bLP/0HZv46Qsm2WIDNiR0RvCiqqX4uuqz6nD5+vhapz1WuKFrVCTUfmWbceNoo/nVqdKu8bQZW7fUP1ycitDKlBeekvD7l80wnQSCmcdoK4ZsLJlVQz0kCOqVh0KL1392H+//+kGyEDDkTORdsGM2+orgcsGemuL+9BXM/N4rMLumUBdIjKFjo8HlgIVQkXmGpaON4epaRjwepDbjFGE5EdqqLKUZEy85j9mP/wDtVzwJm8aFsXF9IYhDAohXRCzLXvi+T+7nkw9YHjdp+JlL55hRARlIpBvdiaGqa1XCeudsSqvj057gdYo8Tfj2UyyfeNFO3vSUWXZO+YoFbq0zDbWT0l+Z6ZMcPg4QExvYOseTp1d571WODz13O5fMt/CSgiZ4CxPFjvimujYto6+hElMtX3U+TR4FuP5fcaF/b8PxRibMDyW/xtubN7GXx3Xp9kjgAh5x0Svmy85u8VvfNs92lshdggYdMKuGeqgBi8WqsqIpb/jcg7z3q8ssY6ItYmBs+bqJIWCYVMX94IVs++irsa8+j+4sfdu8QYyd8rhkSvF4+NpoXGROVfmqMoiNwQEkCJ3zZzjl3S9l/i9ehj1rE5kExHt6BsZpG1GdzAkC+3LlRz/5EP+4x7IpW+atT53nnDkhWI8NKQZPueK73rxkiXPVk2pjN1m7jzIIy1qQ+mg7otJirtXjzRdNct2ZM7znvx7kb+7psnsloOKi2G0CJkAQU2whEER9tKk01RG7aMaB5BsPiw/PGFNY3JYfI+As5AETDMEYrBHOn/W86uwpvvfcWbY6IWrXJr5qC0lQgpFoxlBpoPpxtY28KjmKqMEGE639S9Wo9gqbXuuxwdVonUHNqCLqi2CkgmjcsBp9PwVCqFnFjMtP8sgMvETral2/iBtdlT0UnFPBW4chMB0SXnVOfG8/9fkDPJQ7JMQuZzXgrQeNW5ijzVagZwImd3ibsdyb5A03HeBAL/Dj508zmWaEkAJ5zEMcptIZUwUViyRKSzx67jw73nU9q6++m8Xf/QR84n50sVPYROV4AplxGDVYjdKhL1iNKazmB4xnWMYqU0vpK5e43uc0Og4MUtoZejyGBKFnID1tM/IjFzD36qtgRwtVj4hjQpXghIn+NjeKpwG6CprE+ayiqfZ2Pb9w434+cNcqzrV56ROnecXjJ2iJJRgtvGisz6jq79g1JVapbpTXtHKWQHQXC6goXh0Gw3kz8Lanb+GHL+jyV9/I+Nu7lrj9YMayN1gE1RwjEn1iGUMoXNf0tzsYQUMhoheMS41Hg8FoEhVmW0xTYXGqBO9Rq8xb4arNU1x3ruN5p6bMpynWlEw3Gi32jf5sZIJ1xt2kXvTTgqFlM1YRcreKNC15HOfUMzYuSYtG7x2qSPC4XJDQQmy24bxM0eGDyfAkpLXrdduqo5dmi5AqBiwk3vLfz5mgZeZ50xcOsndZ0SRg8hjlZki9JkpRwcRFGTUZmVfe9uVFdve6vOWSOeaTLi6kiOaoC4Dtl8X0tQ0h2nsZfAr2maex5dJX073pXpbe/1myG+7EP9AlCZa25sQJfUAVkRwRJWirohZWJGpKZlU9z3HiCSoYLMEAajAhoScJbiKQX7CF1ndfxMzLLsadOoVawQQwJHHiSKTYVjfcphIgNwmIjTtitMO9PfjhTz3EJ+72qJ3g0pmcX7x4CxPWIRKtzxBiGxwiDTGscZbETdQkhUSK81aGyJHPmk742Sdbfuj8SW7e2+Ff7lrhn/d3+MpBx0pmC9ctGabYo6SFWK5xx+2gcRRsHkf4OEo7TKaoCTiTM9cyXLTJ8R27Jrnm1CnOnRHaJsEUVrhgGplutc6DrTfjcbkRRB09EUJoozow8ztRyOARjYqINRmKQ40hWA+mh+rGmXSgjeQJNrPkdW7F2kFhlJpwxHHGM+kd158ZmJuY5Q03LnFHJ8YPtEbxpYvgob2nUE452DCBDxnv++oK9y1afu1pMzx+pkfi04JJj5YARQI2FDZdU0Lr6tNIrzoDvWM//sN3sHzDV8m/cB+yL5CLwyg4deTl3E9fMawYixZXqlupAxbRBFHIxGDxhJbCOdNMX3M2rRecS/q008k3p4h4AnGxIIjGzco6RopVQC02ZKgEvrwkvO4z9/OpvQ6fOLanOb991Wa2t5WgGZa0cTBpzLoBJyHE8M5NRqOjRq1xONXoxVBNRXfW2KSq0lcvlnpw11KX/3ywxy17lvnKYuDelYTd3cBKp4vYODJFUVz6jWMxBNtjk4XT2gm7Zg0Xz8IVO2Y5Z044tZ3QVkFdXCpOcKhEr5JJLeTYKIa1kTTv4/rOaujyazft4falqXVfwPFGz94Z+IGzp8lsi0Q9XoSg8EffWOTGew+S6/SG8zJmiTc+eZLzNs1jg8G5+F4fOcmqOS0LvagGBoO3yi37O7zhsw9w44NTEDr4oRB3VXUohq1XGwgSsFmCOsulmwK/8fRNXLG1RWKi183GsqgSQoaXOKXQlejbMwGQYpN0V8nvfAh/8/2sfuoO9Mt70dv2k+3rIL6cvRqmuqZdfrlmKkFOm8ZdsA130SlMXHM28sStsH2iEJqiyh+nJMt+qnHLmhk28SnrESmQK4hXPrKnx8/cuMBtB7sEY0i88r5rZ3n56TMYEz2vuLJfj8ivSZuppg8xrDqonr6eFNLEzCoZFQNTfYNHbPZOgJWespwHFnqePd2c/d0e3dz2X0I7dWx3yvxUwqZEmHLCtLMkfeZYMHzRyq4gAa2OQ4N6NdWjqaGacDHKSDzymLhE219dqkzcxgyH2uGY4arpTee1dK/R2VvZLZQoO+do3IYhG89PFWIU8fjxl3vXRrV/0/nRwNW7uKrn7tXAL9/0IH/y9Yxctai44LylZ8pgWP07ilYRkB7Wt9nZ7vLzl0zzmidsIhVBbC/OhYkdlK+4s/p0gWIeSPsfc/yR6Ep5JYeFVbr7Fwl3LpPvfpDkgVV6DxzE5AMj1oBiZ9qwbZKwcwb3hM20Tt+ETCbYuUk0aTY9XatcVtLLgToERGJU54AhIHS84Q9v28fvfKnLfR2LU09uLL94ySQ/fdFsMV9ZyIDrvJ9Rx/3z0h9WNZFKATfy8vuVGyPmjWICa4FxuVN9iC3lXL/ZtFrxcpVkRDnrz14Pt9HyHe+4+gLKqLweqzivPcQ7cu/58/tWedO/L/Fgz4O1BK0YFPcHRB30dBOwvRTvEmbsCq88p8UvX7SZLYkDcpxzD7t8vmByUho6SQcVi1cBLSTWolyqkIfCQYAEomvA+Oz1JJpx5YPoX54AmY3beu896HnjF7/Fh+6agEzxbgUJgdc+cSu/cvkEbdcam99Gn9vHlRJWtdHqx9Ubmxhalda7ryl9qFA+DiseQJTS+YVQ+E/SqFqKTYqBr1nSq1d+vQ+iXsbHCm7s4PEYxHVUSUOXXByuk/G5hcD33vgt7jg4C34VbyoqYl8kKdpbDDbLMU7JnGByx3lzLX71ioQXbJsmsc2eCDZSvqCKasBIdPocVPsr7ArYyrYi1TjtYorjqgeFEALWDk92j+qvo5gIeY9eED5w3ypv+cIB7lpMMOpQemASXnVW4B1P286MFUyxGDU2v40+F5o9jpYZjOJyG01bbzSpFrY818KNRfS/XrHMLX1GybBX7CYax3ibMI8VXEnjBpnHOi7QQXFYVTIc+B57Oo43ff4B/u4uy0ooQt4J0A/GG/MwQaIkJgETAkYDQR1J4vnBc1u89eKtzKdSuZ9h/WtM+UIIkeFQqHxBENF+OLDqInXQyNCMmMI2TPoBsct+WWeMQ8yyXywZSJCxMGQh49YlePN/7OdjdwZywEhGZpXUG15yxiR/9IwZ2i4uEhhjRj5rzXM3gBtiWPXGqp43YcalNRVoXP7r0dHAleUbVacTEVdix10/iatIK15Rm7OSW95z+wHe+aVV7lyxoDli88I+zRFXTEPhGq4QvVQLCSyygCfNBt586Xa+Y6chTWwRMa+cC5Qhi4EjVY+m/lje14QDyIMvVoYNIXikmMW8bQXe+7UF/vzWBe7XGdAuRnOEBOMML9+V8ntXbmE+XSvZr/fcDeOqKmGVHskRb9SzjyauqYFG1eFEwTV9IE10Ejcg7wNqFNfLyZzhP/f1eOst+/nkfS06KjG8Oz28lBuBdVhVFKE0gxBt0TI9XrDL8roLp3na1hYJ0exGbcCaZrukR7K+qoqGDHAg0KPHXYuev74j44/vWOaulR5qUvBxF4sCLe94zfnKr14yz3xrItb5KJVvjVlDE5PZiMRVxzWlb0QaaGJsRxq3Xt3G1eF4xj2sEe0xjlOFnoBVwXgFCRz0yvtvO8jbv7bA7qWcYNqVrTUV3azPuOKJVSUkCeK7zLlJvuv0lO89P+WyeUNLYvDQI12PEjvuvioOoOcDWehxywHhQ9/o8td3LXF3J0OlhckNXjPEBVQDk87w+vM28aYLpkha0BIz9KyNPnfDuFF2WE0PrVesiq0/9FA7zbgGPIk7MrhRaU10ElekBy08koa4NUnAhoCXhJsWVnjHTQf44D3QUYshi7s2VAvHgMWfRtXQiMGEQI7FFotJsy3Dc7cbvu+8LVy5vcVsEVonKmFx6jzyvaJ8pRdRIZrplAyxZI4FCVpJrm7gqbj/q2isoooX4d5Oxsd3d/jbOxa5cZ9yMM8JavHGYSRDQhdvDBJabEty3nzZPN9z1hRtG0BdtOZvaOtxgsSh4NaYNdRvbDqvM6J+ZrUHrYd7uIU+XNw4bIk/kXHj2r5+/0ncaFwWAl0f+Oi9y/zaLQvcdEAxueCT6BhSi32Yg4l17c9XDbhMPJtMHBdsEl5xZsqLd7Y5Z9YiYuNEPoYgkaEokYkm4uMKuRQOZ0KMlNxfBiiYUFw8jGHVVDOQAMHgjcXjscHwjaWcz+1d5aP35nzygZzdvYDP8+g4ABCx4LMiek/A+AnO3QLvvHyOq3e2+5FyNtKnDxc31nD0cEfmjRZsVOFGSQiHi6uWr6lOJyqufu14YAqPZpyXwtTAC3t68Gd3LPLOW/dzb6cFSD8QK1BRDalIQgOGZTSqWCQps8Zz+WbLs0+b4epTWlyyOWVCuogYytXzgC3mzHwRVF7QEFATt8UJoYg5aDCaRy/w4vC5snuly837V/mPB4VP3dfh9iXDgUzoGoEQw7eJMcXWOEMMbBAXFSxw3a6EX79skm0zkPp2P/TZI9H+jXEJy+Mh4DpqYR036nod2yQlPRJp4+p4ouKa6EiNfI9FnOYa42Wm0ftHCD0e6MDvfmWZP/3aMns7RcDYYg4LqR4DRUAIMaXPOIfJBDE53gBYJo2ydSLhgs2GC7d4Lpx2nLNpktPbhrnJBKfRoNoWxlZeYXHVs79r2dvtcd/yMvetGm5bcPzX0ipfW+mwuCz43JAHxScJJmTYoJjgwUFPYiANClMKUYuScLZb4LVXbOd7zpxiQgSrBmM1SmAj2u5Ip61hWFVqUutGdfxx6mAT7limHevnH8u0UXQ8SDSPNpxHEQ0YpVDNTPRooMrdXeGdtx3kL/7rAN/KpoAOSo4JLYIIkJGokImJjCwEMA7EF66WpD93ZQh4k6DVSObEcHYSQvR4QvTnJsZGSQ0BccQIVh6RACqUlzCRWZpcwBiCiYzHZAafdLAeAvH+Ged55eMS/s+3beWcKY8TfqU2AAAE6klEQVQQ41WqKEKz9f5GvsGHg9vQ5uc682mSXqq46gPHdaSmQj0SuFFM+ETHNb2XepudxB05nM8z7lkNvPebi3zwm8t8c6FFT2P0TafgbRZFInUgbrDXtfZeGcVYx+G0nC+DgSpaTZAaLo/zW2KwWRu1wjQdnrIz5Q1PmedZ21Om8Wjh42tcuxzNNNFIzQ0y1BajM2liaKMksaY8H879h4sb1dkfC2nrMfqTuCODC6pI3kUd7Msc/3zXIu/9+hL/ud+wnHkI04hkBOOL+a6SiRT/iQx4i9bO18VpWdhh3KCgfZwJoC46w5TgmJ5Y5aLZhB954iwvOaPNlCPOikmc2B/li33U4HgkcX2GVe/YTS+l/oKaXliZXtI4Ma9Ko55XpyOBG1fm9RrzeMeNks5O4o48LpCRq8WFuIrWNYGgPW7eH/j7u1b52O4D3LaQsKoJohnaD6xa5NNnTFXm0+dM6+BgiJkNSja4v7g2oULHCdvtKs/a4bj+7G08c0fKdNKFMIGIQUyOqIGau5z1BIEjjWtkWGWDN920HkM7HNzRlKjWw42TAk80XJ0OZSA6ids4TpXozrvgGQaNNloiBIS9Xc9Nezt8ePcy/7Y34/YF6GU+Gp+KFtt+DEZLV+LFSqIR6DM3LfbflozIFI4CBMVDcU1FKouUEuefJDBrEp40C88/bYKXnD7Nk6ZTWmkpgJWMEUqDib7r5RFCzHD9j8L0TpOle/3GUaPJ4XLMcZ1oXKWOBK5+/FjAnaRHDylKFnKs5gRNWOhavr7S49MPrPKp/YEv7etx33KXbp4j1iE+2kT5InyiCWXEi/gX2VX5v4IJSDAQXGRWRhAfcGI4dcpy4ZbAtdtTnrVjirOmHNMtgJzcOtq4DX83jzTukO2w6tj1JLNREkBTIZvE7VHi+OHg1hs5R5X3eMcdq8HhJG4tDohW9AaUuCexXPnLfGChE9jbddzyUODmfR1uW+7xrY7nnsXAQx3BaxZlJxPDsRYZIqIQAg7DnINTJhN2TTnOmLZcNK9csq3NaZOOrS1LS3K8yRBaRMksMkJxdqg+69EjiVvDsMob+oAGRjQu01HMayOFeqTTxn1gJyLuJD16SFXJQ5wXEiNIOemuZshaPZoP5GRq6QWh5xXvlQeyjJVMWOoKK5kSvNJOhem2oWUD84ljysKEVVpGSYyJEXwQci23/AgiORqS6E9LAyqCNW5kmY81Axuyw3q49GhgPoeS9mgpx7FMO0nHmGoT4VFqKE+LzjkE00YfcFoDSv1aP7EAVefc68UowCKPrm91LMMaJ0mN+/AfDu7RRI+G0eMk7iTuSOFOVGpkWOvNlxyJtFF0LF/wOBX2JO4k7njDnYhkYNAAVd6lOhyuvmQ8TRpknTGN0jIPJZ96GY40rn5cv+9ExjXRSdyJh6v356bj4w3XuDWniek0MaS6JPZw0zZy7Wjg6oy2mnYi49ajk7gTA7fRb+B4SluzHFAFNzGaano1o0NNayrQRhjQkcKNKtO4xjpRcOPoJO7Exh3vtCbWeClaVplNk4hWl7bK9OpvedyEa2KATXS0cONU1xMVV087iXvs4Y73tEZ/WPXOME6lasI1iaZ13LEeidbDP9ZwdfxJ3ImJO97JNElQVRqlwpXXNirN1HF1hjaKwx4tXNmJH0u4avooOok7sXFVOh5x/x8rHmI96RmaVQAAAABJRU5ErkJggg=='
                doc.addImage(imgData, 'PNG', 3, 3, 27, 16);
                doc.setFontSize(12);
                doc.setFontType("bold");
                doc.text(initX + 30, initY + 8, "AGS  ");
                doc.text(initX + 30, initY + 14, "GERO");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 1 * 190 / 3 + initX;
            }

            var bloc3 = function () {
                //Bloc3
                initY = 3;
                initX = 140;
                doc.setFontType("italic");
                doc.setFontSize(8);
                doc.text(initX + 15, initY + 5, "ADRESSE");
                doc.text(initX + 15, initY + 10, "CODE POST VILLE");
                doc.text(initX + 15, initY + 15, "tél. +33 (0)805 696 443 ");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 70 + initX;
            }

            var cadre1 = function () {
                //Bloc3
                initY = 25;
                initX = 5;
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.rect(initX, initY, 200, 12);
                doc.text(initX + 1, initY + 3, "Date : " + (moment().format('DD-MM-YYYY') || "") +"" );
                doc.text(initX + 50, initY + 3, "Heure : " + (moment().format('HH:mm') || "") +"" );
                doc.text(initX + 140, initY + 3, "Poste : " );
                initY = initY + 15
            }


            var cadreIdentification = function () {

                initX = 5;
                var newY =  24;
                var i;
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "IDENTIFICATION");
                doc.text( "NIV :  " + print.cas.niv , initX + 40, initY + 4);
                
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.text( "Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
                doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);
                /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
                doc.text(initX + 140, initY + 8, "Date de naissance : " + (moment(print.patient.age).format('DD-MM-YYYY') || "") +"  ");
                /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

                doc.text( "Adresse : " + (print.patient.adresse || " ") +" "+ (print.patient.cp || "")+"  "  +(print.patient.ville || ""), initX + 1, initY + 12);
                doc.text( "Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
                doc.text( "Personne à prévénir : " , initX + 1, initY + 20);
                    if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                    doc.setFontSize(8);
                    
                    for (i = 0; i < print.patient.prevenir.length; i++) 
                        {
                        doc.text(initX + 1,  initY + 24 + i * 4, "" +  (print.patient.prevenir[i].nom));
                        doc.text(initX + 40, initY + 24 + i * 4, "antécédent : " + (print.patient.prevenir[i].prenom || ""));
                        doc.text(initX + 80, initY + 24 + i * 4, "" + (print.patient.prevenir[i].tel || ""));
                        }
                        newY = i * 4 + 24;
                    }
                       
                doc.rect(initX, initY, 200, newY);
                initY = initY + newY
            }

        

        

        var conseil = function () {
            header(initX, initY, "Conseils", 201, true);
          

            doc.setFontSize(16);
                doc.setFontType("bold");
                doc.text(initX + 50, initY + 20, "Conseils aux traumatisé cranien");
            doc.setFontSize(11);
            doc.setFontType("normal");
                initY = initY + 30 
                
            doc.text(initX + 15, initY + 12 , " Madame, Monsieur" );
  
                var paragraphe1 = "Les circonstances du traumatisme et l'examen clinique n'ont pas identifié d'élément de gravité. Par conséquent, il n'est pas nécessaire de pratiquer de radiographies du crâne ou d'autre examen complémentaire ni de vous hospitaliser.  ";
                var paragraphe2 = "De nombreuses études faites chez des personnes victimes de traumatisme crânien semblable au votre ont montré que la surveillance est plus utile que la radiographie du crâne pour dépister une éventuelle complication (conférence de consensus de la Société de Réanimation de Langue Française). ";
                var paragraphe3 = "Cette surveillance sera effectuée au mieux par vous­même et votre entourage pendant au moins 24 heures.  ";

                var p1 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe1, 180)
                var p2 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe2, 180)
                var p3 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe3, 180)
            

                doc.text(initX + 15, initY + 30, p1);
                doc.text(initX + 15, initY + 50, p2);
                doc.text(initX + 15, initY + 70, p3);

                doc.text(initX + 15, initY + 80 ,  " Vous ne devez pas rester seul et l'apparition d'un des signes suivants : ");
                doc.text(initX + 25, initY + 85 ,  " -  maux de tête persistants ou devenant de plus en plus violents, ");
                doc.text(initX + 25, initY + 90 ,  " -  somnolence anormale avec difficulté à obtenir un réveil complet,");
                doc.text(initX + 25, initY + 95 ,  " -  vomissements répétés (deux ou plus),");
                doc.text(initX + 25, initY + 100 , " -  troubles de la parole avec langage incohérent ou incompréhensible,");
                doc.text(initX + 25, initY + 105 , " -  comportement inhabituel ou anormal.");
                doc.text(initX + 15, initY + 110 , " doit vous faire appeler le 15 ou consulter dans l'hôpital le plus proche de votre domicile. ");
                doc.text(initX + 15, initY + 115 , "  ");
                doc.text(initX + 15, initY + 120 , "  Nous vous recommandons pendant quelques jours de :");
                doc.text(initX + 25, initY + 125 , "  - de ne pas rester seul");
                doc.text(initX + 25, initY + 130 , "  - d'éviter tout effort violent,");
                doc.text(initX + 25, initY + 135 , "  - d'éviter la conduite de véhicule ,");
                doc.text(initX + 15, initY + 140 , "  ");

                doc.text(initX + 15, initY + 150 , "  Au moindre doute, vous pouvez toujours appeler votre médecin traitant ou consulter dans un ");
                doc.text(initX + 15, initY + 155 , "  service d'urgence qui est à votre disposition 24 H/24.");

            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 ;
        


        function footer() {
                doc.setFontSize(7);
                doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");

    };


        var pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        var page_1 = function () {
            conseil();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

       


        doc.save('Conseil_TC_.pdf');
        doc.autoPrint();

    }


   public pdfConseilTCEN(data) {
        console.log(data);
        var doc = new jsPDF();
        

        var initY = 5;
        var initX = 5;
        doc.setFontSize(5);
        doc.setFontType("normal");

        // metadata
        doc.setProperties({
            title: 'Conseil aux traumatisés Cranien sur GERO',
            subject: 'Fiche TC secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        var print = data; 

            var header = function (posX, posY, text, size, tgle) {


                var dec = 0;
                if (size == undefined)
                    size = 285
                if (tgle == undefined) {
                    tgle = false;
                }
                doc.setFillColor(200, 200, 200);
                doc.rect(posX, posY, size, 5, 'FD');
                doc.setFillColor(255, 255, 255);

            }

            var bloc2 = function () {
               
            }
            var bloc1 = function () {
                initY = 3;
                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABvCAYAAABWxv0DAAAgAElEQVR4nOy9ebQkR3Xn/7kRmVX13uvXu1pqqSWBQICEkZEAsRizGTAYG2xsbJ+fNxjsGQYv/GyPzwy2h+Ofx9gGL3gYYx8vgPE2gLFZjFlssS8akAQYkFiEJLQgtLS6X2/vVVVm3Pv7IzKrMrOylve6tcAQfV5XVeQ3I27ciLhx741NVNUARAQzQ0QApn6f9XuruGaY9/xU4P5vjWvWK9D6+1u4b3xc+aytLVTf/0bCicUwKmyz4GXcrNBkXDWNWbgmQc24tgKdCtyscixS7m9k3LRBqIz7Fu6bA9cW7glF4O7GiRWlnSWlq89LzLRMZo0KiwiZNoJPNW7ae/Pw3yy4Jv5buP87cPekUnB34ZJqRFPYVEPbqC0iM7WmatptuGnCxYr/R3ECVsRi8/NoY0ZbOZtl+2bHTdPM2gaZb+G+cXAgxF5TfoIw3eqZprl9I+CS8kuzoVcTKOM2o9U0GbzI+6O8iv8NQ7TyjkzSt0h6bbhFyvbNiit/N0Nb3X0Ld9/Eqepo8B4N5lT6MDqRblvfqGLa2tF9DZe0jcrVUJXyzbhqYtM0sGm4qQJMQEwQBcwRfFalBoevpTeNxmZh5+HmlfmbBTePR9+K+8aIEwRzEADFcDT6LQ5nRKEG4OYPbLMEyH0FN+F0r77cFteWSFMoTRsZ2lTdZjDNQTyKEDC8hXEaCM75MbZFQLaFWfTMovmbDVd9Po1H035/C3ffwqkZUggkg5H1EcEgZiACbhx9XyzHZnEjp3sJKgu2GSEwT7AVxh3ODFAQP5b85Kg4jpjn5mNDrj2m3LWRc3A942g/MLRx/ql37F9J2N11nL2ty/23O87sCd7ASZG0M7AAlqDOorYmECTgrK6dtdHe5MM03Hg0UK44JuhgLFi/WcL2Jc+FK4aJw1AcHrEh15wQDg48qd47ZTYUMUPMWO52OdCD7T2HR8EycClGQM1HTQMQAmjR9pyrp7eAtnp347T4wwwPSDDUg4rhLfqnDIfrC/mNR7EbDhO+fpj8ruP0Dx0h7VeEmTNkR5d0xzbcvlX8OTtJz98Ne3uAIhgmYOJwQVCnKIJHwARxk7TeV0KrwBo9nCGspuEmhJWBqiG2QZAU9Q6nxu19z0fuOMYHbuvz77cFrlsbsO5WMF1HMYIkqCUggbFtLoUAyvGSkZCwp7PMo/Yqj9+f8ORzVnngkmPJO5SAzz2S5KhkOF1B3GSZttoISx/CUJVnv+cm3n8omZvGN1r4zw/s8j8fuxtTAW84EnILvPTKI/z5Z46w3r2Xyux81C5USAzoGPvSwLdt9zx8/w6eckbKY3akLDklc0YiQuYUR4KYkLr2ER4W00TvDhyFmyT6zRUVCCb4NSO//CY2Pnwd2RU3Eq6+nexoxkBytg0Fr8IQwReqlBUeLQd4AhuJQuLoCegZK/jH3Y/upeex/J0Pwi5YJaSKA1KFoQjeOdKGFVMtx7RwT+FqyxpmmWqLZjipXYESyBS+ui584KY+b7vpOB8+eIRsuA2HoknALMomdd34luWIxNG9mpiIx/KAx2MSUBcQ5yAozgvnb3c858yU591/OxfsMhKXgksQlAR3ygRWKLSL4yo8/T13ceWd/blpfKOFFz1kiVc/Zld0lLhiVDb4xSuP8idf6CP3loYlhc5uIOJIrIOimGSo5SRhmTP2rPP9Zzp+8vwzuHCb0JMkOqK9w1c65H0lmCpBFVWHHBqQ/du19P/hs+iHroNjSmaO3IGYsmJGXxRvDkVQ5xBVohbGyDz0ZiBRNzZLMBfomJFTdLb7b2fbcx5O5wcuQC8+g7TncZbjkk5rv7gvxC2sYU0zm0rcNN/JRpZzxcGMv7punXfecoKDuUHuccGhaUGEWjE+aKGwljyPpmQcTovegsaHDpyCs4RginiPDhVJPGYZPe959BmOF563jWee0WV1OaHjZGLE26z/q8RlpnjL6Kvjye85xBUHB63vfyOHn37wEq957F4kB8jBe5IQ+MUrDvG/vngclfTeIUwV8Q5Fo59GM8DjSBFJENtAQw+P0HOBp56d8vMXrHDp6V1S5+hUTMJmO54W7m5cPgwMrjnI8G+uYviWz5Pfchw1jyMg5IVhC+Y8Q4wUKbR8R4KPvIiI6JAnCnZxYBpIXAJqDCXgRCEIKgl4j/iMbY86gP7HR7H6PReQ7lmq0Tivn9yTuKQa0RQ80/xUTWZrAASCKT4IIVHykPPxg8ofX3OUd38tp58bRCs5mokeQMEUA0JBtxQjpzGKiP+VUkwEVJDMxTydxpFfM0jLwiYMNOfDX1c+ctsRvn1Vef63bedHDiyzd8kDgZwUbwFvZa7UytQm6WsMNMMkNhVnseF8swUxD2bRdytxHspwBBKC9KJj996gSxIIRa2JIJaARH8PlgFJoREqxyTwtpuU99864AcfkPJLD13h/O0reAwVITGpO6y5+8ybHMERcBb9R2hgaIZ89k76r76co2//PP5IjphHEJaADMglJZjhECQIPTzR6eEwpFjCUNBS8ghwJrgAhieEaCqm5sklGpBdA8kDIU/Y+OjX0I9+lfxhB9j24kvxz3soya4OIFG5cGCi+MLKKcvV1kfayn+qcElVOJWAZkVM+ywzyVygE6KzLvicLx4W/ujq4/zDzRscHySxIUnpOCy5Wk5vOEZSDLBSKDWmaUeCqxhIzOtYqEF0ppY4UcwEJxkqKZ8+Klzz0UO87owhv3b+Es84r8OKKSZCXkpuNcS7GuPGjvXJUdObEfAUdmplLUyF4LIYIw2RyXLdk7iSh5TvSeOz8rP0F44ii8fOilqMA8Vm0jtVOJNQtBEKOqq4MoSi2LEMR0LKG67L+cjXjvHyRwrff0BwSQ8V8A0vc5tJ0ha/WZzPYegDCYZagt64RvjjT3D07/4dd3BAoowWKAjCkGjmJVU7D0MJRGNcCs7UhVXJKyMKyRJXxvmiPYwN+rgwQugSPncHx37+nfD6K1h+6RNZevoDGfYSPODygDk/7moLuJBONW5TakHT3CszSdWTy5BBH/70i8d51vsO8oYvH+NE1gFbZ7IxjRKsJlRphER8OUO4RZzqEpILPhi59Ljm9sBPfHyN539ojavXhmgY4MMGJvkEfTW7ecakQo0OqfxVBUlJXylA7i1cZcZ1DKuMyWXHsDovpoatpHeP46IAT6xPEOX6DeMFl9/Bb119jI1guHvQDReckVjKsO/I/u7zHHn237P26ivhYFZoq1L8qxWsGJjjX/l9Ho4RjhGuFG5j8daGA1NlcNVBjvzEP3HwF96Nu+44CuQi5PfyFOJE7mVHrfp6Jmf+rPbMZMCNxz0v/Pid/MoVx7lpY4D5FLUB6pbKtyZzFxlHj/Koak0nhxMxxDnMO0yG5MkGIfO885Y+z3nfXfzxtQM2SHDUW221/FWezB0lzMb0lbhqSyi/3Fs4qeBqdBdpyBxcM5xsevcEDitm3FLIEwIZWX+Z3/1Mzi9/4jYO6WQyE8VcYFJmEZwjwMF1jv/Kuzny4neiX7yTjg4I5CjjGdexUCnSBdpXs0/HVT+rOGmIrUmcoJKS2IDe8T7215/ijh94HYN/vgZnQnO6YpqWOY83W8VNzEu3LtZqJoIRdIhZCsH40J1DfvmTR/n8mmBOICQEMcCB5fWRf9SgSu2gJa7IpWYmbAFnxNnH+I6H4Mh9AE24YcP49SvXuer2nN959DIHenlct1M0YBWHk0mf3XShVRUYJU0VgdJQfu4dnNTkPJXoqJHYbFzbiyeT3j2BKxpHXIJnoAlDFzXq134lJw8HedVjdrHNG0Yal2m1dJotLX0JWTFDJygZg2sOc/SF/0Dy6cN0gpLhol/LSj9UpHGsO40rWCYKPBvX/GSElFpcKy4oRkKG4TKBLxzh8PPfzuAX72T5l76TXs8TLRgDLySVZSLzJq9OFteq31U1qKaPyyzavi5PEHL++sYT/NQHj/LZo8v40InTq6MRsDLajUb5xu+yU1Xjyu8iJ4+r5eeK39FJOWSdN914gmdddoSPH+0TLCNzgopiZBPCqmRg++ggk/nWtJ7y+b2IiwwaC7WyjmpmVQW3UNhEevcKriyKgRVtE0CMYJ6//eoGL//cnVF4+EHUMhas83k4xSESMFHsX7/Kie97LatXHiJTo18IzdIPG0tQ13pqeY20IlkI1/w3YsNURJvWFXeb9J2SrBn9l7+f9Re/FVs3TIZ4Aa91PsyzyE4W59ochW0zglV/jlNl4DL+8Op1XnL5MW7NQPQwYn1MPHVBMSdIo3HdIzgBA6+rBFK+cEj50fes856bAy4bkAePryi/bb6sqeku9PvewhXfa4JfpiSzYP1tJr37IC7kHV5zTYc3Xn8E0TizVoa2TtUWpuHUCSEkZH/zWe76kTfgblZOIDgxOhpFlIz+lWbCWAMae5a2gqv/Y/SGVN6ej/MIy7mh3nBZl+zvvsDB7/8Lwp2B3BT1k7KjyZe2/rNVnKv+qH62VUr5eTwoL7+6z8s+fYiNEPDqMecYdrqYFVOs5ag2+mMcZ5X4Znaj/Atc5Wc1qfH7BWZilIXJfMfRAqhkgBAkcOtA+Q8fXuN1Nw5wMkBlXGYtfXUNxjYIr3+vanrVMtTotmLPV0ty94U4AczG7IWJYo4eGJPp1fherfcWx9Gm6LPGH0y2KVuIPic5G1mf//6ZPv9+WIk7K6pKarsAa3YwkYrmU+BcljP8yys48nPvJAyWUVFggEcZuoRyvnVMnjXYO9Z0xqUrxYpS1Zao/F+NizOAYw3LKunRkleT2QEICGnIwIaYOeQDt3HHD78evfUEASuacsGbhr+3KYSmrd9cFOfmaQ9SOLL7GjA1TmTKH1yzxh99+jZCWAUTgsvBUrAhcSNSOYpJ5Q9GuzU90Rg1xUseV5NY3A8opGAuOsyTHJGcOPTFZBKBREHM4y1F8DgPgovrhkZCwhr51kdWEyEzI677dWAZazn8yuWH+bOvnEAHGUEDQXMGmsVqbmFmhVEjIVTrGVWBJBEnKCIG4nDE3W5yH/wzyYAcxMV1blitaGV56qHCm7gUHQQchneCExfX8pwUbeDM8Go4M6TYciMIoq6W7zz61ARNc25eT/mjzx7ixDB2vMyM6kL+tqU9Tc3KTFHNCSg6yDj2uqs48mvvwZ1wpPkgLnglxdThtNRmWikrno2fqjNElAQhdxIHVHTkb5Ny6ZBELSRBcFBscwsoSgrRrTyRr9Xyqoq9yFWHFp1WrE8/gc5HbufWF/4N4aajZKaEYOTDuHy1bVBvG+y3gpt5HlY1gR5GZoHXXneMP/xsnxN+JS4JKNYujTvmFFOicISXHd+yHHE+CgznwBKwAYk7xvZOh92dDivSZSm1QgY4NrKcowiH1jOOD63YQ61IMBIZEgyMzqi6xzUjZT00BNoYpy6mdVy38eufvJNukvCCc7ajyYCeerDx+Bcba0v5qvmWQWQi37jlKMGFIeqXEYaILTBddQ8HpykmHq8Bcy37BktNpjoRUi7ELOu7EOCeDoQNgu8BDmG4ZbpMfBzpBUyUJGRgcRFl7uKq9/HAMZu+4FycaNGcd9465H13bPB9Z3bj3LFPokCsuEamzRjHfuIwM3I18rd8kfDSy5AjngwjrWhAFYOHauMof9GCS9UQcQwEnPm48n3Fw86UdHsKHvKVDuSBZCMnHyisZSRHcySP29wyAmKGKxacVnW6MbfGCyeoPBkvhpBi43nKtg/eyZFf/md2veYHkb2C+MmdD22rDmYtGp2HS6ZJsppK5gLDkPCOW47xm58+QZZ1SSxHnBZbAqQxkrXo3zL+YkFx4kCVhA77UuHivTmPOH2Vx+zby+k9z76eZ7szlrrlLAv0hznHhsItgyGfP6Jc9fUhl9+e85VjG5xgqSAhNPJroaHmlB6HXqb0fcZ62MV/u+Iu9qUJzzprieAEL+2jaj39qmY1I1917E2NH7twlV0OcDunifl7NVyyO3Z8cw6xancqQnUwKMs+0ialhlNyfurCFe63lJDhSG3blunKfYaK59jQcfPhY3zuRM7X1jI2dAmnhrqyfhegTwDxOCccDUv8xReP8ZQzeiwl9QPwmkKq7bdTJTPIP3ILG//tX8iOxEEwkbjlLGZXFQRjguqiaxI3dIL1hOULT8ddej/kiftZOns37rQl/I4lQuLwyx0sV7SfY8MMO3gC+9pxBl+8ncEHbiD9zO0MbzkWhZ1ZJZ/JZQ/jIHWceLwa/biEnuRt19Lf/a+s/MGzkOXJbrfICvdN4azS+2qOdgOVeCRMTsZn1zJ++L2HuaHvwWVRDlshe0fbGypMrmo3xXMxRTRgkpJIziP3dfnR+6/y1ANdzlsxOiIEKVT+4vW4BaYcDRhpciaGmnJoQ/nUEeN/X3uCf73lOHduKOq7YGGUvyDRt9bUAMtRWAAUcYaooQ66wx5nrWb8/ZN3cfFpPVJ8fUTVnCCeEAJPes9B/s/BvJ7eSKNsY7pw3jb452eexoOXAyKddmy9TU8Pi+I2GXINxS61TjwayBmo8YufXOPVX9oo8i3L28y/EmFGYo5/feZ2nrS3h7oE52zr9JohFmfYcoQ71jP+z6Ehf/Wlo1z29RP0dRuQM/batNhBRYSYYU6ilm6O5QTe+6zdXLTb0SGZvVHaIMeKY1mie2Hw5cOsP+/v6F9zBDHFm5IJLFvCoHipyHmku4AUm7fjEoekcFSYOBDFnbvK0g9/OyvPvhB56G7cqgPzhdlbbs8ZT/mXVWViFOojeQ7csk744M0cectnyD94Pcl6KbSs2OZjODHqC4frGpbi6ACBIbkTOpoQloXl33oGvZ+7hK6PdJWlKrfxTFZh3Uc1varruCnng0QzTAJkibKWO1764du4YbiEJ5AHHzlC2UdbpHI5gJmRGIgqufdoJ+GRu5SXPmwPTzk9ZVu3Q1LR6tKmuj1OqpA3UswWRH/GaT3lGb3A4/bt4oajq7z+y8f56+uPc3TgMAEnhuWhYGJprpWtt5K4CRYKoahC7vvcMAj83OVrvOW79nHOkkTTdSpnG8SOmMPYLKwo3rnAkgkm3Th62bjiRgMHdXOkWnFbwU2QPMenkBblFYsLcLW6LKCskWrl1IS0VMwyIYjiJMESwWn0340Vz03SV6m6FNi9LDy3m/D0M/Zx2S0b/PoVd/KFjRUSjqN0os49hT6L6h/mhAzjmMJ7b17nEXu2j47nnkofcfWUV6MvgfRIxuH//m70msMkGkWIkuDNGG+NLzWa0m8U000kntlmdOgnRhoGyNmrrP7nJ9D9kYfhD6zikgo9bnq9jfreaOiHTgrcvwP334n/oQfBlbdz+FUfhPdej+ZCpsJ2POuujwtVsVDXDD3llp6URCFzSrphrP3ee9l30WmEJ5+LyzQWSBQmlpoWqS64imDuOiwrGpkJ4IYMzXjVZ47w/rVOZLMF6iuNqw1zMpgYuTOypMvp/hh/eInjHd+9j+fsX2ZHksZD1Ub5MtHp2ukrKgkQiU7WVXIuWk34g0u28f6nncb3HkhI1BNCB01TrFwRGl8aNfqao7zaOSTBZzu46rjnZVfeipaddxqj56RXy3dOaJod0/LdLG6WOdt0BywynV+Aqxk1M55b3kXznYXrmiN4Ty9Vnn0/zz8+8yy+c++g8GfWPOfT6SvSU4GP3zpgPZv0VU042SGaoOTkFhi8/nPo266LgtkJWvEPjd+p+4hKzSUA4gQlBxuw8oJHsu+yF5H+l0fC2UvT+v1U+qatcQJIVlJ4whlsf9Pz2PnXz8M9YAcrLueoz2rKR3NtVnUmsfTIOTXAs3Kbsvarl5HfvoEmSk4ghKTVHz6Pvlm4CYEllRHIcFxxh/InX1grNvoK6qOaWvcBTG+VzhxOlEfv7vPOZz6An3vwLvaRYImhPuDLZAqHW9Xx1haqGCsO0TNLEXGIKM7DQ3b3+PvH7+EvHrPM+f4oqCMxV9FyqGs/ZSVVRmB1gvNHeeLuLr904f5R3lM78pz0JvKdE6qzJbPy3QyuLEP1dxma8TNnRMepjss3EtQTGVPvsvWwaL6zcAJ4PD4kuNDjwcvK65+8j4duy8fWzTz6ynI448q7hKN5iAeUzs1XUQfLnz3O8VdehuBJQ04otJsyx6rTujSxqrNz3hxBhO4ZKXve/FMsveY55OftIMETFav2DjyNvmk+obKtpKQk3Q780PnseP8LkR+9AGcZnZBQ1aqaa7NolCMhIZM4G8mVNzN41cfI8vLon8l8F6VvGs6VoKpEMyCQsRE8v/apO1jPOiSWgxYzdnW7Z6I9igQScsQZqXh+8twl3vZdZ/NtexyJEyRRvHggjcfvzpC21biJynKlv0swScg9QEIv5PS6nh8/v8fffv/9eNzpIVreopRnMToLCBZXTIRYGQlJLI/k7JUBL73kNN7+tFUeukdqkr1doDZH72bcdE1UaN+72Fappwo3bWBYFFdJvKWM1WeLq/5bpc9cND/EB7IUcuc5Z0n4rUftZcmqM1cz6CvjTTgyDNw8gOp5Bq30IWSABuG233wP3D5ENGPowWtzM02lPoh1bgIiUfDlYrhLzmD7ZS8mffZ5+FSjq8QlIEmtAy/Cl3ERJwWcihbjqid1HfxZS+x47fNYfcX3kq/EOUQnRu7iFRe19BrpZxT6ixlOhcGfX4G/6k46KpUzuqZbJtME8DRcq1NGUJIg/O8bj/KZO/vkJHGXdrmeqSJpixfGxTFI8x65M3rB8TMXCr/7+D3sS7LiEDKPE48IeIn7tpqaQetM5TSck8JXBSkJzjkkTePZQR4esc3xj0/Yxw8+YEgiPcTW48l/ksRlK04wH30CZvHY5UftEP7umfv5rxcusYrCQidUNjSotr4qDb41U2ioxc24k8VNW7rSfGcRHFDRJhk3i5oZXJR3ohG2d6yt0hfbkEckIbW4bkhUeNQZPS7YXclyFn0VTVGccssxifcDzMhXUZLg0Pd+BXnv1XEmUOPhlNWStpV+ZGBJiiUGTzuH0/72J5CHbCcNHrEULzLS0UYm5IJ8KePaBIDHgxOSot8klmBdpfeSS1n9/e+FfSmGw1uCJJOCthoccd1XKL7pkcBdr/hnbCNu25kVptE3CzfibF0FE9YG8JprjjNEqDlaazwpemXV1BLIvNLTZV70UM9vPfw09gqoT6OLUqb7Ktqk7cnggqU4y9mxJPzppQd48XmCd9tx1kckHruvopjkGIHVToefvTDhrc/Yz9N2JnRNCKNTNWetch8RNv5coCKaoekzqZZ3QqvYAq75bBYdi+BqZpZV4iaeLWYGnwr6BCM3EDy7k8Ale0q65tBXmlSm5JJw8Oj6RB1OaoBGUDj62+/H9R3lBuZoZBYDSWWipfxd+rFS58lN6Tzxfuz7s+ch5y2RGljcp39SfNnUQAYkwWE+0P3ph7Hyqu/D7/SYeSRUyzG537Bapvhd0XffwPGPfXW8vGRKvovSV/2sbc2pdvy/vbXPl9YEo4OX8m5AqwwXlbFD6r9Tp/zQuYGXXbSXbT7ERZnO8OSUToVFTJuTxanlZDi6QdjWyfkfj97Of3xAB5/3ig0LAihJCFy0Q3jt47fxiot3cHrXEzyoN5Bi2pqxhtc+ysl4pJbC9GjCpphIxjjNpnCpapbVvLeKq5MzWY5FcWWRaxpWtXgi43YxkeTW810E15FA8IIzz4EdK4vRNyqHYS5hPShi+cx8czOy911LdsUdDF2n9qyqiVgl47GzXcg0I33MXpb+8rm4c5YRc6i5eHit5ifFl6qvdxZWJFoo6jw9S0hESH7oQjqv/G78ckZe3gNa+dcsXzUegSTv0f+fH0GyyWObtkJfFedGmVvANBBQDg2NN3/5EAN1qATUisoYezCr7MNpSkJOEjxO4PGnCb976WlsSz3iPAlSuOyTtv46YQJOk76bxXWcI3UJ4gXvElZ9wssf3eMH7tct9gEp28zzMw/expufuofnnLtCJ01wCJ4EL1JMCkSB1RQC9Uwbmkb5WtN0lpbGxqS52/a9zSzeLK5pUk/zH07F1apexr9HK8jHplV82aiv66H+/FTTB2jwGANUHKE8e3sefaPXPS4oQw/FmTSj/EZ/weKlJxls/MXluEzoaDwHtBRHZXJVZ3XujERyzOUEH3Dn7WLHH30/nL0cafAO5+OCncSNjyHeCl+m9Y22545CXjsH4kg99H7sIvxLvgPv4jVguQ94seLYqFjGaknLWIeAJWQfuIHBVV9HQx63OWnc7rQV+qqfrszUIncRzbjizj6fvVOjpDewivScGB0NcCEexeoT9neG/OGlezkz1VIvIe4vdIWfYb5KO0sr2BwuKTpvzN+cspL0eOWjtvO4XcoDl3L+7PG7eOWle7j/8hJeov/DSXm7Tkm3FIrTLPOoLa4YWat1PMM6avPdtZVvq7g2M3LaCD4VV4uuaNzlUpeqBtN0F9QT3Fy+C+IM4g4MSyFs8KXDw8Xoq47FouxK0/a8iwnyDWeEqw/S/9h1hQ5io3PvocqmsfDqmHHCe5Y1IVnusP3/eyqdi0+nG8Ak3mle+nSr/NkKX6a5Dab1nVG+CIgn6Xp2veQJ2HcfYEUTvCXkUtzEUynheAFsDMGU3ALdIRx9/ZVk4vBq8RBmlS3R1+7DAoIZuTrect1hjrEEBFxu5SHQjaooep8I6oZ0QhfjCC97xF4u2BXIU8gdYGPfzywHri0gfU8WJ0FQ63NgxfOGJ5zFPz79DH743C7LGteNVN9tS2sW/YxmoKwyejc6edkgpoRF890qrho3i3eL40o7q/guVU2lipmvYZ1S+iTHieO2TLjycFmv8+grNUjDW59ze0tog2wDCIqSozgGb7+Gpbvi8tF4E5qrCar4/3h5aDBFgpClQvKjF7H0gw8h80YuguipqI/JwWkRDaaKEwwJsCEgexJ2/MYzOHZmB1EhnuA1Pmgw/l+qM/G3L1bne0vgH64hWcsJFlfbC3bS9FXvOyITx8ENz0fuMlKLI+wT+I8AACAASURBVFNKSrznqSm0xg3R5Y5hCk84bTs/dv8OqfXwoUOqgSD1qc1pjK7aqbM0p5PB5RKNPVXh3BXlITssOjhTR1fr7zbTmWZvj4ONBVKJa5a1FGhTwqL5bhU363OzuMob9fLVTN4Z5d1ivovgXOjgTXnrjRtcf7Ry/dpM+kajCt4pB7YJecu0vBVXjHc2jBPv+jwnJCFIvFXIUT22RUb/lzqWI8HhyM4UVn71CdBJcepxzmGFUrDV+thsvc3CBYZ0gpCLJ3n4fpZ+9lLEZ8V+0tJMHmtY1d+5BRxKH3DHPMff83mCM7xYvFfxJOlzZVZBHKn1+dyxY1x3zJG5OBoN4hkVI4JiHVvNzHEIHbfBb337DpIkQZzEI1+cx1fWkJQZz+74d19InCeVlCQRvO/gpUviPIkkOD+5dGGaIGwNbdpUE9sW15rUYvluNm5Rvi9cP03YhAZZlHciudmNdLP05aaEPCfTnKFBrhmXHRzwis8ORprLovQ5CZy2vMKZywEL9dMHhPJ6uoRwzW1w7Tq+3K2kVhwDUHFAj94rTxZVXBLY9l+eQXL2Kk6EpDhi2NO6wuik+LIljECSdEm9o+OENIHVFz4SvWAv6lI82QjarnM7Sj+hWkZ41/V0KA86OPkyuDLjoQJ0+NCtG9GpWPNbVRIpf1cEa/A9vmtfl0v2F5tkBShH/RkmUDPMNrlOHhflRV0jGa3eXaCyF833VIe7my9bxlnjc9r3TbJt0/RZIG5vNTTPeM+dGS++7DC39uNaqYXpMwiScPHuDttST+oyqiGKnLiqO7viZsKReCOUjF6vJ2iNzHKElf09lv+fi0mK43FG49wpbH/zXCWzcVXLhHjO2J4Vdv3kpfTIyMSNyic1DbU0D8eaJRjr778GW4fcOUJ5Rd9J0DcS66lAPzg+fihgXphey9Xfhd2qfX7mgp0kZDUzqDazMofZTQfcLBv9voCbHzbXS++Jcoz8FKcIV3NkW71NxOcV3GQmp4w+MYfPN7hlqPzOp4/x/H87xPVDR6oniCdhLEBfUQ5Hzvee1aUjtV2IozcdCqroB67Hjy7PnTSPys5bpd6Jx37qYsLO4kSDU1wfE3yZom1vBlfelJb+wIPRM1eIM3GlOCr/6tuMGJVbWL5L0WtuI6AjYXMy9I20NCfGwfWc646VanNDdR6RWAwLZsWHcN4qPG5vhxxIW46/bRI2q/PHI5YFsRDHrpbjKWpVJZPHvZ4S3Gj0jsd+VMeNxcJ0rAGJaqFcF3O0d1c5GjSZxYaVoyQWEDc2e8p6mdeQxi9UzamRqjBqJlWckyE5RnkQolRm1KKwkHaaq3FW8ikeoDg040g/58snBrz7xg3edMNRblgXkHj6bfDb4m5mCTPoKxzeLh6TtHdFeMqBBKPYM0u1vQqJKsMTgf41dxAKkdQ8bC8mXx30iw6eGMs/cSnecoZ06M0x6efVxzxcWz/bbFzczGbo/VdIH3cO+Zu+QOMK41FpqzplyZl+JvirbmT5kafjGsXYCn0Vs9Jxx2DAwf46hNW4fWWi5RXfzeEkFCQlfOd+2JOAc57q2pVpmZaNv42woHk8jlUz3n/HgIRua3p3d1ALBBKEIcvO89j9nsS687WssvM1f1fjPRzswws+tEYvCZFn95S16Ye43LjwtGV+4+Jt7JyyonrqoFK3HirCpvq88HFW/HpOe/zqJ46zJznIRifF58nWy+xyRHMGJNwxcNy0oWwMs4qIcCA+HoczGmTb6RNzeIunkIgkPPd+Xc5acuTO8OqgcsmCUFwm+rVjcHCjMmNGRUjFX+XvKgfTi0/Dn7eKw4863qzBe6H6mIGbJegWxcXz6OLdnUvfdz5H//FayLOaYCpLLBMcAEzIPndXcX4doxNct0rfSGAJxnXHIWTJaGQps25qC6KKeYlL77XPU/bvxMsQQofgfDw/aUbHnjVTqJKQhsC67/Dj/3YHh6edq3E3B3M5ab7MMAlcut346LP3F/sN55iGUukZVnNSjDHBccwcn7h9A01CXDO0Ke1ta0FMSE3JXIfg+iS2o/Z8XiOOD5q/m+WtgsZtR2XAlWsZZilpJgzTrR+RjBhJMJwOscSRmcYLc2tmX5Pv7fQleU6WGmIp+7t9/sMDT6NroObB5YAfT60XnVBvO0p+dECcqncjbaruw6l25Bh6T3lAPAPRxZtzoF17KsNC9bEJXBW7MI5ofRGgc+l5sOqQw+XQIKPyNQVXyQNDGX7x69F8lpOnL4kJx/y/cmIAtgQ+FCPRNIkeD+/HHNtcziV7luIBiFqYio2D7iad3+MtJJOMNlSFJa/kiWFhdgHvtuAcSE6SJwQ/ZPK06inBIPpEmCqDxA1RVSxJEUvjIYNtN8mc6uCEoQRc5lDxiNXraSEfXZs7s3xNpj2AQIdEE1weCGmGtPm1FgxmRi4peAcSz3Ays3i2mhv7WBahLysO7kxV+ekHbePh2yWe/KkQfBzPm7Pc/pYN3EAxsdoGh6rjoKpxlCR0HncOAw9dKw3H2eFkZwfbBtc27GycoWbRNXPuKuGsZfzh4ahMbYKqOdmQ3HAElzsak65boi+JScbI2zes2KUwpVWWPgQRcOACnL2jy+7UEfAkYpSLw5pSsk2ralNlPYoljmAOlQ6m+cR790hQI7gMl0PAEySuQJnfiBomSCMawOUdRDwhi5vB9Z4QVsTG1TElSwbgFJFdTD0ZbnoilVD10UyoXjWcE8jZiL3VXFwf0LZlZ5Hgh8AwnmgQIBei1Jk4VaONvuhpjb8D+AQJcMkex3960A6cF3IcaXUpTyUVQ/C3HouXpFpe6bjtY1TclGYEn9O56Cz6xbS/YqOz4O7JsOjk0VirjMekB4272VYu2k//84ehRVCVYWyaF4gTGfmxHLfbM8mhzdGXlDkYwqETirp87HOpkUBFvXaIDBFJ2LOSspIKaeE0ndb8m36rtt8x5RRzWRz9zYjbeqYWby4DpuImHMNN31M86Trzhgt+dJ3STN9O2XHa0qtYJsHHyfEyPWalN42+LeAMI/NgmiAhzD7yeZTcrAbUIphb6Su9Pcn4eCWBiVNAFq3K4owrk7hKvPXF0oc28SiF0MHJECMg5ti7Evjtx+1j71JxAJLE0alt3ZACevA48YQ1GV0uUeoadROp0LscpDt6sLfHsvhijKinvllBskg4Nbh4NZslcWsN5+1AoFjbP+Z926boMgyygN11HLd750nTFxeOFuZ930skKpQTlu3BRBGNSu3urtBNFhulF1H/NhcWfb8prE7Ow73l9ViLvnd34uawbJr5PgU9J7H7Js7JEE0C1ltiW8f4/Udu4wk7hVQKv8u0JQOFDBocOxH9uDT1yPKzHHxjvzINpGfsRLyrp1cJ0/i8Vdy0uLYwCyeUVpegDty+HTVrI/bhqj41fg/AnOCDkh8/QbM+tkKfM4sG4ECVwxsnikP2Sj/AFMHlCn+VeFbI8VMIaS4CW2Qt1vSHo/+mPNsMrqIFjaKkBTdJ30yH5dz0Fs33bsLV4qaHZr1NL2813baE7T6IC8AQfIczsqO84tLt/Og521GfEKQwcKb6hIoU1vstlkTsKzL6Hk2oIIqgDLclE5rvonzeCg7aFYKtxBUX+eEVuttXyYuV+WKTd3i3eUI65kmM4vark6NldPOzUdwKYzS2RFdsmVEJDF8c81mOM20LRJuLwKrO9ukCrBFXYmT03yRONomTIr7VdLJJri8U5qS3aL53Jw6mlq1aH00nc7OYrWGqttx44Z7GlSeQSgByxDwqXfaGjJc/9nRe+MAOPk1IxCEyaQTW2mlhYXorttLIpOvcSiCQmRZrlmJfaaY3i88ni2srR9v3ubjCPeQKKyxd6qGF800Lk749tULLNDAUcYKE6b7aRekb1ZAXoYPDLBSdvymoxjY5Es+gFrXoVJSx7VodAUaOu4bQauLaCjo/nAyuqcxX4bPT3TTdNeyi+d6NuCltoVovC/tJapAGLSOBKfcyLkdcjst7cVBOjQf3hvz+Y3bz9P1LJFLOBLpW/2uVL+XYEK+Jh7jI2VMXU+PgkaiFiJGUPp4F+Xx34mbV7QTOotCRQpfRwQAvxcbw8r7RqX3RiglzKWYaN5HvlDDa/JyKsJqmBNeQguXafBh/iqA+IBpYDwl55WiMpo296GrWaiEbJZmCOQncRD+3KabFJsK89BbN9x7BzSjGPEE1RXkdH4xXxstYaNybOPPIcAlLjJQNnru/y5u++0yeeUZ03DePkJla7HJyyAS6KQGjPEzGRn/V3XXxf28QRJDj/Vrd3BOO81Phw4LxindR6N91DIYZo3PiRu+Wpa7+g4BGU9s5mrlshb7x1hyFnnOYxBMItWwItQYapaRoPMbVMO4c5vTzQNqZVKdroxOTgqr5e7qDt6ohlOp5ReMbSe8FcSPlsUWhH0VNNoBmeSbJnJHeovnenbhWkmebHTMblVT4Xc74CUxkeC/ixITEK/dfzfnFb9vJj5y9jZVuQq7xJiQzJi4lrSVR5UvxX3fHKutOivXVzWn98T66oTPS4kYZvf0wplpHzuDzovUxC9emtTRn5+fioBA8UUDna8fomMNEMAu4uBCJqbOEIuAcSad+hPRW6RutwxKBvUs53rqYK6fdy4ZRbRQGlgNLGMrtxzLWcmUpjWefm2VIcXFDmz9r1qpcs2J3vXrUKYkmiPQbDGAyLBjnnCNkhnPxng9tLpkY+XtkQjEr6Zs54k34eyrpVRMsj+xxHiQQj7FcrAwnF+fo5I6MgHMpWI5IOtEJpi3omzsijprLpPov5nEoYgGnXbJ0SLlYVoh+ITWKC2+jwDWXxrTmmBOz8jXneNI+48++80zOWgJxxRHYbrSiZ+HyjnSIvcskBHIBV+6NbGG4L2bSxRzhYCBby5AuCCnOxc6+UL6L0jej4zffbaYzDRcsLiKPez8N/crhyAcDiuvtJ8+vr9DkjOXUk+/bRnd0zd7W6RurRQ4OLHUR1sfaVdMULDUsFNOAeuGWjQ3W+oGzekZwHZzG4yea/qpq5s1QnX1THEkeUAnkMsBCZwK/1aAG3mUEG4BvrAkqv8wwn+qNYaIQ4xdb0yuEllncn+UTJOSkmhZriU7SJF0gmANNhqhLUB22NpppI/govmaOUdHyKkyTysOy7hkCRt7r4cMxJF8pDoSLQV1AnY/3VVogyJAkDMA8QTwm9fQWzRfN+fihlMvvGvDDZ60ghNGAOq+8bXwRE/yBHcU+xcoyhRZfb7kBuKQzv+Y2uvvOiZeyNNrZvHw3i6ti23zKbZpZG86JRLNZDRQGV986KnFZ3qbQqq1+V8hWElZ3dCoD+NbpS0q1T0V50EoP7BCwNGWEjpFqEvfVaWAoHS4/mHHBjg6iATVIXJ2AmbNOzSxMUe9Yx7OHnJCculXgvSD0nSfTHjbMydNaj4vla7akRpiqZTVN0mnpieC0g1nKavcwAehowslsVVk0GEKSdbA8sANhoI7lpF6WWU5bazS4ItEYVxUoIvXvZqjvsdfW6fczXKeDpZXbw82ADTQ4EuuSZcLQr5A7BRlE3mg9vUXzRTzHcuO/fuwwFzyjw4W7OrVtVvOc1LXfBqBwoId1HG5ArbNW7ZFmKzKMwQdvovvkc2JNmK+ZojPzXZS+RpjmcpnnU67iSjnoRJHb+iQ3ro+u2ijLVS17VUgLQmKe7Nzt8QYq6gvLt0LfyCRUU87d5klSCFnB8trIX2oIEi8v1TwubdAO7/96n588b5mOxXUni6i0TWlafrqov7HNTvDeHzyXpVPYj4Mqv33NHbzuCzna7YJmTDStUSdqvDzXLKnyqy29cScKbsCDun3e9j3n8JCluKewKueQGZ9sHadiqCoexzrCimWYdVp9H1NH8CpfagK5wRurPBehkx3nb75nD0/bt4Li8KOrWiJvVI1D6rhlo89XDh3hsls3eN/NcMtAyEIgHjMk9XQXyFcskKhwU9bhpVfcwd886Sx2dUohU2hCU8o78RshKMi+Vbrbl8ju7MPIvTzurELZkceeHcMIH76ezB5Pj6jtTs1nwfqYh2vzB7W9PxMXq4cMI/vUVwnH8wmzr1reMR9inBNYvuAAwWK7WyTfkTYbQTVcMvItScLe3gb7l1a5IR80OqeMP0dfhSAC5Hz8lsDBfuC0FU9a7HCs+qyqoal1NYl3zhWK9ioPWqY1LDrtPoEz2JN2UeljYVhyZPSsWdRaqJizrb6cprCaSE9Gn2LCIIElAU2KW6qb5mTlsyosRuXZAs7BaL/dKkwVVpHM9s7QxpdRpqOlBFUNJz7PXIcl6SJOSFRAxhc2GEbihX0e9qXLXLy6zHPPDqxdYrz5luO87po+nzt4ApUlUCEvt48tkC/RCAQ1PnjTBr//uTVednEPpwleMpxfjmJvIWEAzhmdc3YSTl+GOwcVIVU3jRrDM4Iw+OT17Lz5BJy9AyxA4/jwWUJps7hp1k3TjzwP5xCQnMQCg3ddi2VjoVSKqapB2Nz0PfQJ2y/YS0KOSHdmviAEjQpPcEKvIqhKnKt2wr29lPN3ZC17qKz+Weq8xbu35h3+9Wt9Ug2YhKnq6SynbbVT3GM4q3wpG3xpbkwm2Np4Fk/PaE+4GJFaaGybpDgZXBXfLMe0dJu4Vs2zVkZqbWNeaM0XxSSwK4H/dO4S7336Dn75EbvZnjpwWcUsnJ+vSbG5XIVhuoM/+fwRrrsLkiTDZBlnkzyYxhcj3n1gSwnuIbuLHR6lSdTQSkb/xl05HTrW3nQVWZSQC+e7FVybRVO+O80P1o4zwCO39jnxb1eTyLi8pQ4p0CjtWOvsuIB71JnEqylm02cWUzAnrA0GxbhTx1UuUhU6Dh67uxMzGzGiqnaPtYTRH0Yw+MsvH2Et+KItjZk4zdaepX1NFOJU4WiEmvlXKdM0eTRPs5uZXpWPba9OCte2hnUyuCa+GbeQ/6RNwxIZl7dW7hm8mpWvgTOPeo8mKTtcl9+8sMcbv2s7D9vtQfLF8zWir0hyFOOo9fiNzxxlw7o4pXDmL8YXEcOrEMTRfeJ55C4ULBn/K1lU/V3m4MQzeN1V+KODGhtPpj42gxuxpG0gmoozUCX/py9it6yD5RPlHWtV4zZe/h6c1qFz0X68pCNGTC1HIfACwpET/YqwH4fRNIe3gIny5DO3kSbxsC1GN47U7yJrZIXTjE/eFfjorUOkPPRfbXxUdsHU2ergJNOa7500ru3hgprAwmFqejY3r7IMs7SjU4GbNXrPi2sgWrTH4rs146ak0JavxG1i3pTUwCWC+R5P3dvjjU8+jWef2cMREHWMGtnUfAUzgUQx7eMZ8I5blXfdfAKTMNIFzOK5T5MKZCWmaNe5KL1HnYNf7ZLFHKjqWFUdREYGk5CbkV5/nPU3fwq1aGJaMHJT4qr5OXzZZFzTZ1WGaT6j8rtZcf4BQo7RX+tz4vWXg3oqHBuVsq5jjeNVhG2PP5+wVBjMwsx8VaJwPN7PcJ20WOJSx432EnpxqKQ8ZGeHB64CKniEaGtXNaxqxysqShxqHX7zqtu4Y+jJMTKMgGI2nMq4JhOnCZxTjauFUgOa1zkXFBIz01uEHuoj0CzhtBVc2+8yrvnufP5VtEdpxktFTs3XLKvBiZCIxCO3ncM5IXUCifCg7R3+5PG7+MkHbMcnQ1JttM9mvuUjFXAJQYRcE37tk1/n68PYQSzEbqYtg0qVPhNBnZAA/oIz8OetFgc9xgWNpbZW1TGqZRcgdwlHXv0x7OsbWFAIkLeMZYvWx2ZwMNv6KalUNUwV8oCZMXjDFegX7irOsC9L0rC6Sp2q8E0KipjSe9b5lGverbIGq40+QXAObjyh7Ol0aJs5H53WoC7uxt6VKk873YGkmIAXo35Oe7VxRtLMCY4+nz66xKu/dARPhqNPwCHmaubJIvZzU6idSlx7qDT0GdC5JuHM9BYTVpv1SWwGN8t0XgQXHzZ+VH1Jo3Gs2QPbmbpovs24M73xu49a4scfsEJw6ex8G/Q58TgGXLdxGr/2mTsLDSGPtxpb3QiZML3N8GqkOPJtnt73PZSuZWTiix2FDS2z5t8ygnlMHb0vHWXt9z5EJkqWZKSFJniyfKniqm2/+rvpkmniwEDAE8iTAF9ao/+az9AfelJKjao6M1rhdSGYVBxdHGE1kD7jAkwcTgXPbPoEYUMDB7M+OzvxRNwmLmpYUqivJnjneO4Dd7FXjqM+HvQ2eSqlTAwJZgnOUl599V382+2GZJAYaHHrzSxH/LzKOGW4CQpGD0cVFdf1tHXUZsXOCK3pTX9npEbPUe9nCeBFcUBtAKnGtWliE+VtalKtvjoavqTZGtYi+VZxmiRs73R41SU7+ZkHLo3OR2/Pt06fWsSIwj9eP+DNN+XkeBwZVppps/jiAYVEjOQHvo1sZ0KiCUPiyZzjLh3zH1MmeDG85mTm6L/+SjbecS15qXXMy3cBvjTj6v63uqCahou0GJkzbM24/TfeS7hpjVQ0ttNyszgyEsRV8WUEzJS+V3rP/Xbc3pREonZbnu4wjT5DueWEsTcVgrkJLJSzhFYQ4ARwPHxXl0tOT3CqqO/grHFMsVnRKCIBLleEhDzd4Hi/x//7sUN8qe9RG6LkmCqqoW5NTmF2W/xWcWpgGre+mOQQhvHcbxOcwQRBBdPnaUPTtaxZ6c3otI1n0wTOohrkNFwbv5qYebgJuStlpNUBc7XazeVb92NAD1jtGr936U5+6KwOzinOEiZOqG3SZ0ZwQDhOP0v53avu4qb1gGQhSg2Zni9C9MN4wamRPGwPK48+ByEed1xuNBLGdTr+33AWGEgAVdKjyolffQ/pZ9dGrMqJN8uYadwSs0m+NP3E0/hcTcPM6KOYjjfjGTl+6Dj+J5+g99av4LQQViFaDWOxPK71sQNeEdchd7D9+Y8caa0qhdFYFZwQT3EwyMxQDfz77es8eHcPr4UgbNA98mE5iTN8TmDFC89/0C58Yg0t28rSjmsQUC8EVxTIwfUnMn72o3dw24YjCR5FGIojiAJhguE1Kduivm4V50wJxFkdNGE9T7h+LRQaZXksGWPzLSbAtDDTHLQxP9rTKzrNlOTbytccHU8W12zczbK1aacLm8DV7+VPm17ezeZbL4NHnMO7Dqsdx6uetIun74l5e11vyVeqCYEZ6hyK8fljQ379qjWOSIfCizU9XwMnxSkF3iFeSF70eOhEp3lIkopZWIZSWAo5QlcdVk7xf+kIh1/yFoZfO06QnCQPlIsFnG2eL/Nw076nGnkhpogGghnrb76a4e+8j34IxXHlHlfhYymIq8I5xiQk5PSe+ED8Iw/gXNwvOTp6ukZDwS2FRAM3rxu5N3okcT+zTJajtg6ryuRnHujxiJ3GctDJs79L7aRZOWZgglrKhw+m/MInD3JLFnB5oJsXMtmmL8CsdrJZps2iOAQMwYcNsjzn9798lLff1AcveFcKFalojKOXpliFM3rfaLiZlZ7U+k7tdanzZdYoulVcm4O2+m5V/Z5r9sa3GDGqdrpnVUBAKzM3me8s3D4xXvOkM7hopxJkuZHvbPqUhLfeqLzxy8egZQ/6rHzFHJ2nP4D04dvJnaOTV7fp2Oj/aqpW+eaBwUdvY+NF/8Dga8cZekHU4ixpw2zaar3N6kNVopwEsCF9E/SfruXEz78NXTe6GhpUW6UcNoobldc8IRnQ/aXHYstuZr6GIgrmFc2Vd90y5LH7tqMMqR9rNX5vfLZoQ5JtTx2/cuEOfDetmIRF5xt57xvVWwqRoBhD3nFz4Bc+dgdfGChWLh2zekVUmVzVCNr+NoszMZwZ6+p45Rc2eMWnjserm0Lpm7PCWpOxJjTDgpuvbcxKrxI3JTRV+jYN8mRw08qwKG5UnkaR618qPJjDrlNFn6PD/m7gtd+xg/2dUM93Dn1OPDk5r/zsCS4/lNU0pHn5CkLaCWz79WeQeyk0kOpoV84UxrixNhL9P1mS0THH8L03c+LFb8ffuM5QFJfnI13vZPgyU9hWcRLbULAO9obPc/uL34ocj5dMZNRvuhnPfY79dGMNSzA8+tQHsPod98MKa2pavmqCBMgZclNIWOtvcGCb4NWjEuIx7I1yuGkN03B8z4Flnnx2vBYpsQAS4u5DM0YqxUQnNIKP9xYq8M5blJ/7wB1cdVQJKlF1DoEQcnJVtEVbajqQy795uKA5GnJQRU0JlrM2yPnVT63zh5/aYMO6aFCwQO6ShhCRkvziZwbSZSlzmF+PTa6h0TSKPTs9rG4qtiWxgJl7MrhZWtOiuAlTq8yvplHauI2UmuVJ5jsLZxLoknDRacv88XessM0BLiEJymhqfAp9YkowuLmvvPQTd3DjekauSh5y8jCbfyZAEIbPPJ/lZzwIh5E5xYvGblIxB0vWWUVwJbknmCLq0HfdwF0veBN69WE2vOHU2AhGZsQ7LGdoWNPom9Y2TCE3I9e4h9OZoQPo//GV3PXL/0LnroCNxvPGHaMV3XGkTYrGW3Sc4nYKO37pu2A5LnGY1WfEjKE3fG787ReP8JQDqzgB71O885WmVRmc2qWfgAouzXnZhWeyJ80g69HJBBfK630KXJMYGasoRrwz7oOH4cffd5A3fnWDE0UDFqQ2IdfUkqr0NOOm4aJrUMkLdl57WHjhxw7z5188xlHXB8lQ57HiyLExvfUixZDg8gEbScL+FRePiZ2rYc1Kb/671fI0R8dThZuVd3NgaAe2RVrjeyms55uVi+Y7C2dOEElwQXjGgV287BE76IUMfLfYejOdPkWjtqApV9wp/I9/P0o/yxFyvIaZ+SpgiWdJjNXffCrZHsFbhw1xDJ02/FlVs7B4YlEnUxQ1GH7kq6w9769wb7uOoRqpZrgwjMJgCi+n0TdNeIkIGQHyHGc5uQ0ZHlznyEvewYlf+xfs+KCgz40F0pjiOt8xLHYMungUx8rzH073sWeBM6Q4lbSaf5U+F8UMN2/AZ9YGPGr3XWL4OAAAIABJREFUEl7iKRZOfDG21PN1zUQiIXGZl88dD93reMlFq1h3QO7AqxFnYqzxR8sniIKXhC8PEn7+w7fzkssP8qW+gcRGUZ3Sb9rbbfb3TJxAsJxB3udPv7TG9152G//8NWXoAhK6EWAVmq3y1wje4uF62/wJXnD+7pHaPF0LmJ1eLd9pKbQMHm0O1ZPBzQuL4hpv1YslVjEJ5wutzeTbahrhoj/EGUvAi85f5gUP6mAacKWAmkJf1JwVExhKlzdeO+TPvrqOalIciTIjX4uX1QcB//+3d+7xlhxVvf+uqureZ5/nnHllJkwSEpIQkhASEgN5EMJDXvIReeXeeAH1+rhX/SCiXPANKD4+vlAuiiICH598FL2iHxGC4Ed5GFBIAkQICZDXhGQeyZw5r713d9W6f1T33r379N7nTGYmk5nMms+Z3V396+qq6q5Va1WtWuuCeWbf+Dy6rks7OJIghZRV3jdQoFiTFvOyJHD7Knu+/y/Jfu4jmD0ZuU1QMevGnG0arJq+VVXFoaw6oeMtnY99k30v+2Py930Rv+JwhTBRSlH98g0mafv1EAQbPLmN5ZPLdzD5k9ewOhHAm0rdmxmox5AGz1u+2OV/nj0FZm3/qtfD1C9SNJ4BvHFA4HVPmuLFO4TgBhsYK3dV6lFXg4ge7H0PkymLMsn778x49t/fzVtuXmRvz9CUY7U8TZLDKJxmjn/+Frzg4wv8xH+scvfqBOgSrpsiFHMbpVom1b+1+XubIep45dnTvHhnwexo/nD7VR+T39Bz16tHA2Mcmjs5ArhRz9/YZHuVtFavyvewTn0P9bkjcUFRY8hNIIhn2hp+7tItXL0rkJt0nfIl0XON7WJCl16u/PKNy3x8b6cfkXvUc40ErIIJcQXN/tBTSV9wFoGcrrGFZbgOnts/qgwqxbEhGlZK6OIWLKu/eRN7r/0Duu/5HGYhR8dwrEN5byJCJtC65X5WX/NXLL3ir3GfWcL7ALbXNycomdRgkn3ty1QUhwWv+E3CzM9eg54yTVvT6IwxhKHnrmk/lM/ugTsWc563K8HoBvp6CEGrKoNq1b4iHgXNuXvR8J037ObWJUWdQXo5mhhcXsRzG3qOMggEoMWvFIWEEAMbss11ePm5m7j+7Fkum45ipRqPGNPv90Gl3/9VFaMhSniieHLyAPcvW/7ung7vu2OJWxfy2PgilTJURvvqnEq/fLGeogqSIqGHinDlFssHn7eNbWkPw2Tf4ZqqEr1iWrz3XPuRfdy4Nx/OTytt0W/RspEMZ7Zz3vnMHZwxyUA9fpSRc3DWhMcYRwggNkqor//sAd5x2ypDA1T/o6l20OKCgKjyLy/cyjNOSeIeQDNaXa1+iyVtFAfg8Rgv3Lyc8aob9nL7YtzGkfQM3YlAyOvl0349BIcYz2lO+Lvnz/Lk+Uk0GNR6LA5jRj1XCPTIvrrIgZe8D719kUDAIhhxdAi4sisMyR5V2atkC/EoSMCIkp29jdnvv5TZV16M7mpjRcmMQQrFTQNx1buSY7lNqFQ3DRaz7Mn+/W72v/tmVj9yK3PLOV4MXge9ndrRgKUOzo1IEffZxj2RaY+ZX3wuMz9xJcaVk/RFnxp6X4EeSuI7KC0WgvLSG/byAxfM8d9OF1ImoBZsdg1v0kqr119E9binns890OPl//oQ+7oG4+NMvtpsEPdo6Dto4JRaMAVjUWMwQSEPYDxnzglXn5pyzSycu22OXVOOGRud9hsjBB/nnTrBcF8mfGmf5yv7O/z7AxmfP9Ch6wF8fzlYtcIgyvKUVe0zUO2ni0KqSmYSgkk4p73M3zx7B+dtdiABhwLJIO8mhlXJbyBRaa1tBKuWYHNS7eI1xcvANvrRRD/2hDa/fvVWnObk6uK3FJTXf65gWEOMCkrmBFTqXny4MMywbPQTPGoVqf4NbhhHjNSiQTC+x8fvt1z/qXs52HEEl+J9VvlGtfIdlPUQMDmYFpfOrPDhF5zB5gRUNMYYcKPLl2mO6Xp6/7abB1/zV+i+DkIgR2iHOMG+3lsuixGjRitWQ1R4JcFPO8yVp5I+6yzSy89g4sythDnBTxjSiulRUMiyQLLi0T09lr94D/mnv8nqh7+CvbuLFYMWMRODxAA0TbKg1spVpgQLGhQjioqhff35zP3+S8mnLGnNcHcNY/eBzIDS5XduCnz0/v186Hk7SU1CaoZ9vlfbuSQ3btm0Dw5gJOeKbSlvv2KO1/3rPh60KRIckvViYEWpfLk1yaVS+vjp+hwRS1CHuAywfH1hgrsP9PgT20LDAhPaYX7S4oyjlaZkWUY3eB7KM3wmGE0JmqOJgLeIyYjBHFzcuFk8b6gTVatYMpJy/stauvTAwS6/n/97zWlcsCkho0fiLZjoFXTQeWqNVctv6Hn154bIoDKmo393GbNt6BjSkqTE7m9qa0Ul1d8vw+cwzBSqwHXm15o+2A3jgpAXAcyv3ZXzs0/dzC98tkMnX0HEDZewVr5UFfWGLAifX5jktZ+5n3c9/RSmJizYuGI+6rmKwdscec5pzPzKizjwE3/LpgVl0ca51bX6cVXpqs4SaVwKUFg1gjFxscsuKuGGe8hvuJcl6cJsQmvHHMn2WZbaldgHPqAHVujev0C2Z5k0tzgVpohOCXLNUVWsGFpBiEPtWvZUDjQD+SuabuQhMKOBngT0hWcy85svxk8LttjFN2pwQZVcwAbDZ/YZ3nnHft799HlSa0m0S2ACW2uieju7oQxHUJBAIMWZHted2mb5ik381Kf38ZCZKebfywIVUWBipmsqH08NGMWELkqA4KK7WLNMtD7pIsaQqWNvR/DGw8oyIkJAIKREsdRj1EIGIemhucXg0CFvlOVzy/JVmWrBZENhLRgUax2bQsavXLmL52yDYBTnE3JrSMvoKCPnC6TxcKCCDp6rVuNksAbUdjGYsXMUx4qcZAieIC2seqizLen/V6nzkIg1SBpqMmXI4+iIQXPUytc4nGo0vEwDqGmhqvyvMy13L6zy+1+ZJjcrcad/daqgUo/M5CgJaAbk/L97ptjZWuJtT3MkMoGY6AZFgkQ7v4pEkXjBi8OKkvyP8+g99GIO/NI/YJcUbzwulN7khybRGDCGAYPwIrQx2BCiNAN4UZCMHjmJOlgIhAML9L56MK4kVt6AEPc7GvUgOSrCqhoQh2j8/lSVjgXj4xxalWmVqmnMr1xNjSzLYOgaz+I1p3Hq21+O3dZGQ3SPbsrnV95H9fWLBvb0erzl8we4dMsU1+5sYQTEp6j1qJqxA9Na56I1UFx+NET+3cI4uP7MGXo9w8/ftMhiEU1D6UGYJtgcMAORe6gJoTQn9poOJDAFxKIaq6sSd/lAlEYQMxiQTSAuRwvBFpJUsCBR5+87yy6lvLqE1VdX48tUE0AtJji2tjr8+hWbue70CYwYjAg44qbWcmG03y5NDVb89qVLBr8VNTmgBLWAB02G86jee8TTivbQ4rgqAfdfVb+RYselhQlKDPNU5zzVwaBBom5Ka6Chj7u2aHCoOBEZjNKqiBFaDn7m4q184+Ae/nF3AiGGTs9DvB79LpV5x28pVtXhWeVdt/fY3J7iJy/0tOLEKrk4khCG5lzExY0pqoq0hM0/ejGu7Vl90z8h3UBXlVYx8MUJjkHU6AGDiOdWA71Bzviyn6jFRL8HpYJWbSFKWUkQ+tbiRTcyRDtI+k8RnC+Vvfo7GkyzR2YZVdsgSisEsqtO54z3XIc5c6oImwe2QQ5XzQmFZqUqZB5+88sHuXUh44PPnaedtiIwoebxoplM/WU3SQ/1lbop4/mh8yZ5x1WzbHVxSVktGOkhWh9FmkiGO1GVoayRmo8CrlQDHQhtJrxh12yXd125netPT3EhGsnWR/QNL/k3wapzaMcaN4SVYezDpcPIb6MrXA8HF3mzY971+O3Lt3L5JsGbBFHBGF9s+K9QrR5GAz16/NaXV/mjr3ajwz6Jdnn1+q2Z/G8ZJr//UqZ/9YWQJrRRLIHcCIm44vMcSDH1lbjy+sPBlbZT9Y31Uvm3MVwRK5QWk9rCiCN/5uPY+uffTThjunGyYFgd9AS1WB8ltA/c2eEPbhde+oQJLt9S9wLT3MeGzBoOfRkbgjhE4PrHTfDuZ85zXhpApoqVg8r80dBgrMO/gxJSGd7X4o8GrlAHpBdjpT1x6yp/fOVWXnS6xYUEbwtrlJr+vK55gG7guccUR03yLRNrpM3JI3EbyW+ImiWnjZivHDpOQXPy4Ng1m/C7z5zjvHSBzJUb4MfXI5BgSOn4Dm/74gofuG2RniqYHCPDJjn18qkKWKX1g5cx9+6XsXpKgkpWMFHtM4vyaXWmQY2pHAquTB1p8LlhHORGEAyrE6u0Xn4m2/7y1fC4NibEPY9D99T6h5JgFbwGPr0/45du2c9ZrcDrz52rCqej76+dNxqOjpqhL695BKuCOsuLdrT5s+dv4xlzXSAvHA5Z0ABGEF/oUqWK1v+IqnmPUGP0cHFa+StShSgGG3A25zvP8Hzg2jN4xikWR0JwSiKevmrJsOQ53gq8oXzlb3XV8ljiqvx86FrlvhK3Hh1KfsMF3EDmR4YEQ3AJqYkKy1NnWrzrOWcwGzKsOFAfTSysqXwulXpoHl0DS4sHfc4bv3CQD3xjORqWqkMzjat/6td0+gQQY3GpYK57Elv+8vvQc+dJNNAxBguFyWuIwUprjKTcBDMsYcmGcPTRUsMNrjThYh4x8nvAowhGUtKJjMkfv5KJP/wuzCkpLoBxZg3DophLy7Wc+ggEybi9a3jtp/fxrdWEH3mK4/ET5bRI7X2NmAYoaY3haHWUqKuCZcdNAWMtiTE467h4vs3fPO9Ufuz8FjYN2NCLdh/eo2lprzPcQIO5nTEfrxwuTqHYbS4GnLfYPCe0AptF+IVLp3n31Ts4a1ZJTIo1ErcGSDJsc1Vrm0ZqVLtk+O9Y46qSWKnS9JkbDbh16HDyq6ptOtpKv2mXw6HgRCARQYwhkbjidtlWx589ZwtpYjFBMaGHzVfjIFu2WZFfEEuQvGBKcMBbfvLGffzFHYt4VXIL6oVVDOTDUriYwtWzGFJraV2zg80f+1Har7yQRHrR5VIxid3SusxTSk0Mpa+VjZpxw2nS/xt+E004sMQQfqltgQitHZb2+65j9s3PZ3J2AmMcJrFYUzotHFDQmJdBwWeICgtdz//+9IPc9lDGSx/nuP7xc5DaIXcz9fc5ippXq8dQ1Xq6pK5RNrU6vP2SzfzDc7Zw4SYhEY8ah82GZqvLYq0dffsjW5mmh48rzkUEDYHcGcRN8+1bLP/0HZv46Qsm2WIDNiR0RvCiqqX4uuqz6nD5+vhapz1WuKFrVCTUfmWbceNoo/nVqdKu8bQZW7fUP1ycitDKlBeekvD7l80wnQSCmcdoK4ZsLJlVQz0kCOqVh0KL1392H+//+kGyEDDkTORdsGM2+orgcsGemuL+9BXM/N4rMLumUBdIjKFjo8HlgIVQkXmGpaON4epaRjwepDbjFGE5EdqqLKUZEy85j9mP/wDtVzwJm8aFsXF9IYhDAohXRCzLXvi+T+7nkw9YHjdp+JlL55hRARlIpBvdiaGqa1XCeudsSqvj057gdYo8Tfj2UyyfeNFO3vSUWXZO+YoFbq0zDbWT0l+Z6ZMcPg4QExvYOseTp1d571WODz13O5fMt/CSgiZ4CxPFjvimujYto6+hElMtX3U+TR4FuP5fcaF/b8PxRibMDyW/xtubN7GXx3Xp9kjgAh5x0Svmy85u8VvfNs92lshdggYdMKuGeqgBi8WqsqIpb/jcg7z3q8ssY6ItYmBs+bqJIWCYVMX94IVs++irsa8+j+4sfdu8QYyd8rhkSvF4+NpoXGROVfmqMoiNwQEkCJ3zZzjl3S9l/i9ehj1rE5kExHt6BsZpG1GdzAkC+3LlRz/5EP+4x7IpW+atT53nnDkhWI8NKQZPueK73rxkiXPVk2pjN1m7jzIIy1qQ+mg7otJirtXjzRdNct2ZM7znvx7kb+7psnsloOKi2G0CJkAQU2whEER9tKk01RG7aMaB5BsPiw/PGFNY3JYfI+As5AETDMEYrBHOn/W86uwpvvfcWbY6IWrXJr5qC0lQgpFoxlBpoPpxtY28KjmKqMEGE639S9Wo9gqbXuuxwdVonUHNqCLqi2CkgmjcsBp9PwVCqFnFjMtP8sgMvETral2/iBtdlT0UnFPBW4chMB0SXnVOfG8/9fkDPJQ7JMQuZzXgrQeNW5ijzVagZwImd3ibsdyb5A03HeBAL/Dj508zmWaEkAJ5zEMcptIZUwUViyRKSzx67jw73nU9q6++m8Xf/QR84n50sVPYROV4AplxGDVYjdKhL1iNKazmB4xnWMYqU0vpK5e43uc0Og4MUtoZejyGBKFnID1tM/IjFzD36qtgRwtVj4hjQpXghIn+NjeKpwG6CprE+ayiqfZ2Pb9w434+cNcqzrV56ROnecXjJ2iJJRgtvGisz6jq79g1JVapbpTXtHKWQHQXC6goXh0Gw3kz8Lanb+GHL+jyV9/I+Nu7lrj9YMayN1gE1RwjEn1iGUMoXNf0tzsYQUMhoheMS41Hg8FoEhVmW0xTYXGqBO9Rq8xb4arNU1x3ruN5p6bMpynWlEw3Gi32jf5sZIJ1xt2kXvTTgqFlM1YRcreKNC15HOfUMzYuSYtG7x2qSPC4XJDQQmy24bxM0eGDyfAkpLXrdduqo5dmi5AqBiwk3vLfz5mgZeZ50xcOsndZ0SRg8hjlZki9JkpRwcRFGTUZmVfe9uVFdve6vOWSOeaTLi6kiOaoC4Dtl8X0tQ0h2nsZfAr2maex5dJX073pXpbe/1myG+7EP9AlCZa25sQJfUAVkRwRJWirohZWJGpKZlU9z3HiCSoYLMEAajAhoScJbiKQX7CF1ndfxMzLLsadOoVawQQwJHHiSKTYVjfcphIgNwmIjTtitMO9PfjhTz3EJ+72qJ3g0pmcX7x4CxPWIRKtzxBiGxwiDTGscZbETdQkhUSK81aGyJHPmk742Sdbfuj8SW7e2+Ff7lrhn/d3+MpBx0pmC9ctGabYo6SFWK5xx+2gcRRsHkf4OEo7TKaoCTiTM9cyXLTJ8R27Jrnm1CnOnRHaJsEUVrhgGplutc6DrTfjcbkRRB09EUJoozow8ztRyOARjYqINRmKQ40hWA+mh+rGmXSgjeQJNrPkdW7F2kFhlJpwxHHGM+kd158ZmJuY5Q03LnFHJ8YPtEbxpYvgob2nUE452DCBDxnv++oK9y1afu1pMzx+pkfi04JJj5YARQI2FDZdU0Lr6tNIrzoDvWM//sN3sHzDV8m/cB+yL5CLwyg4deTl3E9fMawYixZXqlupAxbRBFHIxGDxhJbCOdNMX3M2rRecS/q008k3p4h4AnGxIIjGzco6RopVQC02ZKgEvrwkvO4z9/OpvQ6fOLanOb991Wa2t5WgGZa0cTBpzLoBJyHE8M5NRqOjRq1xONXoxVBNRXfW2KSq0lcvlnpw11KX/3ywxy17lvnKYuDelYTd3cBKp4vYODJFUVz6jWMxBNtjk4XT2gm7Zg0Xz8IVO2Y5Z044tZ3QVkFdXCpOcKhEr5JJLeTYKIa1kTTv4/rOaujyazft4falqXVfwPFGz94Z+IGzp8lsi0Q9XoSg8EffWOTGew+S6/SG8zJmiTc+eZLzNs1jg8G5+F4fOcmqOS0LvagGBoO3yi37O7zhsw9w44NTEDr4oRB3VXUohq1XGwgSsFmCOsulmwK/8fRNXLG1RWKi183GsqgSQoaXOKXQlejbMwGQYpN0V8nvfAh/8/2sfuoO9Mt70dv2k+3rIL6cvRqmuqZdfrlmKkFOm8ZdsA130SlMXHM28sStsH2iEJqiyh+nJMt+qnHLmhk28SnrESmQK4hXPrKnx8/cuMBtB7sEY0i88r5rZ3n56TMYEz2vuLJfj8ivSZuppg8xrDqonr6eFNLEzCoZFQNTfYNHbPZOgJWespwHFnqePd2c/d0e3dz2X0I7dWx3yvxUwqZEmHLCtLMkfeZYMHzRyq4gAa2OQ4N6NdWjqaGacDHKSDzymLhE219dqkzcxgyH2uGY4arpTee1dK/R2VvZLZQoO+do3IYhG89PFWIU8fjxl3vXRrV/0/nRwNW7uKrn7tXAL9/0IH/y9Yxctai44LylZ8pgWP07ilYRkB7Wt9nZ7vLzl0zzmidsIhVBbC/OhYkdlK+4s/p0gWIeSPsfc/yR6Ep5JYeFVbr7Fwl3LpPvfpDkgVV6DxzE5AMj1oBiZ9qwbZKwcwb3hM20Tt+ETCbYuUk0aTY9XatcVtLLgToERGJU54AhIHS84Q9v28fvfKnLfR2LU09uLL94ySQ/fdFsMV9ZyIDrvJ9Rx/3z0h9WNZFKATfy8vuVGyPmjWICa4FxuVN9iC3lXL/ZtFrxcpVkRDnrz14Pt9HyHe+4+gLKqLweqzivPcQ7cu/58/tWedO/L/Fgz4O1BK0YFPcHRB30dBOwvRTvEmbsCq88p8UvX7SZLYkDcpxzD7t8vmByUho6SQcVi1cBLSTWolyqkIfCQYAEomvA+Oz1JJpx5YPoX54AmY3beu896HnjF7/Fh+6agEzxbgUJgdc+cSu/cvkEbdcam99Gn9vHlRJWtdHqx9Ubmxhalda7ryl9qFA+DiseQJTS+YVQ+E/SqFqKTYqBr1nSq1d+vQ+iXsbHCm7s4PEYxHVUSUOXXByuk/G5hcD33vgt7jg4C34VbyoqYl8kKdpbDDbLMU7JnGByx3lzLX71ioQXbJsmsc2eCDZSvqCKasBIdPocVPsr7ArYyrYi1TjtYorjqgeFEALWDk92j+qvo5gIeY9eED5w3ypv+cIB7lpMMOpQemASXnVW4B1P286MFUyxGDU2v40+F5o9jpYZjOJyG01bbzSpFrY818KNRfS/XrHMLX1GybBX7CYax3ibMI8VXEnjBpnHOi7QQXFYVTIc+B57Oo43ff4B/u4uy0ooQt4J0A/GG/MwQaIkJgETAkYDQR1J4vnBc1u89eKtzKdSuZ9h/WtM+UIIkeFQqHxBENF+OLDqInXQyNCMmMI2TPoBsct+WWeMQ8yyXywZSJCxMGQh49YlePN/7OdjdwZywEhGZpXUG15yxiR/9IwZ2i4uEhhjRj5rzXM3gBtiWPXGqp43YcalNRVoXP7r0dHAleUbVacTEVdix10/iatIK15Rm7OSW95z+wHe+aVV7lyxoDli88I+zRFXTEPhGq4QvVQLCSyygCfNBt586Xa+Y6chTWwRMa+cC5Qhi4EjVY+m/lje14QDyIMvVoYNIXikmMW8bQXe+7UF/vzWBe7XGdAuRnOEBOMML9+V8ntXbmE+XSvZr/fcDeOqKmGVHskRb9SzjyauqYFG1eFEwTV9IE10Ejcg7wNqFNfLyZzhP/f1eOst+/nkfS06KjG8Oz28lBuBdVhVFKE0gxBt0TI9XrDL8roLp3na1hYJ0exGbcCaZrukR7K+qoqGDHAg0KPHXYuev74j44/vWOaulR5qUvBxF4sCLe94zfnKr14yz3xrItb5KJVvjVlDE5PZiMRVxzWlb0QaaGJsRxq3Xt3G1eF4xj2sEe0xjlOFnoBVwXgFCRz0yvtvO8jbv7bA7qWcYNqVrTUV3azPuOKJVSUkCeK7zLlJvuv0lO89P+WyeUNLYvDQI12PEjvuvioOoOcDWehxywHhQ9/o8td3LXF3J0OlhckNXjPEBVQDk87w+vM28aYLpkha0BIz9KyNPnfDuFF2WE0PrVesiq0/9FA7zbgGPIk7MrhRaU10ElekBy08koa4NUnAhoCXhJsWVnjHTQf44D3QUYshi7s2VAvHgMWfRtXQiMGEQI7FFotJsy3Dc7cbvu+8LVy5vcVsEVonKmFx6jzyvaJ8pRdRIZrplAyxZI4FCVpJrm7gqbj/q2isoooX4d5Oxsd3d/jbOxa5cZ9yMM8JavHGYSRDQhdvDBJabEty3nzZPN9z1hRtG0BdtOZvaOtxgsSh4NaYNdRvbDqvM6J+ZrUHrYd7uIU+XNw4bIk/kXHj2r5+/0ncaFwWAl0f+Oi9y/zaLQvcdEAxueCT6BhSi32Yg4l17c9XDbhMPJtMHBdsEl5xZsqLd7Y5Z9YiYuNEPoYgkaEokYkm4uMKuRQOZ0KMlNxfBiiYUFw8jGHVVDOQAMHgjcXjscHwjaWcz+1d5aP35nzygZzdvYDP8+g4ABCx4LMiek/A+AnO3QLvvHyOq3e2+5FyNtKnDxc31nD0cEfmjRZsVOFGSQiHi6uWr6lOJyqufu14YAqPZpyXwtTAC3t68Gd3LPLOW/dzb6cFSD8QK1BRDalIQgOGZTSqWCQps8Zz+WbLs0+b4epTWlyyOWVCuogYytXzgC3mzHwRVF7QEFATt8UJoYg5aDCaRy/w4vC5snuly837V/mPB4VP3dfh9iXDgUzoGoEQw7eJMcXWOEMMbBAXFSxw3a6EX79skm0zkPp2P/TZI9H+jXEJy+Mh4DpqYR036nod2yQlPRJp4+p4ouKa6EiNfI9FnOYa42Wm0ftHCD0e6MDvfmWZP/3aMns7RcDYYg4LqR4DRUAIMaXPOIfJBDE53gBYJo2ydSLhgs2GC7d4Lpx2nLNpktPbhrnJBKfRoNoWxlZeYXHVs79r2dvtcd/yMvetGm5bcPzX0ipfW+mwuCz43JAHxScJJmTYoJjgwUFPYiANClMKUYuScLZb4LVXbOd7zpxiQgSrBmM1SmAj2u5Ip61hWFVqUutGdfxx6mAT7limHevnH8u0UXQ8SDSPNpxHEQ0YpVDNTPRooMrdXeGdtx3kL/7rAN/KpoAOSo4JLYIIkJGokImJjCwEMA7EF66WpD93ZQh4k6DVSObEcHYSQvR4QvTnJsZGSQ0BccQIVh6RACqUlzCRWZpcwBiCiYzHZAafdLAeAvH+Ged55eMS/s+3beWcKY8TfqU2AAAE6klEQVQQ41WqKEKz9f5GvsGHg9vQ5uc682mSXqq46gPHdaSmQj0SuFFM+ETHNb2XepudxB05nM8z7lkNvPebi3zwm8t8c6FFT2P0TafgbRZFInUgbrDXtfZeGcVYx+G0nC+DgSpaTZAaLo/zW2KwWRu1wjQdnrIz5Q1PmedZ21Om8Wjh42tcuxzNNNFIzQ0y1BajM2liaKMksaY8H879h4sb1dkfC2nrMfqTuCODC6pI3kUd7Msc/3zXIu/9+hL/ud+wnHkI04hkBOOL+a6SiRT/iQx4i9bO18VpWdhh3KCgfZwJoC46w5TgmJ5Y5aLZhB954iwvOaPNlCPOikmc2B/li33U4HgkcX2GVe/YTS+l/oKaXliZXtI4Ma9Ko55XpyOBG1fm9RrzeMeNks5O4o48LpCRq8WFuIrWNYGgPW7eH/j7u1b52O4D3LaQsKoJohnaD6xa5NNnTFXm0+dM6+BgiJkNSja4v7g2oULHCdvtKs/a4bj+7G08c0fKdNKFMIGIQUyOqIGau5z1BIEjjWtkWGWDN920HkM7HNzRlKjWw42TAk80XJ0OZSA6ids4TpXozrvgGQaNNloiBIS9Xc9Nezt8ePcy/7Y34/YF6GU+Gp+KFtt+DEZLV+LFSqIR6DM3LfbflozIFI4CBMVDcU1FKouUEuefJDBrEp40C88/bYKXnD7Nk6ZTWmkpgJWMEUqDib7r5RFCzHD9j8L0TpOle/3GUaPJ4XLMcZ1oXKWOBK5+/FjAnaRHDylKFnKs5gRNWOhavr7S49MPrPKp/YEv7etx33KXbp4j1iE+2kT5InyiCWXEi/gX2VX5v4IJSDAQXGRWRhAfcGI4dcpy4ZbAtdtTnrVjirOmHNMtgJzcOtq4DX83jzTukO2w6tj1JLNREkBTIZvE7VHi+OHg1hs5R5X3eMcdq8HhJG4tDohW9AaUuCexXPnLfGChE9jbddzyUODmfR1uW+7xrY7nnsXAQx3BaxZlJxPDsRYZIqIQAg7DnINTJhN2TTnOmLZcNK9csq3NaZOOrS1LS3K8yRBaRMksMkJxdqg+69EjiVvDsMob+oAGRjQu01HMayOFeqTTxn1gJyLuJD16SFXJQ5wXEiNIOemuZshaPZoP5GRq6QWh5xXvlQeyjJVMWOoKK5kSvNJOhem2oWUD84ljysKEVVpGSYyJEXwQci23/AgiORqS6E9LAyqCNW5kmY81Axuyw3q49GhgPoeS9mgpx7FMO0nHmGoT4VFqKE+LzjkE00YfcFoDSv1aP7EAVefc68UowCKPrm91LMMaJ0mN+/AfDu7RRI+G0eMk7iTuSOFOVGpkWOvNlxyJtFF0LF/wOBX2JO4k7njDnYhkYNAAVd6lOhyuvmQ8TRpknTGN0jIPJZ96GY40rn5cv+9ExjXRSdyJh6v356bj4w3XuDWniek0MaS6JPZw0zZy7Wjg6oy2mnYi49ajk7gTA7fRb+B4SluzHFAFNzGaano1o0NNayrQRhjQkcKNKtO4xjpRcOPoJO7Exh3vtCbWeClaVplNk4hWl7bK9OpvedyEa2KATXS0cONU1xMVV087iXvs4Y73tEZ/WPXOME6lasI1iaZ13LEeidbDP9ZwdfxJ3ImJO97JNElQVRqlwpXXNirN1HF1hjaKwx4tXNmJH0u4avooOok7sXFVOh5x/x8rHmI96RmaVQAAAABJRU5ErkJggg=='
                doc.addImage(imgData, 'PNG', 3, 3, 27, 16);
                doc.setFontSize(12);
                doc.setFontType("bold");
                doc.text(initX + 30, initY + 8, "AGS  ");
                doc.text(initX + 30, initY + 14, "GERO");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 1 * 190 / 3 + initX;
            }

            var bloc3 = function () {
                //Bloc3
                initY = 3;
                initX = 140;
                doc.setFontType("italic");
                doc.setFontSize(8);
                doc.text(initX + 15, initY + 5, "ADRESSE");
                doc.text(initX + 15, initY + 10, "CODE POST VILLE");
                doc.text(initX + 15, initY + 15, "tél. +33 (0)805 696 443 ");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 70 + initX;
            }

            var cadre1 = function () {
                //Bloc3
                initY = 25;
                initX = 5;
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.rect(initX, initY, 200, 12);
                doc.text(initX + 1, initY + 3, "Date : " + (moment().format('DD-MM-YYYY') || "") +"" );
                doc.text(initX + 50, initY + 3, "Heure : " + (moment().format('HH:mm') || "") +"" );
                doc.text(initX + 140, initY + 3, "Poste : " );
                initY = initY + 15
            }


            var cadreIdentification = function () {

                initX = 5;
                var newY =  24;
                var i;
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "IDENTIFICATION");
                doc.text( "NIV :  " + print.cas.niv , initX + 40, initY + 4);
                
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.text( "Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
                doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);
                /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
                doc.text(initX + 140, initY + 8, "Date de naissance : " + (moment(print.patient.age).format('DD-MM-YYYY') || "") +" ");
                /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

                doc.text( "Adresse : " + (print.patient.adresse || " ") +" "+ (print.patient.cp || "")+"  "  +(print.patient.ville || ""), initX + 1, initY + 12);
                doc.text( "Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
                doc.text( "Personne à prévénir : " , initX + 1, initY + 20);
                    if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                    doc.setFontSize(8);
                    
                    for (i = 0; i < print.patient.prevenir.length; i++) 
                        {
                        doc.text(initX + 1,  initY + 24 + i * 4, "" +  (print.patient.prevenir[i].nom));
                        doc.text(initX + 40, initY + 24 + i * 4, "antécédent : " + (print.patient.prevenir[i].prenom || ""));
                        doc.text(initX + 80, initY + 24 + i * 4, "" + (print.patient.prevenir[i].tel || ""));
                        }
                        newY = i * 4 + 24;
                    }
                       
                doc.rect(initX, initY, 200, newY);
                initY = initY + newY
            }

        

        

        var conseil = function () {
            header(initX, initY, "Conseils", 201, true);
          

            doc.setFontSize(16);
                doc.setFontType("bold");
                doc.text(initX + 50, initY + 20, "RECOMMENDATIONS TO PATIENTS ");
                doc.text(initX + 50, initY + 30, "WITH HEAD TRAUMA");
            doc.setFontSize(11);
            doc.setFontType("normal");
                initY = initY + 30 
                
            doc.text(initX + 15, initY + 12 , " Madam, Sir," );
  
                var paragraphe1 = "You have been victim of what seems to be a light head trauma. There is no necessity to undergo neither X­rays nor other exams, or to hospitalise you.  ";
                var paragraphe2 = "Nevertheless it is wise to watch you for the coming days. This can be done best by yourself or the persons close to you. ";
                var paragraphe3 = "  ";

                var p1 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe1, 180)
                var p2 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe2, 180)
                var p3 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe3, 180)
            

                doc.text(initX + 15, initY + 30, p1);
                doc.text(initX + 15, initY + 50, p2);
                doc.text(initX + 15, initY + 70, p3);
            initY = initY - 10
                doc.text(initX + 15, initY + 80 ,  " Appearing of : ");
                doc.text(initX + 25, initY + 85 ,  " -  Persistent headache, ");
                doc.text(initX + 25, initY + 90 ,  " -  Sleepiness or alteration of consciousness,");
                doc.text(initX + 25, initY + 95 ,  " -  Repetitive vomiting,");
                doc.text(initX + 25, initY + 100 , " -  Impaired vision,");
                doc.text(initX + 25, initY + 105 , " -  Trouble in talking");
                doc.text(initX + 25, initY + 110 , " -  Unusual behaviour,");
                doc.text(initX + 15, initY + 115 , " should make you quickly come back to the Emergency Department, closer from your home ");
                doc.text(initX + 15, initY + 130 , " We advise you for the coming days : ");
                doc.text(initX + 25, initY + 135 , "  - not to stay alone,");
                doc.text(initX + 25, initY + 140 , "  - to beware of heavy physical efforts,");
                doc.text(initX + 25, initY + 145 , "  - to avoid steering any car.");
                doc.text(initX + 15, initY + 150 , "  ");

                doc.text(initX + 15, initY + 170 , "  If there is any doubt, you may always call your general practitioner or come to the Emergency ");
                doc.text(initX + 15, initY + 175 , "  Department, which is at your disposition 24 hours a day.");

            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 ;
        


        function footer() {
                doc.setFontSize(7);
                doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");

    };


        var pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        var page_1 = function () {
            conseil();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

       


        doc.save('Conseil_TC_EN.pdf');
        doc.autoPrint();
    }





     public pdfConseilPlaies(data) {
        console.log(data);
        var doc = new jsPDF();
        

        var initY = 5;
        var initX = 5;
        doc.setFontSize(5);
        doc.setFontType("normal");

        // metadata
        doc.setProperties({
            title: 'Conseil aux traumatisés Cranien sur GERO',
            subject: 'Fiche TC secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        var print = data; 

            var header = function (posX, posY, text, size, tgle) {


                var dec = 0;
                if (size == undefined)
                    size = 285
                if (tgle == undefined) {
                    tgle = false;
                }
                doc.setFillColor(200, 200, 200);
                doc.rect(posX, posY, size, 5, 'FD');
                doc.setFillColor(255, 255, 255);

            }

            var bloc2 = function () {
               
            }
            var bloc1 = function () {
                initY = 3;
                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABvCAYAAABWxv0DAAAgAElEQVR4nOy9ebQkR3Xn/7kRmVX13uvXu1pqqSWBQICEkZEAsRizGTAYG2xsbJ+fNxjsGQYv/GyPzwy2h+Ofx9gGL3gYYx8vgPE2gLFZjFlssS8akAQYkFiEJLQgtLS6X2/vVVVm3Pv7IzKrMrOylve6tcAQfV5XVeQ3I27ciLhx741NVNUARAQzQ0QApn6f9XuruGaY9/xU4P5vjWvWK9D6+1u4b3xc+aytLVTf/0bCicUwKmyz4GXcrNBkXDWNWbgmQc24tgKdCtyscixS7m9k3LRBqIz7Fu6bA9cW7glF4O7GiRWlnSWlq89LzLRMZo0KiwiZNoJPNW7ae/Pw3yy4Jv5buP87cPekUnB34ZJqRFPYVEPbqC0iM7WmatptuGnCxYr/R3ECVsRi8/NoY0ZbOZtl+2bHTdPM2gaZb+G+cXAgxF5TfoIw3eqZprl9I+CS8kuzoVcTKOM2o9U0GbzI+6O8iv8NQ7TyjkzSt0h6bbhFyvbNiit/N0Nb3X0Ld9/Eqepo8B4N5lT6MDqRblvfqGLa2tF9DZe0jcrVUJXyzbhqYtM0sGm4qQJMQEwQBcwRfFalBoevpTeNxmZh5+HmlfmbBTePR9+K+8aIEwRzEADFcDT6LQ5nRKEG4OYPbLMEyH0FN+F0r77cFteWSFMoTRsZ2lTdZjDNQTyKEDC8hXEaCM75MbZFQLaFWfTMovmbDVd9Po1H035/C3ffwqkZUggkg5H1EcEgZiACbhx9XyzHZnEjp3sJKgu2GSEwT7AVxh3ODFAQP5b85Kg4jpjn5mNDrj2m3LWRc3A942g/MLRx/ql37F9J2N11nL2ty/23O87sCd7ASZG0M7AAlqDOorYmECTgrK6dtdHe5MM03Hg0UK44JuhgLFi/WcL2Jc+FK4aJw1AcHrEh15wQDg48qd47ZTYUMUPMWO52OdCD7T2HR8EycClGQM1HTQMQAmjR9pyrp7eAtnp347T4wwwPSDDUg4rhLfqnDIfrC/mNR7EbDhO+fpj8ruP0Dx0h7VeEmTNkR5d0xzbcvlX8OTtJz98Ne3uAIhgmYOJwQVCnKIJHwARxk7TeV0KrwBo9nCGspuEmhJWBqiG2QZAU9Q6nxu19z0fuOMYHbuvz77cFrlsbsO5WMF1HMYIkqCUggbFtLoUAyvGSkZCwp7PMo/Yqj9+f8ORzVnngkmPJO5SAzz2S5KhkOF1B3GSZttoISx/CUJVnv+cm3n8omZvGN1r4zw/s8j8fuxtTAW84EnILvPTKI/z5Z46w3r2Xyux81C5USAzoGPvSwLdt9zx8/w6eckbKY3akLDklc0YiQuYUR4KYkLr2ER4W00TvDhyFmyT6zRUVCCb4NSO//CY2Pnwd2RU3Eq6+nexoxkBytg0Fr8IQwReqlBUeLQd4AhuJQuLoCegZK/jH3Y/upeex/J0Pwi5YJaSKA1KFoQjeOdKGFVMtx7RwT+FqyxpmmWqLZjipXYESyBS+ui584KY+b7vpOB8+eIRsuA2HoknALMomdd34luWIxNG9mpiIx/KAx2MSUBcQ5yAozgvnb3c858yU591/OxfsMhKXgksQlAR3ygRWKLSL4yo8/T13ceWd/blpfKOFFz1kiVc/Zld0lLhiVDb4xSuP8idf6CP3loYlhc5uIOJIrIOimGSo5SRhmTP2rPP9Zzp+8vwzuHCb0JMkOqK9w1c65H0lmCpBFVWHHBqQ/du19P/hs+iHroNjSmaO3IGYsmJGXxRvDkVQ5xBVohbGyDz0ZiBRNzZLMBfomJFTdLb7b2fbcx5O5wcuQC8+g7TncZbjkk5rv7gvxC2sYU0zm0rcNN/JRpZzxcGMv7punXfecoKDuUHuccGhaUGEWjE+aKGwljyPpmQcTovegsaHDpyCs4RginiPDhVJPGYZPe959BmOF563jWee0WV1OaHjZGLE26z/q8RlpnjL6Kvjye85xBUHB63vfyOHn37wEq957F4kB8jBe5IQ+MUrDvG/vngclfTeIUwV8Q5Fo59GM8DjSBFJENtAQw+P0HOBp56d8vMXrHDp6V1S5+hUTMJmO54W7m5cPgwMrjnI8G+uYviWz5Pfchw1jyMg5IVhC+Y8Q4wUKbR8R4KPvIiI6JAnCnZxYBpIXAJqDCXgRCEIKgl4j/iMbY86gP7HR7H6PReQ7lmq0Tivn9yTuKQa0RQ80/xUTWZrAASCKT4IIVHykPPxg8ofX3OUd38tp58bRCs5mokeQMEUA0JBtxQjpzGKiP+VUkwEVJDMxTydxpFfM0jLwiYMNOfDX1c+ctsRvn1Vef63bedHDiyzd8kDgZwUbwFvZa7UytQm6WsMNMMkNhVnseF8swUxD2bRdytxHspwBBKC9KJj996gSxIIRa2JIJaARH8PlgFJoREqxyTwtpuU99864AcfkPJLD13h/O0reAwVITGpO6y5+8ybHMERcBb9R2hgaIZ89k76r76co2//PP5IjphHEJaADMglJZjhECQIPTzR6eEwpFjCUNBS8ghwJrgAhieEaCqm5sklGpBdA8kDIU/Y+OjX0I9+lfxhB9j24kvxz3soya4OIFG5cGCi+MLKKcvV1kfayn+qcElVOJWAZkVM+ywzyVygE6KzLvicLx4W/ujq4/zDzRscHySxIUnpOCy5Wk5vOEZSDLBSKDWmaUeCqxhIzOtYqEF0ppY4UcwEJxkqKZ8+Klzz0UO87owhv3b+Es84r8OKKSZCXkpuNcS7GuPGjvXJUdObEfAUdmplLUyF4LIYIw2RyXLdk7iSh5TvSeOz8rP0F44ii8fOilqMA8Vm0jtVOJNQtBEKOqq4MoSi2LEMR0LKG67L+cjXjvHyRwrff0BwSQ8V8A0vc5tJ0ha/WZzPYegDCYZagt64RvjjT3D07/4dd3BAoowWKAjCkGjmJVU7D0MJRGNcCs7UhVXJKyMKyRJXxvmiPYwN+rgwQugSPncHx37+nfD6K1h+6RNZevoDGfYSPODygDk/7moLuJBONW5TakHT3CszSdWTy5BBH/70i8d51vsO8oYvH+NE1gFbZ7IxjRKsJlRphER8OUO4RZzqEpILPhi59Ljm9sBPfHyN539ojavXhmgY4MMGJvkEfTW7ecakQo0OqfxVBUlJXylA7i1cZcZ1DKuMyWXHsDovpoatpHeP46IAT6xPEOX6DeMFl9/Bb119jI1guHvQDReckVjKsO/I/u7zHHn237P26ivhYFZoq1L8qxWsGJjjX/l9Ho4RjhGuFG5j8daGA1NlcNVBjvzEP3HwF96Nu+44CuQi5PfyFOJE7mVHrfp6Jmf+rPbMZMCNxz0v/Pid/MoVx7lpY4D5FLUB6pbKtyZzFxlHj/Koak0nhxMxxDnMO0yG5MkGIfO885Y+z3nfXfzxtQM2SHDUW221/FWezB0lzMb0lbhqSyi/3Fs4qeBqdBdpyBxcM5xsevcEDitm3FLIEwIZWX+Z3/1Mzi9/4jYO6WQyE8VcYFJmEZwjwMF1jv/Kuzny4neiX7yTjg4I5CjjGdexUCnSBdpXs0/HVT+rOGmIrUmcoJKS2IDe8T7215/ijh94HYN/vgZnQnO6YpqWOY83W8VNzEu3LtZqJoIRdIhZCsH40J1DfvmTR/n8mmBOICQEMcCB5fWRf9SgSu2gJa7IpWYmbAFnxNnH+I6H4Mh9AE24YcP49SvXuer2nN959DIHenlct1M0YBWHk0mf3XShVRUYJU0VgdJQfu4dnNTkPJXoqJHYbFzbiyeT3j2BKxpHXIJnoAlDFzXq134lJw8HedVjdrHNG0Yal2m1dJotLX0JWTFDJygZg2sOc/SF/0Dy6cN0gpLhol/LSj9UpHGsO40rWCYKPBvX/GSElFpcKy4oRkKG4TKBLxzh8PPfzuAX72T5l76TXs8TLRgDLySVZSLzJq9OFteq31U1qKaPyyzavi5PEHL++sYT/NQHj/LZo8v40InTq6MRsDLajUb5xu+yU1Xjyu8iJ4+r5eeK39FJOWSdN914gmdddoSPH+0TLCNzgopiZBPCqmRg++ggk/nWtJ7y+b2IiwwaC7WyjmpmVQW3UNhEevcKriyKgRVtE0CMYJ6//eoGL//cnVF4+EHUMhas83k4xSESMFHsX7/Kie97LatXHiJTo18IzdIPG0tQ13pqeY20IlkI1/w3YsNURJvWFXeb9J2SrBn9l7+f9Re/FVs3TIZ4Aa91PsyzyE4W59ochW0zglV/jlNl4DL+8Op1XnL5MW7NQPQwYn1MPHVBMSdIo3HdIzgBA6+rBFK+cEj50fes856bAy4bkAePryi/bb6sqeku9PvewhXfa4JfpiSzYP1tJr37IC7kHV5zTYc3Xn8E0TizVoa2TtUWpuHUCSEkZH/zWe76kTfgblZOIDgxOhpFlIz+lWbCWAMae5a2gqv/Y/SGVN6ej/MIy7mh3nBZl+zvvsDB7/8Lwp2B3BT1k7KjyZe2/rNVnKv+qH62VUr5eTwoL7+6z8s+fYiNEPDqMecYdrqYFVOs5ag2+mMcZ5X4Znaj/Atc5Wc1qfH7BWZilIXJfMfRAqhkgBAkcOtA+Q8fXuN1Nw5wMkBlXGYtfXUNxjYIr3+vanrVMtTotmLPV0ty94U4AczG7IWJYo4eGJPp1fherfcWx9Gm6LPGH0y2KVuIPic5G1mf//6ZPv9+WIk7K6pKarsAa3YwkYrmU+BcljP8yys48nPvJAyWUVFggEcZuoRyvnVMnjXYO9Z0xqUrxYpS1Zao/F+NizOAYw3LKunRkleT2QEICGnIwIaYOeQDt3HHD78evfUEASuacsGbhr+3KYSmrd9cFOfmaQ9SOLL7GjA1TmTKH1yzxh99+jZCWAUTgsvBUrAhcSNSOYpJ5Q9GuzU90Rg1xUseV5NY3A8opGAuOsyTHJGcOPTFZBKBREHM4y1F8DgPgovrhkZCwhr51kdWEyEzI677dWAZazn8yuWH+bOvnEAHGUEDQXMGmsVqbmFmhVEjIVTrGVWBJBEnKCIG4nDE3W5yH/wzyYAcxMV1blitaGV56qHCm7gUHQQchneCExfX8pwUbeDM8Go4M6TYciMIoq6W7zz61ARNc25eT/mjzx7ixDB2vMyM6kL+tqU9Tc3KTFHNCSg6yDj2uqs48mvvwZ1wpPkgLnglxdThtNRmWikrno2fqjNElAQhdxIHVHTkb5Ny6ZBELSRBcFBscwsoSgrRrTyRr9Xyqoq9yFWHFp1WrE8/gc5HbufWF/4N4aajZKaEYOTDuHy1bVBvG+y3gpt5HlY1gR5GZoHXXneMP/xsnxN+JS4JKNYujTvmFFOicISXHd+yHHE+CgznwBKwAYk7xvZOh92dDivSZSm1QgY4NrKcowiH1jOOD63YQ61IMBIZEgyMzqi6xzUjZT00BNoYpy6mdVy38eufvJNukvCCc7ajyYCeerDx+Bcba0v5qvmWQWQi37jlKMGFIeqXEYaILTBddQ8HpykmHq8Bcy37BktNpjoRUi7ELOu7EOCeDoQNgu8BDmG4ZbpMfBzpBUyUJGRgcRFl7uKq9/HAMZu+4FycaNGcd9465H13bPB9Z3bj3LFPokCsuEamzRjHfuIwM3I18rd8kfDSy5AjngwjrWhAFYOHauMof9GCS9UQcQwEnPm48n3Fw86UdHsKHvKVDuSBZCMnHyisZSRHcySP29wyAmKGKxacVnW6MbfGCyeoPBkvhpBi43nKtg/eyZFf/md2veYHkb2C+MmdD22rDmYtGp2HS6ZJsppK5gLDkPCOW47xm58+QZZ1SSxHnBZbAqQxkrXo3zL+YkFx4kCVhA77UuHivTmPOH2Vx+zby+k9z76eZ7szlrrlLAv0hznHhsItgyGfP6Jc9fUhl9+e85VjG5xgqSAhNPJroaHmlB6HXqb0fcZ62MV/u+Iu9qUJzzprieAEL+2jaj39qmY1I1917E2NH7twlV0OcDunifl7NVyyO3Z8cw6xancqQnUwKMs+0ialhlNyfurCFe63lJDhSG3blunKfYaK59jQcfPhY3zuRM7X1jI2dAmnhrqyfhegTwDxOCccDUv8xReP8ZQzeiwl9QPwmkKq7bdTJTPIP3ILG//tX8iOxEEwkbjlLGZXFQRjguqiaxI3dIL1hOULT8ddej/kiftZOns37rQl/I4lQuLwyx0sV7SfY8MMO3gC+9pxBl+8ncEHbiD9zO0MbzkWhZ1ZJZ/JZQ/jIHWceLwa/biEnuRt19Lf/a+s/MGzkOXJbrfICvdN4azS+2qOdgOVeCRMTsZn1zJ++L2HuaHvwWVRDlshe0fbGypMrmo3xXMxRTRgkpJIziP3dfnR+6/y1ANdzlsxOiIEKVT+4vW4BaYcDRhpciaGmnJoQ/nUEeN/X3uCf73lOHduKOq7YGGUvyDRt9bUAMtRWAAUcYaooQ66wx5nrWb8/ZN3cfFpPVJ8fUTVnCCeEAJPes9B/s/BvJ7eSKNsY7pw3jb452eexoOXAyKddmy9TU8Pi+I2GXINxS61TjwayBmo8YufXOPVX9oo8i3L28y/EmFGYo5/feZ2nrS3h7oE52zr9JohFmfYcoQ71jP+z6Ehf/Wlo1z29RP0dRuQM/batNhBRYSYYU6ilm6O5QTe+6zdXLTb0SGZvVHaIMeKY1mie2Hw5cOsP+/v6F9zBDHFm5IJLFvCoHipyHmku4AUm7fjEoekcFSYOBDFnbvK0g9/OyvPvhB56G7cqgPzhdlbbs8ZT/mXVWViFOojeQ7csk744M0cectnyD94Pcl6KbSs2OZjODHqC4frGpbi6ACBIbkTOpoQloXl33oGvZ+7hK6PdJWlKrfxTFZh3Uc1varruCnng0QzTAJkibKWO1764du4YbiEJ5AHHzlC2UdbpHI5gJmRGIgqufdoJ+GRu5SXPmwPTzk9ZVu3Q1LR6tKmuj1OqpA3UswWRH/GaT3lGb3A4/bt4oajq7z+y8f56+uPc3TgMAEnhuWhYGJprpWtt5K4CRYKoahC7vvcMAj83OVrvOW79nHOkkTTdSpnG8SOmMPYLKwo3rnAkgkm3Th62bjiRgMHdXOkWnFbwU2QPMenkBblFYsLcLW6LKCskWrl1IS0VMwyIYjiJMESwWn0340Vz03SV6m6FNi9LDy3m/D0M/Zx2S0b/PoVd/KFjRUSjqN0os49hT6L6h/mhAzjmMJ7b17nEXu2j47nnkofcfWUV6MvgfRIxuH//m70msMkGkWIkuDNGG+NLzWa0m8U000kntlmdOgnRhoGyNmrrP7nJ9D9kYfhD6zikgo9bnq9jfreaOiHTgrcvwP334n/oQfBlbdz+FUfhPdej+ZCpsJ2POuujwtVsVDXDD3llp6URCFzSrphrP3ee9l30WmEJ5+LyzQWSBQmlpoWqS64imDuOiwrGpkJ4IYMzXjVZ47w/rVOZLMF6iuNqw1zMpgYuTOypMvp/hh/eInjHd+9j+fsX2ZHksZD1Ub5MtHp2ukrKgkQiU7WVXIuWk34g0u28f6nncb3HkhI1BNCB01TrFwRGl8aNfqao7zaOSTBZzu46rjnZVfeipaddxqj56RXy3dOaJod0/LdLG6WOdt0BywynV+Aqxk1M55b3kXznYXrmiN4Ty9Vnn0/zz8+8yy+c++g8GfWPOfT6SvSU4GP3zpgPZv0VU042SGaoOTkFhi8/nPo266LgtkJWvEPjd+p+4hKzSUA4gQlBxuw8oJHsu+yF5H+l0fC2UvT+v1U+qatcQJIVlJ4whlsf9Pz2PnXz8M9YAcrLueoz2rKR3NtVnUmsfTIOTXAs3Kbsvarl5HfvoEmSk4ghKTVHz6Pvlm4CYEllRHIcFxxh/InX1grNvoK6qOaWvcBTG+VzhxOlEfv7vPOZz6An3vwLvaRYImhPuDLZAqHW9Xx1haqGCsO0TNLEXGIKM7DQ3b3+PvH7+EvHrPM+f4oqCMxV9FyqGs/ZSVVRmB1gvNHeeLuLr904f5R3lM78pz0JvKdE6qzJbPy3QyuLEP1dxma8TNnRMepjss3EtQTGVPvsvWwaL6zcAJ4PD4kuNDjwcvK65+8j4duy8fWzTz6ynI448q7hKN5iAeUzs1XUQfLnz3O8VdehuBJQ04otJsyx6rTujSxqrNz3hxBhO4ZKXve/FMsveY55OftIMETFav2DjyNvmk+obKtpKQk3Q780PnseP8LkR+9AGcZnZBQ1aqaa7NolCMhIZM4G8mVNzN41cfI8vLon8l8F6VvGs6VoKpEMyCQsRE8v/apO1jPOiSWgxYzdnW7Z6I9igQScsQZqXh+8twl3vZdZ/NtexyJEyRRvHggjcfvzpC21biJynKlv0swScg9QEIv5PS6nh8/v8fffv/9eNzpIVreopRnMToLCBZXTIRYGQlJLI/k7JUBL73kNN7+tFUeukdqkr1doDZH72bcdE1UaN+72Fappwo3bWBYFFdJvKWM1WeLq/5bpc9cND/EB7IUcuc5Z0n4rUftZcmqM1cz6CvjTTgyDNw8gOp5Bq30IWSABuG233wP3D5ENGPowWtzM02lPoh1bgIiUfDlYrhLzmD7ZS8mffZ5+FSjq8QlIEmtAy/Cl3ERJwWcihbjqid1HfxZS+x47fNYfcX3kq/EOUQnRu7iFRe19BrpZxT6ixlOhcGfX4G/6k46KpUzuqZbJtME8DRcq1NGUJIg/O8bj/KZO/vkJHGXdrmeqSJpixfGxTFI8x65M3rB8TMXCr/7+D3sS7LiEDKPE48IeIn7tpqaQetM5TSck8JXBSkJzjkkTePZQR4esc3xj0/Yxw8+YEgiPcTW48l/ksRlK04wH30CZvHY5UftEP7umfv5rxcusYrCQidUNjSotr4qDb41U2ioxc24k8VNW7rSfGcRHFDRJhk3i5oZXJR3ohG2d6yt0hfbkEckIbW4bkhUeNQZPS7YXclyFn0VTVGccssxifcDzMhXUZLg0Pd+BXnv1XEmUOPhlNWStpV+ZGBJiiUGTzuH0/72J5CHbCcNHrEULzLS0UYm5IJ8KePaBIDHgxOSot8klmBdpfeSS1n9/e+FfSmGw1uCJJOCthoccd1XKL7pkcBdr/hnbCNu25kVptE3CzfibF0FE9YG8JprjjNEqDlaazwpemXV1BLIvNLTZV70UM9vPfw09gqoT6OLUqb7Ktqk7cnggqU4y9mxJPzppQd48XmCd9tx1kckHruvopjkGIHVToefvTDhrc/Yz9N2JnRNCKNTNWetch8RNv5coCKaoekzqZZ3QqvYAq75bBYdi+BqZpZV4iaeLWYGnwr6BCM3EDy7k8Ale0q65tBXmlSm5JJw8Oj6RB1OaoBGUDj62+/H9R3lBuZoZBYDSWWipfxd+rFS58lN6Tzxfuz7s+ch5y2RGljcp39SfNnUQAYkwWE+0P3ph7Hyqu/D7/SYeSRUyzG537Bapvhd0XffwPGPfXW8vGRKvovSV/2sbc2pdvy/vbXPl9YEo4OX8m5AqwwXlbFD6r9Tp/zQuYGXXbSXbT7ERZnO8OSUToVFTJuTxanlZDi6QdjWyfkfj97Of3xAB5/3ig0LAihJCFy0Q3jt47fxiot3cHrXEzyoN5Bi2pqxhtc+ysl4pJbC9GjCpphIxjjNpnCpapbVvLeKq5MzWY5FcWWRaxpWtXgi43YxkeTW810E15FA8IIzz4EdK4vRNyqHYS5hPShi+cx8czOy911LdsUdDF2n9qyqiVgl47GzXcg0I33MXpb+8rm4c5YRc6i5eHit5ifFl6qvdxZWJFoo6jw9S0hESH7oQjqv/G78ckZe3gNa+dcsXzUegSTv0f+fH0GyyWObtkJfFedGmVvANBBQDg2NN3/5EAN1qATUisoYezCr7MNpSkJOEjxO4PGnCb976WlsSz3iPAlSuOyTtv46YQJOk76bxXWcI3UJ4gXvElZ9wssf3eMH7tct9gEp28zzMw/expufuofnnLtCJ01wCJ4EL1JMCkSB1RQC9Uwbmkb5WtN0lpbGxqS52/a9zSzeLK5pUk/zH07F1apexr9HK8jHplV82aiv66H+/FTTB2jwGANUHKE8e3sefaPXPS4oQw/FmTSj/EZ/weKlJxls/MXluEzoaDwHtBRHZXJVZ3XujERyzOUEH3Dn7WLHH30/nL0cafAO5+OCncSNjyHeCl+m9Y22545CXjsH4kg99H7sIvxLvgPv4jVguQ94seLYqFjGaknLWIeAJWQfuIHBVV9HQx63OWnc7rQV+qqfrszUIncRzbjizj6fvVOjpDewivScGB0NcCEexeoT9neG/OGlezkz1VIvIe4vdIWfYb5KO0sr2BwuKTpvzN+cspL0eOWjtvO4XcoDl3L+7PG7eOWle7j/8hJeov/DSXm7Tkm3FIrTLPOoLa4YWat1PMM6avPdtZVvq7g2M3LaCD4VV4uuaNzlUpeqBtN0F9QT3Fy+C+IM4g4MSyFs8KXDw8Xoq47FouxK0/a8iwnyDWeEqw/S/9h1hQ5io3PvocqmsfDqmHHCe5Y1IVnusP3/eyqdi0+nG8Ak3mle+nSr/NkKX6a5Dab1nVG+CIgn6Xp2veQJ2HcfYEUTvCXkUtzEUynheAFsDMGU3ALdIRx9/ZVk4vBq8RBmlS3R1+7DAoIZuTrect1hjrEEBFxu5SHQjaooep8I6oZ0QhfjCC97xF4u2BXIU8gdYGPfzywHri0gfU8WJ0FQ63NgxfOGJ5zFPz79DH743C7LGteNVN9tS2sW/YxmoKwyejc6edkgpoRF890qrho3i3eL40o7q/guVU2lipmvYZ1S+iTHieO2TLjycFmv8+grNUjDW59ze0tog2wDCIqSozgGb7+Gpbvi8tF4E5qrCar4/3h5aDBFgpClQvKjF7H0gw8h80YuguipqI/JwWkRDaaKEwwJsCEgexJ2/MYzOHZmB1EhnuA1Pmgw/l+qM/G3L1bne0vgH64hWcsJFlfbC3bS9FXvOyITx8ENz0fuMlKLI+wT+I8AACAASURBVFNKSrznqSm0xg3R5Y5hCk84bTs/dv8OqfXwoUOqgSD1qc1pjK7aqbM0p5PB5RKNPVXh3BXlITssOjhTR1fr7zbTmWZvj4ONBVKJa5a1FGhTwqL5bhU363OzuMob9fLVTN4Z5d1ivovgXOjgTXnrjRtcf7Ry/dpM+kajCt4pB7YJecu0vBVXjHc2jBPv+jwnJCFIvFXIUT22RUb/lzqWI8HhyM4UVn71CdBJcepxzmGFUrDV+thsvc3CBYZ0gpCLJ3n4fpZ+9lLEZ8V+0tJMHmtY1d+5BRxKH3DHPMff83mCM7xYvFfxJOlzZVZBHKn1+dyxY1x3zJG5OBoN4hkVI4JiHVvNzHEIHbfBb337DpIkQZzEI1+cx1fWkJQZz+74d19InCeVlCQRvO/gpUviPIkkOD+5dGGaIGwNbdpUE9sW15rUYvluNm5Rvi9cP03YhAZZlHciudmNdLP05aaEPCfTnKFBrhmXHRzwis8ORprLovQ5CZy2vMKZywEL9dMHhPJ6uoRwzW1w7Tq+3K2kVhwDUHFAj94rTxZVXBLY9l+eQXL2Kk6EpDhi2NO6wuik+LIljECSdEm9o+OENIHVFz4SvWAv6lI82QjarnM7Sj+hWkZ41/V0KA86OPkyuDLjoQJ0+NCtG9GpWPNbVRIpf1cEa/A9vmtfl0v2F5tkBShH/RkmUDPMNrlOHhflRV0jGa3eXaCyF833VIe7my9bxlnjc9r3TbJt0/RZIG5vNTTPeM+dGS++7DC39uNaqYXpMwiScPHuDttST+oyqiGKnLiqO7viZsKReCOUjF6vJ2iNzHKElf09lv+fi0mK43FG49wpbH/zXCWzcVXLhHjO2J4Vdv3kpfTIyMSNyic1DbU0D8eaJRjr778GW4fcOUJ5Rd9J0DcS66lAPzg+fihgXphey9Xfhd2qfX7mgp0kZDUzqDazMofZTQfcLBv9voCbHzbXS++Jcoz8FKcIV3NkW71NxOcV3GQmp4w+MYfPN7hlqPzOp4/x/H87xPVDR6oniCdhLEBfUQ5Hzvee1aUjtV2IozcdCqroB67Hjy7PnTSPys5bpd6Jx37qYsLO4kSDU1wfE3yZom1vBlfelJb+wIPRM1eIM3GlOCr/6tuMGJVbWL5L0WtuI6AjYXMy9I20NCfGwfWc646VanNDdR6RWAwLZsWHcN4qPG5vhxxIW46/bRI2q/PHI5YFsRDHrpbjKWpVJZPHvZ4S3Gj0jsd+VMeNxcJ0rAGJaqFcF3O0d1c5GjSZxYaVoyQWEDc2e8p6mdeQxi9UzamRqjBqJlWckyE5RnkQolRm1KKwkHaaq3FW8ikeoDg040g/58snBrz7xg3edMNRblgXkHj6bfDb4m5mCTPoKxzeLh6TtHdFeMqBBKPYM0u1vQqJKsMTgf41dxAKkdQ8bC8mXx30iw6eGMs/cSnecoZ06M0x6efVxzxcWz/bbFzczGbo/VdIH3cO+Zu+QOMK41FpqzplyZl+JvirbmT5kafjGsXYCn0Vs9Jxx2DAwf46hNW4fWWi5RXfzeEkFCQlfOd+2JOAc57q2pVpmZaNv42woHk8jlUz3n/HgIRua3p3d1ALBBKEIcvO89j9nsS687WssvM1f1fjPRzswws+tEYvCZFn95S16Ye43LjwtGV+4+Jt7JyyonrqoFK3HirCpvq88HFW/HpOe/zqJ46zJznIRifF58nWy+xyRHMGJNwxcNy0oWwMs4qIcCA+HoczGmTb6RNzeIunkIgkPPd+Xc5acuTO8OqgcsmCUFwm+rVjcHCjMmNGRUjFX+XvKgfTi0/Dn7eKw4863qzBe6H6mIGbJegWxcXz6OLdnUvfdz5H//FayLOaYCpLLBMcAEzIPndXcX4doxNct0rfSGAJxnXHIWTJaGQps25qC6KKeYlL77XPU/bvxMsQQofgfDw/aUbHnjVTqJKQhsC67/Dj/3YHh6edq3E3B3M5ab7MMAlcut346LP3F/sN55iGUukZVnNSjDHBccwcn7h9A01CXDO0Ke1ta0FMSE3JXIfg+iS2o/Z8XiOOD5q/m+WtgsZtR2XAlWsZZilpJgzTrR+RjBhJMJwOscSRmcYLc2tmX5Pv7fQleU6WGmIp+7t9/sMDT6NroObB5YAfT60XnVBvO0p+dECcqncjbaruw6l25Bh6T3lAPAPRxZtzoF17KsNC9bEJXBW7MI5ofRGgc+l5sOqQw+XQIKPyNQVXyQNDGX7x69F8lpOnL4kJx/y/cmIAtgQ+FCPRNIkeD+/HHNtcziV7luIBiFqYio2D7iad3+MtJJOMNlSFJa/kiWFhdgHvtuAcSE6SJwQ/ZPK06inBIPpEmCqDxA1RVSxJEUvjIYNtN8mc6uCEoQRc5lDxiNXraSEfXZs7s3xNpj2AQIdEE1weCGmGtPm1FgxmRi4peAcSz3Ays3i2mhv7WBahLysO7kxV+ekHbePh2yWe/KkQfBzPm7Pc/pYN3EAxsdoGh6rjoKpxlCR0HncOAw9dKw3H2eFkZwfbBtc27GycoWbRNXPuKuGsZfzh4ahMbYKqOdmQ3HAElzsak65boi+JScbI2zes2KUwpVWWPgQRcOACnL2jy+7UEfAkYpSLw5pSsk2ralNlPYoljmAOlQ6m+cR790hQI7gMl0PAEySuQJnfiBomSCMawOUdRDwhi5vB9Z4QVsTG1TElSwbgFJFdTD0ZbnoilVD10UyoXjWcE8jZiL3VXFwf0LZlZ5Hgh8AwnmgQIBei1Jk4VaONvuhpjb8D+AQJcMkex3960A6cF3IcaXUpTyUVQ/C3HouXpFpe6bjtY1TclGYEn9O56Cz6xbS/YqOz4O7JsOjk0VirjMekB4272VYu2k//84ehRVCVYWyaF4gTGfmxHLfbM8mhzdGXlDkYwqETirp87HOpkUBFvXaIDBFJ2LOSspIKaeE0ndb8m36rtt8x5RRzWRz9zYjbeqYWby4DpuImHMNN31M86Trzhgt+dJ3STN9O2XHa0qtYJsHHyfEyPWalN42+LeAMI/NgmiAhzD7yeZTcrAbUIphb6Su9Pcn4eCWBiVNAFq3K4owrk7hKvPXF0oc28SiF0MHJECMg5ti7Evjtx+1j71JxAJLE0alt3ZACevA48YQ1GV0uUeoadROp0LscpDt6sLfHsvhijKinvllBskg4Nbh4NZslcWsN5+1AoFjbP+Z926boMgyygN11HLd750nTFxeOFuZ930skKpQTlu3BRBGNSu3urtBNFhulF1H/NhcWfb8prE7Ow73l9ViLvnd34uawbJr5PgU9J7H7Js7JEE0C1ltiW8f4/Udu4wk7hVQKv8u0JQOFDBocOxH9uDT1yPKzHHxjvzINpGfsRLyrp1cJ0/i8Vdy0uLYwCyeUVpegDty+HTVrI/bhqj41fg/AnOCDkh8/QbM+tkKfM4sG4ECVwxsnikP2Sj/AFMHlCn+VeFbI8VMIaS4CW2Qt1vSHo/+mPNsMrqIFjaKkBTdJ30yH5dz0Fs33bsLV4qaHZr1NL2813baE7T6IC8AQfIczsqO84tLt/Og521GfEKQwcKb6hIoU1vstlkTsKzL6Hk2oIIqgDLclE5rvonzeCg7aFYKtxBUX+eEVuttXyYuV+WKTd3i3eUI65kmM4vark6NldPOzUdwKYzS2RFdsmVEJDF8c81mOM20LRJuLwKrO9ukCrBFXYmT03yRONomTIr7VdLJJri8U5qS3aL53Jw6mlq1aH00nc7OYrWGqttx44Z7GlSeQSgByxDwqXfaGjJc/9nRe+MAOPk1IxCEyaQTW2mlhYXorttLIpOvcSiCQmRZrlmJfaaY3i88ni2srR9v3ubjCPeQKKyxd6qGF800Lk749tULLNDAUcYKE6b7aRekb1ZAXoYPDLBSdvymoxjY5Es+gFrXoVJSx7VodAUaOu4bQauLaCjo/nAyuqcxX4bPT3TTdNeyi+d6NuCltoVovC/tJapAGLSOBKfcyLkdcjst7cVBOjQf3hvz+Y3bz9P1LJFLOBLpW/2uVL+XYEK+Jh7jI2VMXU+PgkaiFiJGUPp4F+Xx34mbV7QTOotCRQpfRwQAvxcbw8r7RqX3RiglzKWYaN5HvlDDa/JyKsJqmBNeQguXafBh/iqA+IBpYDwl55WiMpo296GrWaiEbJZmCOQncRD+3KabFJsK89BbN9x7BzSjGPEE1RXkdH4xXxstYaNybOPPIcAlLjJQNnru/y5u++0yeeUZ03DePkJla7HJyyAS6KQGjPEzGRn/V3XXxf28QRJDj/Vrd3BOO81Phw4LxindR6N91DIYZo3PiRu+Wpa7+g4BGU9s5mrlshb7x1hyFnnOYxBMItWwItQYapaRoPMbVMO4c5vTzQNqZVKdroxOTgqr5e7qDt6ohlOp5ReMbSe8FcSPlsUWhH0VNNoBmeSbJnJHeovnenbhWkmebHTMblVT4Xc74CUxkeC/ixITEK/dfzfnFb9vJj5y9jZVuQq7xJiQzJi4lrSVR5UvxX3fHKutOivXVzWn98T66oTPS4kYZvf0wplpHzuDzovUxC9emtTRn5+fioBA8UUDna8fomMNEMAu4uBCJqbOEIuAcSad+hPRW6RutwxKBvUs53rqYK6fdy4ZRbRQGlgNLGMrtxzLWcmUpjWefm2VIcXFDmz9r1qpcs2J3vXrUKYkmiPQbDGAyLBjnnCNkhnPxng9tLpkY+XtkQjEr6Zs54k34eyrpVRMsj+xxHiQQj7FcrAwnF+fo5I6MgHMpWI5IOtEJpi3omzsijprLpPov5nEoYgGnXbJ0SLlYVoh+ITWKC2+jwDWXxrTmmBOz8jXneNI+48++80zOWgJxxRHYbrSiZ+HyjnSIvcskBHIBV+6NbGG4L2bSxRzhYCBby5AuCCnOxc6+UL6L0jej4zffbaYzDRcsLiKPez8N/crhyAcDiuvtJ8+vr9DkjOXUk+/bRnd0zd7W6RurRQ4OLHUR1sfaVdMULDUsFNOAeuGWjQ3W+oGzekZwHZzG4yea/qpq5s1QnX1THEkeUAnkMsBCZwK/1aAG3mUEG4BvrAkqv8wwn+qNYaIQ4xdb0yuEllncn+UTJOSkmhZriU7SJF0gmANNhqhLUB22NpppI/govmaOUdHyKkyTysOy7hkCRt7r4cMxJF8pDoSLQV1AnY/3VVogyJAkDMA8QTwm9fQWzRfN+fihlMvvGvDDZ60ghNGAOq+8bXwRE/yBHcU+xcoyhRZfb7kBuKQzv+Y2uvvOiZeyNNrZvHw3i6ti23zKbZpZG86JRLNZDRQGV986KnFZ3qbQqq1+V8hWElZ3dCoD+NbpS0q1T0V50EoP7BCwNGWEjpFqEvfVaWAoHS4/mHHBjg6iATVIXJ2AmbNOzSxMUe9Yx7OHnJCculXgvSD0nSfTHjbMydNaj4vla7akRpiqZTVN0mnpieC0g1nKavcwAehowslsVVk0GEKSdbA8sANhoI7lpF6WWU5bazS4ItEYVxUoIvXvZqjvsdfW6fczXKeDpZXbw82ADTQ4EuuSZcLQr5A7BRlE3mg9vUXzRTzHcuO/fuwwFzyjw4W7OrVtVvOc1LXfBqBwoId1HG5ArbNW7ZFmKzKMwQdvovvkc2JNmK+ZojPzXZS+RpjmcpnnU67iSjnoRJHb+iQ3ro+u2ijLVS17VUgLQmKe7Nzt8QYq6gvLt0LfyCRUU87d5klSCFnB8trIX2oIEi8v1TwubdAO7/96n588b5mOxXUni6i0TWlafrqov7HNTvDeHzyXpVPYj4Mqv33NHbzuCzna7YJmTDStUSdqvDzXLKnyqy29cScKbsCDun3e9j3n8JCluKewKueQGZ9sHadiqCoexzrCimWYdVp9H1NH8CpfagK5wRurPBehkx3nb75nD0/bt4Li8KOrWiJvVI1D6rhlo89XDh3hsls3eN/NcMtAyEIgHjMk9XQXyFcskKhwU9bhpVfcwd886Sx2dUohU2hCU8o78RshKMi+Vbrbl8ju7MPIvTzurELZkceeHcMIH76ezB5Pj6jtTs1nwfqYh2vzB7W9PxMXq4cMI/vUVwnH8wmzr1reMR9inBNYvuAAwWK7WyTfkTYbQTVcMvItScLe3gb7l1a5IR80OqeMP0dfhSAC5Hz8lsDBfuC0FU9a7HCs+qyqoal1NYl3zhWK9ioPWqY1LDrtPoEz2JN2UeljYVhyZPSsWdRaqJizrb6cprCaSE9Gn2LCIIElAU2KW6qb5mTlsyosRuXZAs7BaL/dKkwVVpHM9s7QxpdRpqOlBFUNJz7PXIcl6SJOSFRAxhc2GEbihX0e9qXLXLy6zHPPDqxdYrz5luO87po+nzt4ApUlUCEvt48tkC/RCAQ1PnjTBr//uTVednEPpwleMpxfjmJvIWEAzhmdc3YSTl+GOwcVIVU3jRrDM4Iw+OT17Lz5BJy9AyxA4/jwWUJps7hp1k3TjzwP5xCQnMQCg3ddi2VjoVSKqapB2Nz0PfQJ2y/YS0KOSHdmviAEjQpPcEKvIqhKnKt2wr29lPN3ZC17qKz+Weq8xbu35h3+9Wt9Ug2YhKnq6SynbbVT3GM4q3wpG3xpbkwm2Np4Fk/PaE+4GJFaaGybpDgZXBXfLMe0dJu4Vs2zVkZqbWNeaM0XxSSwK4H/dO4S7336Dn75EbvZnjpwWcUsnJ+vSbG5XIVhuoM/+fwRrrsLkiTDZBlnkzyYxhcj3n1gSwnuIbuLHR6lSdTQSkb/xl05HTrW3nQVWZSQC+e7FVybRVO+O80P1o4zwCO39jnxb1eTyLi8pQ4p0CjtWOvsuIB71JnEqylm02cWUzAnrA0GxbhTx1UuUhU6Dh67uxMzGzGiqnaPtYTRH0Yw+MsvH2Et+KItjZk4zdaepX1NFOJU4WiEmvlXKdM0eTRPs5uZXpWPba9OCte2hnUyuCa+GbeQ/6RNwxIZl7dW7hm8mpWvgTOPeo8mKTtcl9+8sMcbv2s7D9vtQfLF8zWir0hyFOOo9fiNzxxlw7o4pXDmL8YXEcOrEMTRfeJ55C4ULBn/K1lU/V3m4MQzeN1V+KODGhtPpj42gxuxpG0gmoozUCX/py9it6yD5RPlHWtV4zZe/h6c1qFz0X68pCNGTC1HIfACwpET/YqwH4fRNIe3gIny5DO3kSbxsC1GN47U7yJrZIXTjE/eFfjorUOkPPRfbXxUdsHU2ergJNOa7500ru3hgprAwmFqejY3r7IMs7SjU4GbNXrPi2sgWrTH4rs146ak0JavxG1i3pTUwCWC+R5P3dvjjU8+jWef2cMREHWMGtnUfAUzgUQx7eMZ8I5blXfdfAKTMNIFzOK5T5MKZCWmaNe5KL1HnYNf7ZLFHKjqWFUdREYGk5CbkV5/nPU3fwq1aGJaMHJT4qr5OXzZZFzTZ1WGaT6j8rtZcf4BQo7RX+tz4vWXg3oqHBuVsq5jjeNVhG2PP5+wVBjMwsx8VaJwPN7PcJ20WOJSx432EnpxqKQ8ZGeHB64CKniEaGtXNaxqxysqShxqHX7zqtu4Y+jJMTKMgGI2nMq4JhOnCZxTjauFUgOa1zkXFBIz01uEHuoj0CzhtBVc2+8yrvnufP5VtEdpxktFTs3XLKvBiZCIxCO3ncM5IXUCifCg7R3+5PG7+MkHbMcnQ1JttM9mvuUjFXAJQYRcE37tk1/n68PYQSzEbqYtg0qVPhNBnZAA/oIz8OetFgc9xgWNpbZW1TGqZRcgdwlHXv0x7OsbWFAIkLeMZYvWx2ZwMNv6KalUNUwV8oCZMXjDFegX7irOsC9L0rC6Sp2q8E0KipjSe9b5lGverbIGq40+QXAObjyh7Ol0aJs5H53WoC7uxt6VKk873YGkmIAXo35Oe7VxRtLMCY4+nz66xKu/dARPhqNPwCHmaubJIvZzU6idSlx7qDT0GdC5JuHM9BYTVpv1SWwGN8t0XgQXHzZ+VH1Jo3Gs2QPbmbpovs24M73xu49a4scfsEJw6ex8G/Q58TgGXLdxGr/2mTsLDSGPtxpb3QiZML3N8GqkOPJtnt73PZSuZWTiix2FDS2z5t8ygnlMHb0vHWXt9z5EJkqWZKSFJniyfKniqm2/+rvpkmniwEDAE8iTAF9ao/+az9AfelJKjao6M1rhdSGYVBxdHGE1kD7jAkwcTgXPbPoEYUMDB7M+OzvxRNwmLmpYUqivJnjneO4Dd7FXjqM+HvQ2eSqlTAwJZgnOUl599V382+2GZJAYaHHrzSxH/LzKOGW4CQpGD0cVFdf1tHXUZsXOCK3pTX9npEbPUe9nCeBFcUBtAKnGtWliE+VtalKtvjoavqTZGtYi+VZxmiRs73R41SU7+ZkHLo3OR2/Pt06fWsSIwj9eP+DNN+XkeBwZVppps/jiAYVEjOQHvo1sZ0KiCUPiyZzjLh3zH1MmeDG85mTm6L/+SjbecS15qXXMy3cBvjTj6v63uqCahou0GJkzbM24/TfeS7hpjVQ0ttNyszgyEsRV8WUEzJS+V3rP/Xbc3pREonZbnu4wjT5DueWEsTcVgrkJLJSzhFYQ4ARwPHxXl0tOT3CqqO/grHFMsVnRKCIBLleEhDzd4Hi/x//7sUN8qe9RG6LkmCqqoW5NTmF2W/xWcWpgGre+mOQQhvHcbxOcwQRBBdPnaUPTtaxZ6c3otI1n0wTOohrkNFwbv5qYebgJuStlpNUBc7XazeVb92NAD1jtGr936U5+6KwOzinOEiZOqG3SZ0ZwQDhOP0v53avu4qb1gGQhSg2Zni9C9MN4wamRPGwPK48+ByEed1xuNBLGdTr+33AWGEgAVdKjyolffQ/pZ9dGrMqJN8uYadwSs0m+NP3E0/hcTcPM6KOYjjfjGTl+6Dj+J5+g99av4LQQViFaDWOxPK71sQNeEdchd7D9+Y8caa0qhdFYFZwQT3EwyMxQDfz77es8eHcPr4UgbNA98mE5iTN8TmDFC89/0C58Yg0t28rSjmsQUC8EVxTIwfUnMn72o3dw24YjCR5FGIojiAJhguE1Kduivm4V50wJxFkdNGE9T7h+LRQaZXksGWPzLSbAtDDTHLQxP9rTKzrNlOTbytccHU8W12zczbK1aacLm8DV7+VPm17ezeZbL4NHnMO7Dqsdx6uetIun74l5e11vyVeqCYEZ6hyK8fljQ379qjWOSIfCizU9XwMnxSkF3iFeSF70eOhEp3lIkopZWIZSWAo5QlcdVk7xf+kIh1/yFoZfO06QnCQPlIsFnG2eL/Nw076nGnkhpogGghnrb76a4e+8j34IxXHlHlfhYymIq8I5xiQk5PSe+ED8Iw/gXNwvOTp6ukZDwS2FRAM3rxu5N3okcT+zTJajtg6ryuRnHujxiJ3GctDJs79L7aRZOWZgglrKhw+m/MInD3JLFnB5oJsXMtmmL8CsdrJZps2iOAQMwYcNsjzn9798lLff1AcveFcKFalojKOXpliFM3rfaLiZlZ7U+k7tdanzZdYoulVcm4O2+m5V/Z5r9sa3GDGqdrpnVUBAKzM3me8s3D4xXvOkM7hopxJkuZHvbPqUhLfeqLzxy8egZQ/6rHzFHJ2nP4D04dvJnaOTV7fp2Oj/aqpW+eaBwUdvY+NF/8Dga8cZekHU4ixpw2zaar3N6kNVopwEsCF9E/SfruXEz78NXTe6GhpUW6UcNoobldc8IRnQ/aXHYstuZr6GIgrmFc2Vd90y5LH7tqMMqR9rNX5vfLZoQ5JtTx2/cuEOfDetmIRF5xt57xvVWwqRoBhD3nFz4Bc+dgdfGChWLh2zekVUmVzVCNr+NoszMZwZ6+p45Rc2eMWnjserm0Lpm7PCWpOxJjTDgpuvbcxKrxI3JTRV+jYN8mRw08qwKG5UnkaR618qPJjDrlNFn6PD/m7gtd+xg/2dUM93Dn1OPDk5r/zsCS4/lNU0pHn5CkLaCWz79WeQeyk0kOpoV84UxrixNhL9P1mS0THH8L03c+LFb8ffuM5QFJfnI13vZPgyU9hWcRLbULAO9obPc/uL34ocj5dMZNRvuhnPfY79dGMNSzA8+tQHsPod98MKa2pavmqCBMgZclNIWOtvcGCb4NWjEuIx7I1yuGkN03B8z4Flnnx2vBYpsQAS4u5DM0YqxUQnNIKP9xYq8M5blJ/7wB1cdVQJKlF1DoEQcnJVtEVbajqQy795uKA5GnJQRU0JlrM2yPnVT63zh5/aYMO6aFCwQO6ShhCRkvziZwbSZSlzmF+PTa6h0TSKPTs9rG4qtiWxgJl7MrhZWtOiuAlTq8yvplHauI2UmuVJ5jsLZxLoknDRacv88XessM0BLiEJymhqfAp9YkowuLmvvPQTd3DjekauSh5y8jCbfyZAEIbPPJ/lZzwIh5E5xYvGblIxB0vWWUVwJbknmCLq0HfdwF0veBN69WE2vOHU2AhGZsQ7LGdoWNPom9Y2TCE3I9e4h9OZoQPo//GV3PXL/0LnroCNxvPGHaMV3XGkTYrGW3Sc4nYKO37pu2A5LnGY1WfEjKE3fG787ReP8JQDqzgB71O885WmVRmc2qWfgAouzXnZhWeyJ80g69HJBBfK630KXJMYGasoRrwz7oOH4cffd5A3fnWDE0UDFqQ2IdfUkqr0NOOm4aJrUMkLdl57WHjhxw7z5188xlHXB8lQ57HiyLExvfUixZDg8gEbScL+FRePiZ2rYc1Kb/671fI0R8dThZuVd3NgaAe2RVrjeyms55uVi+Y7C2dOEElwQXjGgV287BE76IUMfLfYejOdPkWjtqApV9wp/I9/P0o/yxFyvIaZ+SpgiWdJjNXffCrZHsFbhw1xDJ02/FlVs7B4YlEnUxQ1GH7kq6w9769wb7uOoRqpZrgwjMJgCi+n0TdNeIkIGQHyHGc5uQ0ZHlznyEvewYlf+xfs+KCgz40F0pjiOt8xLHYMungUx8rzH073sWeBM6Q4lbSaf5U+F8UMN2/AZ9YGPGr3XWL4OAAAIABJREFUEl7iKRZOfDG21PN1zUQiIXGZl88dD93reMlFq1h3QO7AqxFnYqzxR8sniIKXhC8PEn7+w7fzkssP8qW+gcRGUZ3Sb9rbbfb3TJxAsJxB3udPv7TG9152G//8NWXoAhK6EWAVmq3y1wje4uF62/wJXnD+7pHaPF0LmJ1eLd9pKbQMHm0O1ZPBzQuL4hpv1YslVjEJ5wutzeTbahrhoj/EGUvAi85f5gUP6mAacKWAmkJf1JwVExhKlzdeO+TPvrqOalIciTIjX4uX1QcB//+3d+7xlhxVvf+uqureZ5/nnHllJkwSEpIQkhASEgN5EMJDXvIReeXeeAH1+rhX/SCiXPANKD4+vlAuiiICH598FL2iHxGC4Ed5GFBIAkQICZDXhGQeyZw5r713d9W6f1T33r379N7nTGYmk5nMms+Z3V396+qq6q5Va1WtWuuCeWbf+Dy6rks7OJIghZRV3jdQoFiTFvOyJHD7Knu+/y/Jfu4jmD0ZuU1QMevGnG0arJq+VVXFoaw6oeMtnY99k30v+2Py930Rv+JwhTBRSlH98g0mafv1EAQbPLmN5ZPLdzD5k9ewOhHAm0rdmxmox5AGz1u+2OV/nj0FZm3/qtfD1C9SNJ4BvHFA4HVPmuLFO4TgBhsYK3dV6lFXg4ge7H0PkymLMsn778x49t/fzVtuXmRvz9CUY7U8TZLDKJxmjn/+Frzg4wv8xH+scvfqBOgSrpsiFHMbpVom1b+1+XubIep45dnTvHhnwexo/nD7VR+T39Bz16tHA2Mcmjs5ArhRz9/YZHuVtFavyvewTn0P9bkjcUFRY8hNIIhn2hp+7tItXL0rkJt0nfIl0XON7WJCl16u/PKNy3x8b6cfkXvUc40ErIIJcQXN/tBTSV9wFoGcrrGFZbgOnts/qgwqxbEhGlZK6OIWLKu/eRN7r/0Duu/5HGYhR8dwrEN5byJCJtC65X5WX/NXLL3ir3GfWcL7ALbXNycomdRgkn3ty1QUhwWv+E3CzM9eg54yTVvT6IwxhKHnrmk/lM/ugTsWc563K8HoBvp6CEGrKoNq1b4iHgXNuXvR8J037ObWJUWdQXo5mhhcXsRzG3qOMggEoMWvFIWEEAMbss11ePm5m7j+7Fkum45ipRqPGNPv90Gl3/9VFaMhSniieHLyAPcvW/7ung7vu2OJWxfy2PgilTJURvvqnEq/fLGeogqSIqGHinDlFssHn7eNbWkPw2Tf4ZqqEr1iWrz3XPuRfdy4Nx/OTytt0W/RspEMZ7Zz3vnMHZwxyUA9fpSRc3DWhMcYRwggNkqor//sAd5x2ypDA1T/o6l20OKCgKjyLy/cyjNOSeIeQDNaXa1+iyVtFAfg8Rgv3Lyc8aob9nL7YtzGkfQM3YlAyOvl0349BIcYz2lO+Lvnz/Lk+Uk0GNR6LA5jRj1XCPTIvrrIgZe8D719kUDAIhhxdAi4sisMyR5V2atkC/EoSMCIkp29jdnvv5TZV16M7mpjRcmMQQrFTQNx1buSY7lNqFQ3DRaz7Mn+/W72v/tmVj9yK3PLOV4MXge9ndrRgKUOzo1IEffZxj2RaY+ZX3wuMz9xJcaVk/RFnxp6X4EeSuI7KC0WgvLSG/byAxfM8d9OF1ImoBZsdg1v0kqr119E9binns890OPl//oQ+7oG4+NMvtpsEPdo6Dto4JRaMAVjUWMwQSEPYDxnzglXn5pyzSycu22OXVOOGRud9hsjBB/nnTrBcF8mfGmf5yv7O/z7AxmfP9Ch6wF8fzlYtcIgyvKUVe0zUO2ni0KqSmYSgkk4p73M3zx7B+dtdiABhwLJIO8mhlXJbyBRaa1tBKuWYHNS7eI1xcvANvrRRD/2hDa/fvVWnObk6uK3FJTXf65gWEOMCkrmBFTqXny4MMywbPQTPGoVqf4NbhhHjNSiQTC+x8fvt1z/qXs52HEEl+J9VvlGtfIdlPUQMDmYFpfOrPDhF5zB5gRUNMYYcKPLl2mO6Xp6/7abB1/zV+i+DkIgR2iHOMG+3lsuixGjRitWQ1R4JcFPO8yVp5I+6yzSy89g4sythDnBTxjSiulRUMiyQLLi0T09lr94D/mnv8nqh7+CvbuLFYMWMRODxAA0TbKg1spVpgQLGhQjioqhff35zP3+S8mnLGnNcHcNY/eBzIDS5XduCnz0/v186Hk7SU1CaoZ9vlfbuSQ3btm0Dw5gJOeKbSlvv2KO1/3rPh60KRIckvViYEWpfLk1yaVS+vjp+hwRS1CHuAywfH1hgrsP9PgT20LDAhPaYX7S4oyjlaZkWUY3eB7KM3wmGE0JmqOJgLeIyYjBHFzcuFk8b6gTVatYMpJy/stauvTAwS6/n/97zWlcsCkho0fiLZjoFXTQeWqNVctv6Hn154bIoDKmo393GbNt6BjSkqTE7m9qa0Ul1d8vw+cwzBSqwHXm15o+2A3jgpAXAcyv3ZXzs0/dzC98tkMnX0HEDZewVr5UFfWGLAifX5jktZ+5n3c9/RSmJizYuGI+6rmKwdscec5pzPzKizjwE3/LpgVl0ca51bX6cVXpqs4SaVwKUFg1gjFxscsuKuGGe8hvuJcl6cJsQmvHHMn2WZbaldgHPqAHVujev0C2Z5k0tzgVpohOCXLNUVWsGFpBiEPtWvZUDjQD+SuabuQhMKOBngT0hWcy85svxk8LttjFN2pwQZVcwAbDZ/YZ3nnHft799HlSa0m0S2ACW2uieju7oQxHUJBAIMWZHted2mb5ik381Kf38ZCZKebfywIVUWBipmsqH08NGMWELkqA4KK7WLNMtD7pIsaQqWNvR/DGw8oyIkJAIKREsdRj1EIGIemhucXg0CFvlOVzy/JVmWrBZENhLRgUax2bQsavXLmL52yDYBTnE3JrSMvoKCPnC6TxcKCCDp6rVuNksAbUdjGYsXMUx4qcZAieIC2seqizLen/V6nzkIg1SBpqMmXI4+iIQXPUytc4nGo0vEwDqGmhqvyvMy13L6zy+1+ZJjcrcad/daqgUo/M5CgJaAbk/L97ptjZWuJtT3MkMoGY6AZFgkQ7v4pEkXjBi8OKkvyP8+g99GIO/NI/YJcUbzwulN7khybRGDCGAYPwIrQx2BCiNAN4UZCMHjmJOlgIhAML9L56MK4kVt6AEPc7GvUgOSrCqhoQh2j8/lSVjgXj4xxalWmVqmnMr1xNjSzLYOgaz+I1p3Hq21+O3dZGQ3SPbsrnV95H9fWLBvb0erzl8we4dMsU1+5sYQTEp6j1qJqxA9Na56I1UFx+NET+3cI4uP7MGXo9w8/ftMhiEU1D6UGYJtgcMAORe6gJoTQn9poOJDAFxKIaq6sSd/lAlEYQMxiQTSAuRwvBFpJUsCBR5+87yy6lvLqE1VdX48tUE0AtJji2tjr8+hWbue70CYwYjAg44qbWcmG03y5NDVb89qVLBr8VNTmgBLWAB02G86jee8TTivbQ4rgqAfdfVb+RYselhQlKDPNU5zzVwaBBom5Ka6Chj7u2aHCoOBEZjNKqiBFaDn7m4q184+Ae/nF3AiGGTs9DvB79LpV5x28pVtXhWeVdt/fY3J7iJy/0tOLEKrk4khCG5lzExY0pqoq0hM0/ejGu7Vl90z8h3UBXlVYx8MUJjkHU6AGDiOdWA71Bzviyn6jFRL8HpYJWbSFKWUkQ+tbiRTcyRDtI+k8RnC+Vvfo7GkyzR2YZVdsgSisEsqtO54z3XIc5c6oImwe2QQ5XzQmFZqUqZB5+88sHuXUh44PPnaedtiIwoebxoplM/WU3SQ/1lbop4/mh8yZ5x1WzbHVxSVktGOkhWh9FmkiGO1GVoayRmo8CrlQDHQhtJrxh12yXd125netPT3EhGsnWR/QNL/k3wapzaMcaN4SVYezDpcPIb6MrXA8HF3mzY971+O3Lt3L5JsGbBFHBGF9s+K9QrR5GAz16/NaXV/mjr3ajwz6Jdnn1+q2Z/G8ZJr//UqZ/9YWQJrRRLIHcCIm44vMcSDH1lbjy+sPBlbZT9Y31Uvm3MVwRK5QWk9rCiCN/5uPY+uffTThjunGyYFgd9AS1WB8ltA/c2eEPbhde+oQJLt9S9wLT3MeGzBoOfRkbgjhE4PrHTfDuZ85zXhpApoqVg8r80dBgrMO/gxJSGd7X4o8GrlAHpBdjpT1x6yp/fOVWXnS6xYUEbwtrlJr+vK55gG7guccUR03yLRNrpM3JI3EbyW+ImiWnjZivHDpOQXPy4Ng1m/C7z5zjvHSBzJUb4MfXI5BgSOn4Dm/74gofuG2RniqYHCPDJjn18qkKWKX1g5cx9+6XsXpKgkpWMFHtM4vyaXWmQY2pHAquTB1p8LlhHORGEAyrE6u0Xn4m2/7y1fC4NibEPY9D99T6h5JgFbwGPr0/45du2c9ZrcDrz52rCqej76+dNxqOjpqhL695BKuCOsuLdrT5s+dv4xlzXSAvHA5Z0ABGEF/oUqWK1v+IqnmPUGP0cHFa+StShSgGG3A25zvP8Hzg2jN4xikWR0JwSiKevmrJsOQ53gq8oXzlb3XV8ljiqvx86FrlvhK3Hh1KfsMF3EDmR4YEQ3AJqYkKy1NnWrzrOWcwGzKsOFAfTSysqXwulXpoHl0DS4sHfc4bv3CQD3xjORqWqkMzjat/6td0+gQQY3GpYK57Elv+8vvQc+dJNNAxBguFyWuIwUprjKTcBDMsYcmGcPTRUsMNrjThYh4x8nvAowhGUtKJjMkfv5KJP/wuzCkpLoBxZg3DophLy7Wc+ggEybi9a3jtp/fxrdWEH3mK4/ET5bRI7X2NmAYoaY3haHWUqKuCZcdNAWMtiTE467h4vs3fPO9Ufuz8FjYN2NCLdh/eo2lprzPcQIO5nTEfrxwuTqHYbS4GnLfYPCe0AptF+IVLp3n31Ts4a1ZJTIo1ErcGSDJsc1Vrm0ZqVLtk+O9Y46qSWKnS9JkbDbh16HDyq6ptOtpKv2mXw6HgRCARQYwhkbjidtlWx589ZwtpYjFBMaGHzVfjIFu2WZFfEEuQvGBKcMBbfvLGffzFHYt4VXIL6oVVDOTDUriYwtWzGFJraV2zg80f+1Har7yQRHrR5VIxid3SusxTSk0Mpa+VjZpxw2nS/xt+E004sMQQfqltgQitHZb2+65j9s3PZ3J2AmMcJrFYUzotHFDQmJdBwWeICgtdz//+9IPc9lDGSx/nuP7xc5DaIXcz9fc5ippXq8dQ1Xq6pK5RNrU6vP2SzfzDc7Zw4SYhEY8ah82GZqvLYq0dffsjW5mmh48rzkUEDYHcGcRN8+1bLP/0HZv46Qsm2WIDNiR0RvCiqqX4uuqz6nD5+vhapz1WuKFrVCTUfmWbceNoo/nVqdKu8bQZW7fUP1ycitDKlBeekvD7l80wnQSCmcdoK4ZsLJlVQz0kCOqVh0KL1392H+//+kGyEDDkTORdsGM2+orgcsGemuL+9BXM/N4rMLumUBdIjKFjo8HlgIVQkXmGpaON4epaRjwepDbjFGE5EdqqLKUZEy85j9mP/wDtVzwJm8aFsXF9IYhDAohXRCzLXvi+T+7nkw9YHjdp+JlL55hRARlIpBvdiaGqa1XCeudsSqvj057gdYo8Tfj2UyyfeNFO3vSUWXZO+YoFbq0zDbWT0l+Z6ZMcPg4QExvYOseTp1d571WODz13O5fMt/CSgiZ4CxPFjvimujYto6+hElMtX3U+TR4FuP5fcaF/b8PxRibMDyW/xtubN7GXx3Xp9kjgAh5x0Svmy85u8VvfNs92lshdggYdMKuGeqgBi8WqsqIpb/jcg7z3q8ssY6ItYmBs+bqJIWCYVMX94IVs++irsa8+j+4sfdu8QYyd8rhkSvF4+NpoXGROVfmqMoiNwQEkCJ3zZzjl3S9l/i9ehj1rE5kExHt6BsZpG1GdzAkC+3LlRz/5EP+4x7IpW+atT53nnDkhWI8NKQZPueK73rxkiXPVk2pjN1m7jzIIy1qQ+mg7otJirtXjzRdNct2ZM7znvx7kb+7psnsloOKi2G0CJkAQU2whEER9tKk01RG7aMaB5BsPiw/PGFNY3JYfI+As5AETDMEYrBHOn/W86uwpvvfcWbY6IWrXJr5qC0lQgpFoxlBpoPpxtY28KjmKqMEGE639S9Wo9gqbXuuxwdVonUHNqCLqi2CkgmjcsBp9PwVCqFnFjMtP8sgMvETral2/iBtdlT0UnFPBW4chMB0SXnVOfG8/9fkDPJQ7JMQuZzXgrQeNW5ijzVagZwImd3ibsdyb5A03HeBAL/Dj508zmWaEkAJ5zEMcptIZUwUViyRKSzx67jw73nU9q6++m8Xf/QR84n50sVPYROV4AplxGDVYjdKhL1iNKazmB4xnWMYqU0vpK5e43uc0Og4MUtoZejyGBKFnID1tM/IjFzD36qtgRwtVj4hjQpXghIn+NjeKpwG6CprE+ayiqfZ2Pb9w434+cNcqzrV56ROnecXjJ2iJJRgtvGisz6jq79g1JVapbpTXtHKWQHQXC6goXh0Gw3kz8Lanb+GHL+jyV9/I+Nu7lrj9YMayN1gE1RwjEn1iGUMoXNf0tzsYQUMhoheMS41Hg8FoEhVmW0xTYXGqBO9Rq8xb4arNU1x3ruN5p6bMpynWlEw3Gi32jf5sZIJ1xt2kXvTTgqFlM1YRcreKNC15HOfUMzYuSYtG7x2qSPC4XJDQQmy24bxM0eGDyfAkpLXrdduqo5dmi5AqBiwk3vLfz5mgZeZ50xcOsndZ0SRg8hjlZki9JkpRwcRFGTUZmVfe9uVFdve6vOWSOeaTLi6kiOaoC4Dtl8X0tQ0h2nsZfAr2maex5dJX073pXpbe/1myG+7EP9AlCZa25sQJfUAVkRwRJWirohZWJGpKZlU9z3HiCSoYLMEAajAhoScJbiKQX7CF1ndfxMzLLsadOoVawQQwJHHiSKTYVjfcphIgNwmIjTtitMO9PfjhTz3EJ+72qJ3g0pmcX7x4CxPWIRKtzxBiGxwiDTGscZbETdQkhUSK81aGyJHPmk742Sdbfuj8SW7e2+Ff7lrhn/d3+MpBx0pmC9ctGabYo6SFWK5xx+2gcRRsHkf4OEo7TKaoCTiTM9cyXLTJ8R27Jrnm1CnOnRHaJsEUVrhgGplutc6DrTfjcbkRRB09EUJoozow8ztRyOARjYqINRmKQ40hWA+mh+rGmXSgjeQJNrPkdW7F2kFhlJpwxHHGM+kd158ZmJuY5Q03LnFHJ8YPtEbxpYvgob2nUE452DCBDxnv++oK9y1afu1pMzx+pkfi04JJj5YARQI2FDZdU0Lr6tNIrzoDvWM//sN3sHzDV8m/cB+yL5CLwyg4deTl3E9fMawYixZXqlupAxbRBFHIxGDxhJbCOdNMX3M2rRecS/q008k3p4h4AnGxIIjGzco6RopVQC02ZKgEvrwkvO4z9/OpvQ6fOLanOb991Wa2t5WgGZa0cTBpzLoBJyHE8M5NRqOjRq1xONXoxVBNRXfW2KSq0lcvlnpw11KX/3ywxy17lvnKYuDelYTd3cBKp4vYODJFUVz6jWMxBNtjk4XT2gm7Zg0Xz8IVO2Y5Z044tZ3QVkFdXCpOcKhEr5JJLeTYKIa1kTTv4/rOaujyazft4falqXVfwPFGz94Z+IGzp8lsi0Q9XoSg8EffWOTGew+S6/SG8zJmiTc+eZLzNs1jg8G5+F4fOcmqOS0LvagGBoO3yi37O7zhsw9w44NTEDr4oRB3VXUohq1XGwgSsFmCOsulmwK/8fRNXLG1RWKi183GsqgSQoaXOKXQlejbMwGQYpN0V8nvfAh/8/2sfuoO9Mt70dv2k+3rIL6cvRqmuqZdfrlmKkFOm8ZdsA130SlMXHM28sStsH2iEJqiyh+nJMt+qnHLmhk28SnrESmQK4hXPrKnx8/cuMBtB7sEY0i88r5rZ3n56TMYEz2vuLJfj8ivSZuppg8xrDqonr6eFNLEzCoZFQNTfYNHbPZOgJWespwHFnqePd2c/d0e3dz2X0I7dWx3yvxUwqZEmHLCtLMkfeZYMHzRyq4gAa2OQ4N6NdWjqaGacDHKSDzymLhE219dqkzcxgyH2uGY4arpTee1dK/R2VvZLZQoO+do3IYhG89PFWIU8fjxl3vXRrV/0/nRwNW7uKrn7tXAL9/0IH/y9Yxctai44LylZ8pgWP07ilYRkB7Wt9nZ7vLzl0zzmidsIhVBbC/OhYkdlK+4s/p0gWIeSPsfc/yR6Ep5JYeFVbr7Fwl3LpPvfpDkgVV6DxzE5AMj1oBiZ9qwbZKwcwb3hM20Tt+ETCbYuUk0aTY9XatcVtLLgToERGJU54AhIHS84Q9v28fvfKnLfR2LU09uLL94ySQ/fdFsMV9ZyIDrvJ9Rx/3z0h9WNZFKATfy8vuVGyPmjWICa4FxuVN9iC3lXL/ZtFrxcpVkRDnrz14Pt9HyHe+4+gLKqLweqzivPcQ7cu/58/tWedO/L/Fgz4O1BK0YFPcHRB30dBOwvRTvEmbsCq88p8UvX7SZLYkDcpxzD7t8vmByUho6SQcVi1cBLSTWolyqkIfCQYAEomvA+Oz1JJpx5YPoX54AmY3beu896HnjF7/Fh+6agEzxbgUJgdc+cSu/cvkEbdcam99Gn9vHlRJWtdHqx9Ubmxhalda7ryl9qFA+DiseQJTS+YVQ+E/SqFqKTYqBr1nSq1d+vQ+iXsbHCm7s4PEYxHVUSUOXXByuk/G5hcD33vgt7jg4C34VbyoqYl8kKdpbDDbLMU7JnGByx3lzLX71ioQXbJsmsc2eCDZSvqCKasBIdPocVPsr7ArYyrYi1TjtYorjqgeFEALWDk92j+qvo5gIeY9eED5w3ypv+cIB7lpMMOpQemASXnVW4B1P286MFUyxGDU2v40+F5o9jpYZjOJyG01bbzSpFrY818KNRfS/XrHMLX1GybBX7CYax3ibMI8VXEnjBpnHOi7QQXFYVTIc+B57Oo43ff4B/u4uy0ooQt4J0A/GG/MwQaIkJgETAkYDQR1J4vnBc1u89eKtzKdSuZ9h/WtM+UIIkeFQqHxBENF+OLDqInXQyNCMmMI2TPoBsct+WWeMQ8yyXywZSJCxMGQh49YlePN/7OdjdwZywEhGZpXUG15yxiR/9IwZ2i4uEhhjRj5rzXM3gBtiWPXGqp43YcalNRVoXP7r0dHAleUbVacTEVdix10/iatIK15Rm7OSW95z+wHe+aVV7lyxoDli88I+zRFXTEPhGq4QvVQLCSyygCfNBt586Xa+Y6chTWwRMa+cC5Qhi4EjVY+m/lje14QDyIMvVoYNIXikmMW8bQXe+7UF/vzWBe7XGdAuRnOEBOMML9+V8ntXbmE+XSvZr/fcDeOqKmGVHskRb9SzjyauqYFG1eFEwTV9IE10Ejcg7wNqFNfLyZzhP/f1eOst+/nkfS06KjG8Oz28lBuBdVhVFKE0gxBt0TI9XrDL8roLp3na1hYJ0exGbcCaZrukR7K+qoqGDHAg0KPHXYuev74j44/vWOaulR5qUvBxF4sCLe94zfnKr14yz3xrItb5KJVvjVlDE5PZiMRVxzWlb0QaaGJsRxq3Xt3G1eF4xj2sEe0xjlOFnoBVwXgFCRz0yvtvO8jbv7bA7qWcYNqVrTUV3azPuOKJVSUkCeK7zLlJvuv0lO89P+WyeUNLYvDQI12PEjvuvioOoOcDWehxywHhQ9/o8td3LXF3J0OlhckNXjPEBVQDk87w+vM28aYLpkha0BIz9KyNPnfDuFF2WE0PrVesiq0/9FA7zbgGPIk7MrhRaU10ElekBy08koa4NUnAhoCXhJsWVnjHTQf44D3QUYshi7s2VAvHgMWfRtXQiMGEQI7FFotJsy3Dc7cbvu+8LVy5vcVsEVonKmFx6jzyvaJ8pRdRIZrplAyxZI4FCVpJrm7gqbj/q2isoooX4d5Oxsd3d/jbOxa5cZ9yMM8JavHGYSRDQhdvDBJabEty3nzZPN9z1hRtG0BdtOZvaOtxgsSh4NaYNdRvbDqvM6J+ZrUHrYd7uIU+XNw4bIk/kXHj2r5+/0ncaFwWAl0f+Oi9y/zaLQvcdEAxueCT6BhSi32Yg4l17c9XDbhMPJtMHBdsEl5xZsqLd7Y5Z9YiYuNEPoYgkaEokYkm4uMKuRQOZ0KMlNxfBiiYUFw8jGHVVDOQAMHgjcXjscHwjaWcz+1d5aP35nzygZzdvYDP8+g4ABCx4LMiek/A+AnO3QLvvHyOq3e2+5FyNtKnDxc31nD0cEfmjRZsVOFGSQiHi6uWr6lOJyqufu14YAqPZpyXwtTAC3t68Gd3LPLOW/dzb6cFSD8QK1BRDalIQgOGZTSqWCQps8Zz+WbLs0+b4epTWlyyOWVCuogYytXzgC3mzHwRVF7QEFATt8UJoYg5aDCaRy/w4vC5snuly837V/mPB4VP3dfh9iXDgUzoGoEQw7eJMcXWOEMMbBAXFSxw3a6EX79skm0zkPp2P/TZI9H+jXEJy+Mh4DpqYR036nod2yQlPRJp4+p4ouKa6EiNfI9FnOYa42Wm0ftHCD0e6MDvfmWZP/3aMns7RcDYYg4LqR4DRUAIMaXPOIfJBDE53gBYJo2ydSLhgs2GC7d4Lpx2nLNpktPbhrnJBKfRoNoWxlZeYXHVs79r2dvtcd/yMvetGm5bcPzX0ipfW+mwuCz43JAHxScJJmTYoJjgwUFPYiANClMKUYuScLZb4LVXbOd7zpxiQgSrBmM1SmAj2u5Ip61hWFVqUutGdfxx6mAT7limHevnH8u0UXQ8SDSPNpxHEQ0YpVDNTPRooMrdXeGdtx3kL/7rAN/KpoAOSo4JLYIIkJGokImJjCwEMA7EF66WpD93ZQh4k6DVSObEcHYSQvR4QvTnJsZGSQ0BccQIVh6RACqUlzCRWZpcwBiCiYzHZAafdLAeAvH+Ged55eMS/s+3beWcKY8TfqU2AAAE6klEQVQQ41WqKEKz9f5GvsGHg9vQ5uc682mSXqq46gPHdaSmQj0SuFFM+ETHNb2XepudxB05nM8z7lkNvPebi3zwm8t8c6FFT2P0TafgbRZFInUgbrDXtfZeGcVYx+G0nC+DgSpaTZAaLo/zW2KwWRu1wjQdnrIz5Q1PmedZ21Om8Wjh42tcuxzNNNFIzQ0y1BajM2liaKMksaY8H879h4sb1dkfC2nrMfqTuCODC6pI3kUd7Msc/3zXIu/9+hL/ud+wnHkI04hkBOOL+a6SiRT/iQx4i9bO18VpWdhh3KCgfZwJoC46w5TgmJ5Y5aLZhB954iwvOaPNlCPOikmc2B/li33U4HgkcX2GVe/YTS+l/oKaXliZXtI4Ma9Ko55XpyOBG1fm9RrzeMeNks5O4o48LpCRq8WFuIrWNYGgPW7eH/j7u1b52O4D3LaQsKoJohnaD6xa5NNnTFXm0+dM6+BgiJkNSja4v7g2oULHCdvtKs/a4bj+7G08c0fKdNKFMIGIQUyOqIGau5z1BIEjjWtkWGWDN920HkM7HNzRlKjWw42TAk80XJ0OZSA6ids4TpXozrvgGQaNNloiBIS9Xc9Nezt8ePcy/7Y34/YF6GU+Gp+KFtt+DEZLV+LFSqIR6DM3LfbflozIFI4CBMVDcU1FKouUEuefJDBrEp40C88/bYKXnD7Nk6ZTWmkpgJWMEUqDib7r5RFCzHD9j8L0TpOle/3GUaPJ4XLMcZ1oXKWOBK5+/FjAnaRHDylKFnKs5gRNWOhavr7S49MPrPKp/YEv7etx33KXbp4j1iE+2kT5InyiCWXEi/gX2VX5v4IJSDAQXGRWRhAfcGI4dcpy4ZbAtdtTnrVjirOmHNMtgJzcOtq4DX83jzTukO2w6tj1JLNREkBTIZvE7VHi+OHg1hs5R5X3eMcdq8HhJG4tDohW9AaUuCexXPnLfGChE9jbddzyUODmfR1uW+7xrY7nnsXAQx3BaxZlJxPDsRYZIqIQAg7DnINTJhN2TTnOmLZcNK9csq3NaZOOrS1LS3K8yRBaRMksMkJxdqg+69EjiVvDsMob+oAGRjQu01HMayOFeqTTxn1gJyLuJD16SFXJQ5wXEiNIOemuZshaPZoP5GRq6QWh5xXvlQeyjJVMWOoKK5kSvNJOhem2oWUD84ljysKEVVpGSYyJEXwQci23/AgiORqS6E9LAyqCNW5kmY81Axuyw3q49GhgPoeS9mgpx7FMO0nHmGoT4VFqKE+LzjkE00YfcFoDSv1aP7EAVefc68UowCKPrm91LMMaJ0mN+/AfDu7RRI+G0eMk7iTuSOFOVGpkWOvNlxyJtFF0LF/wOBX2JO4k7njDnYhkYNAAVd6lOhyuvmQ8TRpknTGN0jIPJZ96GY40rn5cv+9ExjXRSdyJh6v356bj4w3XuDWniek0MaS6JPZw0zZy7Wjg6oy2mnYi49ajk7gTA7fRb+B4SluzHFAFNzGaano1o0NNayrQRhjQkcKNKtO4xjpRcOPoJO7Exh3vtCbWeClaVplNk4hWl7bK9OpvedyEa2KATXS0cONU1xMVV087iXvs4Y73tEZ/WPXOME6lasI1iaZ13LEeidbDP9ZwdfxJ3ImJO97JNElQVRqlwpXXNirN1HF1hjaKwx4tXNmJH0u4avooOok7sXFVOh5x/x8rHmI96RmaVQAAAABJRU5ErkJggg=='
                doc.addImage(imgData, 'PNG', 3, 3, 27, 16);
                doc.setFontSize(12);
                doc.setFontType("bold");
                doc.text(initX + 30, initY + 8, "AGS  ");
                doc.text(initX + 30, initY + 14, "GERO");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 1 * 190 / 3 + initX;
            }

            var bloc3 = function () {
                //Bloc3
                initY = 3;
                initX = 140;
                doc.setFontType("italic");
                doc.setFontSize(8);
                doc.text(initX + 15, initY + 5, "ADRESSE");
                doc.text(initX + 15, initY + 10, "CODE POST VILLE");
                doc.text(initX + 15, initY + 15, "tél. +33 (0)805 696 443 ");
                /*doc.rect(initX, initY, 285/3, 20);*/
                initX = 70 + initX;
            }

            var cadre1 = function () {
                //Bloc3
                initY = 25;
                initX = 5;
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.rect(initX, initY, 200, 12);
                doc.text(initX + 1, initY + 3, "Date : " + (moment().format('DD-MM-YYYY') || "") +"" );
                doc.text(initX + 50, initY + 3, "Heure : " + (moment().format('HH:mm') || "") +"" );
                doc.text(initX + 140, initY + 3, "Poste : "  );
                initY = initY + 15
            }


            var cadreIdentification = function () {

                initX = 5;
                var newY =  24;
                var i;
                doc.setFontType("bold");
                doc.setFontSize(10);
                doc.text(initX + 1, initY + 4, "IDENTIFICATION");
                doc.text( "NIV :  " + print.cas.niv , initX + 40, initY + 4);
                
                doc.setFontType("normal");
                doc.setFontSize(9);
                doc.text( "Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
                doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);
                /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
                doc.text(initX + 140, initY + 8, "Date de naissance : " + (moment(print.patient.age).format('DD-MM-YYYY') || "") +" ");
                /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

                doc.text( "Adresse : " + (print.patient.adresse || " ") +" "+ (print.patient.cp || "")+"  "  +(print.patient.ville || ""), initX + 1, initY + 12);
                doc.text( "Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
                doc.text( "Personne à prévénir : " , initX + 1, initY + 20);
                    if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                    doc.setFontSize(8);
                    
                    for (i = 0; i < print.patient.prevenir.length; i++) 
                        {
                        doc.text(initX + 1,  initY + 24 + i * 4, "" +  (print.patient.prevenir[i].nom));
                        doc.text(initX + 40, initY + 24 + i * 4, "antécédent : " + (print.patient.prevenir[i].prenom || ""));
                        doc.text(initX + 80, initY + 24 + i * 4, "" + (print.patient.prevenir[i].tel || ""));
                        }
                        newY = i * 4 + 24;
                    }
                       
                doc.rect(initX, initY, 200, newY);
                initY = initY + newY
            }

        

        

        var conseil = function () {
            header(initX, initY, "Conseils", 201, true);
          

                doc.setFontSize(16);
                doc.setFontType("bold");
                doc.text(initX + 50, initY + 20, "CONSEILS AUX PATIENTS ");
                doc.text(initX + 50, initY + 30, "PORTEURS DE PETITES PLAIES");
                doc.setFontSize(11);
                doc.setFontType("normal");
            initY = initY + 30 
                
                var paragraphe1 = "Vous avez été victime d’un traumatisme ayant nécessité la pose de points de suture. ";
                var paragraphe2 = "Pour votre bien-être et votre sécurité, veuillez respecter les instructions suivantes : ";
                var paragraphe3 = " ";

                var p1 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe1, 180)
                var p2 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe2, 180)
                var p3 = doc
                            .setFontSize(11)
                            .splitTextToSize(paragraphe3, 180)
            
             

                doc.text(initX + 15, initY + 20 , " Madame, Monsieur" );

                doc.text(initX + 15, initY + 30, p1);
                doc.text(initX + 15, initY + 40, p2);
                doc.text(initX + 15, initY + 70, p3);

            initY = initY - 30

                doc.text(initX + 25, initY + 80 ,  " - Exécuter immédiatement l’ordonnance qui vous a été, éventuellement, remise ");
                doc.text(initX + 25, initY + 85 ,  " - Retirer le pansement sec mis sur la plaie après 24 h. ");
                doc.text(initX + 25, initY + 90 ,  " - Garder la plaie propre et sèche (Eviter les bains et les shampooing).");
                doc.text(initX + 25, initY + 95 ,  " - Surveiller votre température quotidiennement jusqu’à l’ablation des fils.");
            initY = initY + 10
                doc.text(initX + 15, initY + 100 , " L’apparition de :");
                doc.text(initX + 25, initY + 110 , " - saignement,");
                doc.text(initX + 25, initY + 115 , " - suintement,");
                doc.text(initX + 25, initY + 120 , " - lâchage d’un ou plusieurs points, ");
                doc.text(initX + 25, initY + 125 , " - rougeur anormale,");
                doc.text(initX + 25, initY + 130 , " - fièvre,");
                doc.text(initX + 25, initY + 135 , " - ganglion,");
                doc.text(initX + 15, initY + 145 , " doit vous faire contacter rapidement le service des urgences le plus proche de chez vous.");
                doc.text(initX + 15, initY + 150 , "  ");
            doc.setFontType("bold");
                doc.text(initX + 15, initY + 160 , "  Le service des urgences est à votre disposition 24h/24. ");

            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 ;
        


        function footer() {
                doc.setFontSize(7);
                doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");

    };


        var pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        var page_1 = function () {
            conseil();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

       


        doc.save('Conseil_plaies_.pdf');

    }

}



import { Injectable, OnDestroy } from '@angular/core';
import { ReplaySubject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { UserProfession } from '../auth/user-profession';
import { ParticipantPermissions } from '../auth/participant-permissions';
import { ParticipantModel } from '../auth/participant.model';

interface Context {
  timeCode: Date;
  auteur: string;
  auteurID?: string;
  poste?: string;
}

interface UserProfile {
  id: string;
  prenom: string;
  nom: string;
  rpps: string;
  metier: UserProfession;
  poste: string;
  isMedic: boolean;
  event?: string;
  niv: boolean;
  fonction: 'user' | 'pc' | 'auditeur' | 'administrateur';
  isOmnipotent: boolean;
}

/**
 * Centralise(ra) les info d'authentification et les infos de la bdd
 * utilisateurs (à creer) contenant des infos supplémentaires, les configs
 * spécifiques et les references des  projet auquel participe l'utilisateur
 * cette table devrait être indépendante.
 */
@Injectable()
export class UserService implements OnDestroy {
  public profile = new ReplaySubject<UserProfile>(1);
  public poste = new ReplaySubject<string>(1);
  private profileCache: UserProfile;
  private posteCache: string;
  private subscription: Subscription;
  private nivCache: boolean;

  constructor(private authService: AuthService) {
    this.subscription = this.authService.participantSession
      .pipe(filter(participant => !!participant))
      .subscribe(participant => {
        this.profileCache = this.parseProfile(participant);
        this.profile.next(this.profileCache);
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public get CONTEXT(): Context {
    return {
      timeCode: new Date(),
      auteur: `${this.profileCache.prenom} ${this.profileCache.nom}`,
      poste: this.posteCache,
    };
  }

  public get INFO(): UserProfile {
    return this.profileCache;
  }

  public selectPoste(poste: string) {
    this.posteCache = poste;
    this.profileCache.poste = poste;
    this.profile.next(this.profileCache);
    if (poste !== 'pc') {
      this.poste.next(poste);
    }
  }

  public selectNiv(niv_setting: boolean) {
    this.nivCache = niv_setting;
    this.profileCache.niv = niv_setting;
    this.profile.next(this.profileCache);
  }

  private parseProfile(participant: ParticipantModel): UserProfile {
    // création d'une var prédéfinie si utilisé plusieurs fois
    const isMedic = [UserProfession.Doctor, UserProfession.Nurse].includes(
      participant.user.profession,
    );
    const fonction = this.role(participant.permissions);
    const isOmnipotent = ['administrateur', 'pc'].includes(fonction);

    const profile: UserProfile = {
      id: participant.user.id,
      prenom: participant.user.givenName,
      nom: participant.user.familyName,
      rpps: participant.user.RPPS,
      metier: participant.user.profession,
      poste: this.posteCache,
      niv: this.nivCache,
      isMedic,
      fonction,
      isOmnipotent,
    };

    return profile;
  }

  private role(
    permissions: ParticipantPermissions,
  ): 'user' | 'pc' | 'auditeur' | 'administrateur' {
    if (permissions.settings) {
      return 'administrateur';
    }

    const rights = Object.entries(permissions)
      .filter(([key, value]) => value)
      .map(([key]) => key);

    if (rights.length === 1 && permissions.statistics) {
      return 'auditeur';
    }

    if (rights.length === 7) {
      return 'pc';
    }

    return 'user';
  }
}

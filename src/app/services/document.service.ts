import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { ParametreService } from './../services/parametre.service';
import { DBService } from '../services/db.service';
import { PDFService } from '../services/pdf.service';


let jsPDF = require('jspdf');
let JsBarcode = require('jsbarcode');

import * as moment from 'moment';
import 'moment/locale/fr';

@Injectable()
export class DocumentService {

    private font: any;

    constructor(
        private USER: UserService,
        private DB: DBService,
        private PDF: PDFService,
        private PARAM: ParametreService) {
    }

    public pdfDetail(data) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.italic.postScriptName, font.italic.base64string);
        doc.addFont(font.italic.postScriptName, font.italic.fontName, font.italic.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        doc.addFileToVFS(font.code128.postScriptName, font.code128.base64string);
        doc.addFont(font.code128.postScriptName, font.code128.fontName, font.code128.fontStyle);



        let user = this.USER.INFO;

        let logo = this.PARAM.currentParametre['logo'];

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);

        // metadata
        doc.setProperties({
            title: 'Fiche bilan GERO',
            subject: 'Fiche bilan secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;

        let header = function (posX, posY, text, size, tgle) {
            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
            doc.setFillColor(200, 200, 200);
            doc.rect(posX, posY, size, 5, 'FD');
            doc.setFillColor(255, 255, 255);
        }

        let bloc2 = function () {
            initY = 3;
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(14);
            doc.text(initX + 5, initY + 10, "FICHE BILAN ");
            doc.text(initX + 45, initY + 10, " ");
            initX = 190 / 3 + initX;
        }

        let bloc1 = function () {
            initY = 3;
            let imgData = 'data:image/' + logo[0].format + ';base64,' + logo[0].base64 + '';
            doc.addImage(imgData, logo[0].format2, 3, 3, Number(logo[0].width), Number(logo[0].height));
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(12);
            doc.text(initX + 30, initY + 8, "  ");
            doc.text(initX + 30, initY + 14, "");
            initX = 1 * 190 / 3 + initX;
        }

        let bloc3 = function () {
            initY = 3;
            doc.setFont(font.italic.fontName, font.italic.fontStyle);
            doc.setFontSize(8);
            doc.text(initX + 15, initY + 5, logo[0].adresse);
            doc.text(initX + 15, initY + 10, "" + logo[0].cp + "  " + logo[0].ville + "");
            doc.text(initX + 15, initY + 15, "tél." + logo[0].tel + " ");
            initX = 190 / 3 + initX;
        }

        let cadre1 = function () {
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                print.cas.niv,
                {
                    format: "CODE128",
                    displayValue: false
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 15);

            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 13);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(print.cas.timeCode).format('DD-MM-YYYY') || ""));
            doc.text(initX + 50, initY + 8, "Heure : " + (moment(print.cas.timeCode).format('HH:mm') || ""));
            doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            initY = initY + 15
        }

        let cadreIdentification = function () {
            initX = 5;
            let newY = 24;
            let i;
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            doc.text("" + print.cas.niv, initX + 70, initY + 4);
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.text(initX + 115, initY + 4, "Homme ");
            if (print.patient.sexe == "homme")
                     { doc.rect(initX + 110, initY + 1, 3, 3, "F" );}
            else     { doc.rect(initX + 110, initY + 1, 3, 3);}
            doc.text(initX + 155, initY + 4, "Femme ");
            if (print.patient.sexe == "femme")
                     { doc.rect(initX + 140, initY + 1, 3, 3, "F" );}
            else     { doc.rect(initX + 140, initY + 1, 3, 3);}

            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);

            // Calcul Age patient
            let formattedAge = 0
            if (print.patient.age != undefined) {
                formattedAge = moment().diff(moment(print.patient.age), "years");
            }

            doc.text(initX + 140, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") + ", " + (formattedAge) || " ans ");
            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévénir : ", initX + 1, initY + 20);

            if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                doc.setFontSize(8);

                for (i = 0; i < print.patient.prevenir.length; i++) {
                    doc.text(initX + 1, initY + 24 + i * 4, "nom : " + (print.patient.prevenir[i].nom) +", " + (print.patient.prevenir[i].prenom || ""));
                    doc.text(initX + 60, initY + 24 + i * 4, "lien : " + (print.patient.prevenir[i].lien || ""));
                    doc.text(initX + 90, initY + 24 + i * 4, "téléphone : " + (print.patient.prevenir[i].tel || ""));

                }
                newY = i * 4 + 24;
            }

            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let cadreAntecedent = function () {
            initX = 5;
            let i;
            let newY = initY;

            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, newY + 4, "INFORMATIONS PATIENT");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            newY += 4;

            if (print.patient.passif.antecedent != undefined && print.patient.passif.antecedent.length > 0) {
                doc.setFontSize(9);
                doc.text(initX + 1, newY + 4, "ANTECEDENTS");
                doc.setFontSize(8);

                for (i = 0; i < print.patient.passif.antecedent.length; i++) {
                    doc.text(initX + 1, newY + 8 + i * 4, "  " + (print.patient.passif.antecedent[i].antecedent || ""));

                }
                newY = newY + i * 4 + 6;
            }

            if (print.patient.passif.traitement != undefined && print.patient.passif.traitement.length > 0) {
                doc.setFontSize(9);
                doc.text(initX + 1, newY + 4, "TRAITEMENTS");
                doc.setFontSize(8);

                for (i = 0; i < print.patient.passif.traitement.length; i++) {
                    doc.text(initX + 1, newY  + 8 + i * 4, " " + (print.patient.passif.traitement[i].traitement || ""));

                }
                newY = newY + i * 4 + 6;
            }


            if (print.patient.passif.allergie != undefined && print.patient.passif.allergie.length > 0) {
                doc.setFontSize(9);
                doc.text(initX + 1, newY + 4, "ALLERGIES");
                doc.setFontSize(8);

                for (i = 0; i < print.patient.passif.allergie.length; i++) {
                    doc.text(initX + 1, newY + 8 + i * 4, "  " + (print.patient.passif.allergie[i].allergie || ""));

                }
                newY = newY + i * 4 + 6;
            }

            doc.rect(initX, initY, 200, (newY - initY) + 4);
            newY += 4;
            initY = newY;
        }

        let cadreCirconstances = function () {
            initX = 5;

            let newY = initY;

            newY += 4;

            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, newY, "CIRCONSTANCES DE LA DETRESSE");

            if (print.observation.circonstancielle != undefined && print.observation.circonstancielle.length > 0) {

                doc.setFont(font.regular.fontName, font.regular.fontStyle);
                doc.setFontSize(8);

                newY += 5;

                for (let i = 0; i < print.observation.circonstancielle.length; i++) {

                    doc.text(initX + 1, newY, "" + moment(print.observation.circonstancielle[i].timeCode).format('DD-MM-YYYY à HH:mm'));
                    doc.text(initX + 30, newY, "" + (print.observation.circonstancielle[i].auteur || ""));
                    doc.text(initX + 70, newY, "" + (print.observation.circonstancielle[i].type || ""));

                    if (print.observation.circonstancielle[i].type == "circonstances") {
                        let  circonstances = "";
                             circonstances = print.observation.circonstancielle[i].circonstances;
                             circonstances ? circonstances = doc.splitTextToSize(circonstances, 110) : circonstances = "-" ;
                             doc.text(initX + 90, newY, circonstances);
                        newY += circonstances.length === 1 ? 5 : 5 * (circonstances.length) -2 ;
                    } else if (print.observation.circonstancielle[i].type == "signe") {
                        let  signe = "";
                             signe = print.observation.circonstancielle[i].signe;
                             signe ? signe = doc.splitTextToSize(signe, 110) : signe = "-" ;
                             doc.text(initX + 90, newY, signe);
                        newY += signe.length === 1 ? 5 : 5 * (signe.length) -2;
                    } else if (print.observation.circonstancielle[i].type == "plainte") {
                         let  plainte = "";
                              plainte = print.observation.circonstancielle[i].plainte;
                              plainte ? plainte = doc.splitTextToSize(plainte, 110) : plainte = "-" ;
                              doc.text(initX + 90, newY, plainte);
                        newY += plainte.length === 1 ? 5 : 5 * (plainte.length) -2;
                    } else if (print.observation.circonstancielle[i].type == "intox") {
                        doc.text(initX + 90, newY, "" + (print.observation.circonstancielle[i].produit || ""));
                         let  details = "";
                              details = print.observation.circonstancielle[i].intoxdetails;
                              details ? details = doc.splitTextToSize(details, 110) : details = "-" ;
                              doc.text(initX + 90, newY, details);
                        newY += details.length === 1 ? 5 : 5 * (details.length) -2;
                    } else if (print.observation.circonstancielle[i].type == "cinetique") {
                        doc.text(initX + 90, newY, ((print.observation.circonstancielle[i].chute == "oui") ? "Chute de Grande hauteur " : ""));
                        doc.text(initX + 90, newY, ((print.observation.circonstancielle[i].vitesse == "oui") ? "Accident à grande vitesse " : ""));
                        doc.text(initX + 90, newY, "Victime d'accident :  " + (print.observation.circonstancielle[i].avp || ""));
                        if (print.observation.circonstancielle[i].cinetique != "") {
                            let cinetique = print.observation.circonstancielle[i].cinetique;
                            cinetique ? cinetique = doc.splitTextToSize(cinetique, 90) : cinetique = "-" ;
                            doc.text(initX + 90, newY + 3 , cinetique);
                            newY += cinetique.length === 1 ? 4 : 5 * (cinetique.length) -2;
                        }
                        newY += 5;
                    }
                }
            }

            doc.rect(initX, initY, 200, (newY - initY) + 4);

            initY = newY + 4;
        }



        let cadreSuite = function () {
            initX = 5;
            doc.rect(initX, initY, 200, 25);
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, initY + 4, "SUITE DONNÉE");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);

            if (print.conclusion.regulation != undefined && print.conclusion.regulation.length > 0) {

                let num = print.conclusion.regulation.length - 1;
                doc.text(initX + 10, initY + 8, "LSP");
                doc.text(initX + 40, initY + 8, "Décharge");
                doc.text(initX + 70, initY + 8, "Transfert");
                doc.text(initX + 100, initY + 8, "Evacuation");
                doc.text(initX + 130, initY + 8, "Autre");
                doc.text(initX + 3, initY + 12, "Commentaire  : " + (print.conclusion.regulation[num].message || "") + "");
                doc.text(initX + 3, initY + 16, "Destination  : " + (print.conclusion.regulation[num].hopital || "") + "");
                doc.text(initX + 3, initY + 20, "Service  :  " + (print.conclusion.regulation[num].service || "") + "");
                doc.text(initX + 3, initY + 24, "Vecteur  :  " + (print.conclusion.regulation[num].moyen || "") + "");

                if (print.conclusion.regulation[num].type == "lsp")      { doc.rect(initX + 3, initY + 5, 3, 3, "F" );}
                else     { doc.rect(initX + 3, initY + 5, 3, 3);}


                if (print.conclusion.regulation[num].type == "decharge") { doc.rect(initX + 33, initY + 5, 3, 3, "F" );}
                else     { doc.rect(initX + 33, initY + 5, 3, 3);}


                if (print.conclusion.regulation[num].type == "transfert") { doc.rect(initX + 63, initY + 5, 3, 3, "F" );}
                else     { doc.rect(initX + 63, initY + 5, 3, 3);}


                if (print.conclusion.regulation[num].type == "regulation") { doc.rect(initX + 93, initY + 5, 3, 3, "F" );}
                else     { doc.rect(initX + 93, initY + 5, 3, 3);}


                if (print.conclusion.regulation[num].type == "autre")      { doc.rect(initX + 123, initY + 5, 3, 3, "F" );}
                else     { doc.rect(initX + 123, initY + 5, 3, 3);}



            }

            initY = initY + 25
        }

        let cadreDiag = function () {
            initX = 5;

            let newY = initY;
            let i = 0;
            let line = 0;

            newY += 4;

            if (print.conclusion.diagnostic != undefined && print.conclusion.diagnostic.length > 0) {

            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, newY, "DIAGNOSTIC");



                doc.setFont(font.regular.fontName, font.regular.fontStyle);
                doc.setFontSize(8);

                newY += 5;

                for (let i = 0; i < print.conclusion.diagnostic.length; i++) {

                    doc.text(initX + 1, newY + i * 5, "" + moment(print.conclusion.diagnostic[i].timeCode).format('DD-MM-YYYY à HH:mm'));
                    doc.text(initX + 30, newY + i * 5, "" + (print.conclusion.diagnostic[i].auteur || ""));
                    doc.text(initX + 65, newY + i * 5, "" + (print.conclusion.diagnostic[i].diagnostic || ""));
                    doc.text(initX + 100, newY + i * 5, "" + (print.conclusion.diagnostic[i].categorie || ""));
                    doc.text(initX + 125, newY + i * 5, "" + (print.conclusion.diagnostic[i].commentaires || ""));


                }
            line = i*5 + 25 ;
            }

            doc.rect(initX, initY, 200, line);

            initY = initY + line ;
        }


        let exposure = function () {

            initY += 4;

            header(initX, initY, "BILAN LÉSIONNEL", 200, true);

            doc.setFontSize(6);

            doc.text(initX + 1, initY + 8, "Horaires");
            doc.text(initX + 30, initY + 8, "par qui ");
            doc.text(initX + 70, initY + 8, "type ");

            let j = 0;
            let line = 0;

            if (print.observation.exposure != undefined && print.observation.exposure.length > 1) {

                doc.setFontSize(8);

                let i;

                for (i = 0; i < print.observation.exposure.length; i++) {

                    doc.text(initX + 1, initY + 13 + line * 5, "" + moment(print.observation.exposure[i].timeCode).format('DD-MM-YYYY à HH:mm'));
                    doc.text(initX + 30, initY + 13 + line * 5, "" + (print.observation.exposure[i].auteur || ""));
                    doc.text(initX + 70, initY + 13 + line * 5, "" + (print.observation.exposure[i].etype || ""));
                    doc.text(initX + 100, initY + 13 + line * 5, "" + (print.observation.exposure[i].elocalisation || ""));
                    doc.text(initX + 115, initY + 13 + line * 5, "" + (print.observation.exposure[i].elateralisation || ""));
                    if (print.observation.exposure[i].edescription != "") {
                            let edescription = print.observation.exposure[i].edescription;
                            edescription = doc.splitTextToSize(edescription, 65);
                            doc.text(initX + 130, initY + 13 + line * 5, edescription);
                            let nbline = edescription.length;
                            line = line + nbline -1;
                        }
                    line++;

                }

                j = i * 5;
            }

            doc.setFontSize(8);
            initX = 5;
            initY += 16 + j;
        }

        let cadreBilanuv = function () {

            initX = 5;

            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, initY + 4, "BILAN D'URGENCE VITALE");

            let line = 0;

            if (print.observation.vitale != undefined && print.observation.vitale.length > 0) {

                doc.setFont(font.regular.fontName, font.regular.fontStyle);

                doc.setFontSize(8);

                let i;

                line = 1;

                for (i = 0; i < 1; i++) {

                    doc.text(initX + 1, initY + 8, "" + moment(print.observation.vitale[i].timeCode).format('DD-MM-YYYY à HH:mm'));
                    doc.text(initX + 40, initY + 8, "" + (print.observation.vitale[i].auteur));

                    doc.text(initX + 3, initY + 13, "Airways ");
                    doc.text(initX + 20, initY + 13, "Liberté des VAS : " + (print.observation.vitale[i].airways.aliberte || "NR") + " Atteinte du rachis : " + (print.observation.vitale[i].airways.arachis || "NR"));
                    doc.text(initX + 20, initY + 18, "Respiration présente : " + (print.observation.vitale[i].airways.arespiration || "") + " Vomissement : " + (print.observation.vitale[i].airways.avomissement || "NR"));
                    doc.text(initX + 3, initY + 28, "Breathing ");
                    doc.text(initX + 20, initY + 28, "Respiration efficace : " + (print.observation.vitale[i].breathing.befficace || "NR") + " le patient peut parler ? : " + (print.observation.vitale[i].breathing.bparler || "NR"));
                    doc.text(initX + 20, initY + 33, "Bruits respiratoires : " + (print.observation.vitale[i].breathing.bbruits || "NR"));
                    doc.text(initX + 20, initY + 38, "Signes de détresse : " + (print.observation.vitale[i].breathing.bdetresse || "NR"));
                    doc.text(initX + 20, initY + 43, "Qualification de la respiration : " + (print.observation.vitale[i].breathing.bqualification || "NR"));
                    doc.text(initX + 3, initY + 53, "Circulation ");
                    doc.text(initX + 20, initY + 53, "Présence d'un pouls radial : " + (print.observation.vitale[i].circulation.cpouls || "NR") + " TRC < 3 secondes : " + (print.observation.vitale[i].circulation.ctrc || "NR") + " Hémorragie : " + (print.observation.vitale[i].circulation.chemorragie || "NR"));
                    doc.text(initX + 20, initY + 58, "Signes de détresse : " + (print.observation.vitale[i].circulation.cdetresse || "NR"));
                    doc.text(initX + 20, initY + 63, "Qualification de la circulation : " + (print.observation.vitale[i].circulation.cqualification || "NR"));
                    doc.text(initX + 3, initY + 73, "Disabilities ");
                    doc.text(initX + 20, initY + 73, "Glasgow = " + (print.observation.vitale[i].disabilities.glasgow || "NR") + ",   Y" + (print.observation.vitale[i].disabilities.dsgy || "(NR)") + "-V" + (print.observation.vitale[i].disabilities.dsgv || "(NR)")+ "-M" + (print.observation.vitale[i].disabilities.dsgm || "(NR)")     );
                    doc.text(initX + 20, initY + 78, "Conscience :" + (print.observation.vitale[i].disabilities.dconscience || "NR") + " Convulsions : " + (print.observation.vitale[i].disabilities.dconvulsions || "NR") + " durée convulsions : " + (print.observation.vitale[i].disabilities.dconvulsionsduree || "NR"));
                    doc.text(initX + 20, initY + 83, "PCI : " + (print.observation.vitale[i].disabilities.dpci || "NR") + " Durée de la PCI : " + (print.observation.vitale[i].disabilities.dpciduree || "NR") + " orientation dans temps et espace : " + (print.observation.vitale[i].disabilities.dorientation || "NR"));
                    doc.text(initX + 20, initY + 88, "Pupilles droite : " + (print.observation.vitale[i].disabilities.dpupilledroite || "NR") + " Pupille gauche : " + (print.observation.vitale[i].disabilities.dpupillegauche || "NR"));
                    doc.text(initX + 20, initY + 93, "Trouble de la motricité : " + (print.observation.vitale[i].disabilities.dmotricite || "NR") + " Trouble de la sensibilite : " + (print.observation.vitale[i].disabilities.dsensibilite || "NR"));

                    initY = initY + 100;
                }
            }

            initY += 20 + 100 * line;
        }



        let traitement = function () {

            initX = 5;
            initY += 1;

            header(initX, initY, "traitement", 201, true);

            doc.setFontSize(6);
            doc.text(initX + 1, initY + 8, "Horaires");
            doc.text(initX + 30, initY + 8, "par qui ");
            doc.text(initX + 70, initY + 8, "type ");
            doc.text(initX + 110, initY + 8, "produit");

            let i = 0;

            if (print.action.traitement != undefined && print.action.traitement.length > 0) {

                doc.setFontSize(8);
                let line = 0;
                for (i = 0; i < print.action.traitement.length; i++) {

                    doc.text(initX + 1, initY + 13 + line  * 5, "" + moment(print.action.traitement[i].timeCode).format('DD-MM-YYYY à HH:mm'));
                    doc.text(initX + 30, initY + 13 + line * 5, "" + (print.action.traitement[i].auteur || ""));
                    doc.text(initX + 70, initY + 13 + line * 5, "" + (print.action.traitement[i].typeTraitement || ""));
                    if (print.action.traitement[i].typeTraitement == "Medicaments") {
                        let medic = "" + (print.action.traitement[i].produit || "") + "  " + (print.action.traitement[i].medicament || "") + "  " + (print.action.traitement[i].posologie || "") + " " + (print.action.traitement[i].unite || "") + " voie :  " + (print.action.traitement[i].voie || "")
                            medic = doc.splitTextToSize(medic, 90);
                        doc.text(initX + 100, initY + 13 + line * 5, medic);
                        initY += 4 * (medic.length - 1);
                    }
                    if (print.action.traitement[i].typeTraitement == "Solutes") {
                        doc.text(initX + 110, initY + 13 + line * 5, "" + (print.action.traitement[i].solute || "") + "  " + (print.action.traitement[i].quantite || "") + " vitesse : " + (print.action.traitement[i].vitesse || ""));
                    }
                    line++
                }
            }

            doc.setFontSize(8);
            initX = 5;
            initY += 16 + i * 5;
        }

        let geste = function () {
            initX = 5;
            initY += 1;
            header(initX, initY, "GESTES EFFECTUÉS", 201, true);
            doc.setFontSize(6);
            doc.text(initX + 1, initY + 8, "Horaires");
            doc.text(initX + 30, initY + 8, "par qui ");
            doc.text(initX + 70, initY + 8, "type ");
            doc.text(initX + 100, initY + 8, "détail");

            let i = 0;

            if (print.action.geste != undefined && print.action.geste.length > 0) {
                doc.setFontSize(8);
                let line = 0;
                for (i = 0; i < print.action.geste.length; i++) {
                    doc.text(initX + 1, initY + 13 + line * 5, "" + moment(print.action.geste[i].timeCode).format('DD-MM-YYYY à HH:mm'));
                    doc.text(initX + 30, initY + 13 + line * 5, "" + (print.action.geste[i].auteur || ""));
                    doc.text(initX + 70, initY + 13 + line * 5, "" + (print.action.geste[i].typeGeste || ""));
                    if (print.action.geste[i].typeGeste == "attelle") {
                        doc.text(initX + 100, initY + 13 + line * 5, "" + (print.action.geste[i].type || "") + " position : " + (print.action.geste[i].position || "") + "    " + (print.action.geste[i].lateralite || ""));
                    }
                    if (print.action.geste[i].typeGeste == "atelle") {
                        doc.text(initX + 100, initY + 13 + line * 5, "" + (print.action.geste[i].type || "") + " position : " + (print.action.geste[i].position || "") + "    " + (print.action.geste[i].lateralite || ""));
                    }
                    if (print.action.geste[i].typeGeste == "iot") {
                        doc.text(initX + 100, initY + 13 + line * 5, " sonde de " + (print.action.geste[i].tube || "") + " repère " + (print.action.geste[i].repere || "") + " pression ballonet " + (print.action.geste[i].pression || ""));
                        line++
                        doc.text(initX + 100, initY + 13 + line * 5, " etCO2 à intubation : " + (print.action.geste[i].etco2 || "") + " Cormack :  " + (print.action.geste[i].cormack || ""));
                    }
                    if (print.action.geste[i].typeGeste == "position") {
                        doc.text(initX + 100, initY + 13 + line * 5, "" + (print.action.geste[i].position || ""));
                    }
                    if (print.action.geste[i].typeGeste == "pansement") {
                        doc.text(initX + 100, initY + 13 + line * 5, "Localisation " + (print.action.geste[i].localisation || "") + " type : " + (print.action.geste[i].type || ""));
                        line++
                         let commentaire = "";
                         commentaire = print.action.geste[i].commentaire;
                            commentaire ? commentaire = doc.splitTextToSize(commentaire, 100) : commentaire = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, commentaire);
                            initY += 4 * (commentaire.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "rcp") {
                        doc.text(initX + 100, initY + 13 + line * 5, "Ventilation artificielle : " + (print.action.geste[i].compression || "") + " Compression thoraciques : " + (print.action.geste[i].compression || "") + " Défibrillation :  " + (print.action.geste[i].defibrillateur || ""));
                        line++
                        let remarques = "-";
                            remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "aspiration") {
                        doc.text(initX + 100, initY + 13 + line * 5, " " + (print.action.geste[i].aspiration || "") + " Sécrétions abondantes  " + (print.action.geste[i].abondant || "") + " Sécrétions sales :  " + (print.action.geste[i].aspect || ""));
                    }
                    if (print.action.geste[i].typeGeste == "vvp") {
                        doc.text(initX + 100, initY + 13 + line * 5, "Type : " + (print.action.geste[i].catheter || "") + " Position : " + (print.action.geste[i].position || "") + " Latéralité :   " + (print.action.geste[i].lateralite || ""));
                        line++
                         let remarques = "-";
                             remarques = print.action.geste[i].remarques;
                             remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                             doc.text(initX + 100, initY + 13 + line * 5, remarques);
                             initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "vvc") {
                        doc.text(initX + 100, initY + 13 + line * 5, " Position" + (print.action.geste[i].position || "") + " nombre de voies " + (print.action.geste[i].voies || "") + " pose echoguidée :  " + (print.action.geste[i].echo || ""));
                        line++
                         let remarques = "-";
                             remarques = print.action.geste[i].remarques;
                             remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                             doc.text(initX + 100, initY + 13 + line * 5, remarques);
                             initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "suture") {
                        doc.text(initX + 100, initY + 13 + line * 5, " Site " + (print.action.geste[i].site || "") + " Fil  " + (print.action.geste[i].fil || "") + " type de fil :  " + (print.action.geste[i].typefil || ""));
                        line++
                         let remarques = "-";
                             remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "sng") {
                        doc.text(initX + 100, initY + 13 + line * 5, "Position " + (print.action.geste[i].position || "") + " Taille " + (print.action.geste[i].taille || "") + " usage :  " + (print.action.geste[i].usage || "") + " vérification :  " + (print.action.geste[i].verification || ""));
                        line++
                         let remarques = "-";
                            remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "dio") {
                        doc.text(initX + 100, initY + 13 + line * 5, "Position  : " + (print.action.geste[i].position || "") + " latéralite " + (print.action.geste[i].lateralite || "") + " type :  " + (print.action.geste[i].type || "") + " taille :  " + (print.action.geste[i].taille || ""));
                        line++
                         let remarques = "-";
                             remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "conditionnement") {
                        doc.text(initX + 100, initY + 13 + line * 5, " Couverture " + (print.action.geste[i].couverture || "") + " immobilisation " + (print.action.geste[i].immobilisation || ""));
                        line++
                         let remarques = "-";
                             remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "bu") {
                        doc.text(initX + 100, initY + 13 + line * 5, "leucocytes " + (print.action.geste[i].leucocytes || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "nitrites " + (print.action.geste[i].nitrites || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "pH " + (print.action.geste[i].ph || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "proteines " + (print.action.geste[i].proteines || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "glucoses " + (print.action.geste[i].glucoses || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "cetones " + (print.action.geste[i].cetones || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "urobilinogene " + (print.action.geste[i].urobilinogene || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "bilirubine " + (print.action.geste[i].bilirubine || "")); line++
                        doc.text(initX + 100, initY + 13 + line * 5, "densite " + (print.action.geste[i].densite || "")); line++
                         let remarques = "-";
                             remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "dc") {
                         let remarques = "-";
                            remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }
                    if (print.action.geste[i].typeGeste == "su") {
                        doc.text(initX + 100, initY + 13 + line * 5, " Type " + (print.action.geste[i].type || "") + " Sonde " + (print.action.geste[i].sonde || "") + " taille :  " + (print.action.geste[i].taille || ""));
                        line++
                         let remarques = "";
                            remarques = print.action.geste[i].remarques;
                            remarques ? remarques = doc.splitTextToSize(remarques, 100) : remarques = "-" ;
                            doc.text(initX + 100, initY + 13 + line * 5, remarques);
                            initY += 4 * (remarques.length - 1);
                    }

                    line++
                }
            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 + i * 5;
        }

        let surveillance = function () {
            header(initX, initY, "EVOLUTIONS DES PARAMÈTRES VITAUX", 201, true);
            doc.setFontSize(6);
            doc.text(initX + 1, initY + 8, "Horaires");
            doc.text(initX + 20, initY + 8, "PA d||g");
            doc.text(initX + 55, initY + 8, "glasgow");
            doc.text(initX + 65, initY + 8, "FC");
            doc.text(initX + 75, initY + 8, "TRC");
            doc.text(initX + 85, initY + 8, "FR | SaO2");
            doc.text(initX + 105, initY + 8, "ENS  EVA");
            doc.text(initX + 120, initY + 8, "EtCO2");
            doc.text(initX + 135, initY + 8, "glycémie");
            doc.text(initX + 150, initY + 8, "température");
            doc.text(initX + 170, initY + 8, "hemocue");

            let i = 0;

            if (print.observation.surveillance != undefined && print.observation.surveillance.length > 0) {
                doc.setFontSize(8);
                let y = 0;

                for (i = 0; i < print.observation.surveillance.length; i++) {

                    doc.text(initX + 1, initY + 13 + y * 5, moment(print.observation.surveillance[i].timeCode).format("HH:mm"));
                    doc.text(initX + 20, initY + 13 + y * 5, "" + (print.observation.surveillance[i].pasd || "") + "/" + (print.observation.surveillance[i].padd || "") + " || " + (print.observation.surveillance[i].pasg || "") + "/" + (print.observation.surveillance[i].padg || ""));
                    doc.text(initX + 55, initY + 13 + y * 5, "" + (print.observation.surveillance[i].glasgow || ""));
                    doc.text(initX + 65, initY + 13 + y * 5, "" + (print.observation.surveillance[i].fc || ""));
                    doc.text(initX + 75, initY + 13 + y * 5, "" + (print.observation.surveillance[i].trc || ""));
                    doc.text(initX + 85, initY + 13 + y * 5, "" + (print.observation.surveillance[i].fr || "") + " | " + (print.observation.surveillance[i].spo2 || ""));
                    doc.text(initX + 105, initY + 13 + y * 5, "" + (print.observation.surveillance[i].ens || ""));
                    doc.text(initX + 120, initY + 13 + y * 5, "" + (print.observation.surveillance[i].etco2 || ""));
                    doc.text(initX + 135, initY + 13 + y * 5, "" + (print.observation.surveillance[i].glycemie || ""));
                    doc.text(initX + 150, initY + 13 + y * 5, "" + (print.observation.surveillance[i].temperature || ""));
                    doc.text(initX + 170, initY + 13 + y * 5, "" + (print.observation.surveillance[i].hemocue || ""));
                    y++
                }

            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 + i * 5;
        }

        const soap = () => {
            const row1X = initX + 1;
            const row2X = initX + 40;
            const lineHeight = 4;
            const maxHeight = 280;
            let cursorY = initY + 10;

            header(initX, initY, 'EXAMENS CLINIQUES et OBSERVATIONS', 201, true);
            doc.setFontSize(8);

            if (!print.observation.soap && !print.observation.soap.length) {
                return;
            }

            for (const item of print.observation.soap) {
                const hdm = (item.hdm) ? doc.splitTextToSize(item.hdm.trim(), 160) : [];
                const examen = (item.examen) ? doc.splitTextToSize(item.examen.trim(), 160) : [];

                // Figure out if we have enough space before printing row
                const requiredY = (hdm.length + examen.length) * lineHeight;
                if ((cursorY + requiredY) > maxHeight) {
                    doc.addPage();
                    cursorY = 10;
                }

                doc.setFontSize(6);
                const created = moment(item.timeCode).format('DD-MM-YYYY à HH:mm');
                doc.text(row1X, cursorY, created);
                doc.text(row1X, cursorY + 2, item.auteur || '');

                doc.setFontSize(8);

                doc.text(row2X, cursorY, hdm);
                cursorY += (hdm.length * lineHeight);

                doc.text(row2X, cursorY, examen);
                cursorY += (examen.length * lineHeight);
            }

            initX = 5;
        };

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        let pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        let page_1 = function () {
            cadreAntecedent();
            cadreCirconstances();
            cadreSuite();
            cadreDiag();
            exposure();
            cadreBilanuv();
            footer();
            initY = 3;
            initX = 5;
        }

        let page_2 = function () {
            surveillance();
            soap();
            footer();
            initY = 3;
            initX = 5;
        }

        let page_3 = function () {
            traitement();
            geste();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

        doc.addPage();

        pdf_header();
        page_2();

        doc.addPage();

        pdf_header();
        page_3();

        let saved = moment(print.cas.timeCode).format('YYYY-MM-DD-HH-mm');

        doc.save(saved + '-Fiche-Bilan_' + print.cas.niv + '.pdf');
        doc.autoPrint();
    }

    public pdfConseilTC(data) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        doc.addFileToVFS(font.italic.postScriptName, font.italic.base64string);
        doc.addFont(font.italic.postScriptName, font.italic.fontName, font.italic.fontStyle);

        let logo = this.PARAM.currentParametre['logo'];

        let initY = 5;
        let initX = 5;

        // doc.setFontType("normal");
        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);

        // metadata
        doc.setProperties({
            title: 'Conseil aux traumatisés Cranien sur GERO',
            subject: 'Fiche TC secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;

        let header = function (posX, posY, text, size, tgle) {
            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
            doc.setFillColor(200, 200, 200);
            doc.rect(posX, posY, size, 5, 'FD');
            doc.setFillColor(255, 255, 255);
        }

        let bloc2 = function () {

        }

        let bloc1 = function () {
            initY = 3;
            let imgData = 'data:image/' + logo[0].format + ';base64,' + logo[0].base64 + '';
            doc.addImage(imgData, logo[0].format2, 3, 3, Number(logo[0].width), Number(logo[0].height));
            doc.setFontSize(12);
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.text(initX + 30, initY + 8, "  ");
            doc.text(initX + 30, initY + 14, "");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 1 * 190 / 3 + initX;
        }

        let bloc3 = function () {
            //Bloc3
            initY = 3;
            doc.setFontSize(8);
            // doc.setFontType("italic");
            doc.setFont(font.italic.fontName, font.italic.fontStyle);
            doc.text(initX + 15, initY + 5, logo[0].adresse);
            doc.text(initX + 15, initY + 10, "" + logo[0].cp + "  " + logo[0].ville + "");
            doc.text(initX + 15, initY + 15, "tél." + logo[0].tel + " ");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 190 / 3 + initX;
        }

        let cadre1 = function () {
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                print.cas.niv,
                {
                    format: "CODE128",
                    displayValue: false
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 15);

            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 13);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(print.cas.timeCode).format('DD-MM-YYYY') || ""));
            doc.text(initX + 50, initY + 8, "Heure : " + (moment(print.cas.timeCode).format('HH:mm') || ""));
            doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            initY = initY + 15
        }

        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFontSize(10);
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            doc.text("" + print.cas.niv, initX + 70, initY + 4);

            // doc.setFontType("normal");
            doc.setFontSize(9);
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 140, initY + 8, "Date de naissance : " + (print.patient.age || " ") + " ");
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévenir : ", initX + 1, initY + 20);
            if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                doc.setFontSize(8);

                for (i = 0; i < print.patient.prevenir.length; i++) {
                    doc.text(initX + 1, initY + 24 + i * 4, "nom : " + (print.patient.prevenir[i].nom) +", " + (print.patient.prevenir[i].prenom || ""));
                    doc.text(initX + 60, initY + 24 + i * 4, "lien : " + (print.patient.prevenir[i].lien || ""));
                    doc.text(initX + 90, initY + 24 + i * 4, "téléphone : " + (print.patient.prevenir[i].tel || ""));

                }
                newY = i * 4 + 24;
            }

            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let conseil = function () {
            header(initX, initY, "Conseils", 201, true);

            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(16);
            // doc.setFontType("bold");
            doc.text(initX + 50, initY + 20, "Conseils aux traumatisé cranien");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(11);
            // doc.setFontType("normal");
            initY = initY + 30

            doc.text(initX + 15, initY + 12, " Madame, Monsieur");

            let paragraphe1 = "Les circonstances du traumatisme et l'examen clinique n'ont pas identifié d'élément de gravité. Par conséquent, il n'est pas nécessaire de pratiquer de radiographies du crâne ou d'autre examen complémentaire ni de vous hospitaliser.  ";
            let paragraphe2 = "De nombreuses études faites chez des personnes victimes de traumatisme crânien semblable au votre ont montré que la surveillance est plus utile que la radiographie du crâne pour dépister une éventuelle complication (conférence de consensus de la Société de Réanimation de Langue Française). ";
            let paragraphe3 = "Cette surveillance sera effectuée au mieux par vous­même et votre entourage pendant au moins 24 heures.  ";

            let p1 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe1, 180)
            let p2 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe2, 180)
            let p3 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe3, 180)

            doc.text(initX + 15, initY + 30, p1);
            doc.text(initX + 15, initY + 50, p2);
            doc.text(initX + 15, initY + 70, p3);

            doc.text(initX + 15, initY + 80, " Vous ne devez pas rester seul et l'apparition d'un des signes suivants : ");
            doc.text(initX + 25, initY + 85, " -  maux de tête persistants ou devenant de plus en plus violents, ");
            doc.text(initX + 25, initY + 90, " -  somnolence anormale avec difficulté à obtenir un réveil complet,");
            doc.text(initX + 25, initY + 95, " -  vomissements répétés (deux ou plus),");
            doc.text(initX + 25, initY + 100, " -  troubles de la parole avec langage incohérent ou incompréhensible,");
            doc.text(initX + 25, initY + 105, " -  comportement inhabituel ou anormal.");
            doc.text(initX + 15, initY + 110, " doit vous faire appeler le 15 ou consulter dans l'hôpital le plus proche de votre domicile. ");
            doc.text(initX + 15, initY + 115, "  ");
            doc.text(initX + 15, initY + 120, "  Nous vous recommandons pendant quelques jours de :");
            doc.text(initX + 25, initY + 125, "  - de ne pas rester seul");
            doc.text(initX + 25, initY + 130, "  - d'éviter tout effort violent,");
            doc.text(initX + 25, initY + 135, "  - d'éviter la conduite de véhicule ,");
            doc.text(initX + 15, initY + 140, "  ");

            doc.text(initX + 15, initY + 150, "  Au moindre doute, vous pouvez toujours appeler votre médecin traitant ou consulter dans un ");
            doc.text(initX + 15, initY + 155, "  service d'urgence qui est à votre disposition 24 H/24.");
        }

        doc.setFontSize(8);
        initX = 5;
        initY += 16;

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };


        let pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        let page_1 = function () {
            conseil();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

        doc.save('Conseil_TC_.pdf');
        doc.autoPrint();
    }

    public pdfConseilTCEN(data) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        doc.addFileToVFS(font.italic.postScriptName, font.italic.base64string);
        doc.addFont(font.italic.postScriptName, font.italic.fontName, font.italic.fontStyle);

        let logo = this.PARAM.currentParametre['logo'];

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        // doc.setFontType("normal");

        // metadata
        doc.setProperties({
            title: 'Conseil aux traumatisés Cranien sur GERO',
            subject: 'Fiche TC secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;

        let header = function (posX, posY, text, size, tgle) {
            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
            doc.setFillColor(200, 200, 200);
            doc.rect(posX, posY, size, 5, 'FD');
            doc.setFillColor(255, 255, 255);
        }

        let bloc2 = function () {

        }

        let bloc1 = function () {
            initY = 3;
            let imgData = 'data:image/' + logo[0].format + ';base64,' + logo[0].base64 + '';
            doc.addImage(imgData, logo[0].format2, 3, 3, Number(logo[0].width), Number(logo[0].height));
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(12);
            // doc.setFontType("bold");
            doc.text(initX + 30, initY + 8, "  ");
            doc.text(initX + 30, initY + 14, "");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 1 * 190 / 3 + initX;
        }

        let bloc3 = function () {
            //Bloc3
            initY = 3;
            // doc.setFontType("italic");
            doc.setFont(font.italic.fontName, font.italic.fontStyle);
            doc.setFontSize(8);
            doc.text(initX + 15, initY + 5, logo[0].adresse);
            doc.text(initX + 15, initY + 10, "" + logo[0].cp + "  " + logo[0].ville + "");
            doc.text(initX + 15, initY + 15, "tél." + logo[0].tel + " ");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 190 / 3 + initX;
        }

        let cadre1 = function () {
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                print.cas.niv,
                {
                    format: "CODE128",
                    displayValue: false
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 15);

            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 13);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(print.cas.timeCode).format('DD-MM-YYYY') || ""));
            doc.text(initX + 50, initY + 8, "Heure : " + (moment(print.cas.timeCode).format('HH:mm') || ""));
            doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            initY = initY + 15
        }

        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
             doc.text("" + print.cas.niv, initX + 70, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 140, initY + 8, "Date de naissance : " + (print.patient.age || " ") + " ");
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévénir : ", initX + 1, initY + 20);
            if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                doc.setFontSize(8);

                for (i = 0; i < print.patient.prevenir.length; i++) {
                    doc.text(initX + 1, initY + 24 + i * 4, "nom : " + (print.patient.prevenir[i].nom) +", " + (print.patient.prevenir[i].prenom || ""));
                    doc.text(initX + 60, initY + 24 + i * 4, "lien : " + (print.patient.prevenir[i].lien || ""));
                    doc.text(initX + 90, initY + 24 + i * 4, "téléphone : " + (print.patient.prevenir[i].tel || ""));

                }
                newY = i * 4 + 24;
            }

            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let conseil = function () {
            header(initX, initY, "Conseils", 201, true);

            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(16);
            // doc.setFontType("bold");
            doc.text(initX + 50, initY + 20, "RECOMMENDATIONS TO PATIENTS ");
            doc.text(initX + 50, initY + 30, "WITH HEAD TRAUMA");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(11);
            // doc.setFontType("normal");
            initY = initY + 30

            doc.text(initX + 15, initY + 12, " Madam, Sir,");

            let paragraphe1 = "You have been victim of what seems to be a light head trauma. There is no necessity to undergo neither X­rays nor other exams, or to hospitalise you.  ";
            let paragraphe2 = "Nevertheless it is wise to watch you for the coming days. This can be done best by yourself or the persons close to you. ";
            let paragraphe3 = "  ";

            let p1 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe1, 180)
            let p2 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe2, 180)
            let p3 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe3, 180)


            doc.text(initX + 15, initY + 30, p1);
            doc.text(initX + 15, initY + 50, p2);
            doc.text(initX + 15, initY + 70, p3);
            initY = initY - 10
            doc.text(initX + 15, initY + 80, " Appearing of : ");
            doc.text(initX + 25, initY + 85, " -  Persistent headache, ");
            doc.text(initX + 25, initY + 90, " -  Sleepiness or alteration of consciousness,");
            doc.text(initX + 25, initY + 95, " -  Repetitive vomiting,");
            doc.text(initX + 25, initY + 100, " -  Impaired vision,");
            doc.text(initX + 25, initY + 105, " -  Trouble in talking");
            doc.text(initX + 25, initY + 110, " -  Unusual behaviour,");
            doc.text(initX + 15, initY + 115, " should make you quickly come back to the Emergency Department, closer from your home ");
            doc.text(initX + 15, initY + 130, " We advise you for the coming days : ");
            doc.text(initX + 25, initY + 135, "  - not to stay alone,");
            doc.text(initX + 25, initY + 140, "  - to beware of heavy physical efforts,");
            doc.text(initX + 25, initY + 145, "  - to avoid steering any car.");
            doc.text(initX + 15, initY + 150, "  ");

            doc.text(initX + 15, initY + 170, "  If there is any doubt, you may always call your general practitioner or come to the Emergency ");
            doc.text(initX + 15, initY + 175, "  Department, which is at your disposition 24 hours a day.");
        }

        doc.setFontSize(8);

        initX = 5;
        initY += 16;

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        let pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        let page_1 = function () {
            conseil();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

        doc.save('Conseil_TC_EN.pdf');
        doc.autoPrint();
    }

    public pdfConseilPlaies(data) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        doc.addFileToVFS(font.italic.postScriptName, font.italic.base64string);
        doc.addFont(font.italic.postScriptName, font.italic.fontName, font.italic.fontStyle);

        let logo = this.PARAM.currentParametre['logo'];

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        //doc.setFontType("normal");

        // metadata
        doc.setProperties({
            title: 'Conseil aux traumatisés Cranien sur GERO',
            subject: 'Fiche TC secouriste',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;

        let header = function (posX, posY, text, size, tgle) {
            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
            doc.setFillColor(200, 200, 200);
            doc.rect(posX, posY, size, 5, 'FD');
            doc.setFillColor(255, 255, 255);
        }

        let bloc2 = function () {

        }

        let bloc1 = function () {
            initY = 3;
            let imgData = 'data:image/' + logo[0].format + ';base64,' + logo[0].base64 + '';
            doc.addImage(imgData, logo[0].format2, 3, 3, Number(logo[0].width), Number(logo[0].height));
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(12);
            // doc.setFontType("bold");
            doc.text(initX + 30, initY + 8, "  ");
            doc.text(initX + 30, initY + 14, "");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 1 * 190 / 3 + initX;
        }

        let bloc3 = function () {
            //Bloc3
            initY = 3;
            // doc.setFontType("italic");
            doc.setFont(font.italic.fontName, font.italic.fontStyle);
            doc.setFontSize(8);
            doc.text(initX + 15, initY + 5, logo[0].adresse);
            doc.text(initX + 15, initY + 10, "" + logo[0].cp + "  " + logo[0].ville + "");
            doc.text(initX + 15, initY + 15, "tél." + logo[0].tel + " ");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 190 / 3 + initX;
        }

        let cadre1 = function () {
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                print.cas.niv,
                {
                    format: "CODE128",
                    displayValue: false
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 15);

            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 13);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(print.cas.timeCode).format('DD-MM-YYYY') || ""));
            doc.text(initX + 50, initY + 8, "Heure : " + (moment(print.cas.timeCode).format('HH:mm') || ""));
            doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            initY = initY + 15
        }

        let cadreIdentification = function () {
            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            doc.text("" + print.cas.niv, initX + 70, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 80, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 140, initY + 8, "Date de naissance : " + (print.patient.age || " ") + " ");
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévénir : ", initX + 1, initY + 20);

            if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                doc.setFontSize(8);

                for (i = 0; i < print.patient.prevenir.length; i++) {
                    doc.text(initX + 1, initY + 24 + i * 4, "nom : " + (print.patient.prevenir[i].nom) +", " + (print.patient.prevenir[i].prenom || ""));
                    doc.text(initX + 60, initY + 24 + i * 4, "lien : " + (print.patient.prevenir[i].lien || ""));
                    doc.text(initX + 90, initY + 24 + i * 4, "téléphone : " + (print.patient.prevenir[i].tel || ""));

                }
                newY = i * 4 + 24;
            }

            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let conseil = function () {
            header(initX, initY, "Conseils", 201, true);

            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(16);
            doc.text(initX + 50, initY + 20, "CONSEILS AUX PATIENTS ");
            doc.text(initX + 50, initY + 30, "PORTEURS DE PETITES PLAIES");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(11);
            // doc.setFontType("normal");
            initY = initY + 30

            let paragraphe1 = "Vous avez été victime d’un traumatisme ayant nécessité la pose de points de suture. ";
            let paragraphe2 = "Pour votre bien-être et votre sécurité, veuillez respecter les instructions suivantes : ";
            let paragraphe3 = " ";

            let p1 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe1, 180)
            let p2 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe2, 180)
            let p3 = doc
                .setFontSize(11)
                .splitTextToSize(paragraphe3, 180)

            doc.text(initX + 15, initY + 20, " Madame, Monsieur");

            doc.text(initX + 15, initY + 30, p1);
            doc.text(initX + 15, initY + 40, p2);
            doc.text(initX + 15, initY + 70, p3);

            initY = initY - 30

            doc.text(initX + 25, initY + 80, " - Exécuter immédiatement l’ordonnance qui vous a été, éventuellement, remise ");
            doc.text(initX + 25, initY + 85, " - Retirer le pansement sec mis sur la plaie après 24 h. ");
            doc.text(initX + 25, initY + 90, " - Garder la plaie propre et sèche (Eviter les bains et les shampooing).");
            doc.text(initX + 25, initY + 95, " - Surveiller votre température quotidiennement jusqu’à l’ablation des fils.");
            initY = initY + 10
            doc.text(initX + 15, initY + 100, " L’apparition de :");
            doc.text(initX + 25, initY + 110, " - saignement,");
            doc.text(initX + 25, initY + 115, " - suintement,");
            doc.text(initX + 25, initY + 120, " - lâchage d’un ou plusieurs points, ");
            doc.text(initX + 25, initY + 125, " - rougeur anormale,");
            doc.text(initX + 25, initY + 130, " - fièvre,");
            doc.text(initX + 25, initY + 135, " - ganglion,");
            doc.text(initX + 15, initY + 145, " doit vous faire contacter rapidement le service des urgences le plus proche de chez vous.");
            doc.text(initX + 15, initY + 150, "  ");
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.text(initX + 15, initY + 160, "  Le service des urgences est à votre disposition 24h/24. ");

        }

        doc.setFontSize(8);

        initX = 5;
        initY += 16;

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        let pdf_header = function () {
            bloc1();
            bloc2();
            bloc3();
            cadre1();
            cadreIdentification();
        }

        let page_1 = function () {
            conseil();
            footer();
            initY = 3;
            initX = 5;
        }

        pdf_header();
        page_1();

        doc.save('Conseil_plaies_.pdf');
    }

    public pdfFicheMed(data) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        let logo = this.PARAM.currentParametre['logo'];

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        //doc.setFontType("normal");
        doc.setTextColor(0, 0, 0);

        // metadata
        doc.setProperties({
            title: 'Fiche Médicale sur GERO',
            subject: '-',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;

        let formattedAge = 0;
        if (print.patient.age != undefined) {
            formattedAge = moment().diff(moment(print.patient.age), "years");
        }

        let header = function (posX, posY, text, size, tgle) {
            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
        }

        let cadre2 = function () {
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(15);
            doc.text(initX + 10, initY - 5, print.cas.niv);

            doc.setFontSize(28);
            doc.text(initX + 70, initY - 3, "Fiche Médicale ");
            doc.setFontSize(32);

            if (formattedAge < 18) {
                //doc.setFontType("bold");
                doc.setFont(font.bold.fontName, font.bold.fontStyle);
                doc.setTextColor(255, 0, 0);
                doc.text(initX + 150, initY - 3, "MINEUR");
            }

            initY = initY + 20
        }

        let cadre1 = function () {
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                print.cas.niv,
                {
                    format: "CODE128",
                    displayValue: false
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 15);

            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 13);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(print.cas.timeCode).format('DD-MM-YYYY') || ""));
            doc.text(initX + 50, initY + 8, "Heure : " + (moment(print.cas.timeCode).format('HH:mm') || ""));
            doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            initY = initY + 15
        }

        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            doc.text("" + print.cas.niv, initX + 70, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 70, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 120, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") + ",  ");
            doc.text(initX + 185, initY + 8, "" + (formattedAge || "") + " ans ");
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévénir : ", initX + 1, initY + 20);

            if (print.patient.prevenir != undefined && print.patient.prevenir.length > 1) {
                doc.setFontSize(8);

                for (i = 0; i < print.patient.prevenir.length; i++) {
                    doc.text(initX + 1, initY + 24 + i * 4, "  " + (print.patient.prevenir[i].nom) + "   " + (print.patient.prevenir[i].prenom || ""));
                    doc.text(initX + 80, initY + 24 + i * 4, "lien : " + (print.patient.prevenir[i].lien || ""));
                    doc.text(initX + 110, initY + 24 + i * 4, " téléphone : " + (print.patient.prevenir[i].tel || ""));
                }
                newY = i * 4 + 24;
            }

            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let cadre3 = function () {

            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(12);
            doc.rect(initX, initY, 200, 20);
            doc.text(initX + 5, initY + 8, "Motif d'admission : ");
            doc.text(initX + 10, initY + 15, print.cas.pathologie);

            initY = initY + 20
        }

        let cadre4 = function () {

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.setFillColor(200, 200, 200);
            doc.rect(initX, initY, 200, 30, 'FD');
            doc.rect(initX, initY, 50, 30);
            doc.rect(initX+50, initY+10, 150, 10);

            doc.text(initX + 5,  initY + 15, "Équipe Médicale  ");
            doc.text(initX + 55, initY + 6, "Interne :  ");
            doc.text(initX + 55, initY + 16, "Médecin sénior : ");
            doc.text(initX + 55, initY + 26, "Infirmier :  ");

            initY = initY + 30
        }

        let cadre6 = function () {

            //doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 60);
            doc.rect(initX, initY, 50, 60);
            doc.rect(initX, initY, 25, 60);
            doc.rect(30, initY + 20, 175, 20);
            doc.text(initX + 5, initY + 30, "Examens  ");
            doc.text(initX + 26, initY + 10, "Surveillance  ");
            doc.text(initX + 26, initY + 30, "ATCD ");
            doc.text(initX + 26, initY + 34, "TTT  ");
            doc.text(initX + 26, initY + 50, "Examen  ");
            doc.text(initX + 26, initY + 54, "détaillé  ");

            initY = initY + 60
        }

        let cadre7 = function () {

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);

            doc.setFillColor(200, 200, 200);

            doc.rect(initX, initY, 200, 40, 'FD');
            doc.rect(initX, initY, 200, 10);
            doc.rect(initX, initY, 200, 20);
            doc.rect(initX, initY, 50, 40);
            doc.rect(initX + 85, initY + 10, 50, 10);     // pour les codifications

            doc.text(initX + 10, initY + 5, "Diagnostic  ");
            doc.text(initX + 10, initY + 15, "Codification  ");
            doc.text(initX + 52, initY + 15, "TRAUMATOLOGIE  ");
            doc.text(initX + 90, initY + 14, "MÉDECINE  ");
            doc.text(initX + 90, initY + 18, "(préciser ci-dessous)");
            doc.text(initX + 140, initY + 15, "TOXIQUES (alcool – drogues)  ");

            doc.text(initX + 10, initY + 25, "Sous classification  ");
            doc.text(initX + 52, initY + 25, "Cardio-respi  ");
            doc.text(initX + 82, initY + 25, "Digestif  ");
            doc.text(initX + 110, initY + 25, "dermato - Allergo  ");
            doc.text(initX + 160, initY + 25, "Neurologie  ");
            doc.text(initX + 52, initY + 30, "Rhumato  ");
            doc.text(initX + 82, initY + 30, "ORL  ");
            doc.text(initX + 110, initY + 30, "Gynéco Urinaire  ");
            doc.text(initX + 52, initY + 35, "Autre (préciser) :  ");

            initY = initY + 40
        }

        let cadre8 = function () {

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 30);
            doc.rect(initX, initY, 50, 30);
            doc.rect(initX, initY, 200, 20);
            doc.text(initX + 10, initY + 8, "Médicaments  ");
            doc.text(initX + 10, initY + 12, "délivrés  ");
            doc.text(initX + 10, initY + 25, "Ordonnance faite  ");
            doc.text(initX + 60, initY + 25, "OUI  ");
            doc.text(initX + 80, initY + 25, "NON  ");

            doc.rect(initX + 100, initY + 20, 50, 10);
            doc.text(initX + 110, initY + 25, "Reconvocation  ");
            doc.text(initX + 160, initY + 25, "OUI  ");
            doc.text(initX + 180, initY + 25, "NON  ");

            initY = initY + 30
        }

        let cadre9 = function () {

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.setFillColor(255, 0, 0);
            doc.rect(initX, initY, 200, 20, 'FD');
            doc.rect(initX, initY, 50, 20);
            doc.rect(initX, initY, 200, 10);
            doc.text(initX + 10, initY + 5, "Heure de sortie  ");
            doc.text(initX + 10, initY + 17, "Orientation  ");

            doc.rect(initX + 85, initY + 10, 50, 10);

            doc.text(initX + 55, initY + 14, "Laissé sur place ou   ");
            doc.text(initX + 55, initY + 18, "retour sur site  ");
            doc.text(initX + 90, initY + 14, "Évacuation vers :   ");
            doc.text(initX + 140, initY + 14, "Évacuation médicalisée vers :   ");

            initY = initY + 20
        }

        doc.setFontSize(8);

        initX = 5;
        initY += 16;

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "fiche générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        cadre2();
        cadre1();
        cadreIdentification();
        cadre3();
        cadre4();
        cadre6();
        cadre7();
        cadre8();
        cadre9();
        footer();

        initY = 3;
        initX = 5;

        doc.save('Fiche_Medicale.pdf');
    }


    public pdfAnonymes() {


    }

    public pdfCourier(data, courier) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        let user = this.USER.INFO;

        let initY = 5;
        let initX = 5;

        // doc.setFontType("normal");
        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        doc.setTextColor(0, 0, 0);

        // metadata
        doc.setProperties({
            title: 'Fiche Médicale sur GERO',
            subject: '-',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;
        // let courier = courier;

        let formattedAge = 0;

        if (print.patient.age != undefined) {
            formattedAge = moment().diff(moment(print.patient.age), "years");
        }

        let header = function (posX, posY, text, size, tgle) {

            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
        }

        let cadre2 = function () {
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(15);
            // doc.text(initX + 10, initY -5 , print.cas.niv);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(28);
            doc.text(75, 20, "Ordonnance");

            initY = initY + 20
        }



        let cadre1 = function () {
            //Bloc3
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                courier.rpps,
                {
                    format: "CODE128",
                    font:"roboto",
                    displayValue: true
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 20);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 20);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(courier.timeCode).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 90, initY + 3, "Heure : " + (moment(courier.timeCode).format('HH:mm') || "") + " ");
            // doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            doc.text(initX + 50, initY + 8, "Prescripteur :  " + (courier.auteur || "") + ", " + (courier.metier || "") + "");
            doc.text(initX + 50, initY + 13, "RPPS : " + courier.rpps || "");
            // doc.text(initX + 1, initY + 18, "numéro Assurance maladie personnel : " + ordo.numeroAM || "" );

            initY = initY + 20
        }

        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;

            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            //doc.text("NIV :  " + print.cas.niv, initX + 40, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 70, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 120, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 180, initY + 8, "Âge : " + (formattedAge || "") + " ans ");
            doc.text("Taille :  " + (print.patient.taille || ""), initX + 180, initY + 13);
            doc.text("Poids :  " + (print.patient.poids || ""), initX + 180, initY + 18);

            if (print.patient.taille != undefined && print.patient.poids != undefined) {
                let taille = print.patient.taille / 100;
                let IMC = print.patient.poids / (taille * taille);

                doc.text("IMC :  " + (IMC.toFixed(2) || ""), initX + 180, initY + 23);
            }
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            //doc.text("Personne à prévénir : ", initX + 1, initY + 20);
            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let courrier = function () {
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(12);
            doc.text("Titre :  " + (courier.titre || ""), initX + 10, initY + 8);
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(10);
            let printcourier = doc.splitTextToSize(courier.texte, 180);
            doc.text((printcourier || ""), initX + 10, initY + 18);
        }

        let acte = function () {
            doc.setTextColor(200);
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(50);
            doc.text(50, 200, "ACTE GRATUIT ", null, 45);
        }

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "ordonnance générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        footer();

        initY = 3;
        initX = 5;

        let page_1 = function () {
            acte();

            cadre2();
            cadre1();
            cadreIdentification();
            courrier();

            footer();

            initY = 3;
            initX = 5;
        }

        page_1();

        doc.save('Ordonnance.pdf');
    }



    public pdfConvoc(data, convoc) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        let user = this.USER.INFO;

        let initY = 5;
        let initX = 5;

        // doc.setFontType("normal");
        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        doc.setTextColor(0, 0, 0);

        // metadata
        doc.setProperties({
            title: 'Fiche Médicale sur GERO',
            subject: '-',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;
        // let courier = courier;

        let formattedAge = 0;

        if (print.patient.age != undefined) {
            formattedAge = moment().diff(moment(print.patient.age), "years");
        }

        let header = function (posX, posY, text, size, tgle) {

            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
        }

        let cadre2 = function () {
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(15);
            // doc.text(initX + 10, initY -5 , print.cas.niv);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(28);
            doc.text(75, 20, "Ordonnance");

            initY = initY + 20
        }



        let cadre1 = function () {
            //Bloc3
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                convoc.rpps,
                {
                    format: "CODE128",
                    font:"roboto",
                    displayValue: true
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 20);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 20);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(convoc.timeCode).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 90, initY + 3, "Heure : " + (moment(convoc.timeCode).format('HH:mm') || "") + " ");
            // doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            doc.text(initX + 50, initY + 8, "Prescripteur :  " + (convoc.auteur || "") + ", " + (convoc.metier || "") + "");
            doc.text(initX + 50, initY + 13, "RPPS : " + convoc.rpps || "");
            // doc.text(initX + 1, initY + 18, "numéro Assurance maladie personnel : " + ordo.numeroAM || "" );

            initY = initY + 20
        }

        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;

            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            //doc.text("NIV :  " + print.cas.niv, initX + 40, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 70, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 120, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 180, initY + 8, "Âge : " + (formattedAge || "") + " ans ");
            doc.text("Taille :  " + (print.patient.taille || ""), initX + 180, initY + 13);
            doc.text("Poids :  " + (print.patient.poids || ""), initX + 180, initY + 18);

            if (print.patient.taille != undefined && print.patient.poids != undefined) {
                let taille = print.patient.taille / 100;
                let IMC = print.patient.poids / (taille * taille);

                doc.text("IMC :  " + (IMC.toFixed(2) || ""), initX + 180, initY + 23);
            }
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            //doc.text("Personne à prévénir : ", initX + 1, initY + 20);
            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let courrier = function () {
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(12);
            doc.text("Titre :  " + (convoc.titre || ""), initX + 10, initY + 8);
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(10);
            let printcourier = doc.splitTextToSize(convoc.texte, 180);
            doc.text((printcourier || ""), initX + 10, initY + 18);
        }

        let acte = function () {
            doc.setTextColor(200);
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(50);
            doc.text(50, 200, "ACTE GRATUIT ", null, 45);
        }

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "ordonnance générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        footer();

        initY = 3;
        initX = 5;

        let page_1 = function () {
            acte();

            cadre2();
            cadre1();
            cadreIdentification();
            courrier();

            footer();

            initY = 3;
            initX = 5;
        }

        page_1();

        doc.save('Ordonnance.pdf');
    }





    public pdfOrdofil(data, geste) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        let user = this.USER.INFO;

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        // doc.setFontType("normal");
        doc.setTextColor(0, 0, 0);

        // metadata
        doc.setProperties({
            title: 'Fiche Médicale sur GERO',
            subject: '-',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;
        let suture = geste;

        let formattedAge = 0;

        if (print.patient.age != undefined) {
            formattedAge = moment().diff(moment(print.patient.age), "years");
        }

        let header = function (posX, posY, text, size, tgle) {
            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
        }

        let cadre2 = function () {
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(15);
            // doc.text(initX + 10, initY -5 , print.cas.niv);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(28);
            doc.text(75, 20, "Ordonnance retrait de fils");

            initY = initY + 20
        }

        let cadre1 = function () {
            //Bloc3
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                user.rpps,
                {
                    format: "CODE128",
                    font:"roboto",
                    displayValue: true
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 20);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 20);
            doc.text(initX + 50, initY + 3, "Date : " + (moment().format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 90, initY + 3, "Heure : " + (moment().format('HH:mm') || "") + " ");
            // doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            doc.text(initX + 50, initY + 8, "Prescripteur :  " + (user.nom || "") + ", " + (user.prenom || "") + "");
            doc.text(initX + 50, initY + 13, "RPPS : " + user.rpps || "");
            // doc.text(initX + 1, initY + 18, "numéro Assurance maladie personnel : " + ordo.numeroAM || "" );

            initY = initY + 20
        }


        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            //doc.text("NIV :  " + print.cas.niv, initX + 40, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 70, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 120, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 180, initY + 8, "Âge : " + (formattedAge || "") + " ans ");
            doc.text("Taille :  " + (print.patient.taille || ""), initX + 180, initY + 13);
            doc.text("Poids :  " + (print.patient.poids || ""), initX + 180, initY + 18);

            if (print.patient.taille != undefined && print.patient.poids != undefined) {
                let taille = print.patient.taille / 100;
                let IMC = print.patient.poids / (taille * taille);

                doc.text("IMC :  " + (IMC.toFixed(2) || ""), initX + 180, initY + 23);
            }
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévénir : ", initX + 1, initY + 20);
            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let prescription = function () {
            header(initX, initY, "prescription", 201, true);
            //columns(initX, initY + 5, 0, 5);
            //doc.setFontSize(6);
            //doc.text(initX + 1, initY + 8, "Horaires");
            //doc.text(initX + 30, initY + 8, "par qui ");
            //doc.text(initX + 70, initY + 8, "type ");
            //doc.text(initX + 110, initY + 8, "produit");

            let i;

            doc.text(initX + 55, initY + 18, "Ablation de fils par IDE dans " + (geste.ablation || "7") + " jours");

            doc.setFontSize(8);
            initX = 65;
            initY += 16 + i * 5;
        }



        let acte = function () {
            doc.setTextColor(200);
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(50);
            doc.text(50, 200, "ACTE GRATUIT ", null, 45);
        }

        let duplicata = function () {
            doc.setTextColor(200);
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(50);
            doc.text(50, 200, "DUPLICATA ", null, 45);
        }

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "ordonnance générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        cadre2();
        cadre1();
        cadreIdentification();
        prescription();
        acte();

        footer();
        initY = 3;
        initX = 5;

        let page_1 = function () {

            cadre2();
            cadre1();
            cadreIdentification();
            prescription();
            acte();

            footer();
            initY = 3;
            initX = 5;
        }

        let page_2 = function () {

            cadre2();
            cadre1();
            cadreIdentification();
            prescription();
            duplicata();

            footer();
            initY = 3;
            initX = 5;
        }

        page_1();

        doc.addPage();

        page_2();

        doc.save('Ordonnance-retrait-fil.pdf');
    }

    public pdfOrdonnance(data, ordonnance) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        let user = this.USER.INFO;

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        // doc.setFontType("normal");
        doc.setTextColor(0, 0, 0);

        // metadata
        doc.setProperties({
            title: 'Fiche Médicale sur GERO',
            subject: '-',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;
        let ordo = ordonnance;

        let formattedAge = 0;

        if (print.patient.age != undefined) {
            formattedAge = moment().diff(moment(print.patient.age), "years");
        }

        let header = function (posX, posY, text, size, tgle) {
            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
        }

        let cadre2 = function () {
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(15);
            // doc.text(initX + 10, initY -5 , print.cas.niv);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(28);
            doc.text(75, 20, "Ordonnance");

            initY = initY + 20
        }

        let cadre1 = function () {
            //Bloc3
            initY = 25;
            initX = 5;

            var canvas = document.createElement("canvas");
            JsBarcode(
                canvas,
                ordo.rpps,
                {
                    format: "CODE128",
                    font:"roboto",
                    displayValue: true
                }
            );

            var imgData = canvas.toDataURL("image/png");
            doc.addImage(imgData, 'PNG', 6, 24, 40, 20);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 20);
            doc.text(initX + 50, initY + 3, "Date : " + (moment(ordo.timeCode).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 90, initY + 3, "Heure : " + (moment(ordo.timeCode).format('HH:mm') || "") + " ");
            // doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            doc.text(initX + 50, initY + 8, "Prescripteur :  " + (ordo.auteur || "") + ", " + (ordo.metier || "") + "");
            doc.text(initX + 50, initY + 13, "RPPS : " + ordo.rpps || "");
            // doc.text(initX + 1, initY + 18, "numéro Assurance maladie personnel : " + ordo.numeroAM || "" );

            initY = initY + 20
        }


        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            //doc.text("NIV :  " + print.cas.niv, initX + 40, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 70, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 120, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 180, initY + 8, "Âge : " + (formattedAge || "") + " ans ");
            doc.text("Taille :  " + (print.patient.taille || ""), initX + 180, initY + 13);
            doc.text("Poids :  " + (print.patient.poids || ""), initX + 180, initY + 18);

            if (print.patient.taille != undefined && print.patient.poids != undefined) {
                let taille = print.patient.taille / 100;
                let IMC = print.patient.poids / (taille * taille);

                doc.text("IMC :  " + (IMC.toFixed(2) || ""), initX + 180, initY + 23);
            }
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévénir : ", initX + 1, initY + 20);
            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let prescription = function () {
            header(initX, initY, "prescription", 201, true);
            //columns(initX, initY + 5, 0, 5);
            //doc.setFontSize(6);
            //doc.text(initX + 1, initY + 8, "Horaires");
            //doc.text(initX + 30, initY + 8, "par qui ");
            //doc.text(initX + 70, initY + 8, "type ");
            //doc.text(initX + 110, initY + 8, "produit");

            let i;
            if (ordo.prescription != undefined && ordo.prescription.length >= 1) {
                doc.setFontSize(10);
                for (i = 0; i < ordo.prescription.length; i++) {
                    // columns(initX, initY+i*5+5, 1, 5);
                    doc.text(initX + 5, initY + 13 + i * 5, "" + (ordo.prescription[i].medicament || ""));
                    doc.text(initX + 70, initY + 13 + i * 5, "" + (ordo.prescription[i].posologie || ""));
                    doc.text(initX + 150, initY + 13 + i * 5, "QSP : " + (ordo.prescription[i].qsp || ""));
                }
            }
            doc.setFontSize(8);
            initX = 5;
            initY += 16 + i * 5;
        }

        let quantite = function () {
            doc.rect(170, 280, 35, 12);
            doc.setFontSize(6);
            doc.text(172, 282, "nombre de médicaments prescrits : ");
            doc.setFontSize(16);
            doc.text(185, 288, "" + (ordo.prescription.length || ""));
            initX = 5;
            initY += 16;
        }

        let acte = function () {
            doc.setTextColor(200);
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(50);
            doc.text(50, 200, "ACTE GRATUIT ", null, 45);
        }

        let duplicata = function () {
            doc.setTextColor(200);
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(50);
            doc.text(50, 200, "DUPLICATA ", null, 45);
        }

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "ordonnance générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        cadre2();
        cadre1();
        cadreIdentification();
        prescription();
        quantite();
        acte();

        footer();
        initY = 3;
        initX = 5;

        let page_1 = function () {

            cadre2();
            cadre1();
            cadreIdentification();
            prescription();
            quantite();
            acte();

            footer();
            initY = 3;
            initX = 5;
        }

        let page_2 = function () {

            cadre2();
            cadre1();
            cadreIdentification();
            prescription();
            quantite();
            duplicata();

            footer();
            initY = 3;
            initX = 5;
        }

        page_1();

        doc.addPage();

        page_2();

        doc.save('Ordonnance.pdf');
    }

    public pdfDecharge(data) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        doc.addFileToVFS(font.italic.postScriptName, font.italic.base64string);
        doc.addFont(font.italic.postScriptName, font.italic.fontName, font.italic.fontStyle);

        let user = this.USER.CONTEXT;

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        // doc.setFontType("normal");
        doc.setTextColor(0, 0, 0);

        // metadata
        doc.setProperties({
            title: 'Fiche Decharge sur GERO',
            subject: '-',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;

        let formattedAge = 0;

        if (print.patient.age != undefined) {
            formattedAge = moment().diff(moment(print.patient.age), "years");
        }

        let header = function (posX, posY, text, size, tgle) {

            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
        }

        let cadre2 = function () {
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(15);
            // doc.text(initX + 10, initY -5 , print.cas.niv);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(24);
            doc.text(25, 20, "Fiche décharge de responsabilité");

            initY = initY + 20
        }

        let cadre1 = function () {
            //Bloc3
            initY = 25;
            initX = 5;
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 10);
            doc.text(initX + 1, initY + 3, "Date : " + (moment().format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 50, initY + 3, "Heure : " + (moment().format('HH:mm') || "") + " ");
            //doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            initY = initY + 10
        }

        let cadreIdentification = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            doc.text(initX + 1, initY + 4, "IDENTIFICATION DU PATIENT");
            //doc.text("NIV :  " + print.cas.niv, initX + 40, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Nom :  " + (print.patient.nom || ""), initX + 1, initY + 8);
            doc.text("Prénom :  " + (print.patient.prenom || ""), initX + 70, initY + 8);
            /*doc.text(initX+140, initY+8,    "Nom JF : ____________________");*/
            doc.text(initX + 120, initY + 8, "Date de naissance :  " + (moment(print.patient.age).format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 180, initY + 8, "Âge : " + (formattedAge || "") + " ans ");
            doc.text("Taille :  " + (print.patient.taille || ""), initX + 180, initY + 13);
            doc.text("Poids :  " + (print.patient.poids || ""), initX + 180, initY + 18);

            if (print.patient.taille != undefined && print.patient.poids != undefined) {
                let taille = print.patient.taille / 100;
                let IMC = print.patient.poids / (taille * taille);
                doc.text("IMC :  " + (IMC.toFixed(2) || ""), initX + 180, initY + 23);
            }
            /*doc.text(initX+80, initY+14, "Lieu de naissance : _____________________");*/

            doc.text("Adresse : " + (print.patient.adresse || " ") + " " + (print.patient.cp || "") + "  " + (print.patient.ville || ""), initX + 1, initY + 12);
            doc.text("Téléphone : " + (print.patient.telephone || ""), initX + 1, initY + 16);
            doc.text("Personne à prévénir : ", initX + 1, initY + 20);
            doc.rect(initX, initY, 200, newY);
            initY = initY + newY
        }

        let cadreDecharge = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            //doc.text("NIV :  " + print.cas.niv, initX + 40, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);

            if (print.patient.sexe == 'homme') {
                doc.text(initX + 10, initY + 8, "Je soussigné,  : " + (print.patient.nom || "") + ", " + (print.patient.prenom || "") + ", ");
                doc.text(initX + 10, initY + 13, "Né le  : " + (moment(print.patient.age).format('DD-MM-YYYY')) + " ");
            }

            if (print.patient.taille == 'femme') {
                doc.text(initX + 10, initY + 8, "Je soussignée,  : " + (formattedAge || "") + " ans ");
                doc.text(initX + 10, initY + 13, "Née le  : " + (moment(print.patient.age).format('DD-MM-YYYY')) + " ");
            }

            doc.text(initX + 10, initY + 18, "Demeurant :  " + (print.patient.adresse || "") + ", " + (print.patient.cp || "") + " " + (print.patient.ville || "") + ", " + (print.patient.pays || "") + "");
            doc.text(initX + 10, initY + 23, "agissant en qualité de  (rayer la mention inutile) :  ");
            doc.text(initX + 20, initY + 28, "   - moi-même :  ");
            doc.text(initX + 20, initY + 33, "   - autre (préciser)  :  ");
            doc.text(initX + 10, initY + 38, "refuse pour : (rayer la mention inutile) ");
            doc.text(initX + 20, initY + 43, "   - moi-même :  ");
            doc.text(initX + 20, initY + 48, "   - autre (préciser) :  ");
            doc.text(initX + 60, initY + 53, "nom   ");
            doc.text(initX + 60, initY + 58, "prenom    ");
            doc.text(initX + 60, initY + 63, "Age:    ");
            initY = initY + 10
            doc.text(initX + 10, initY + 68, "Reconnait avoir été informé(e) par le personnel  de :   ");
            doc.text(initX + 10, initY + 73, "Représenté par :   " + (user.auteur || "") + " ");
            doc.setFontSize(7);
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);

            doc.text(initX + 10, initY + 78, "Entourer la mention correspondante    ");
            doc.setFontSize(9);
            // doc.setFontType("italic");
            doc.setFont(font.italic.fontName, font.italic.fontStyle);

            initY = initY + 10

            doc.text(initX + 10, initY + 83, "  - de mon état de santé tel qu’il est décrit sur la fiche d’intervention établie ce jour par ses soins dont le double m’a été remis, ");
            doc.text(initX + 10, initY + 88, "  - des conséquences éventuelles qui pourraient en découler ");
            doc.text(initX + 10, initY + 93, "  - de mon refus de soin");
            doc.text(initX + 10, initY + 98, "  - de mon refus de transport en milieu hospitalier");
            doc.text(initX + 10, initY + 103, "  - de mon refus de soins et de mon refus de transport en milieu hospitalier");
            doc.text(initX + 10, initY + 108, "  A savoir (mentionner le motif d’intervention) :");

            initY = initY + 10
            doc.text(initX + 10, initY + 113, "et déclare persister dans ce refus et décharger l’équipe de secours et le responsable d’intervention sus cités de  ");
            doc.text(initX + 10, initY + 118, "toutes les conséquences qui pourraient s’en suivre.  ");
            initY = initY + 10
            doc.text(initX + 10, initY + 123, " Fait le " + (moment().format('DD-MM-YYYY') || "") + " à  : ");
            doc.setFontSize(7);
            // doc.setFontType("italic");
            doc.setFont(font.italic.fontName, font.italic.fontStyle);
            doc.text(initX + 10, initY + 127, " en deux exemplaires originaux - un exemplaire remis à l’intéressé, l’autre conservé par l'équipe de secours ");

            doc.rect(initX + 10, initY + 130, 90, 30);
            doc.rect(initX + 100, initY + 130, 90, 30);
            doc.rect(initX + 10, initY + 160, 90, 30);
            doc.rect(initX + 100, initY + 160, 90, 30);
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text(initX + 15, initY + 134, "Signature de l’intéressé précédé de la mention  ");
            doc.setFontSize(7);
            doc.text(initX + 15, initY + 138, " «lu et approuvé, bon pour décharge»  ");
            doc.setFontSize(9);
            doc.text(initX + 105, initY + 134, " Signature d'un représentant des secours ");
            doc.setFontSize(7);
            doc.text(initX + 105, initY + 138, " (noter nom, prénom et fonction) ");
            doc.setFontSize(9);
            doc.text(initX + 15, initY + 163, " Témoin n°1 ");
            doc.text(initX + 105, initY + 163, " Témoin n°2 ");

            initY = initY + newY
        }

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "Décharge générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        cadre2();
        cadre1();
        cadreIdentification();
        cadreDecharge();

        footer();
        initY = 3;
        initX = 5;

        doc.save('Decharge.pdf');
    }

    public pdfIntervention(data, moyen) {

        let doc = new jsPDF();

        let font = this.PDF.fonts.get('Arimo');

        doc.addFileToVFS(font.regular.postScriptName, font.regular.base64string);
        doc.addFont(font.regular.postScriptName, font.regular.fontName, font.regular.fontStyle);

        doc.addFileToVFS(font.bold.postScriptName, font.bold.base64string);
        doc.addFont(font.bold.postScriptName, font.bold.fontName, font.bold.fontStyle);

        doc.addFileToVFS(font.italic.postScriptName, font.italic.base64string);
        doc.addFont(font.italic.postScriptName, font.italic.fontName, font.italic.fontStyle);

        let user = this.USER.CONTEXT;
        let logo = this.PARAM.currentParametre['logo'];

        let initY = 5;
        let initX = 5;

        doc.setFont(font.regular.fontName, font.regular.fontStyle);
        doc.setFontSize(5);
        // doc.setFontType("normal");
        doc.setTextColor(0, 0, 0);

        // metadata
        doc.setProperties({
            title: 'Fiche d intervention sur GERO',
            subject: '-',
            author: 'SARL AGS',
            keywords: ' Assistance Gestion Secours, AGS, www.gestion-secours.com',
            creator: 'PM MAUXION'
        });

        let print = data;

        let header = function (posX, posY, text, size, tgle) {

            let dec = 0;
            if (size == undefined)
                size = 285
            if (tgle == undefined) {
                tgle = false;
            }
        }

        let bloc1 = function () {
            initY = 3;
            let imgData = 'data:image/' + logo[0].format + ';base64,' + logo[0].base64 + '';
            doc.addImage(imgData, logo[0].format2, 3, 3, Number(logo[0].width), Number(logo[0].height));
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(12);
            // doc.setFontType("bold");
            doc.text(initX + 30, initY + 8, "  ");
            doc.text(initX + 30, initY + 14, "");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 1 * 190 / 3 + initX;
        }

        let bloc2 = function () {
            initY = 3;
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(14);
            // doc.setFontType("bold");
            doc.text(initX - 10, initY + 10, "Fiche d intervention N°" + (print.numero || "") + "");
            doc.text(initX + 45, initY + 10, "");
            initX = 190 / 3 + initX;
        }

        let bloc3 = function () {
            //Bloc3
            initY = 3;
            // doc.setFontType("italic");
            doc.setFont(font.italic.fontName, font.italic.fontStyle);
            doc.setFontSize(8);
            doc.text(initX + 15, initY + 5, logo[0].adresse);
            doc.text(initX + 15, initY + 10, "" + logo[0].cp + "  " + logo[0].ville + "");
            doc.text(initX + 15, initY + 15, "tél." + logo[0].tel + " ");
            /*doc.rect(initX, initY, 285/3, 20);*/
            initX = 190 / 3 + initX;
        }

        let cadre1 = function () {
            //Bloc3
            initY = 25;
            initX = 5;
            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setTextColor(0, 0, 0);
            doc.setFontSize(9);
            doc.rect(initX, initY, 200, 10);
            doc.text(initX + 1, initY + 3, "Date : " + (moment().format('DD-MM-YYYY') || "") + " ");
            doc.text(initX + 50, initY + 3, "Heure : " + (moment().format('HH:mm') || "") + " ");
            // doc.text(initX + 140, initY + 3, "Poste : " + (print.cas.poste || ""));
            initY = initY + 10
        }

        let cadreIntervention = function () {

            initX = 5;
            let newY = 24;
            let i;
            // doc.setFontType("bold");
            doc.setFont(font.bold.fontName, font.bold.fontStyle);
            doc.setFontSize(10);
            doc.setTextColor(0, 0, 0);
            doc.text(initX + 1, initY + 4, "INTERVENTION");
            // doc.text("NIV :  " + print.cas.niv, initX + 40, initY + 4);

            // doc.setFontType("normal");
            doc.setFont(font.regular.fontName, font.regular.fontStyle);
            doc.setFontSize(9);
            doc.text("Requérant :  " + (print.requerant || ""), initX + 1, initY + 8);
            doc.text("telephone :  " + (print.telephone || ""), initX + 70, initY + 8);

            doc.text("Adresse : " + (print.adresse || " "), initX + 1, initY + 12);
            doc.text("observation : " + (print.observation || ""), initX + 1, initY + 16);
            doc.text("motif : " + (print.motif || ""), initX + 1, initY + 20);
            doc.rect(initX, initY, 200, newY);
            initY = initY + newY

            newY = initY

            if (moyen != undefined && moyen.length > 0) {
                doc.setFontSize(8);
                let z;
                for (z = 0; z < moyen.length; z++) {
                    doc.text(initX + 1, newY + 8 + z * 5, " Moyen : " + moyen[z].moyen.nom || "");
                    doc.text(initX + 60, newY + 8 + z * 5, " type :" + (moyen[z].moyen.type || ""));
                    doc.text(initX + 90, newY + 8 + z * 5, " famille :" + (moyen[z].moyen.famille || ""));
                    doc.text(initX + 140, newY + 8 + z * 5, " balise GPS " + (moyen[z].moyen.balise || ""));
                }
            }
        }

        function footer() {
            doc.setFontSize(7);
            doc.text(1, 295, "Fiche d intervention générée par GERO" + '\u00A9' + " -Version Gero3-  édité par AGS - www.gestion-secours.com ");
        };

        bloc1();
        bloc2();
        bloc3();
        cadre1();
        cadreIntervention();

        footer();
        initY = 3;
        initX = 5;

        doc.autoPrint();
        doc.save("Intervention-" + (print.numero || "") + ".pdf");
    }
}

import { Injectable } from '@angular/core';
import { Subject, from } from 'rxjs';
import { map, mergeMap, mapTo } from 'rxjs/operators';
import * as moment from 'moment';

import { ActionModel } from '../visualisation/models/action.model';
import { CasModel } from '../visualisation/models/cas.model';
import { ConclusionModel } from '../visualisation/models/conclusion.model';
import { CourierModel } from '../visualisation/detail-cas/forms/conclusion-courier.component';
import { InterventionModel } from '../visualisation/models/intervention.model';
import { NotificationModel } from '../visualisation/models/notification.model';
import { ObservationModel } from '../visualisation/models/observation.model';
import { OrdonnanceModel } from '../visualisation/detail-cas/forms/conclusion-ordonnance.component';
import { PassageModel } from '../visualisation/models/passage.model';
import { PatientModel } from '../visualisation/models/patient.model';

import { DBService } from './db.service';
import { FileService } from './file.service';
import { UserService } from './user.service';
import { ParametreService } from './parametre.service';

const modelsRef = {
    patient: PatientModel,
    cas: CasModel,
    observation: ObservationModel,
    action: ActionModel,
    conclusion: ConclusionModel,
    passage: PassageModel,
    ordonnance: OrdonnanceModel,
    courier: CourierModel,
};

export type Cloture = 'lsp' | 'décédé' | 'décharge' | 'confié' | 'autre' | 'transport';

@Injectable()
export class PassageService {

    private logStyle = 'color: #d35400;';

    private modelRef: any = {
        cas: new CasModel(),
        patient: new PatientModel(),
        action: new ActionModel(),
        observation: new ObservationModel(),
        conclusion: new ConclusionModel(),
        passage: new PassageModel()
    };

    public currentSubject = {
        cas: new Subject<CasModel>(),
        patient: new Subject<PatientModel>(),
        action: new Subject<ActionModel>(),
        observation: new Subject<ObservationModel>(),
        conclusion: new Subject<ConclusionModel>(),
        passage: new Subject<PassageModel>()
    };

    private _currentObjects: any = { ...this.modelRef };

    private dependencieList = [
        'patient',
        'action',
        'observation',
        'conclusion',
        'passage',
    ];

    public set currentObjects(obj: any) {
        Object.keys(obj).forEach(k => {
            if (this._currentObjects[k] !== obj[k]) {
                this._currentObjects[k] = obj[k];
                this.currentSubject[k].next(obj[k]);
            }
        });
    }

    public get currentObjects() {
        let retour = {
            cas: new CasModel(),
            patient: new PatientModel()
        };
        retour = { ...retour, ...this._currentObjects };
        return retour;
    }

    public get casObservable() {
        return this.currentSubject.cas.asObservable();
    }

    public get patientObservable() {
        return this.currentSubject.patient.asObservable();
    }

    public get actionObservable() {
        return this.currentSubject.action.asObservable();
    }

    public get observationObservable() {
        return this.currentSubject.observation.asObservable();
    }

    public get conclusionObservable() {
        return this.currentSubject.conclusion.asObservable();
    }

    public get passageObservable() {
        return this.currentSubject.passage.asObservable();
    }

    constructor(
        private DB: DBService,
        private USER: UserService,
        private fileService: FileService,
        private parametre: ParametreService
    ) { }

    // maintain & update current cas
    public set currentCas(idOrCas) {
        if (idOrCas === 'new') {
            const newCas = new CasModel();
            this.currentObjects = { cas: newCas };
            this.defineCurrent('patient', null);
            return;
        }
        if (idOrCas === null) {
            this.currentObjects = {};
            this.defineAllDependencies(new CasModel());
            return;
        }
        if (idOrCas.urgence) {
            this.currentObjects = { cas: idOrCas };
            this.defineAllDependencies(idOrCas);
            return;
        }

        const ID = (idOrCas instanceof CasModel) ? idOrCas._id : idOrCas;
        if (typeof ID !== 'string') {
            return;
        }

        this.DB.getById('cas', ID)
            .then((doc) => {
                if (!doc) {
                    return;
                }
                this.currentObjects = { cas: doc };
                this.defineAllDependencies(doc);
            })
            .catch((err) => console.error(err));
    }

    public async addPatient(patient) {
        // create patient
        let newPatient = new PatientModel();
        newPatient = { ...this.currentObjects.patient, ...patient };

        // add current cas to patient
        if (!newPatient.cas || !Array.isArray(newPatient.cas)) {
            newPatient.cas = [];
        }
        if (!newPatient.cas.includes(this.currentObjects.cas._id)) {
            newPatient.cas.push(this.currentObjects.cas._id);
        }

        let ID: string;
        // add || edit
        if (newPatient._id) {
            await this.DB.update('patient', newPatient).then(() => ID = newPatient._id);
        } else {
            await this.DB.put('patient', newPatient).then(id => ID = id);
        }

        // update patient cas list
        const casData = {
            nom: newPatient.nom,
            prenom: newPatient.prenom,
            age: newPatient.age,
            sexe: newPatient.sexe,
            patient: ID
        };

        this.currentObjects = { patient: newPatient };

        await this.DB.getByIdArray('cas', newPatient.cas)
            .then(casList => {
                const promises: any[] = casList.map(cas => this.DB.update('cas', { ...cas, ...casData }));
                return Promise.all(promises);
            })
            .then(() => this.defineCurrent('patient', newPatient));
    }

    public defineAllDependencies(cas: CasModel = this.currentObjects.cas) {
        this.dependencieList.forEach(dependency => this.defineCurrent(dependency, cas[dependency] || null));
    }

    public defineCurrent(objName: string, idOrObj: string | object) {
        if (this.currentObjects[objName]) {
            if (typeof idOrObj === 'string' && idOrObj === this.currentObjects[objName]._id) {
                return;
            }
            if (idOrObj instanceof modelsRef[objName] && idOrObj === this.currentObjects[objName]) {
                return;
            }
        }

        if (!idOrObj) {
            this.currentObjects = { [objName]: new modelsRef[objName]() };
            return;
        }
        if (idOrObj instanceof modelsRef[objName]) {
            if (!this.currentObjects[objName] || this.currentObjects[objName] !== idOrObj) {
                this.currentObjects = { [objName]: idOrObj };
            }
            return;
        }

        if (typeof idOrObj !== 'string') {
            return;
        }

        this.DB.getById(objName, idOrObj)
          .then((doc) => {
              if (!this.currentObjects[objName] || this.currentObjects[objName] !== doc) {
                  this.currentObjects = { [objName]: doc };
              }
          })
          .catch((err) => console.error(err));
    }

    public addFile(file) {
        const doc = {
            titre: file.titre,
            type: file.type,
            ...this.USER.CONTEXT,
        };
        let docId: string;

        return this.fileService.upload(file.doc).pipe(
            map((upload) => Object.assign(doc, upload)),
            mergeMap(() => from(this.DB.put('document', doc))),
            mergeMap((id) => {
                docId = id;
                return from(this.setConclusionDoc(docId));
            }),
            mapTo(docId),
        );
    }

    public addNewItem(categorie, nouvelItem) {
        const cat = categorie[0];
        if (!cat) {
            return;
        }

        let catObj = this.currentObjects[cat];
        if (!catObj) {
            catObj = this.modelRef[cat];
        }

        const context = this.USER.CONTEXT;
        nouvelItem = { ...context, ...nouvelItem };

        // add to current list
        if (!catObj[categorie[1]]) {
          catObj[categorie[1]] = [];
        }
        catObj[categorie[1]].push(nouvelItem);

        // update DB
        if (catObj._id) {
            this.currentObjects = { [cat]: catObj };
            return this.DB.update(cat, catObj);
        } else {
            catObj.cas = this.currentObjects.cas._id;
            this.currentObjects = { [cat]: catObj };

            this.DB.put(cat, catObj).then(id => {
                catObj._id = id;
                this.currentObjects.cas[cat] = id;
                this.currentObjects = { [cat]: catObj };
                return this.DB.update('cas', this.currentObjects.cas);
            });
        }
    }

    public addNewItem2(categorie, nouvelItem) {
        const cat = categorie[0];
        const catObj = this.currentObjects[cat];
        if (!cat || !catObj) {
          return;
        }

        const context = this.USER.CONTEXT;
        nouvelItem = { ...context, ...nouvelItem };

        // add to current list
        if (!catObj[categorie[1]]) {
          catObj[categorie[1]] = [];
        }
        catObj[categorie[1]].push(nouvelItem);

        // update DB
        if (catObj._id) {
          this.DB.update(cat, catObj);
        } else {
            catObj.cas = this.currentObjects.cas._id;
            this.DB.put(cat, catObj).then(id => {
                catObj._id = id;
                this.currentObjects.cas[cat] = id;
                this.currentObjects = { [cat]: catObj };
                this.DB.update('cas', this.currentObjects.cas);
            });
        }
    }

    public affectToInter(cas: CasModel, inter: InterventionModel) {
        cas.currentIntervention = inter._id;
        return this.DB.update('cas', cas);
    }

    public cloreCurrentCas(type: Cloture, info: any = null) {
        return this.cloreCas(this._currentObjects.cas, type, info);
    }

    public cloreCas(cas: CasModel, type: Cloture, info?: any) {
        // TODO : cloture cas ...

        let cloture = { type, ...this.USER.CONTEXT };
        if (info) {
            cloture = { ...cloture, ...info };
        }

        if (cas.clos) {
            if (!cas.historique) {
                cas.historique = [];
            }
            cas.historique.push(cas.clos);
        }

        cas.clos = cloture;
        this.clearNotif(['discussion', 'regulation', 'intervention'], cas);

        if (cas.position) {
            this.closeCasPassage(cas);
        } else {
            // le cas a deja Г©tГ© clos ou sortit
            this.DB.update('cas', cas);
            this.addCasNewItem(cas, ['conclusion', 'sortie'], cloture).then(() => {
                this._currentObjects = { cas };
            });
        }

        return Promise.resolve(cloture);
    }

    public closePassage(passageID: string = null, info: any = null) {
        return this.closeCasPassage(this._currentObjects.cas, passageID, info).then(r => {
            this.currentCas = this._currentObjects.cas;
            return this.currentCas;
        });
    }

    public closeCasPassage(cas: CasModel, passageID?: string, info?: any) {
        if (!passageID) {
            passageID = cas.position;
        }
        if (!cas.position) {
            // TODO : que faire en cas de sortie sans entree?
        }
        cas.position = null;
        if (!cas.historique) {
            cas.historique = [];
        }
        cas.historique.push(passageID);
        cas.poste = null;

        return this.DB.update('cas', cas)
            .then(() => this.DB.getById('passage', passageID))
            .then((doc) => {
                doc.sortie = this.USER.CONTEXT;
                if (info) {
                    doc = { ...doc, ...info };
                }
                return this.DB.update('passage', doc).then(() => {
                    this.addCasNewItem(cas, ['conclusion', 'sortie'], doc.sortie)
                      .then(() => this._currentObjects = { cas });
                    return doc;
                });
            });
    }

    public generatePassage(cas: any, poste?: string) {
        poste = poste || this.USER.CONTEXT.poste;

        const newPassage = new PassageModel();
        newPassage.cas = cas._id;
        newPassage.lieu = poste;
        newPassage.entree = this.USER.CONTEXT;
        newPassage.patient = cas.patient;
        newPassage.pathologie = cas.pathologie;

        return newPassage;
    }

    public openCasPassage(cas: CasModel, poste?: string) {
        if (!cas) {
          cas = this.currentObjects.cas;
        }
        const newPassage = this.generatePassage(cas, poste);

        return this.DB.put('passage', newPassage, poste)
            .then(passageID => {
                cas.position = passageID;
                cas.poste = poste;
                return this.DB.update('cas', cas);
            })
            .then(() => {
                this.currentCas = cas;
                this._currentObjects = { cas };
            });
    }

    public setIntervention(cas: CasModel, interID: string) {
        cas.currentIntervention = interID;
        return this.DB.update('cas', cas);
    }

    public cancelIntervention(cas: CasModel) {
        cas.currentIntervention = null;
        return this.DB.update('cas', cas);
    }

    public addCasNewItem(cas: CasModel, categorie: string[], nouvelItem: any) {
        // init vars
        const cat = categorie[0];
        if (!cat || !cas[cat] || typeof cas[cat] !== 'string') {
            return new Promise(r => r());
        }

        return this.DB.getById(cat, cas[cat]).then(catObj => {
            if (!catObj) {
                return;
            }
            const context = this.USER.CONTEXT;
            nouvelItem = { ...context, ...nouvelItem };

            // add to current list
            if (!catObj[categorie[1]]) {
                catObj[categorie[1]] = [];
            }
            catObj[categorie[1]].push(nouvelItem);

            // update DB
            if (catObj._id) {
                return this.DB.update(cat, catObj);
            } else {
                catObj.cas = cas._id;
                const ID = this.DB.generateID(cat);
                // TODO : passage prefix?
                cas[cat] = ID;

                this.DB.putWithID(cat, catObj, ID).then(id => { });
                return this.DB.update('cas', cas);
            }
        });
    }

    public addNotif(type: 'discussion' | 'regulation' | 'intervention') {
        const notif = new NotificationModel(this.currentObjects.cas._id, type);

        this.DB.put('notification', notif, type).then(id => {
            if (!this.currentObjects.cas.notification) {
              this.currentObjects.cas.notification = [];
            }
            this.currentObjects.cas.notification.push(id);
            return this.DB.update('cas', this.currentObjects.cas);
        }).then(r => {
            console.log('notification : ', type);
        });
    }

    public clearNotif(
        notifType: string[] = ['discussion', 'regulation', 'intervention'],
        cas: CasModel = this.currentObjects.cas
    ): void {
        if (!cas || !cas.notification || !cas.notification.length) {
          return;
        }

        const notifToRemoveFromCas: string[] = [];

        notifType.forEach(type => {
            // get cas notif for type
            const notifID = cas.notification.filter(ID => ID.includes(type));
            if (!notifID || notifID.length < 1) {
              return;
            }
            // close notif
            this.DB.getByIdArray('notification', notifID).then((list: NotificationModel[]) => {
                list.forEach(notif => {
                    notif.close = this.USER.CONTEXT;
                    this.DB.update('notification', notif);
                });
            });

            notifToRemoveFromCas.push(...notifID);
        });

        // archive notif for cas
        if (!cas.historique) {
            cas.historique = [];
        }
        cas.historique.push(...notifToRemoveFromCas);
        // clear cas notifs
        cas.notification = cas.notification.filter((idNotif) => !notifToRemoveFromCas.includes(idNotif));
        this.DB.update('cas', cas);
    }


/**
 * Find position and Nick
 */
    public async getPosition(passage) {
        let position ;


        let poste = passage.lieu || '';
        let detailPoste: any;
            if (this.parametre.currentParametre.poste)
                detailPoste = this.parametre.currentParametre.poste.find(p => p.nom == poste);

            let nick: string;
            if (detailPoste && detailPoste.nick) nick = detailPoste.nick;
            else nick = poste.substring(0, 2);

            position = nick;

        return position;
    }


/**
 * Find cas by ID and set calulated properties
 */
    public async getCas(casId: string): Promise<CasModel> {
        const cas = await this.DB.getById<CasModel>('cas', casId);

        if (!cas) {
            return null;
        }

        if (cas.action) {
            cas.lastAction = new Date(parseInt(cas.action.split(':')[1], 10));
        }

        if (cas.observation) {
            cas.lastObservation = new Date(parseInt(cas.observation.split(':')[1], 10));
        }

        if (cas.conclusion) {
            cas.theconclusion = await this.DB.getById('conclusion', cas.conclusion);
        }

        if (cas.age) {
            cas.ageYears = moment().diff(cas.age, 'years');
            cas.isMinor = cas.ageYears < 18;
        }

        if (cas.position) {
            const [ positionTs, poste ] = cas.position.split(':').splice(1).reverse();
            cas.positionCreated = new Date(parseInt(positionTs, 10));
            cas.currentPosition = this.getCurrentPosition(poste);
        }

        cas.primaryNotification = this.getPrimaryNotification(cas.notification);

        return cas;
    }

/**
 * Format post as current position nickname
 */
    private getCurrentPosition(poste: string): string {
        if (!poste) {
            return null;
        }
        // Try to find preset position nickname
        let detailPoste: any;
        if (this.parametre.currentParametre.poste) {
            detailPoste = this.parametre.currentParametre.poste.find(p => (p.nom === poste));
        }
        if (detailPoste && detailPoste.nick) {
            return detailPoste.nick;
        }
        // Default to first two letters
        return poste.substring(0, 2);
    }

/**
 * Find most important notification in array of notification ID strings
 */
    private getPrimaryNotification(notifications: string[]) {
        if (!notifications || !notifications.length) {
            return { type: null, created: null };
        }
        // Notifications will be sorted in this order
        const order = ['intervention', 'regulation', 'discussion'];
        return notifications
            // Convert notification ID strings to objects
            .map((id) => {
                const [type, ts] = id.split(':').splice(1);
                const created = new Date(parseInt(ts, 10));
                return { type, created };
            })
            // Ignore unknown types
            .filter((n) => order.includes(n.type))
            // Sort by notification importance
            .sort((a, b) => order.indexOf(a.type) - order.indexOf(b.type))
            // Select most important
            .shift();
    }

    private setConclusionDoc(docId) {
        if (!this.currentObjects.conclusion.document) {
            this.currentObjects.conclusion.document = [];
        }
        this.currentObjects.conclusion.document.push(docId);

        if (this.currentObjects.conclusion._id) {
            return this.DB.update('conclusion', this.currentObjects.conclusion);
        }

        return this.DB.put('conclusion', this.currentObjects.conclusion).then((conclusionId) => {
            this.currentObjects.conclusion._id = conclusionId;
            this.currentObjects.cas.conclusion = conclusionId;
            this.currentObjects = { conclusion: this.currentObjects.conclusion };
            return this.DB.update('cas', this.currentObjects.cas);
        });
    }

    // Handle bug where cas position is null when it shouldn't be
    // Also used to check for open passages when creating a new one
    public async bugfixFindPassageAndCloseExtraneous(casId: string, passages: PassageModel[]) {
      // find open passages
      const openPassages = passages.filter((passage) => {
          return passage.cas === casId && !passage.sortie;
      });
      // we found the matching passage
      if (openPassages.length === 1) {
          return openPassages[0]._id;
      }
      // we found too many open passages
      // force closure of duplicates and return most recent
      if (openPassages.length > 1) {
          const newestPassage = openPassages.shift();
          const closeDuplicates = openPassages.map((passage) => {
            return this.forceClosePassage(passage._id, 'Duplicate force closed');
          });
          await Promise.all(closeDuplicates);
          return newestPassage._id;
      }
      // found nothing
      return null;
  }

  // Force close passage that should not be open
  public async forceClosePassage(passageId: string, error?: string) {
      const passage = await this.DB.getById('passage', passageId);
      passage.sortie = {
          ...this.USER.CONTEXT,
          timeCode: new Date(),
          error,
      };
      return await this.DB.update('passage', passage);
  }

}

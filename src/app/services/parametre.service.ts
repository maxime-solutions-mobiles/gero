


import { DBService } from './db.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UserService } from './user.service';


@Injectable()
export class ParametreService {
    private _currentParametre: any = {};
    private _current: any = new Subject<any>();
    public get current() { return this._current.asObservable(); }
    public set current(param: any) {
        this._currentParametre = param;
        this._current.next(param);
    }
    public get currentParametre() { return this._currentParametre }
    private _moyenType = ['VL', 'Ambulance', 'Golfette', 'Hélico', 'Vélo', 'Moto', 'Quad', 'Pédestre', 'Equestre'];
    private _moyenEtat = ['disponible', 'alerte', 'sll', 'transport', 'retour'];
    public get moyenType() { return this._moyenType; }
    public get moyenEtat() { return this._moyenEtat; }


    constructor(
        private DB: DBService,
        private USER: UserService
    ) {
        this.DB.ready
            .then(() => {
                this.DB.getAll('parametre').then(params => this.current = this.getParamByType(params));
                this.DB.TABLE.parametre
                    .changes({
                        since: 'now',
                        live: true,
                        include_docs: true   
                    })
                    .on('change', (change: any) => {

                        let list = this._currentParametre[change.doc.type] || [];
                        if (change.deleted) {
                            list = list.filter(l => l._id !== change.id);
                        } else {
                            let index = list.findIndex(l => l._id === change.id);
                            if (index > -1) {
                                list[index] = change.doc;
                            } else {
                                list.push(change.doc);
                            }

                        }
                        this._currentParametre[change.doc.type] = list;
                        this._current.next(list);
                    })

                    .on('error', (err: any) => console.error('err on change TABLE parametre : ', err));



                this.DB.getAll('moyen')
                    .then(moyens => {
                        if (!this._currentParametre['moyen']) {
                            this._currentParametre['moyen'] = [];
                        }
                        this._currentParametre['moyen'] = moyens;
                        this._current.next(this._currentParametre);
                    })


                this.DB.TABLE.moyen
                    .changes({
                        since: 'now',
                        live: true,
                        include_docs: true
                    })
                    .on('change', (change: any) => {
                        let list = this._currentParametre['moyen'] || [];
                        if (change.deleted) {
                            list = list.filter(l => l._id !== change.id);
                        } else {
                            let index = list.findIndex(l => l._id === change.id);
                            if (index > -1) {
                                list[index] = change.doc;
                            } else {
                                list.push(change.doc);
                            }
                        }
                        this._currentParametre['moyen'] = list;
                        this._current.next(list);
                    })
                    .on('error', (err: any) => console.error('err on change TABLE moyen : ', err));

            });
    }
    public getParamByType(params) {
        let list: any = {};
        if (this._currentParametre.moyen) {
            list.moyen = this._currentParametre.moyen;
        }
        params.forEach((param: any) => {
            if (!list[param.type]) {
                list[param.type] = [];
            }
            list[param.type].push(param)
        });
        return list;
    }

    // CRUD

    public addParam(type: string, param: any) {
        if (param._id) {
            return this.editParam(type, param);
        }
        param.type = type;
        return this.DB.put('parametre', param, type);
    }
    public editParam(type: string, param: any) {
        return this.DB.update('parametre', param);
    }

    public deleteParam(type: string, data: any) {
        return this.DB.delete('parametre', data, type);
        // TODO : close?
    }

}



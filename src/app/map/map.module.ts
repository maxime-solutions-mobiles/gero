import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletMarkerClusterModule} from '@asymmetrik/ngx-leaflet-markercluster';

import { MaterialModule } from '../material.module';
import { DynamicFormModule } from '../forms/dynamic-form.module';
import { GeroMap } from './map.page';


@NgModule({
    imports: [
        CommonModule,
        LeafletModule,
        DynamicFormModule,
        MaterialModule,
        MatSelectModule,
    ],
    declarations: [
        GeroMap
    ],
    providers: []
})
export class MapModule { }
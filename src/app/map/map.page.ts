
import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material';
import { UserService } from '../services/user.service';
import { Subscription } from 'rxjs';

import { DBService } from '../services/db.service';
import { VisualistationService } from '../visualisation/visualisation.service';

import * as moment from 'moment';
import * as L from 'leaflet';
import * as omnivore from 'leaflet-omnivore'

require('leaflet-google-places-autocomplete');
require('leaflet-kml');
require('leaflet-omnivore');
require('leaflet-extra-markers');


@Component({
    selector: 'map',
    templateUrl: './map.page.html'
})
export class GeroMap implements OnInit {

    public currentUser: any;

    LAYER_GOOGLE_ROADS = {
        id: 'googleroads',
        name: 'Google Maps Roads',
        enabled: false,
        layer: L.tileLayer('http://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
            maxZoom: 20
        })
    };

    LAYER_GOOGLE_TERRAIN = {
        id: 'googleterrain',
        name: 'Google Maps Terrain',
        enabled: false,
        layer: L.tileLayer('http://mt1.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
            maxZoom: 20
        })
    };

    LAYER_GOOGLE_SATELLITE = {
        id: 'googlesatellite',
        name: 'Google Maps Satellite',
        enabled: false,
        layer: L.tileLayer('http://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
            maxZoom: 20
        })
    };

    LAYER_GOOGLE_HYBRID = {
        id: 'googlehybrid',
        name: 'Google Maps Hybrid',
        enabled: false,
        layer: L.tileLayer('http://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
            maxZoom: 20
        })
    };

    LAYER_OSM = {
        id: 'openstreetmap',
        name: 'Open Street Map',
        enabled: true,
        layer: L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            maxZoom: 20,
            attribution: '&copy; Openstreetmap France | &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        })
    };

    LAYER_HERE = {
        id: 'here',
        name: 'HERE',
        enabled: false,
        layer: L.tileLayer('https://{s}.{base}.maps.cit.api.here.com/maptile/2.1/{type}/{mapID}/normal.day/{z}/{x}/{y}/{size}/{format}?app_id={app_id}&app_code={app_code}&lg={language}', {
            attribution: 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>',
            subdomains: '1234',
            mapID: 'newest',
            app_id: 'vAExp9PiHgwiXRuyXEJz',
            app_code: 'Jt9iY4_prLaQ_OUkp4yTwg',
            base: 'base',
            maxZoom: 20,
            type: 'maptile',
            language: 'eng',
            format: 'png8',
            size: '256'
        })
    }

    // Values to bind to Leaflet Directive
    layersControl = {
        position: 'bottomright',
        baseLayers: {
            'Google Maps Roads': this.LAYER_GOOGLE_ROADS.layer,
            'Google Maps Terrain': this.LAYER_GOOGLE_TERRAIN.layer,
            'Google Maps Satellite': this.LAYER_GOOGLE_SATELLITE.layer,
            'Google Maps Hybrid': this.LAYER_GOOGLE_HYBRID.layer,
            'OSM': this.LAYER_OSM.layer,
            'Here': this.LAYER_HERE.layer
        }
    }

    options = {
        layers: [
            this.LAYER_OSM.layer,
            // this.LAYER_PUTTER.layer
        ],
        zoom: 6,
        center: L.latLng(48.8566, 2.3522)
    };

    markerGroup: L.LayerGroup = L.layerGroup([]);
    map: L.Map;

    icons: any = {
        'moyen': L.icon({
            iconUrl: 'assets/images/moyen.png',
            iconSize: [16, 16],
        }),
        'intervention': L.ExtraMarkers.icon({
            icon: 'fa-exclamation-triangle',
            prefix: 'fa',
            iconColor: 'black',
            markerColor: 'yellow',
            shape: 'star'
        }),
        'Check-Point': L.ExtraMarkers.icon({
            icon: 'fa-clipboard-check',
            prefix: 'fa',
            iconColor: 'white',
            markerColor: 'blue-dark',
            shape: 'round'
        }),
        'Sortie': L.ExtraMarkers.icon({
            icon: 'fa-sign-out-alt',
            prefix: 'fa',
            iconColor: 'white',
            markerColor: 'green-light',
            shape: 'square'
        }),
        'Arrivee': L.ExtraMarkers.icon({
            icon: 'fa-running',
            prefix: 'fa',
            iconColor: 'black',
            markerColor: 'white',
            shape: 'square'
        }),
        'Depart': L.ExtraMarkers.icon({
            icon: 'fa-running',
            prefix: 'fa',
            iconColor: 'black',
            markerColor: 'white',
            shape: 'square'
        }),
        'Ravitaillement': L.ExtraMarkers.icon({
            icon: 'fa-utensils-alt',
            prefix: 'fa',
            iconColor: 'white',
            markerColor: 'green-light',
            shape: 'square'
        }),
        'DAE': L.ExtraMarkers.icon({
            //icon: 'fa-heartbeat',
            prefix: 'fa',
            iconColor: 'red',
            markerColor: 'green-light',
            shape: 'square',
            icon: 'fa-heartbeat',
            extraClasses: 'fa-spin fa-5x'
        }),
        'poste': L.ExtraMarkers.icon({
            icon: 'fa-star-of-life',
            prefix: 'fa',
            iconColor: 'white',
            markerColor: 'blue',
            shape: 'square',
            spin: true
        }),
        'number': L.ExtraMarkers.icon({
            icon: 'fa-number',
            number: '42',
            iconColor: 'black',
            shape: 'star',
            markerColor: 'white'
        }),
        'DZ': L.ExtraMarkers.icon({
            icon: 'fa-helicopter',
            prefix: 'fa',
            iconColor: 'whithe',
            shape: 'square',
            markerColor: 'orange'
        }),

        // choice of colors :'red', 'orange-dark', 'orange', 'yellow', 'blue-dark', 'cyan', 'purple', 'violet', 'pink', 'green-dark', 'green', 'green-light', 'black', or 'white'
        // choice of icons ? ['font', 'cloud-download', 'medkit', 'github-alt', 'coffee', 'twitter', 'shopping-cart', 'tags', 'star'];
        // choice of shape 'circle', 'square', 'star', or 'penta'

    };

    types: string[] = ['Ambulance', 'VL', 'Quad', 'Vélo', 'Pédestre', 'Equestre', 'Hélico', 'Golfette', 'Moto'];
    etats: string[] = ['disponible', 'indisponible', 'alerte', 'transport', 'intervention'];
    //TODO : make manageable all categories on parametres
    categories: string[] = ['Arrivee', 'Depart', 'Ravitaillement', 'DAE', 'Check-Point', 'DZ', 'Sortie', 'Autre'];

    checkedTypes: string[] = [];
    checkedEtats: string[] = [];
    checkedCategories: string[] = [];

    filters = {
        'moyen': (types, etats) => {
            let filter: any = {
                _id: { $gt: null },
                'lat': { $exists: true },
                'lng': { $exists: true },
                'type': { $regex: new RegExp(types.join('|'), 'i') },
                'etat': { $regex: new RegExp(etats.join('|'), 'i') },
                'visible': 'true'
            };
            return filter;
        },
        'intervention': {
            _id: { $gt: null },
            'clos': { $exists: false },
            'lat': { $exists: true },
            'lng': { $exists: true , $gt: null}
        },
        'poi': (categories) => {
            let filter: any = {
                _id: { $gt: null },
                'type': 'poi',
                'lat': { $exists: true },
                'lng': { $exists: true },
                'category': { $regex: new RegExp(categories.join('|'), 'i') },
            };
            return filter;
        },
        'poste': {
            _id: { $gt: null },
            'type': 'poste',
            'lat': { $exists: true },
            'lng': { $exists: true }
        },
        'overlay': {
            _id: { $gt: null },
            'type': 'overlay',
            'file': { $exists: true },
            'latn': { $exists: true },
            'lats': { $exists: true },
            'lngw': { $exists: true },
            'lnge': { $exists: true }
        },
        'track': {
            _id: { $gt: null },
            'type': 'track',
            'file': { $exists: true }
        }
    };

    moyens: any[];
    interventions: any[];
    pois: any[];
    postes: any[];
    overlay: any[];

    saveMarkerChange;

    private subscription: Subscription[] = [];

    constructor(

        private DB: DBService,
        private VISU: VisualistationService,
        private USER: UserService
        ) {

        this.VISU.currentDisplay = { list: 'map', listMod: 'display' };

        this.saveMarkerChange = (marker) => {

            if (marker['type'] === 'moyen') {
                    this.DB.TABLE[marker['type']].get(marker['id']).then((doc) => {
                    let latLng = marker.getLatLng();
                    doc.lat = latLng.lat;
                    doc.lng = latLng.lng;
                    return this.DB.TABLE[marker['type']].put(doc);
                });
            }
            else {
                    this.DB.TABLE['parametre'].get(marker['id']).then((doc) => {
                    let latLng = marker.getLatLng();
                    doc.lat = latLng.lat;
                    doc.lng = latLng.lng;
                    return this.DB.TABLE['parametre'].put(doc);
                });
            }

        };

        let promises = [];

        promises.push(this.DB.filter('moyen', this.filters['moyen']([], [])));

        promises.push(this.DB.filter('intervention', this.filters['intervention']));

        promises.push(this.DB.filter('parametre', this.filters['poi']([])));

        promises.push(this.DB.filter('parametre', this.filters['poste']));


        this.DB.filter('parametre', this.filters['overlay']).then(data => {
            let overlays = {};

            for (let i = 0; i < data.list.length; i++) {

                overlays[data.list[i].nom] = L.imageOverlay('https://v3.gero.fr/assets/' + data.list[i].file,
                    [
                        [data.list[i].latn, data.list[i].lngw],
                        [data.list[i].lats, data.list[i].lnge]
                    ],
                    {
                        opacity: data.list[i].opacity
                    });

                if (data.list[i].enabled) {
                    this.map.addLayer(overlays[data.list[i].nom]);
                }
            };

            this.layersControl['overlays'] = overlays;
        });



        this.DB.filter('parametre', this.filters['track']).then(data => {
            console.log('we get list of KML ?');
            console.log(data);

            for (let i = 0; i < data.list.length; i++) {

                    if (data.list[i].format == 'kml') {

                        fetch('assets/kml/'+ data.list[i].file)
                         .then(res => res.text())
                            .then(kmltext => {
                                // Create new kml overlay
                                const parser = new DOMParser();
                                const kml = parser.parseFromString(kmltext, 'text/xml');
                                const track = new L.KML(kml);
                                this.map.addLayer(track);
                                });
                    }
                    if (data.list[i].format == 'gpx') {
                        omnivore.gpx('assets/kml/' + data.list[i].file).addTo(this.map);
                    }
            }


        });


        Promise.all(promises).then(lists => {
            this.moyens = lists[0].list;
            this.interventions = lists[1].list;
            this.pois = lists[2].list;
            this.postes = lists[3].list;
            this.createMarkers();
            this.fitMarkers();
        });

        this.DB.TABLE.moyen.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', (doc: any) => {
            this.processChange(doc, 'moyen');
        });

        this.DB.TABLE.intervention.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', (doc: any) => {
            this.processChange(doc, 'intervention');
        });

        this.DB.TABLE.parametre.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', (doc: any) => {
            if (doc.doc.type === 'poi') {
                this.processChange(doc, 'poi');
            } else if (doc.doc.type === 'poste') {
                this.processChange(doc, 'poste');
            }
        });




    }

    ngOnInit() {

        this.subscription.push(this.USER.profile.subscribe((profile) => {
        this.currentUser = profile;
        }));
    }

    onMapReady(map: L.Map) {
        this.map = map;
        this.markerGroup.addTo(this.map);

        new L.Control.GPlaceAutocomplete({
            callback: function(place) {
                let loc = place.geometry.location;
                console.log(place);
                map.setView( [loc.lat(), loc.lng()], 16);
            }
        }).addTo(map);
    }

    updateMarkerByIndex(index: number, lat: number, lng: number, draggable: string) {

        let markers = this.markerGroup.getLayers() as L.Marker[];

        let latLng = markers[index].getLatLng();

        if (latLng.lat !== lat || latLng.lng !== lng) {
            markers[index].setLatLng([lat, lng]);
            // this.fitMarkers();
        }

        if (draggable === 'false' && markers[index].options.draggable) {
            markers[index].dragging.disable();
            markers[index].options.draggable = false;
            markers[index].off('dragend');
        } else if (draggable === 'true' && !markers[index].options.draggable) {
            markers[index].dragging.enable();
            markers[index].options.draggable = true;
            markers[index].on('dragend', () => this.saveMarkerChange(markers[index]));
        }
    }

    isChangeForMap(doc: any, type: string) {

        if (doc.doc.lat === undefined  || doc.doc.lat === null || doc.doc.lat === "0" || doc.doc.lat === 0  ) {
            return false;
        }

        let isForMap = true;

        switch (type) {

            case 'moyen':

                if (this.checkedTypes.length > 0) {
                    isForMap = this.checkedTypes.includes(doc.doc.type);
                }

                if (this.checkedEtats.length > 0) {
                    isForMap = this.checkedEtats.includes(doc.doc.etat);
                }

                break;

            case 'intervention':

                isForMap = doc.doc.clos === undefined;

                break;

            case 'poi':

                if (this.checkedCategories.length > 0) {
                    isForMap = this.checkedCategories.includes(doc.doc.category);
                }

                break;

            case 'poste':

                break;
        }

        return isForMap;
    }

    processChange(doc: any, type: string) {

        let markers = this.markerGroup.getLayers() as L.Marker[];

        let index = markers.findIndex(l => l['id'] === doc.id);

        if (index > -1) {

            if (doc.deleted === true ) {

                this.markerGroup.removeLayer(markers[index]);

                // this.fitMarkers();

            } else {

                if (this.isChangeForMap(doc, type)) {

                    this.updateMarkerByIndex(index, Number(doc.doc.lat), Number(doc.doc.lng), doc.doc.draggable);

                } else {

                    this.markerGroup.removeLayer(markers[index]);

                    // this.fitMarkers();
                }
            }

        } else {

            if (this.isChangeForMap(doc, type)) {

                this.markerGroup.addLayer(this.createMarkerByType(doc, type));

                // this.fitMarkers();
            }
        }
    }

    fitMarkers() {

        let bounds = [];

        let markers = this.markerGroup.getLayers() as L.Marker[];

        for (let i = 0; i < markers.length; i++) {
            bounds.push(markers[i].getLatLng());
        }

        if (bounds.length > 0) {
            this.map.fitBounds(bounds);
        } else {
            this.map.setView(this.options.center, this.options.zoom);
        }
    }

    createMarker(id, lat, lng, tooltip, icon, type, popup, draggable) {

        let marker = L.marker([lat, lng], { icon: icon, draggable: draggable === 'true' });

        if (draggable === 'true') {
            marker.on('dragend', () => this.saveMarkerChange(marker));
            console.log ('saveMarkerChange  ')
            console.log (marker)
        }

        marker['id'] = id

        marker['type'] = type;

        marker.bindTooltip(tooltip, { permanent: true })

        marker.bindPopup(popup).openPopup()

        return marker;
    }


    createMarkerByType(doc: any, type: string) {

        switch (type) {
            case 'moyen':
                return this.createMoyenMarker(doc.doc);
            case 'intervention':
                return this.createInterventionMarker(doc.doc);
            case 'poi':
                return this.createPOIMarker(doc.doc);
            case 'poste':
                return this.createPosteMarker(doc.doc);
        }
    }

    createMoyenMarker(doc: any) {
        return this.createMarker(
            doc._id,
            Number(doc.lat),
            Number(doc.lng),
            ' ' + doc.nom + '',
            L.icon({ iconUrl: 'assets/images/moyens/' + doc.type + '-' + doc.etat + '.png', iconSize: [40, 40], }),
            'moyen',
            doc.nom + ' <br> Etat :' + doc.etat + ' <br> date dernier GPS le ' + moment.unix(doc.lastgpstimestamp).format('DD-MM-YYYY à HH:mm') + '<br> batterie GPS :' + doc.lastBatteryLevel + ' % ',
            doc.draggable
        );
    }

    createInterventionMarker(doc: any) {

        // TODO change color of icone depending status of interventions...
        return this.createMarker(
            doc._id,
            Number(doc.lat),
            Number(doc.lng),
            ' ' + doc.numero + '',
            this.icons['intervention'],
            'intervention',
            ' <h3>N° :' + doc.numero + ' </h3> <br> Motif : ' + doc.motif + ' <br> Lieu : ' + doc.lieu + ' <br> Adresses : ' + doc.adresse + ' <br><i> observations : ' + doc.observation + ' </i><br>Requérant : ' + doc.requérant + ' <br>Contre appel :' + doc.telephone + '',
            doc.draggable
        );
    }

    createPOIMarker(doc: any) {
        console.log ('trying create them POI...')
        return this.createMarker(
            doc._id,
            Number(doc.lat),
            Number(doc.lng),
            ' ' + doc.nom + '',
            this.icons[doc.category],
            'poi',
            '<h2>' + doc.nom + '</h2><br>Catégorie : <b>' + doc.category + '</b><br>Zone : ' + doc.zone + '<br>Adresse : ' + doc.adresse + '<br>Remarque : ' + doc.remarques + ' ',
            doc.draggable
        );
    }

    createPosteMarker(doc: any) {
        return this.createMarker(
            doc._id,
            Number(doc.lat),
            Number(doc.lng),
            ' ' + doc.nom + '',
            this.icons['poste'],
            'poste',
            '<h2>Poste : ' + doc.nom + '</h2> Capacité du poste : ' + doc.capacite + '<br>  Nb. patients encours :' + doc.encours + ' <br> le ' + moment().format('DD-MM-YYYY HH:mm') + ' <br>Nb. patients vus :' + doc.encours + ' ',
            doc.draggable
        );
    }

    createMarkers() {
        this.createMoyenMarkers();
        this.createInterventionMarkers();
        this.createPOIMarkers();
        this.createPosteMarkers();
    }

    createMoyenMarkers() {
        for (let i = 0; i < this.moyens.length; i++) {
            this.markerGroup.addLayer(this.createMoyenMarker(this.moyens[i]));
        }
    }

    createInterventionMarkers() {
        for (let i = 0; i < this.interventions.length; i++) {
            this.markerGroup.addLayer(this.createInterventionMarker(this.interventions[i]));
        }
    }

    createPOIMarkers() {
        for (let i = 0; i < this.pois.length; i++) {
            console.log('create POImarkers')
            console.log(this.pois)
            this.markerGroup.addLayer(this.createPOIMarker(this.pois[i]));
        }
    }

    createPosteMarkers() {
        for (let i = 0; i < this.postes.length; i++) {
            this.markerGroup.addLayer(this.createPosteMarker(this.postes[i]));
        }
    }

    addMoyenMarkersToMap() {
        this.createMoyenMarkers();
        this.fitMarkers();
    }

    removeMoyenMarkersFromMap() {
        this.markerGroup.getLayers().forEach(marker => {
            if (marker['type'] === 'moyen') {
                this.markerGroup.removeLayer(marker);
            }
        })
    }

    addPOIMarkersToMap() {
        this.createPOIMarkers();
        this.fitMarkers();
    }

    removePOIMarkersFromMap() {
        this.markerGroup.getLayers().forEach(marker => {
            if (marker['type'] === 'poi') {
                this.markerGroup.removeLayer(marker);
            }
        })
    }

    filterMoyen(event: MatCheckboxChange, filter: string) {

        if (filter === 'type') {

            let index = this.checkedTypes.indexOf(event.source.value);

            if (event.checked && index === -1) {
                this.checkedTypes.push(event.source.value);
            } else if (!event.checked && index > -1) {
                this.checkedTypes.splice(index, 1);
            }

        } else if (filter === 'etat') {

            let index = this.checkedEtats.indexOf(event.source.value);

            if (event.checked && index === -1) {
                this.checkedEtats.push(event.source.value);
            } else if (!event.checked && index > -1) {
                this.checkedEtats.splice(index, 1);
            }
        }

        this.DB.filter('moyen', this.filters.moyen(this.checkedTypes, this.checkedEtats)).then(data => {

            this.removeMoyenMarkersFromMap();

            this.moyens = data.list;

            this.addMoyenMarkersToMap();
        })
    }

    filterPOI(event: MatCheckboxChange) {

        let index = this.checkedCategories.indexOf(event.source.value);

        if (event.checked && index === -1) {

            this.checkedCategories.push(event.source.value);

        } else if (!event.checked && index > -1) {

            this.checkedCategories.splice(index, 1);
        }

        this.DB.filter('parametre', this.filters.poi(this.checkedCategories)).then(data => {

            this.removePOIMarkersFromMap();

            this.pois = data.list;

            this.addPOIMarkersToMap();
        })
    }
}

import { ParametreService } from './../../services/parametre.service';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';

import { DynamicFormComponent } from '../../forms/dynamic-form.component';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FieldConfig } from '../../forms/field-config.interface';

@Component({
    selector: 'param-admin',
    template: `
        <aside>
            <nav>
            <ul>
            <li>
                <button mat-stroked-button (click)="addNew()" color="success">
                <i class="fal fa-plus"></i>Ajouter
                </button>
            </li>
            <li *ngFor="let line of liste" (click)="showDetails(line)">{{line.nom}}</li>
            </ul>
            </nav>

        </aside>
        <div>

            <dynamic-form *ngIf="config.formConfig"
            [config]="config.formConfig"
            #form="dynamicForm"
            (submit)="submit($event)">
            </dynamic-form>


        </div>

    `
})

export class ParamAdminComponent implements OnInit, OnDestroy {
    @Input() config: any;
    public liste: any[];
    @Output() submitForm: EventEmitter<any> = new EventEmitter<any>();
    public formConfig: FieldConfig[];
    private subscription: any;
    @ViewChild(DynamicFormComponent) form: DynamicFormComponent;


    constructor(private PARAM: ParametreService) { }

    ngOnInit() {
        if (this.config.formConfig) {
            this.formConfig = this.config.formConfig;

            if (this.PARAM.currentParametre[this.config.type])
                this.liste = this.PARAM.currentParametre[this.config.type]


            this.subscription = this.PARAM.current.subscribe(param => {
                if (param[this.config.type]) this.liste = param[this.config.type];

            });
        }

    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    public addNew() { this.form.reset() }
    private submit(evt) { this.submitForm.emit(evt) }
    private showDetails(line) {
        this.form.reset();
        this.form.setValues(line);
    }
}

import { Component, OnInit } from '@angular/core';
import { DBService } from 'src/app/services/db.service';
import { FileService } from 'src/app/services/file.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { settings } from 'cluster';


@Component({
  selector: 'import-export',
  template: `
    <h2>{{title | titlecase}}</h2>
    <div class="tool_pan">
      <aside>
        <nav>
        <h3> Tables </h3>
        <ul *ngFor="let tablegroup of dbTableList | keyvalue" class="table_scope">
            <h4> {{tablegroup.key}} </h4>
            <li *ngFor="let table of tablegroup.value" class="table_name" (click)="onSelectTable(table, tablegroup.key)" >
              <span [ngClass] = "{'selected_table': table === selected_table.tbName && tablegroup.key === selected_table.dbName}">
              {{table}}
              </span>
            </li>
        </ul>
        </nav>
      </aside>
      <div>
        <div *ngIf="message.content || message.type == 'update'" class="alert {{message.type}}">
          <p *ngIf="message.type != 'update'"> Message : {{message.content}}</p>
          <p *ngIf="message.type == 'update'"> There are(is) updated {{message.update_count}}(s) and added {{message.add_count}}(s).</p>
        </div>
        <div>
          <div>
            <button
              mat-raised-button color="success" class="button_left" (click)="importButton()"
              ><img src="assets/images/icons8-upload-48.png" class="icon" alt="import"> Import </button>
            <button mat-raised-button color="primary" class="button_right" (click)="exportButton()"
            ><img src="assets/images/icons8-download-48.png" class="icon" alt="export"> Export </button>
          </div>
          <div class="import_file_choose">
            <label class="file-label" for="import_file_select">
              <span > {{select_file_label}}</span>
            </label>
            <input style="visibility: hidden;" type="file" id="import_file_select" (change)="fileUpload($event.target.files)">
          </div>
        </div>
        <div>
          <h3 style="text-decoration: underline; margin-block-end: 5px">Data Preview</h3>
          <textarea class="json_view" [(ngModel)]="jsonData" ref-textarea> 
          </textarea>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .alert p{
      margin-block-end: 0.5em;
      margin-block-start: 0.5em;
    }
    .active_li {
      font-style: bold;
    }
    .icon {
      width: 30px;
      height: 30px;
    }
    .table_scope {
      padding-left: 10px;
    }
    .json_view {
      border-width: 2px;
      width: -webkit-fill-available;
      height: -webkit-fill-available;
    }
    .tool_pan  {
      display: flex;
    }
    .table_name {
      padding-left: 20px;
    }
    .example-container {
      width: 400px;
      height: 200px;
      margin: 10px;
      border: 1px solid #555;
    }
    .button_right {
      float: right;
      margin-right: 50px;
    }
    .button_left {
      margin-left: 50px;
    }
    .alert-info {
      color: #0c5460;
      background-color: #d1ecf1;
      border-color: #bee5eb;
    }
    .alert-warning {
      color: #856404;
      background-color: #fff3cd;
      border-color: #ffeeba;
    }  
    .alert {
      position: relative;
      padding: .25rem 1.25rem;
      margin-bottom: 1rem;
      border: 1px solid #000000eb;
      border-radius: .25rem;
    }
    .alert-dark {
      color: #1b1e21;
      background-color: #d6d8d9;
      border-color: #c6c8ca;
    }
    .import_file_choose {
      margin-left: 50px;
      margin-top: 8px;
    }

    .selected_table{
      color: #3e07a7;
      text-decoration: underline;
      font-size: large;
      font-weight: bold;
    }
  `]
})
export class ImportexportComponent implements OnInit {

  public title = "Import or Export Data From Computer";
  public dbTableList: any;
  public jsonData: string = '';
  public selected_table = {
    tbName : '',
    dbName : ''
  }
  
  public message = {
    type:'',
    content:'',
    update_count: 0,
    add_count: 0,
  };
  
  private settings = {
    element: {
      dynamicDownload: null as HTMLElement
    },
    select_file_label : 'Choose files...'
  }  
  
  public select_file_label = this.settings.select_file_label;
  
  constructor(
    private sanitizer: DomSanitizer,
    private DB: DBService,
    private fileService: FileService
  ) {}
  ngOnInit() {
    this.dbTableList = this.DB.getTableList();
    for(let scope in this.dbTableList){
      console.log(scope);
    }
  }
  onSelectTable(tablename, dbName) {
    this.message.type = 'alert-dark';
    this.message.content = "Table '" + tablename + "' is selected."    
    this.selected_table.tbName = tablename;
    this.selected_table.dbName = dbName;
    
    this.DB.getAll(tablename).then((list: any[]) => {
      this.jsonData = JSON.stringify(list);
    })    
  }

  private calculation() {
    let docs = JSON.parse(this.jsonData);
    let that = this;
    this.message.add_count = 0;
    this.message.update_count = 0;
    for(let index in docs) {
      let new_doc = docs[index];      
      if(new_doc._rev) {
        delete new_doc._rev;
      }
      if(new_doc._id) {
        // console.log(new_doc._id);
        this.DB.getById(this.selected_table.tbName, new_doc._id).then(function(doc){
          console.log(new_doc);
          that.DB.update(that.selected_table.tbName, new_doc).then(function(){
            that.message.update_count ++;
          });          
        }).catch((err) =>{
          if(err.status == '404' && err.name == "not_found"){
            that.DB.add(that.selected_table.tbName, new_doc, new_doc._id).then(function(){
              that.message.add_count ++;
            });
          } else {
            console.log("test" + err);
          }
        });
      }      
    }
  }
  public  importButton() {
    if(this.selected_table.tbName == '' || this.jsonData == '' || this.select_file_label == this.settings.select_file_label) {      
      if(this.selected_table.tbName == '') {
        alert('Table is not selected.');
      } else if(this.jsonData == '') {
        alert('Export data is empty.');
      } else {
        alert('File is not select.');        
      }
      return;
    }
    let that = this;
    that.message.type = 'update'      
    this.calculation();
  }
  
  public exportButton() {      
    this.select_file_label = this.settings.select_file_label;
    if(this.selected_table.tbName == '' || this.jsonData == '') {
      if(this.selected_table.tbName == '') {
        alert('Table is not selected.');
      } else if(this.jsonData == '') {
        alert('Export data is empty.');
      }
      return;
    }
    let curTime = new Date();
    let datestring = curTime.getFullYear() + "" + (curTime.getMonth()+1) + "" + curTime.getDate() + "" +  curTime.getHours() + "" + curTime.getMinutes();
    let file_name = this.selected_table + "_" + datestring + '.json';
    this.dyanmicDownloadByHtmlTag({
      fileName: file_name,
      text: JSON.stringify(this.jsonData)
    });    
    this.message.content = file_name + " is successfully downloaded!";
  }
  

  private dyanmicDownloadByHtmlTag(arg: { fileName: string, text: string}) {
    if (!this.settings.element.dynamicDownload) {
      this.settings.element.dynamicDownload = document.createElement('a');
    }
    const element = this.settings.element.dynamicDownload;
    const fileType = arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/plain';
    element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`);
    element.setAttribute('download', arg.fileName);

    var event = new MouseEvent("click");
    element.dispatchEvent(event);
  }  
  
  private readTextFile(file){        
    let jsonText: string = '';
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function() {
        if(rawFile.readyState === 4) {
            if(rawFile.status === 200 || rawFile.status == 0) {
                jsonText = JSON.parse(rawFile.responseText);
            }
        }
    }
    rawFile.send(null);
    return jsonText;
  }

  public fileUpload(files: FileList) {
    let selectFile = files.item(0);   
    this.select_file_label = selectFile.name;
    let that = this;
    this.DB.load_file_from_local(selectFile).then(function(url){      
      if(url && typeof(url) == 'string') {        
        that.jsonData = that.readTextFile(url);        
      }
    });
    
    this.message.content = "File '" + this.select_file_label + "' is selected!";
  }
}

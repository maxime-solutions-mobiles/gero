import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ListService } from './../services/list.service';
import { VisualistationService, CasMode } from './../visualisation/visualisation.service';
import { DBService } from './../services/db.service';
import { FieldConfig } from '../forms/field-config.interface';
import { ParametreService } from '../services/parametre.service';
import { MoyenService } from '../services/moyen.service';
import { MoyenModel } from '../visualisation/models/moyen.model';
import { UserService } from '../services/user.service';
import { AuthService } from '../auth/auth.service';

@Component({
    selector: 'parametres',
    templateUrl: './parametres.page.html'
})
export class Parametres implements OnInit, OnDestroy {

    public postes: string[];
    public currentPoste: string;
    public currentFonction: string;
    public currentUser: any;
    public parametre: any = {};

    public niv_setting:boolean = true;
    public nivModel:any;

    public formZoneConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            type: 'select',
            name: 'famille',
            options: ['CRF', 'FNPC', 'OM', 'FFSS', 'ANPS', 'CIR', 'MS2C', 'MIP', 'Médical', 'SDIS', 'SAMU', 'Police', 'Gendarmerie'],
            placeholder: 'Famille'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formSecteurposteConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'nick',
            placeholder: 'Nickname',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'select',
            name: 'famille',
            options: ['CRF', 'FNPC', 'OM', 'FFSS', 'ANPS', 'CIR', 'MS2C', 'MIP', 'Médical', 'SDIS', 'SAMU', 'Police', 'Gendarmerie'],
            class: 'grid-full',
            placeholder: 'Famille'
        },
        {
            type: 'input',
            name: 'capacite',
            class: 'grid-full',
            placeholder: 'Capacité'
        },
        {
            type: 'textArea',
            name: 'remarque',
            placeholder: 'Remarque'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formPosteConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'nick',
            placeholder: 'Nickname',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'lat',
            placeholder: 'Latitude',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'lng',
            placeholder: 'Longitude',
            validation: [Validators.required]
        },
        {
            type: 'select',
            name: 'famille',
            options: ['CRF', 'FNPC', 'OM', 'FFSS', 'ANPS', 'CIR', 'MS2C', 'MIP', 'Médical', 'SDIS', 'SAMU', 'Police', 'Gendarmerie'],
            class: 'grid-full',
            placeholder: 'Famille'
        },
        {
            type: 'input',
            name: 'capacite',
            class: 'grid-full',
            placeholder: 'Capacité'
        },
        {
            type: 'date',
            name: 'debut',
            placeholder: 'Date de début'
        },
        {
            type: 'date',
            name: 'fin',
            placeholder: 'Date de fin'
        },
        {
            type: 'textArea',
            name: 'remarque',
            class: 'grid-full',
            placeholder: 'Remarque'
        },
        {
            type: 'switch',
            name: 'draggable',
            placeholder: 'Bougeable avec la souris',
            options: ['true', 'false']
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];


    public formMedicamentConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'dci',
            placeholder: 'DCI',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom Commercial'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];


    public formHopitalConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'lat',
            placeholder: 'Latitude'
        },
        {
            type: 'input',
            name: 'long',
            placeholder: 'Longitude'
        },
        {
            type: 'input',
            name: 'services',
            placeholder: 'liste des services (séparés par des virgules)'
        },
        {
            type: 'input',
            name: 'adresse',
            placeholder: 'adresse'
        },
        {
            type: 'input',
            name: 'telephone',
            placeholder: 'téléphone',
            class: 'grid-full'
        },
        {
            type: 'switch',
            name: 'draggable',
            placeholder: 'Bougeable avec la souris',
            options: ['true', 'false']
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];


    public formCategorieConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];


    public formTypesConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formModesConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formMotifConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formNationaliteConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

     public formMoyenEvacConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formMcnoOrigineConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

     public formMcnoDestinataireConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formMcnoCanalConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formMedecinConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formInterneConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formInfirmierConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formPoiConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'lat',
            placeholder: 'Latitude'
        },
        {
            type: 'input',
            name: 'lng',
            placeholder: 'Longitude'
        },
        {
            type: 'select',
            name: 'category',
            options: ['Arrivee', 'Depart', 'Ravitaillement', 'DAE', 'Check-Point', 'DZ', 'Sortie', 'Autre'],
            placeholder: 'Type'
        },
        {
            type: 'input',
            name: 'zone',
            placeholder: 'Zone'
        },
        {
            type: 'input',
            name: 'adresse',
            placeholder: 'adresse'
        },
        {
            type: 'textArea',
            name: 'remarques',
            class: 'grid-full',
            placeholder: 'Remarques'
        },
        {
            type: 'switch',
            name: 'draggable',
            placeholder: 'Bougeable avec la souris',
            options: ['true', 'false']
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formMoyenConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'lat',
            placeholder: 'Latitude'
        },
        {
            type: 'input',
            name: 'lng',
            placeholder: 'Longitude'
        },
        {
            type: 'input',
            name: 'capacite',
            placeholder: 'Capacité'
        },
        {
            type: 'select',
            name: 'type',
            placeholder: 'Type',
            class: 'grid-full',
            options: ['VL', 'Ambulance', 'Golfette', 'Hélico', 'Vélo', 'Moto', 'Quad', 'Pédestre', 'Equestre']

        },
        {
            type: 'select',
            name: 'famille',
            placeholder: 'Famille',
            class: 'grid-full',
            options: ['CRF', 'FNPC', 'OM', 'FFSS', 'ANPS', 'CIR', 'MS2C', 'MIP', 'Médical', 'SDIS', 'SAMU', 'Police', 'Gendarmerie'],
        },
        /* {
             type: 'select',
             name: 'etat',
             placeholder: 'État',
             options: ['disponible', 'alerte', 'sll', 'transport', 'retour']
         },*/
        {
            type: 'switch',
            name: 'draggable',
            placeholder: 'Bougeable avec la souris',
            options: ['true', 'false']
        },
        {
            type: 'switch',
            name: 'geoloc',
            placeholder: 'Geolocalisation',
            options: ['true', 'false']
        },
        {
            type: 'switch',
            name: 'visible',
            placeholder: 'Visible sur la carte',
            options: ['true', 'false']
        },
        {
            type: 'input',
            name: 'balise',
            placeholder: 'Balise'
        },
        {
            type: 'input',
            name: 'chef',
            placeholder: 'Chef de bord'
        },
        {
            type: 'input',
            name: 'telephone',
            placeholder: 'Téléphone du chef de bord'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public formOverlayConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'file',
            placeholder: 'Fichier à utiliser',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'latn',
            placeholder: 'Latitude Nord'
        },
        {
            type: 'input',
            name: 'lngw',
            placeholder: 'Longitude Ouest'
        },
        {
            type: 'input',
            name: 'lats',
            placeholder: 'Latitude Sud'
        },
        {
            type: 'input',
            name: 'lnge',
            placeholder: 'Longitude Est'
        },
        {
            type: 'select',
            name: 'opacity',
            placeholder: 'Opacité (entre 0 et 1.0)',
            options: ['0.3', '0.5', '0.7', '0.8', '1.0']

        },
        {
            type: 'checkbox',
            name: 'enabled',
            placeholder: 'Enabled',
            class: 'grid-full',
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];


    public formTrackConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'input',
            name: 'file',
            placeholder: 'Fichier à utiliser',
            class: 'grid-full',
            validation: [Validators.required]
        },
        {
            type: 'select',
            name: 'format',
            placeholder: 'type de fichier',
            options: ['kml', 'gpx', 'geojson', 'autre']

        },
        {
            type: 'checkbox',
            name: 'enabled',
            placeholder: 'Enabled',
            class: 'grid-full',
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];


    public formLogoConfig: FieldConfig[] = [
        {
            type: 'input',
            name: 'nom',
            placeholder: 'Nom',
            validation: [Validators.required]
        },
        {
            type: 'textArea',
            name: 'base64',
            class: 'grid-full',
            placeholder: 'base64 Encoded image'
        },
        {
            type: 'select',
            name: 'format',
            options: ['jpeg', 'png'],
            class: 'grid-demi',
            placeholder: 'format image'
        },
        {
            type: 'select',
            name: 'format2',
            options: ['JPG', 'PNG'],
            class: 'grid-demi',
            placeholder: 'format image'
        },
        {
            type: 'input',
            name: 'width',
            inputType: 'number',
            placeholder: 'Largeur logo'
        },
        {
            type: 'input',
            name: 'height',
            inputType: 'number',
            placeholder: 'Hauteur logo'
        },
        {
            type: 'input',
            name: 'adresse',
            placeholder: 'Adresse'
        },
        {
            type: 'input',
            name: 'cp',
            placeholder: 'Code Postal'
        },
        {
            type: 'input',
            name: 'ville',
            placeholder: 'ville'
        },
        {
            type: 'input',
            name: 'tel',
            placeholder: 'Numéro de téléphone'
        },
        {
            label: 'Valider',
            name: 'submit',
            type: 'button'
        },
    ];

    public showSection = 'user';

    public settingList: any[] = [];

    public theSettingList: any = [
        {
            label: 'Zones',
            type: 'zone',
            formConfig: this.formZoneConfig
        },
        {
            label: 'Postes',
            type: 'poste',
            formConfig: this.formPosteConfig
        },
        {
            label: 'Moyens',
            type: 'moyen',
            formConfig: this.formMoyenConfig
        },
        {
            label: 'Hopitaux',
            type: 'hopital',
            formConfig: this.formHopitalConfig
        },
        {
            label: 'Moyens d\'évacuations',
            type: 'moyenevac',
            formConfig: this.formMoyenEvacConfig
        },
        {
            label: 'Médicaments',
            type: 'medicament',
            formConfig: this.formMedicamentConfig
        },
        {
            label: 'Points d\'intérêts',
            type: 'poi',
            formConfig: this.formPoiConfig
        },
        {
            label: 'Secteurs dans les Postes',
            type: 'secteur',
            formConfig: this.formSecteurposteConfig
        },
        {
            label: 'Fond de carte',
            type: 'overlay',
            formConfig: this.formOverlayConfig
        },
        {
            label: 'Trace ',
            type: 'track',
            formConfig: this.formTrackConfig
        },
        {
            label: 'Catégories médicales',
            type: 'categorie',
            formConfig: this.formCategorieConfig
        },
        {
            label: 'Motifs de passages',
            type: 'motif',
            formConfig: this.formMotifConfig
        },
        {
            label: 'Modes d\'admission',
            type: 'modes',
            formConfig: this.formModesConfig
        },
        {
            label: 'Types de patients',
            type: 'types',
            formConfig: this.formTypesConfig
        },
        {
            label: 'Nationalité patients',
            type: 'nationalite',
            formConfig: this.formNationaliteConfig
        },
        {
            label: 'Logo Client',
            type: 'logo',
            formConfig: this.formLogoConfig
        },
        {
            label: 'MCNO Origine',
            type: 'origine',
            formConfig: this.formMcnoOrigineConfig
        },
        {
            label: 'MCNO Destinataire',
            type: 'destinataire',
            formConfig: this.formMcnoDestinataireConfig
        },
        {
            label: 'MCNO Canal',
            type: 'canal',
            formConfig: this.formMcnoCanalConfig
        },
        {
            label: 'Médecin Conclusion',
            type: 'medecin',
            formConfig: this.formMedecinConfig
        },
        {
            label: 'Interne Conclusion',
            type: 'interne',
            formConfig: this.formInterneConfig
        },
        {
            label: 'Infirmier Conclusion',
            type: 'infirmier',
            formConfig: this.formInfirmierConfig
        }
    ];

    public casMode: any;
    private subscription: Subscription[] = [];




    constructor(
        private authService: AuthService,
        private DB: DBService,
        private PARAM: ParametreService,
        private MOYEN: MoyenService,
        private USER: UserService,
        private VISU: VisualistationService,
        private LIST: ListService,
    ) { }

    public ngOnInit() {
        this.subscription.push(this.PARAM.current.subscribe(param => {
            this.parametre = param;
            if (param.poste) {
                this.postes = param.poste.map(p => p.nom);
            }

            if (param.niv && param.niv.length > 0) {
                this.nivModel = param.niv[0];
                this.niv_setting = this.nivModel.value;
                this.USER.selectNiv(this.niv_setting);
                console.log('NIVVVVVVVVVVVVVVVVVV 3');
            }
        }));

        this.subscription.push(this.VISU.display.subscribe((display) => {
            this.casMode = display.casMode;
        }));

        this.subscription.push(this.USER.profile.subscribe((profile) => {
            this.currentUser = profile;
            this.currentPoste = profile.poste;
            this.currentFonction = profile.fonction;

            // affectation conditionnelle dans ce cas + simple
            if (this.currentFonction === 'administrateur') {
                this.settingList = this.theSettingList;
            }
        }));

        if (this.PARAM.currentParametre.poste) {
            this.postes = this.PARAM.currentParametre.poste.map(p => p.nom);
        }
        ////// jts added
        if (this.PARAM.currentParametre.niv && this.PARAM.currentParametre.niv.length > 0 && this.PARAM.currentParametre.niv[0]._id) {
            this.nivModel = this.PARAM.currentParametre.niv[0];
            this.niv_setting = this.nivModel.value;
            this.USER.selectNiv(this.niv_setting);
            console.log('NIVVVVVVVVVVVVVVVVVV 1');
        } else {
            this.niv_setting = true;
            console.log('NIVVVVVVVVVVVVVVVVVVVV 2');
        }
        this.casMode = this.VISU.currentDisplay.casMode;
    }

    public ngOnDestroy() {
        this.subscription.forEach(sub => sub && sub.unsubscribe());
    }

    public logout() {
        this.authService.logout();
    }

    public open(sectionName: string) {
        this.showSection = sectionName;
    }

    public toggleCasMode() {
        const mode: CasMode = (this.casMode === 'medium') ? 'large' : 'medium';
        this.VISU.changeCasMode(mode);
    }

    public syncAll() {
        this.DB.syncAllTables();
    }

    public destroyLocalTables() {
        this.DB.destroyAllLocalTables();
    }

    public reload() {
        window.location.reload(true);
    }

    public submitSetting(type: string, value: any) {
        console.log("type=" + type);
        console.log("value=" + value);
        if (type === 'moyen') {
            let moyen = new MoyenModel();
            moyen = { ...moyen, ...value };
            moyen.currentStates = [{
                etat: 'disponible',
                timeCode: this.USER.CONTEXT.timeCode,
                auteur: this.USER.CONTEXT.auteur,
            }];
            return this.MOYEN.addMoyen(moyen);
        }
        this.PARAM.addParam(type, value);
    }

    public selectPoste(evt) {
        const poste = evt.value;
        if (poste === this.currentPoste) {
            return;
        }
        this.currentPoste = poste;
        this.USER.selectPoste(poste);
        this.LIST.resetList('cas');
    }

    public nivchange() {

        this.USER.selectNiv(this.niv_setting);
        if(this.nivModel) {
            this.nivModel.value = this.niv_setting;
            this.PARAM.editParam("niv", this.nivModel);
        } else {
            this.nivModel = {};
            this.nivModel.value = this.niv_setting;
            this.PARAM.addParam("niv", this.nivModel);
        }
    }


}

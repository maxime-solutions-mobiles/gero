import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Parametres } from './parametres.page';
import { MatButtonModule, MatSelectModule } from '@angular/material';
import { ParamAdminComponent } from './components/param-admin.component';
import { DynamicFormModule } from '../forms/dynamic-form.module';
import { ImportexportComponent } from './components/importexport.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-toggle-switch';

@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatSelectModule,
        DynamicFormModule,
        BrowserModule,
        FormsModule,
        UiSwitchModule
    ],
    declarations: [
        Parametres,
        ParamAdminComponent,
        ImportexportComponent,
    ],
    providers: []
})
export class ParametresModule { }

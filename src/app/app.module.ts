// @angular
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { APP_BASE_HREF } from '@angular/common';
import { OrderModule } from 'ngx-order-pipe';

import { environment } from 'src/environments/environment';
import { AuthModule } from './auth/auth.module';

// vendor
import { MaterialModule } from './material.module';

// map
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import * as L from 'leaflet';

// services
import { DocumentService } from './services/document.service';
import { ListService } from './services/list.service';
import { UserService } from './services/user.service';
import { NotificationService } from './services/notification.service';

// pages
import { ParametresModule } from './parametres/parametres.module';
import { VisualisationModule } from './visualisation/visualisation.module';
import { McnoModule } from './mcno/mcno.module';
import { StatistiquesModule } from './statistiques/statistiques.module';
import { MapModule } from './map/map.module';

// components
import { AppRoutingModule } from './app-routing.module';
import { DynamicFormModule } from './forms/dynamic-form.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ParametreService } from './services/parametre.service';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    LeafletModule.forRoot(),
    LeafletMarkerClusterModule.forRoot(),
    AuthModule,
    AppRoutingModule,
    DynamicFormModule,
    McnoModule,
    StatistiquesModule,
    ParametresModule,
    VisualisationModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule,
    ChartsModule,
    MapModule,
    NgxChartsModule,
    OrderModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],

  declarations: [
    AppComponent,
    HeaderComponent,
    UserComponent,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: (window as any).baseHref,
    },
    DocumentService,
    ListService,
    UserService,
    ParametreService,
    NotificationService,
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
